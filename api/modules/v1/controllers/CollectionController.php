<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Expression;
use yii\web\UploadedFile;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use frontend\models\Collection;
use frontend\models\CollectionFollow;
use frontend\models\PostForm;
use frontend\models\LoginForm;
use frontend\models\Like;
use frontend\models\Comment;
use frontend\models\SavePost;
use frontend\models\ProfileVisitor;
use frontend\models\HidePost;
use frontend\models\MuteFriend;
use frontend\models\UserForm;
use frontend\models\Friend;


/**
 * Site controller
 */
class CollectionController extends ActiveController {

    public $modelClass = 'api\modules\v1\models\UserForm';
   
    /*Add Collection Code Start*/
	public function actionAddcollection() 
	{
		$userid = $_POST['user_id'];
		$data 	= array();
		$date = time(); 
		$collection = new Collection();
		$collection->user_id = $userid;
		$collection->name = $_POST['title']; 
		$collection->tagline = $_POST['tagline'];
		$collection->privacy = $_POST['visible'];
		if(!empty($_FILES['images']))
		{
			$name = $_FILES["images"]["name"];
			$tmp_name = $_FILES["images"]["tmp_name"];
			$url = '../../frontend/web/uploads/collection/';
			$image_extn = explode('.',$_FILES["images"]["name"]);
			$image_extn = end($image_extn);
			$rand = rand(111,999);
			$image = $date.'.'.$image_extn;
			move_uploaded_file($_FILES["images"]["tmp_name"], $url.$date.'.'.$image_extn);
			$collection->collection_thumb =$image;
		}		
		$collection->created_date = "$date";
		$collection->modified_date = "$date";
		$collection->is_deleted = "1";
		$collection->entity_collection= "1";
		$collection->insert();
		$last_insert_id = $collection->_id;
			if($last_insert_id)
			{
				$data['status'] = true;
				$data['collection_id'] = "$last_insert_id";
			}
			else
			{
				$data['status'] = false;
			}
		
		return $data;
    }
	
	/*Collection Edit code start*/
	public function actionCollectionedit() 
	{
		$result = array();
		$user_id = $_POST['user_id'];
		$collection_id = $_POST['colletion_id'];
		if($user_id)
        {
            $date = time(); 
			$collection = new Collection();
			$update = Collection::find()->where(['_id' => "$collection_id"])->one();
            $update->user_id = $user_id;
            $update->name = $_POST['title']; 
            $update->tagline = $_POST['tagline'];
			$update->privacy = $_POST['visible'];
			if(isset($_POST['images']) && (!empty($_POST['images'])))
			{
				$name = $_FILES["images"]["name"];
				$tmp_name = $_FILES["images"]["tmp_name"];
				$url = '../../frontend/web/uploads/collection/';
				$image_extn = explode('.',$_FILES["images"]["name"]);
				$image_extn = end($image_extn);
				$rand = rand(111,999);
				$image = $date.'.'.$image_extn;
				move_uploaded_file($_FILES["images"]["tmp_name"], $url.$date.'.'.$image_extn);
				$collection->collection_thumb =$image;
			}
            $update->modified_date = "$date";
			$update->is_deleted = "1";
            $update->update();
			$result['status'] = true;
		}
        else
        {
            $result['status'] = false;
        }
		return $result;
	}
	
	/*All Collection code start*/
	public function actionAllcollection()
	{
		$result = array();
		$login_user_id = $_POST['user_id'];
		$type = $_POST['type'];
		if($type == 'your')
		{
			$collections  = Collection::yourscollection($login_user_id);
		}
		else
		{
			$collections  = Collection::allcollection();
		} 
			
		if(!empty($collections))
		{
			$i=0;
			$col = array();
			foreach($collections as $collection)
			{
				$user_id = $collection['user_id'];
				$collection_id = $collection['_id'];
				$col[$i]['id'] =  $collection['_id'];
				$col[$i]['name'] = $collection['name'];
				$col[$i]['tagline'] = $collection['tagline'];
				if($collection['collection_thumb'])
				{
					$col[$i]['collection_photo'] = 'uploads/collection/'.$collection['collection_thumb'];
				} else {
					$col[$i]['collection_photo'] = 'images/additem-collections.png';
				}
				$col[$i]['created_date'] = $collection['created_date'];
				$col[$i]['user_name'] = $this->getuserdata($user_id,'fullname');
				$col[$i]['user_image'] = $this->getimageAPI($user_id,'thumb');
				$col[$i]['follow_count'] = CollectionFollow::getcollectionfollowcount($collection_id);
				$col[$i]['follow'] = CollectionFollow::getcollectionfollow($collection_id,$login_user_id);
				$i++;
			}
			$result['data'] = $col;
			$result['status'] = true;
        }
		else
		{
			$result['message'] = "No collection found";
			$result['status'] = false;
        }	
		return $result;	
	}
	
	/*following collection code start*/
	public function actionFollowingcollection()
	{
		$result = array();
		$login_user_id = $_POST['user_id'];
		$collections  = Collection::followingcollection($login_user_id);
			
		if(!empty($collections))
		{
			$i=0;
			$col = array();
			foreach($collections as $collection)
			{
				$user_id = $collection['collectiondata']['user_id'];
				$collection_id = $collection['collectiondata']['_id'];
				$col[$i]['id'] =  $collection['collectiondata']['_id'];
				$col[$i]['name'] = $collection['collectiondata']['name'];
				$col[$i]['tagline'] = $collection['collectiondata']['tagline'];
				if($collection['collectiondata']['collection_thumb'])
				{
					$col[$i]['collection_photo'] = 'uploads/collection/'.$collection['collectiondata']['collection_thumb'];
				} else {
					$col[$i]['collection_photo'] = 'images/additem-collections.png';
				}
				$col[$i]['created_date'] = $collection['collectiondata']['created_date'];
				$col[$i]['user_name'] = $this->getuserdata($user_id,'fullname');
				$col[$i]['user_image'] = $this->getimageAPI($user_id,'thumb');
				$col[$i]['follow_count'] = CollectionFollow::getcollectionfollowcount($collection_id);
				$col[$i]['follow'] = CollectionFollow::getcollectionfollow($collection_id,$login_user_id);
				$i++;
			}
			$result['data'] = $col;
			$result['status'] = true;
        }
		else
		{
			$result['message'] = "No collection found";
			$result['status'] = false;
        }	
		return $result;	
	}
	
	
	/*Collection Details Code start*/
	public function actionCollectiondetail()
	{
		$result = array();
		$col = array();
		$login_user_id = $_POST['user_id'];
		$collection_id = $_POST['colletion_id'];
		$collection = Collection::collectiondetail($collection_id);
		if($collection)
		{
			$user_id = $collection['user_id'];
			$collection_id = $collection['_id'];
			$col['id'] =  $collection['_id'];
			$col['name'] = $collection['name'];
			$col['tagline'] = $collection['tagline'];
			if($collection['collection_thumb'])
			{
				$col['collection_photo'] = 'uploads/collection/'.$collection['collection_thumb'];
			} else {
				$col['collection_photo'] = 'images/additem-collections.png';
			}
			$col['created_date'] = $collection['created_date'];
			$col['user_name'] = $this->getuserdata($user_id,'fullname');
			$col['user_image'] = $this->getimageAPI($user_id,'thumb');
			$col['follow_count'] = CollectionFollow::getcollectionfollowcount($collection_id);
			$col['follow'] = CollectionFollow::getcollectionfollow($collection_id,$login_user_id);
			$col['privacy'] = $collection['privacy'];
			$result['data'] = $col; 
			$result['status']= true;
		} 
		else 
		{
			$result['message'] = "No collection found";
			$result['status']= false;
		}	
		return $result;
	}
	
	  public function actionCollectionmainfeed()
	{
		$model = new LoginForm();
		$user_id = $_POST['user_id'];
		$collection_id = $_POST['colletion_id'];
		$count_post = count(PostForm::getCollectionPosts($collection_id));
		$posts =  PostForm::getCollectionPosts($collection_id);
		if(count($posts) > 0)
		{
			$post_array = array();
			$i = 0;
			foreach ($posts as $doc)
			{
				$like_array = array();
				$comment_array = array();
				$feedback_array = array();
				$attachment_array = array();
				$tagedactornames_array = array();
				$pictures_array = array();
				$videos_array = array();
				$links_array = array();
				$shares_array = array();
				$j = 0;
				$k = 0;
				$post_id = (string) $doc['_id'];
				$likes = Like::find()->where(['post_id' => $post_id ,'status' => '1'])->all();
				foreach($likes as $docLike)
				{
					$like_array[$j] = UserForm::getUserDetails($docLike['user_id']); 
					$j++;
				}
				$loaded_comments = Comment::find()->with('user')->with('post')->where(['post_id' => "$post_id",'status' => '1','parent_comment_id' => '0'])->orderBy(['created_date'=>SORT_DESC])->all();
				foreach($loaded_comments as $docComment)
				{
					$comment_array[$k]['main_comment'] = $docComment;
					$comment_array[$k]['actor'] = UserForm::getUserDetails($docComment['user_id']);
					$pcid = (string) $docComment['_id'];
					$main_comment_likes = Like::find()->where(['comment_id' => $pcid ,'status' => '1'])->count();
					$comment_array[$k]['main_comment_likes'] = $main_comment_likes;
					$loaded_sub_comments = Comment::find()->with('user')->with('post')->where(['parent_comment_id' => "$pcid",'status' => '1'])->orderBy(['created_date'=>SORT_DESC])->all();
					$l = 0;
					$subcomment_array = array();
					foreach($loaded_sub_comments as $docSubComment)
					{
						$subcomment_array[$l]['sub_comment'] = $docSubComment;
						$subcomment_array[$l]['actor'] = UserForm::getUserDetails($docSubComment['user_id']); 
						$sub_id = $docSubComment['_id'];
						$sub_comment_likes = Like::find()->where(['comment_id' => "$sub_id" ,'status' => '1'])->count();
						$subcomment_array[$l]['sub_comment_likes'] = $sub_comment_likes;
						$l++;
					}
					$comment_array[$k]['subcomment'] = $subcomment_array;
					$k++;
				}
				if(isset($doc['image']) && $doc['image'] != 'No Image')
				{
					$images = rtrim($doc['image'], ",");
					$images = explode(',',$images );
					$p = 0;
					for($m = 0; $m < count($images); $m++)
					{
						$pictures_array[$p]['urls'] = $images[$m];
						$p++;
					}
				}
				if($doc['post_type'] == 'link')
				{
					$p = 0;
					$links_array[$p]['urls'] = $doc['post_text'];
				}
				if(isset($doc['post_tags']) && $doc['post_tags'] != 'null')
				{
					$tags = explode(',', $doc['post_tags']);
					$p = 0;
					for($m = 0; $m < count($tags); $m++)
					{
						$tagedactornames_array[$p]['tags'] = UserForm::getUserDetails($tags[$m]);
						$p++;
					}
				}
				if(isset($doc['parent_post_id']))
				{
					$p = 0;
					$pid = $doc['parent_post_id'];
					$data[$i] = PostForm::find()->where(['_id'=>"$pid"])->one();
					$uid = $data[$i]['post_user_id'];
					$shares_array[$p]['parent_post'] = $data[$i];
					$shares_array[$p]['actor'] = UserForm::getUserDetails($uid);
				}
				if(!empty($doc['share_by']))
				{
					$shares_count = substr_count( $doc['share_by'], ",");
				}
				else
				{
					$shares_count = 0;
				}
				// Save post
				$savestatus = new SavePost();
                $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => "$post_id"])->one();
                $savestatuscalue = $savestatus['is_saved'];
				
				//Hide Post
				$hidestatus = new HidePost();
                $hidestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_ids' => "$post_id"])->one();
                if($hidestatus){
					$hidestatuss = "1";
				}else{
					$hidestatuss = "0";
				}
				$frd_id = (string) $doc['post_user_id'];
				// Userexits check
				$userexist = MuteFriend::find()->where(['user_id' => "$user_id"])->one();
				if ($userexist)
				{
					if (strstr($userexist['mute_ids'], "$frd_id"))
					{
						$getnot = 'Unmute notifications';
					}
					else
					{
						$getnot = 'Mute notifications';
					}
				}
				else
				{
					$getnot = 'Mute notifications';
				}
				if($like_array == null)
				{
					$like_array = array();
				}
				
				
				$feedback_array['likers'] = $like_array;
				$feedback_array['comments'] = $comment_array;
				
				$post_array[$i]['main_post'] = $doc; 
				//$post_array[$i]['actor'] = UserForm::getUserDetails($doc['post_user_id']);
				$post_array[$i]['feedback'] = $feedback_array;
				$post_array[$i]['savestatus'] = $savestatuscalue;
				$post_array[$i]['hidestatus'] = $hidestatuss;
				$post_array[$i]['mutepostfriend'] = $getnot;
				
				$attachment_array['pictures'] = $pictures_array;
				$attachment_array['videos'] = $videos_array;
				$attachment_array['links'] = $links_array;
				$attachment_array['shares'] = $shares_array;
				$attachment_array['shares_count'] = $shares_count;
				
				$post_array[$i]['attachment'] = $attachment_array;
				$post_array[$i]['tagedactornames'] = $tagedactornames_array;
				$i++;
			}
        }
		else
		{
            $post_array = array();
        }
        $result =  PostForm::find()->with('user')->orderBy(['post_created_date' => SORT_DESC])->all();
        
		if( count($posts) > 0)
            $count =  count($posts);
        else
            $count = 0;
        
		$rr = array();
		$rr['model'] = $model;
		$rr['posts'] = $post_array;
		return $rr;
	}
  
	
	/*Collection followers list code start*/
	public function actionCollectionfollowers()
	{
		$result = array();
		$collection_id = $_POST['colletion_id'];
		$followers = CollectionFollow::getcollectionfollowuser((string)$collection_id);
		if($followers)
		{
			$i = 0;
			$col = array();
			foreach ($followers as $follower)
			{
				$user_id = $follower['user_id'];
				$col[$i]['user_name'] = $this->getuserdata($user_id,'fullname');
				$col[$i]['user_image'] = $this->getimageAPI($user_id,'thumb');
				$i++;
			}
			$result['follower'] = $col;
			$result['status'] = true;
		}
		else
		{
			$result['status']= false;
		}		
		return $result;
	}
	
	/*Collection delete code start*/
	public function actionCollectiondelete()
	{
		$result = array();
		$user_id = $_POST['user_id'];
		$collection_id = $_POST['colletion_id'];
		if($user_id)
        {
			$date = time(); 
			$update = Collection::find()->where(['_id' => "$collection_id"])->one();
			$update->is_deleted = "0";
			$update->modified_date = "$date";
			if($update->update())
			{
				CollectionFollow::deleteAll(['collection_id'=>$collection_id]);
				$result['status']= true;
			}
		}
		else
		{
			 $result['status'] = false;
		}
		return $result;	
	}
	
	/*Collection follow code start*/
	public function actionCollectionfollow()
	{
		$result = array();
		$user_id = $_POST['user_id'];
		$collection_id = $_POST['colletion_id'];
		if($user_id)
        {
			$collectionfollow = new CollectionFollow();
			$date = time();
			$datas 	= array();
			$usercolexist = CollectionFollow::find()->where(['user_id'=>$user_id,'collection_id'=>"$collection_id"])->one();
			if ($usercolexist)
			{
				if($usercolexist['is_deleted']=='0'){
					$usercolexist->is_deleted = "1";
					$usercolexist->modified_date = "$date";
					if ($usercolexist->update())
					{
						$result['status']= true;
						$result['msg']= 'following';
					}
					else
					{
						$result['status']= false;
					}	
				}
				else
				{
					if ($usercolexist->delete())
					{
						$result['status']= true;
						$result['msg']= 'follow';
					}
					else
					{
						$result['status']= false;
					}
				}
			}
			else
			{
				$collectionfollow->user_id = $user_id;
				$collectionfollow->collection_id = $collection_id;
				$collectionfollow->is_deleted = "1";
				$collectionfollow->created_date = "$date";
				$collectionfollow->modified_date = "$date";
				if ($collectionfollow->insert())
				{
					$result['status']= true;
					$result['msg']= 'following';
				}
				else
				{
					$result['status']= false;
				}
			}
		}
		return $result;		
	}
}	