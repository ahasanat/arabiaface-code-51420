<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
   // public $basePath = '@webroot';
   // public $baseUrl = '@web';
    public $sourcePath = '@bower/backend/';
    public $css = [
        'bootstrap/css/bootstrap.min.css',
        'plugins/ionicons/ionicons.min.css',
        /*'plugins/font-awesome/font-awesome.min.css',*/
        'dist/css/AdminLTE.min.css',
        'dist/css/admin-template.css',
        'plugins/datatables/dataTables.bootstrap.css',
        'dist/css/skins/_all-skins.min.css',
        'plugins/iCheck/flat/blue.css',
        'plugins/morris/morris.css',
        'plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'plugins/datepicker/datepicker3.css',
        'plugins/daterangepicker/daterangepicker.css',
        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
		'dist/css/magnific-popup.css',
		'dist/css/custom-theme.css',
		'dist/css/font-awesome.css',
    ];
    public $js = [
        'plugins/jQuery/jquery-2.2.3.min.js',
        'dist/js/jquery-ui.min.js',
        'bootstrap/js/bootstrap.min.js',
        'plugins/datatables/jquery.dataTables.min.js',
        'plugins/datatables/dataTables.bootstrap.min.js',
        'dist/js/raphael-min.js',
        //'plugins/morris/morris.min.js',
        'plugins/sparkline/jquery.sparkline.min.js',
        'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'plugins/knob/jquery.knob.js',
        'dist/js/moment.min.js',
        'plugins/daterangepicker/daterangepicker.js',
        'plugins/datepicker/bootstrap-datepicker.js',
        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js',
        'plugins/fastclick/fastclick.js',
        'dist/js/app.min.js',
        'dist/js/custome-function-backend.js',
        //'dist/js/pages/dashboard.js',
		'dist/js/demo.js',
		'dist/js/jquery.magnific-popup.js',
        'plugins/chartjs/Chart.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
