<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TravelAsset extends AssetBundle
{
   // public $basePath = '@webroot';
   // public $baseUrl = '@web';
    public $sourcePath = '@bower/travel/';
    public $css = [
		'css/default-skin.css',
		'css/croppic.css',
        'css/bootstrap-dialog.min.css',
        'css/emoticons.css',
        'css/emostickers.css',
        'css/template.css',
		'dist/css/custom-theme.css',
		'css/subcss-commonsec.css'
    ];
    public $js = [
		'js/croppic.js',
		'js/bootstrap-dialog.min.js',
        'js/emoticons.js',
        'js/emostickers.js',
        'js/custom-emotions.js',
        'js/custom-emostickers.js',
        'js/jquery.cropit.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
