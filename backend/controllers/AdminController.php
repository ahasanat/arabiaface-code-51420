<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use backend\models\Admin;

class AdminController  extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['all'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionAll()
    {
		if (Yii::$app->user->isGuest)
        {
            return $this->goHome();
        } else {
			$admins = Admin::getAdminUser();
			return $this->render('all',['admins' =>$admins]);	
		}
	}
	
	/* public function actionAlls()
    {
		
			$admins = Admin::getAdminUser();
			$data = array();
			foreach($admins as $admin)
			{
				$sub_array = array();
				$sub_array['username'] = $admin->username;
				$sub_array['admin_email'] = $admin->admin_email;
				$data[] = $sub_array;
			}
			$output = array(
				"data" => $data,
			);
			
			return json_encode($output);
	}*/
	
	public function actionAddadmin()
	{
		if (Yii::$app->user->isGuest)
        {
            return $this->goHome();
        } else {
			return $this->render('add');	
		}
	}
	
	public function actionAddadmindata()
	{
		if (Yii::$app->user->isGuest)
        {
            return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$username = $_POST['username'];
				$password = $_POST['password'];
				$email = $_POST['email'];
				$record = new Admin();
				$data = Admin::find()->where(['username'=>"$username"])->one();
				if($data){
					return $this->redirect(array('admin/addadmin'));	
				} else {
					$record->username = $username;
					$record->password = $password;
					$record->admin_email = $email;
					$record->admin_role = "admin";
					if($record->insert()){
						return $this->redirect(array('admin/all'));	
					}
				}
				   
			}
		}
	}
	
	public function actionAdminremove()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$mt_exist = Admin::find()->where(['_id' => $id ])->one();
				if($mt_exist->delete())
				{
					return true;
				}
				else
				{
					return false;   
				}
			}
			else
			{
				return false;   
			}
		}	
	}
}