<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use frontend\models\PostForm;
use frontend\models\TravAds;

class AdsController  extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','allcollection'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionAll()
    {
		$ads = TravAds::getAds();
		return $this->render('all',['ads' =>$ads]);	
	}
	
	public function actionActive()
    {
		$ads = TravAds::getActiveAds();
		return $this->render('active',['ads' =>$ads]);	
	}
	
	public function actionUpdate()
	{
		if(isset($_POST) && !empty($_POST))
        {
            $id = $_POST['id'];
			$status = $_POST['status'];

			if($status == 'Active')
			{
				$dl = "2";
			}
			else
			{
				$dl = "1";
			}
			$update = PostForm::find()->where(['_id' => "$id"])->one();
			$update->is_ad = $dl;
			if($update->update())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public function actionRemovepost()
	{
		if(isset($_POST) && !empty($_POST))
        {
			$post_id = $_POST['post_id'];
			PostForm::deleteAll(['_id' => $post_id]);
			Notification::deleteAll(['post_id' => $post_id]);
			Like::deleteAll(['post_id' => $post_id]);
			Comment::deleteAll(['post_id' => $post_id]);
			return true;
		}
		else
		{
			return false;
		}
	}
}