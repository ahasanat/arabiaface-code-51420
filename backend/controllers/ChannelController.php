<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use frontend\models\UserForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\models\Channel; 
/**
 * Site controller
 */
class ChannelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'add','profileimagecrop','channeldelete', 'edit', 'editprofileimagecrop','statusupdate', 'editfetch'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],      
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionAdd() 
    {
        $information = Channel::channelList();
        return $this->render('add', array('information' => $information));
        exit;
    }
    
    public function actionProfileimagecrop()
    {       
        $result = array('status' => false);
        if(isset($_POST) && !empty($_POST)) {
            $post = $_POST;
            $channelName = isset($post['channelName']) ? $post['channelName'] : '';
            if($channelName != '') {
                $imgUrl = isset($post['imgUrl']) ? $post['imgUrl'] : '';
                if($imgUrl != '') {
                    $user_id = (string)$_SESSION['email'];
                    $rand = time();
                    $imgUrl = $post['imgUrl'];
                    // original sizes
                    $imgInitW = $post['imgInitW'];
                    $imgInitH = $post['imgInitH'];
                    // resized sizes
                    $imgW = $post['imgW'];
                    $imgH = $post['imgH'];
                    // offsets
                    $imgY1 = $post['imgY1'];
                    $imgX1 = $post['imgX1'];
                    // crop box
                    $cropW = $post['cropW'];
                    $cropH = $post['cropH'];
                    // rotation angle
                    $angle = $post['rotation'];

                    $jpeg_quality = 100;

                    $output_filename = "../frontend/web/uploads/channel/";

                    $what = getimagesize($imgUrl);

                    switch(strtolower($what['mime']))
                    {
                        case 'image/png':
                            $img_r = imagecreatefrompng($imgUrl);
                            $source_image = imagecreatefrompng($imgUrl);
                            $type = '.png';
                            break;
                        case 'image/jpeg':
                            $img_r = imagecreatefromjpeg($imgUrl);
                            $source_image = imagecreatefromjpeg($imgUrl);
                            error_log("jpg");
                            $type = '.jpeg';
                            break;
                        case 'image/gif':
                            $img_r = imagecreatefromgif($imgUrl);
                            $source_image = imagecreatefromgif($imgUrl);
                            $type = '.gif';
                            break;
                        default: die('image type not supported');
                    }
                
                    // resize the original image to size of editor
                    $resizedImage = imagecreatetruecolor($imgW, $imgH);
                    imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
                    // rotate the rezized image
                    $rotated_image = imagerotate($resizedImage, -$angle, 0);
                    // find new width & height of rotated image
                    $rotated_width = imagesx($rotated_image);
                    $rotated_height = imagesy($rotated_image);
                    // diff between rotated & original sizes
                    $dx = $rotated_width - $imgW;
                    $dy = $rotated_height - $imgH;
                    // crop rotated image to fit into original rezized rectangle
                    $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
                    imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
                    imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
                    // crop image into selected area
                    $final_image = imagecreatetruecolor($cropW, $cropH);
                    imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
                    imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
                    // finally output png image
                    //imagepng($final_image, $output_filename.$type, $png_quality);
                    $thumbName = 'thumb_'.$rand.$type;
                    imagejpeg($final_image, '../frontend/web/uploads/channel/'.$thumbName, $jpeg_quality);

                    // Store For Original Image..
                    $imageDataEncoded = base64_encode(file_get_contents($imgUrl));
                    $imageData = base64_decode($imageDataEncoded);
                    $source = imagecreatefromstring($imageData);

                    $oriName = $rand.$type;
                    $imageSave = imagejpeg($source, '../frontend/web/uploads/channel/'.$oriName,100);
                    imagedestroy($source);

                    $transferPost = array(
                            'channelName' => $channelName,
                            'profilePicture' => $oriName
                        );
                    $information = Channel::addChannel($user_id, $transferPost);
                    return json_encode($information, true);
                    exit;   

                } else {
                    $result['error'] = 'Please upload channel profile picture';    
                }
            } else {
                $result['error'] = 'Please enter channel name';
            }
        }

        return json_encode($result, true);
        exit;
    }

    public function actionChanneldelete() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            if(isset($_POST['$id']) && $_POST['$id'] != '') {
                $id = $_POST['$id'];
                $information = Channel::channelDelete($uId, $id);
                return $information;
                exit;
            }
        }
    } 

    public function actionEdit($id) {
        if(isset($id) && $id != '') {
            $session = Yii::$app->session;
            $uId = (string)$session->get('email');
            if(isset($uId)) {
                $information = Channel::channelEdit($uId, $id);
                return $this->render('edit', array('information' => $information));
                exit;
            }
        }
    } 

    public function actionStatusupdate() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if($uId) {
            if(isset($_POST['$id']) && $_POST['$id'] != '') {
                $id = $_POST['$id'];
                $information = channel::statusUpdate($id);
                return $information;
                exit;
            }
        }
    }    

    public function actionEditfetch() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if($uId) {
            if(isset($_POST['$id']) && $_POST['$id'] != '') {
                $id = $_POST['$id'];
                $data = channel::editfetch($id);
                return $this->render('edit_fetch',array('data'=>$data));
            }
        }
    }

    public function actionEditprofileimagecrop()
    {  
        if(isset($_POST) && !empty($_POST)) {
            $user_id = (string)$_SESSION['email'];
            $post = $_POST;
            $id = isset($post['$id']) ? $post['$id'] : '';
            $channelName = isset($post['$channelName']) ? $post['$channelName'] : '';
            if($id != '' && $channelName != '') {
                $imgUrl = isset($post['imgUrl']) ? $post['imgUrl'] : '';
                $oriName = '';
                if($imgUrl != '') {
                    $rand = time();
                    $imgUrl = $post['imgUrl'];
                    // original sizes
                    $imgInitW = $post['imgInitW'];
                    $imgInitH = $post['imgInitH'];
                    // resized sizes
                    $imgW = $post['imgW'];
                    $imgH = $post['imgH'];
                    // offsets
                    $imgY1 = $post['imgY1'];
                    $imgX1 = $post['imgX1'];
                    // crop box
                    $cropW = $post['cropW'];
                    $cropH = $post['cropH'];
                    // rotation angle
                    $angle = $post['rotation'];

                    $jpeg_quality = 100;

                    $output_filename = "../frontend/web/uploads/channel/";

                    $what = getimagesize($imgUrl);

                    switch(strtolower($what['mime']))
                    {
                        case 'image/png':
                            $img_r = imagecreatefrompng($imgUrl);
                            $source_image = imagecreatefrompng($imgUrl);
                            $type = '.png';
                            break;
                        case 'image/jpeg':
                            $img_r = imagecreatefromjpeg($imgUrl);
                            $source_image = imagecreatefromjpeg($imgUrl);
                            error_log("jpg");
                            $type = '.jpeg';
                            break;
                        case 'image/gif':
                            $img_r = imagecreatefromgif($imgUrl);
                            $source_image = imagecreatefromgif($imgUrl);
                            $type = '.gif';
                            break;
                        default: die('image type not supported');
                    }
                
                    // resize the original image to size of editor
                    $resizedImage = imagecreatetruecolor($imgW, $imgH);
                    imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
                    // rotate the rezized image
                    $rotated_image = imagerotate($resizedImage, -$angle, 0);
                    // find new width & height of rotated image
                    $rotated_width = imagesx($rotated_image);
                    $rotated_height = imagesy($rotated_image);
                    // diff between rotated & original sizes
                    $dx = $rotated_width - $imgW;
                    $dy = $rotated_height - $imgH;
                    // crop rotated image to fit into original rezized rectangle
                    $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
                    imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
                    imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
                    // crop image into selected area
                    $final_image = imagecreatetruecolor($cropW, $cropH);
                    imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
                    imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
                    // finally output png image
                    //imagepng($final_image, $output_filename.$type, $png_quality);
                    $thumbName = 'thumb_'.$rand.$type;
                    imagejpeg($final_image, '../frontend/web/uploads/channel/'.$thumbName, $jpeg_quality);

                    // Store For Original Image..
                    $imageDataEncoded = base64_encode(file_get_contents($imgUrl));
                    $imageData = base64_decode($imageDataEncoded);
                    $source = imagecreatefromstring($imageData);

                    $oriName = $rand.$type;
                    $imageSave = imagejpeg($source, '../frontend/web/uploads/channel/'.$oriName,100);
                    imagedestroy($source);
                }
                    
                $transferPost = array(
                    'id' => $id,
                    'channelName' => $channelName,
                    'profilePicture' => $oriName
                );
                
                $information = Channel::editChannel($user_id, $transferPost);
                return $information;
            }
        }

        return false;
    }
}
