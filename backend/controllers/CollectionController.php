<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use frontend\models\Collection;
use frontend\models\CollectionFollow;
use frontend\models\CollectionReport;
use frontend\models\PostForm;
use frontend\models\DefaultImage;

class CollectionController  extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['all'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
	
/*All collection*/
    public function actionAll()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$collections = Collection::allcollectionadmin();
			return $this->render('all',['collections' =>$collections]);	
		}
	}
	
/*Followers Collection*/
	public function actionFollowers()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_GET['collection_id']))
			{
				$collection_id = $_GET['collection_id'];
				$followers = CollectionFollow::getcollectionfollowuser($collection_id);
				return $this->render('follower',['followers' =>$followers]);	
			}	
		}
	}	

/*Remove Collection */	
	public function actionCollectionremove()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$mt_exist = Collection::find()->where(['_id' => $id ])->one();
				if($mt_exist->delete())
				{
					CollectionFollow::deleteAll(['collection_id'=>$id]);
					return true;
				}
				else
				{
					return false;   
				}
			}
			else
			{
				return false;   
			}
		}	
	}

/*Update Collection*/	
	public function actionCollectionupdate()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$date = time();
				$id = $_POST['id'];
				$status = $_POST['status'];
				
				if($status == 'Active'){
					$dl = "0"; 
				}else{
					$dl = "1";
				}
				$update = Collection::find()->where(['_id' => "$id"])->one();
				$update->is_deleted = $dl;
				$update->status_date = "$date";
				$update->update();
				return true;
			}else{
				return false;
			}	
		}	
	}

/*Collection Post*/	
	public function actionPost()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$posts = ArrayHelper::map(Collection::find()->Where(['is_deleted' => '1'])->all(), function($scope){ return (string)$scope['_id']; },'is_deleted');
			if(!empty($posts))
			{
				$ids = array_keys($posts);
				$posts = PostForm::find()->Where(['in', 'collection_id', $ids])->asarray()->all();
			}
			return $this->render('post',['posts' =>$posts]);
		}	
	}
	
/*Collection Default Images*/
	public function actionImage()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome(); 
		} else {
			$defaultimages = DefaultImage::find()->where(['type'=>"collection"])->all();
			return $this->render('image',['defaultimages' =>$defaultimages]);	
		}	
	}
	

/*Collection Image Add*/	
	public function actionImageCrop()
	{
		$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
		$imgpaths = $front_url.'/uploads/defaultimage';
		if (!file_exists($imgpaths))
		{
			mkdir($imgpaths, 0777, true);
		}
		$dt = time();
		$imgUrl = $_POST['imgUrl'];
		// original sizes
		$imgInitW = $_POST['imgInitW'];
		$imgInitH = $_POST['imgInitH'];
		// resized sizes
		$imgW = $_POST['imgW'];
		$imgH = $_POST['imgH'];
		// offsets
		$imgY1 = $_POST['imgY1'];
		$imgX1 = $_POST['imgX1'];
		// crop box
		$cropW = $_POST['cropW'];
		$cropH = $_POST['cropH'];
		// rotation angle
		$angle = $_POST['rotation'];

		$jpeg_quality = 100;


		$output_filename = $front_url."/uploads/defaultimage/".$dt;

		$what = getimagesize($imgUrl);

		switch(strtolower($what['mime']))
		{
			case 'image/png':
				$img_r = imagecreatefrompng($imgUrl);
				$source_image = imagecreatefrompng($imgUrl);
				$type = '.png';
				break;
			case 'image/jpeg':
				$img_r = imagecreatefromjpeg($imgUrl);
				$source_image = imagecreatefromjpeg($imgUrl);
				error_log("jpg");
				$type = '.jpeg';
				break;
			case 'image/gif':
				$img_r = imagecreatefromgif($imgUrl);
				$source_image = imagecreatefromgif($imgUrl);
				$type = '.gif';
				break;
			default: die('image type not supported');
		}

		// resize the original image to size of editor
		$resizedImage = imagecreatetruecolor($imgW, $imgH);
		
		imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
		// rotate the rezized image
		$rotated_image = imagerotate($resizedImage, -$angle, 0);
		// find new width & height of rotated image
		$rotated_width = imagesx($rotated_image);
		$rotated_height = imagesy($rotated_image);
		// diff between rotated & original sizes
		$dx = $rotated_width - $imgW;
		$dy = $rotated_height - $imgH;
		// crop rotated image to fit into original rezized rectangle
		$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
		imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
		imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
		// crop image into selected area
		$final_image = imagecreatetruecolor($cropW, $cropH);
		/*if($type == ".gif" or $type == ".png")
		{
			imagesavealpha($final_image, true);
			imagealphablending($final_image, false);
			$transparent = imagecolorallocatealpha($final_image, 0, 0, 0, 127);
			imagefill($final_image, 0, 0, $transparent);
		}*/
		imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
		imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
		// finally output png image
		//imagepng($final_image, $output_filename.$type, $png_quality);
		imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
		$response = Array(
			"status" => 'success',
			"url" => $output_filename.$type
		);
		//$pageimage = DefaultImage::find()->where(['type'=>"collection"])->one();
		$image = $output_filename.$type;
		$date = time();
		/*if($pageimage){
			$pageimage->image = $image;
			$pageimage->updated_date = "$date"; 
			$pageimage->update();
		}else{*/
			$addimage = new DefaultImage();
			$addimage->type = "collection";
			$addimage->image = $image;
			$addimage->updated_date = "$date"; 
			$addimage->insert();
		//}
		return json_encode($response);
	}
	
/*Collection Image Remove*/
	public function actionRemovedefaultimage()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$mt_exist = DefaultImage::find()->where(['_id' => $id ])->one();
				$mt_exist->delete();
			}
			else
			{
				return false;   
			}
		}	
	}
	
/*Collection Reported Flag*/
	public function actionReportedflag()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } 
		else 
		{
			$reportcollection = CollectionReport::find()->select(['collection_id'])->distinct('collection_id');
			$flagcollection = Collection::find()->select(['_id'])->where(['flagger'=>'1'])->asarray()->all();
			$col = array();
			foreach($flagcollection as $flagcollect)
			{
				$col[] = (string)$flagcollect['_id'];
			}
			
			$reportcollection = array_merge($reportcollection,$col);
			$reportcollections = array_unique($reportcollection);
			return $this->render('reportedflag',['reportcollections' =>$reportcollections]);
		}		
	}
	
	public function actionReporter()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } 
		else 
		{
			if(isset($_GET['collection_id']))
			{
				$id = $_GET['collection_id'];
				$reportrs =  CollectionReport::getreportdata($id);
				return $this->render('reportrs',['reportrs' =>$reportrs,'collection_id'=>$id]);
			}	
		}	
	}

}