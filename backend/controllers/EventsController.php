<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use frontend\models\PageEvents;
use frontend\models\EventVisitors;
use frontend\models\PostForm;
use frontend\models\DefaultImage;

class EventsController  extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','all'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionAll()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$events = PageEvents::getAllEventsAdmin();
			return $this->render('all',['events' =>$events]);	
		}
	}
	
	public function actionEventremove()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$mt_exist = PageEvents::find()->where(['_id' => $id ])->one();
				if($mt_exist->delete())
				{
					EventVisitors::deleteAll(['event_id'=>$id]);
					return true;
				}
				else
				{
					return false;   
				}
			}
			else
			{
				return false;   
			}
		}	
	} 
	
	public function actionEventupdate()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$status = $_POST['status'];
				
				if($status == 'Active'){
					$dl = "0"; 
				}else{
					$dl = "1";
				}
				$update = PageEvents::find()->where(['_id' => "$id"])->one();
				$update->is_deleted = $dl;
				$update->update();
				return true;
			}else{
				return false;
			}	
		}	
	}
	
	public function actionPost()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$posts = ArrayHelper::map(PageEvents::find()->Where(['is_deleted' => '1'])->all(), function($scope){ return (string)$scope['_id']; },'is_deleted');
			if(!empty($posts))
			{
				$ids = array_keys($posts);
				$posts = PostForm::find()->Where(['in', 'event_id', $ids])->asarray()->all();
			}
			return $this->render('post',['posts' =>$posts]);
		}	
	}
	
	public function actionImage()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$defaultimages = DefaultImage::find()->where(['type'=>"event"])->all();
			return $this->render('image',['defaultimages' =>$defaultimages]);	
		}	
	}
	
	public function actionImageCrop()
	{
		$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
		$imgpaths = $front_url.'/uploads/defaultimage';
		if (!file_exists($imgpaths))
		{
			mkdir($imgpaths, 0777, true);
		}
		$dt = time();
		$imgUrl = $_POST['imgUrl'];
		// original sizes
		$imgInitW = $_POST['imgInitW'];
		$imgInitH = $_POST['imgInitH'];
		// resized sizes
		$imgW = $_POST['imgW'];
		$imgH = $_POST['imgH'];
		// offsets
		$imgY1 = $_POST['imgY1'];
		$imgX1 = $_POST['imgX1'];
		// crop box
		$cropW = $_POST['cropW'];
		$cropH = $_POST['cropH'];
		// rotation angle
		$angle = $_POST['rotation'];

		$jpeg_quality = 100;


		$output_filename = $front_url."/uploads/defaultimage/".$dt;

		$what = getimagesize($imgUrl);

		switch(strtolower($what['mime']))
		{
			case 'image/png':
				$img_r = imagecreatefrompng($imgUrl);
				$source_image = imagecreatefrompng($imgUrl);
				$type = '.png';
				break;
			case 'image/jpeg':
				$img_r = imagecreatefromjpeg($imgUrl);
				$source_image = imagecreatefromjpeg($imgUrl);
				error_log("jpg");
				$type = '.jpeg';
				break;
			case 'image/gif':
				$img_r = imagecreatefromgif($imgUrl);
				$source_image = imagecreatefromgif($imgUrl);
				$type = '.gif';
				break;
			default: die('image type not supported');
		}



		// resize the original image to size of editor
		$resizedImage = imagecreatetruecolor($imgW, $imgH);
		imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
		// rotate the rezized image
		$rotated_image = imagerotate($resizedImage, -$angle, 0);
		// find new width & height of rotated image
		$rotated_width = imagesx($rotated_image);
		$rotated_height = imagesy($rotated_image);
		// diff between rotated & original sizes
		$dx = $rotated_width - $imgW;
		$dy = $rotated_height - $imgH;
		// crop rotated image to fit into original rezized rectangle
		$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
		imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
		imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
		// crop image into selected area
		$final_image = imagecreatetruecolor($cropW, $cropH);
		imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
		imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
		// finally output png image
		//imagepng($final_image, $output_filename.$type, $png_quality);
		imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
		$response = Array(
			"status" => 'success',
			"url" => $output_filename.$type
		);
		$pageimage = DefaultImage::find()->where(['type'=>"event"])->one();
		$image = $output_filename.$type;
		$date = time();
		/*if($pageimage){
			$pageimage->image = $image;
			$pageimage->updated_date = "$date"; 
			$pageimage->update();
		}else{*/
			$addimage = new DefaultImage();
			$addimage->type = "event";
			$addimage->image = $image;
			$addimage->updated_date = "$date"; 
			$addimage->insert();
		//}
		return json_encode($response);
	}
}