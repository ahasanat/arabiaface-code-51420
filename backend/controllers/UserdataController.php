<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use frontend\models\UserForm;
use backend\models\Userdata;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use frontend\models\Order;
use frontend\models\Credits;
/**
 * Site controller
 */
class UserdataController
        extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','viewuser','deleteuser','deleteusertype','updateusertype','paymenthistory','credithistory','free','resetpassword', 'getusercountmonthyear'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
       
	    $model = new Userdata();
        $userlists = $model->getUser();
		return $this->render('user',['userdatas' => $userlists]);
    }

    public function actionGetusercountperdays() {
    	$model = new Userdata();
        if(isset($_POST['$month']) && $_POST['$month']) {
	     	$month = $_POST['$month'];
	        $result = $model->getUserCountPerDays($month);
	        return $result;
	       	exit;
	    }
    }  
	
	
	 public function actionGetusercountmonthyear() {
    	$model = new Userdata();
        if(isset($_POST['$month']) && $_POST['$month']) {
			if(isset($_POST['$year']) && $_POST['$year']) {
				$month = $_POST['$month'];
				$year = $_POST['$year'];
				$result = $model->getusercountmonthyear($month, $year);
				return $result;
				exit;
			}
	    }
    }  
	
	public function actionResetPassword()
    {
      
	    $model = new Userdata();
		$user_id = $_POST['id'];
		$user = Userdata::find()->where(['_id' => $user_id])->one();
		$rand = rand(0, 99999999);
		$user->password = "$rand";
		$user->con_password = "$rand";
		$user->update();
		return true;
	}
	
	public function actionViewuser()
    {
        $model = new Userdata();
		$request = Yii::$app->request;
		$user_id = $request->get('id'); 
        $user = Userdata::find()->where(['_id' => $user_id])->one();
        return $this->render('view_user',['user' =>$user]);
    }
	
	 public function actionDeleteuser()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
			$id = (string)$_POST['id'];
			$user =  new UserForm();
			$user = UserForm::find()->where(['_id' => $id])->one();
			$user->status = '3';
            if($user->update()){
				return true;
			}
			else{
				return false;
			}
        }
    }
	
	public function actionDeleteusertype()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
			$id = (string)$_POST['id'];
			$user =  new UserForm();
			$user = UserForm::find()->where(['_id' => $id])->one();
			if($_POST['d_type'] == '1'){
				$user->status = '4';
				$user->delete();
				return true;
			}
			else
			{
				$user->status = '3';		
				$user->update();
				return false;
			}
        }
    }
	
	public function actionUpdateusertype()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
			$id = (string)$_POST['id'];
			$user =  new UserForm();
			$user = UserForm::find()->where(['_id' => $id])->one();
			if($_POST['d_type'] == '1'){
				$user->status = '3';
				$user->update();
				return 3;
			}
			if($_POST['d_type'] == '2'){
				$user->status = '1';
				$user->update();
				return 1;
			}
			else
			{
				$user->status = '1';		
				$user->update();
				return 1;
			}
        }
		else
		{
			return 0;
		}
    }
	
	public function actionOut(){
		\Yii::$app->user->logout();

        return $this->goHome();
	}
	
	public function actionPaymentHistory()
    {
		if(isset($_GET['user_id']) &&  !empty($_GET['user_id']))
		{
			$model = new Order();
			$id = $_GET['user_id'];
			$userlists = $model->orderhistory($id);
			return $this->render('payment_history',['userdatas' => $userlists]);
			exit;
		}
		return $this->render('payment_history');
    }
	
	public function actionCreditHistory()
    {
		if(isset($_GET['user_id']) &&  !empty($_GET['user_id']))
		{
			$model = new Credits();
			$id = $_GET['user_id'];
			$userlists = $model->usercreditshistory2($id);
			return $this->render('credit_history',['userdatas' => $userlists]);
			exit;
		}
		return $this->render('credit_history');
    }
	
	public function actionFree()
    {
        $model = new Userdata();
        $userlists = $model->getUser();
		

        $user = ArrayHelper::map($userlists, 'fname', '_id');

        $users =  new UserForm();
		
        $gender = ArrayHelper::map(UserForm::find()->asArray()->where(['in','_id',$user])->all(),function($scope) { return (string)$scope['_id'];}, 'gender');
		$gender = array_count_values($gender);
		return $this->render('free_user_statastics',['gender' => $gender]);
    }

   
}
