<?php
namespace backend\models;
use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile; 
use yii\helpers\ArrayHelper;

/**
 * This is the model class for collection "occupation".
 *
 * @property \MongoId|string $_id
 * @property mixed $post_text
 * @property mixed $post_status
 * @property mixed $post_created_date
 * @property mixed $post_user_id
 * @property mixed $image
 */

class Channel extends ActiveRecord
{

   /*public $imageFile1;*/
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'channel';
    }

   /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
         return ['_id', 'name', 'profile', 'created_at', 'created_by', 'updated_at','status'];

    }

    public function addChannel($uid, $post) {
        if(isset($post) && !empty($post)) {
            $channelName = $post['channelName'];
            $profilePicture = $post['profilePicture'];
            $created_at = strtotime('now');
            $query = new Channel();
            $query->name = $channelName;
            $query->profile = $profilePicture;
            $query->created_at = $created_at;
            $query->created_by = $uid;
            $query->status = true;
            if($query->save()) {
                $created_at  = date("Y-m-d H:i:s", $created_at);
                $data = array();
                $data['id'] = (string)$query['_id'];
                $data['profile'] = $profilePicture;
                $data['name'] = $channelName;
                $data['created_at'] = $created_at;
                $result = array('status' => true, 'data' => $data);
                return json_encode($result, true);
                exit;
            }
        }
        $result = array('status' => false);
        return json_encode($result, true);
        exit;
    }

    public function editChannel($uid, $post) {
        if(isset($post) && !empty($post)) {
            $channelName = $post['channelName'];
            $profilePicture = $post['profilePicture'];
            $editId = $post['id'];
            $channel = Channel::find()->where([(string)'_id' => $editId])->one();
            if(!empty($channel)) {
                $oldProfile = $channel['profile'];
               
                //$channel = Channel::find($editId)->one();
                $channel->name = $channelName;
                if($profilePicture != '') {
                    $channel->profile = $profilePicture;
                }
                $channel->updated_at = strtotime('now');
                $channel->created_by = $uid;
                if($channel->update()) {
                    //remove old profile pictures
                    if($profilePicture != '') {
                        if(file_exists('../frontend/web/uploads/channel/'.$oldProfile)) {
                            unlink('../frontend/web/uploads/channel/'.$oldProfile);
                        }
                        if(file_exists('../frontend/web/uploads/channel/thumb_'.$oldProfile)) {
                            unlink('../frontend/web/uploads/channel/thumb_'.$oldProfile);
                        }
                    }
                    return true;
                }
            }
        }

        return false;
    }

    public function channelList() {
        $channelList = Channel::find()->asarray()->all();
        return json_encode($channelList, true);
        exit;
    }

    public function channelDelete($uid, $id) {
        if($uid) {
            $channel = Channel::find()->select(['profile'])->where([(string)'_id' => $id])->asarray()->one();
            if(!empty($channel)) {
                $oldProfile = $channel['profile'];
                $oldProfileName = substr($oldProfile, strrpos($oldProfile, '/') + 1);

                $channel = Channel::find($id)->one();
                if(!empty($channel)) {
                    $channel->delete();
                    //remove old profile pictures
                    if(file_exists('../frontend/web/uploads/channel/'.$oldProfileName)) {
                        unlink('../frontend/web/uploads/channel/'.$oldProfileName);
                    }
                    if(file_exists('../frontend/web/uploads/channel/thumb_'.$oldProfileName)) {
                        unlink('../frontend/web/uploads/channel/thumb_'.$oldProfileName);
                    }

                    return true;
                    exit;                    
                }
            }
        }

        return false;
        exit;
    }

    public function editfetch($id) {
        return Channel::find()->where([(string)'_id' => $id])->asarray()->one();
    }

    public function statusUpdate($id) {
        $data = Channel::find()->where([(string)'_id' => $id])->one();
        if(!empty($data)) {
            $status = $data['status'];
            $changedStatus = ($status == true) ? false : true;
            $data->status = $changedStatus;
            if($data->update()) {
                $label = ($changedStatus == true) ? 'Active' : 'Inactive';
                $result = array('status' => true, 'label' => $label);
                return json_encode($result, true);
                exit;        
            }
        }

        $result = array('status' => false);
        return json_encode($result, true);
        exit;        
    } 

}