<?php
namespace backend\models;
use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;

use yii\web\UploadedFile;

/**
 * This is the model class for collection "business_category".
 *
 * @property \MongoId|string $_id
 * @property mixed $post_text
 * @property mixed $post_status
 * @property mixed $post_created_date
 * @property mixed $post_user_id
 * @property mixed $image
 */

class Mail extends ActiveRecord
{
   /*public $imageFile1;*/
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'mail_tpl';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'tpl_name', 'tpl_subject', 'tpl_body', 'sendername', 'senderemail', 'added_on'];
    }
    
    public function getAll()
    {
        return Mail::find()->orderBy(['tpl_name'=>SORT_DESC])->all();
    }
}