<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Like;
use frontend\models\TravAdsVisitors;
$this->title = 'Ads';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Ads</h1>
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Ad List</h3>
					</div>
					<div class="box-body">
						<table id="alladslist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Name</th>
								  <th>Ad Object</th>
								  <th>Created By</th>
								  <th>Created Date</th>
								  <th>Likes / Impression</th>
								  <th>Take Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($ads as $ad)
								{
									$postid = (string)$ad['_id'];
									$adobj = $this->context->getAdType($ad['adobj']);
									$ad_owner = $ad['post_user_id'];
									$owner_name = $this->context->getuserdata($ad_owner,'fullname');
									$like_count = Like::getLikeCount($postid);
									$impression = TravAdsVisitors::getCount($postid,'impression');
									if($ad['is_ad']=='1'){$status = 'Active';}else{$status= 'Inactive';}
									if(empty($ad['adname'])){$ad['adname'] = '-';}
								?>
									<tr id="ad_<?=$postid?>">
										<td><?= $ad['adname'];?></td>
										<td><?= $adobj;?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $ad_owner;?>"><?= $owner_name;?></a></td>
										<td><?= date('d-M-Y',$ad['post_created_date']);?></td>
										<td><?= $like_count.' / '.$impression;?></td>
										<td>
											<a target="_blank" href="<?= $front_url;?>?r=site/travpost&postid=<?= $postid;?>">View</a> / <a id = "remove_<?= $postid;?>" href="javascript:void(0)" onclick="remove('<?= $postid;?>')">Delete</a> / <a id = "update_<?= $postid;?>" href="javascript:void(0)" onclick="update('<?= $postid;?>','<?= $status;?>')" ><?= $status;?></a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this ad?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=ads/remove', 
			type: 'POST',
			data: 'id='+id,
			success: function (data){
				$('#ad_'+id).html('');
				$('#ad_'+id).remove();
				var row = $("#"+id).parents('tr');
				$('#alladslist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
function update(id,status)
{	
	if(status == 'Active')
	{
		var statuss = 'Inactive';
	}
	else
	{
		var statuss = 'Active';
	}
	var r = confirm("Are you sure to "+statuss+" this ad?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=ads/update', 
			type: 'POST',
			data: 'id='+id+'&status='+status,
			success: function (data)
			{
				$('#update_'+id).html(statuss);
			}
		});
	}
}
</script>