<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;

/*$travelasset = backend\assets\TravelAsset::register($this);
$travelbaseUrl = $travelasset->baseUrl;
*/
$travelbaseUrl='';
$this->title = 'Channel';
$session = Yii::$app->session;
$email = $session->get('username'); 

$baseUrl = AppAsset::register($this)->baseUrl;

if(isset($information) && !empty($information)) {
	$information = json_decode($information, true);
} else {
	$information = array();
} 

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<h1>Channel</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-lg-12 col-xs-12">
		  <div class="box">
			<div class="box-header">
              <h3 class="box-title">Add Channel</h3>
            </div>
			<div class="box-body">
				<div class="settings-content-holder">
					<div id="menu-coverpic" class="coverpic-content settings-content" style="display:block; min-height: auto !important;">
						<div class="cover-settings">						
							<div class="row">

								<div class="form-group">
				                  <label for="exampleInputEmail1">Channel Name</label>
				                  <input type="input" class="form-control" id="channelName" placeholder="Enter channel name">
				                </div><br/>
				                <div class="form-group">
									<div class="uploadProfile-stuff">
										<p class="grayp">Upload channel profile picture</p>
										<div class="uploadChannelpic-cropping">
											<div class="crop-holder">
												<div id="channelpicCrop"></div>
											</div>		
										</div>
										<div class="fakeFileButton">
											<img src="<?=$baseUrl?>/images/upload-green.png" /> &nbsp; Upload a photo from your computer
											<input type="file" id="coverpic-upload" accept="image/*">
											
										</div>
										<div class="btn-holder">						
											<input type="button" onclick="cancelImageCrop();" value="Cancel" class="btn btn-primary btn-sm">		
											<input type="button" value="Save" class="btn btn-primary btn-sm clkbackcover">
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>				
			</div>
		  </div>
        </div>
        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <section class="content-header">
    	<h1>Channel List</h1>
	</section>
	

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Channel List</h3>
					</div>
					<div class="box-body">
						<div class="contentExist">
							<table id="channelListTable" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Image</th>
										<th>Name</th>
										<th>Created at</th>
										<th>Updated at</th>
										<th>Action</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								<?php
									$i=1;
									foreach ($information as $key => $value) {
										$idArray = array_values($value['_id']);
										$id = $idArray[0];
										//$id = (string)$value['_id']['$id'];
										$image = $value['profile'];
										$name = $value['name'];
										$profile = $value['profile'];
										if(!(file_exists('../frontend/web/uploads/channel/thumb_'.$profile))) {
											$profile = 'eventdefault.png';
										}

										$created_at = $value['created_at'];
										if($created_at != '') {
											$created_at = date("Y-m-d H:i:s", $created_at);
										}

										$updated_at = isset($value['updated_at']) ? $value['updated_at'] : '';
										if($updated_at != '') {
											$updated_at = date("Y-m-d H:i:s", $updated_at);
										}
										$status = (isset($value['status']) && $value['status'] == true) ? 'Active' : 'Inactive';	

										echo '<tr>
											<td>'.$i.'</td>
											<td><figure class="coverpicbox-holder"><a href="../frontend/web/uploads/channel/thumb_'.$profile.'" data-size="1600x1600" data-med="../frontend/web/uploads/channel/thumb_'.$profile.'" data-med-size="1024x1024" class="coverpic-box"><img src="../frontend/web/uploads/channel/thumb_'.$profile.'"></a></figure></td>
											<td>'.$name.'</td>
											<td>'.$created_at.'</td>
											<td>'.$updated_at.'</td>
											<td><center><a href="javascript:void(0)" data-toggle="modal" data-target="#editchannelpopup" data-whatever="@getbootstrap" onclick="cep188207(\''.$id.'\', this);">Edit</a></center></td>
											<td><center><a href="javascript:void(0);" onclick="statusUpdate(\''.$id.'\', this);">'.$status.'</a></center></td>
										</tr>';
										$i++;
									}
								?>
								<?php 
								?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>
    </section>
  </div>

	<div class="modal fade" id="editchannelpopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel">Edit Channel</h4>
	      </div>
	      <div class="modal-body">
	        <div class="post-loader text-center postfeed-loader">
 				<img src="<?=$baseUrl?>/images/loader.svg" />
  			</div>
	    </div>
	  </div>
	</div>



  <script>
		document.getElementById('coverpic-upload').onchange = function (evt) {
			var tgt = evt.target || window.event.srcElement,
			files = tgt.files;
			// FileReader support
			var fr = new FileReader();
			fr.onload = function (e) 
			{					
				coverpicCrop(e.target.result);
			}
			fr.readAsDataURL(files[0]);			
		}

		
		function previewFiles($id) {
			if($id != undefined && $id != null && $id != '') {
			 	$("#editchannelpopup").find('#editchannelpicCrop').html('<img src="../frontend/web/images/loader.svg">'); 
			  	var files   = document.querySelector('#editcoverpic-upload').files;

			  	function readAndPreview(file) {

				    // Make sure `file.name` matches our extensions criteria
				    if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
				      var reader = new FileReader();
				      reader.addEventListener("load", function () {
				      	editcoverpicCrop(this.result, $id);

				      }, false);
				      reader.readAsDataURL(file);
				    }

				}

			  	if (files) {
			    	[].forEach.call(files, readAndPreview);
			  	}
			} 
		}

		function coverpicCrop($imgPath) { 
			$channelName = $("#channelName").val();
			if($channelName != undefined || $channelName != null || $channelName != '') {
				if($imgPath != undefined || $imgPath != null || $imgPath != '') {
				var croppicContainerPreloadOptions = 
				{
					cropUrl:'?r=channel/profileimagecrop',
					loadPicture:$imgPath,
					zoomFactor:10,
					doubleZoomControls:true,
					rotateFactor:10,
					rotateControls:true,
					cropData:{
	                    "channelName": $channelName
	                },
					enableMousescroll:true,
					onAfterImgCrop:function($result)
					 { 
					 	$filterResult = JSON.parse($result);
						if($filterResult.status != undefined && $filterResult.status == true) {
					 		$('#channelpicCrop').html('');
							var $data = $filterResult.data;
							var $profile = $data.profile;
					 		if($profile != 'undefined' || $profile != undefined) 
							{
								$('input#channelName').val('');
								$id = $data.id;
								$profile = '<figure class="coverpicbox-holder"><a href="../frontend/web/uploads/channel/thumb_'+$profile+'" data-size="1600x1600" data-med="../frontend/web/uploads/channel/thumb_'+$profile+'" data-med-size="1024x1024" class="coverpic-box"><img src="../frontend/web/uploads/channel/thumb_'+$profile+'"/></a></figure>';
								$name = $data.name;
								$created_at = $data.created_at;

								$edit = '<center><a href="javascript:void(0)" data-toggle="modal" data-target="#editchannelpopup" data-whatever="@getbootstrap" onclick="cep188207(\''+$id+'\', this);">Edit</a></center>';

								$status = '<a href="javascript:void(0);" onclick="statusUpdate(\''+$id+'\', this);">Active</a>';
								var t = $('#channelListTable').DataTable();
								var page = t.page.info();
								var counter = page.recordsTotal+1;
								t.row.add( [counter, $profile, $name, $created_at,'', $edit, $status] ).draw( false );
							}
					 	} else if($filterResult.status != undefined && $filterResult.status == false && $filterResult.error != undefined) {
					 		alert($filterResult.error);
					 	} else {
					 		window.href='';
					 	}
					}
				}

				var cropContainerPreload = new Croppic('channelpicCrop', croppicContainerPreloadOptions);				
				cropContainerPreload.destroy();
				cropContainerPreload.reset();
			} 
			}
		}

		function cancelImageCrop() {
			var cropper = new Croppic('channelpicCrop');
			cropper.destroy();
			cropper.reset();
		}
		
		function editcoverpicCrop($imgPath, $id) {
			$channelName = $("#editchannelName").val();
			if($id != undefined && $id != null && $id != '') {
				if($channelName != undefined && $channelName != null && $.trim($channelName) != '') {
					var croppicContainerPreloadOptions = 
					{
						cropUrl:'?r=channel/editprofileimagecrop',
						loadPicture:$imgPath,
						enableMousescroll:true,
						zoomFactor:10,
						doubleZoomControls:true,
						rotateFactor:10,
						rotateControls:true,
						cropData:{$channelName, $id},
						onAfterImgCrop:function($result)
						{ 
							window.location.href="";
						}
					}
					var cropContainerPreload = new Croppic('editchannelpicCrop', croppicContainerPreloadOptions);				
				}
			}
		}

		function deleteThis($id) {
			if($id != undefined && $id != null) {
				BootstrapDialog.show({
					size: BootstrapDialog.SIZE_SMALL,
					title: 'Delete Event',
					cssClass: 'custom-sdialog',
					message: 'Are you sure to delete this channel ?',
					buttons: [{
							label: 'Keep',
							action: function (dialogItself) {
								dialogItself.close();
							}
						}, {
							label: 'Delete',
							action: function (dialogItself) {
								$.ajax({
									url: '?r=channel/channeldelete', 
									type: 'POST',
									data: {$id},
									success: function (data) {
										dialogItself.close();
										var row = $("#"+$id).parent('td').parents('tr');
										$('#channelListTable').dataTable().fnDeleteRow(row); 
									}
								});
							}
						}]
				});				
			}
		}

		function statusUpdate($id, $obj) {
			if($id != undefined && $id != null && $id != '') {
				$.ajax({
					url: '?r=channel/statusupdate', 
					type: 'POST',
					data: {$id},
					success: function (data) {
						$filterResult = JSON.parse(data);
						if($filterResult.status != undefined && $filterResult.status == true) {
							$label = $filterResult.label;
							$($obj).html($label);
						}
					}
				});
			}
		}

		function cep188207($id) {
			if($id != undefined && $id != null && $id != '') {
				$.ajax({
					url: '?r=channel/editfetch', 
					type: 'POST',
					data: {$id},
					success: function (data) {
						cropper = new Croppic('editchannelpicCrop');
						cropper.reset(); 
						$('#editchannelpopup').find('.modal-body').html(data);
						if($(data).find('#editchannelpicCrop').find('img').length) {
							var $src = $(data).find('#editchannelpicCrop').find('img').attr('src');
							if($src != undefined && $src != null && $src != '') {
								editcoverpicCrop($src, $id);
							}			
						}
					}
				});
			}
		}

		/*function editsavechannel($id) {
			var isfileupload = $("#editcoverpic-upload").val();	
			if(isfileupload == '') {
				if($id != undefined && $id != null && $id != '') {
					$channelName = $.trim($("#editchannelName").val());
					if($channelName != undefined && $channelName != null && $.trim($channelName) != '') {
						$.ajax({
							url: '?r=channel/editprofileimagecrop', 
							type: 'POST',
							data: {$id, $channelName},
							success: function (data) {
								window.location.href="";
							}
						});
					}
				}
			} else {
				$( ".cropControlCrop" ).trigger( "click" );
			}
		}*/
  </script>