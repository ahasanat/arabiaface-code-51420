<?php 
use yii\helpers\Html;
use yii\helpers\Url;

/*$travelasset = backend\assets\TravelAsset::register($this);
$travelbaseUrl = $travelasset->baseUrl;
*/
$travelbaseUrl='';
$this->title = 'Edit Channel';
$session = Yii::$app->session;
$email = $session->get('username'); 

if(isset($information) && !empty($information)) {
	$information = json_decode($information, true);
	$name = $information['name'];
	$idArray = array_values($information['_id']);
	$editId = $idArray[0];
	//$editId = (string)$information['_id']['$id'];
	$profile = $information['profile'];
} else {
	 return $this->redirect('?r=channel/add');
	 exit;
} 

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      	<h1>Channel</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-lg-12 col-xs-12">
		  <div class="settings-page box">
			<div class="box-header">
              <h3 class="box-title">Add Channel</h3>
            </div>
			<div class="box-body">
				<div class="settings-content-holder">
					<div id="menu-coverpic" class="coverpic-content settings-content" style="display:block;">			<div class="cover-settings">						
							<div class="row">
								<div class="form-group">
				                  <label for="exampleInputEmail1">Channel Name</label>
				                  <input type="input" class="form-control" id="channelName" value="<?=$name?>" placeholder="Enter email">
				                </div><br/>
				                <div class="form-group">
									<div class="uploadProfile-stuff">
										<p class="grayp">Upload channel profile picture</p>
										<div class="uploadChannelpic-cropping">
											<div class="crop-holder">
												<div id="channelpicCrop"></div>
											</div>		
										</div>
										<div class="fakeFileButton">
											<img src="<?=$travelbaseUrl?>/images/upload-green.png" /> &nbsp; Upload a photo from your computer
											<div class="form-group">
												<input type="file" id="coverpic-upload" accept="image/*">
											</div>									
										</div>
										<div class="btn-holder">						
											<input type="button" value="Save" class="btn btn-primary btn-sm clkchanneledit">
											<input type="button" onclick="hidewebcam();" value="Cancel" class="btn btn-primary btn-sm">						
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>				
			</div>
		  </div>
        </div>
        
      </div>
      <!-- /.row -->
    </section>


  <script>
		var croppicContainerPreloadOptions = {};
		document.getElementById('coverpic-upload').onchange = function (evt) {
			var tgt = evt.target || window.event.srcElement,
			files = tgt.files;
			// FileReader support
			var fr = new FileReader();
			fr.onload = function (e) 
			{
				coverpicCrop(e.target.result);					
			}
			fr.readAsDataURL(files[0]);			
		}
		
		$(document).ready(function() {
			$pofileURL = '<?php echo $profile; ?>';
			if($pofileURL != undefined || $pofileURL != null || $pofileURL != '') {
				coverpicCrop($pofileURL);
			}
		});
		

		function coverpicCrop($imgPath) {
			$editId = "<?php echo isset($editId) ? $editId : '';?>";
			if($editId != undefined || $editId != null || $editId != '') {
				$channelName = $("#channelName").val();
				if($channelName != undefined || $channelName != null || $channelName != '') {
					if($imgPath != undefined || $imgPath != null || $imgPath != '') {
						croppicContainerPreloadOptions = 
						{
							cropUrl:'?r=channel/editprofileimagecrop',
							loadPicture:$imgPath,
							enableMousescroll:true,
							cropData:{
			                    "channelName": $channelName,
			                    "editId" : $editId
			                },
							onAfterImgCrop:function($result)
							{ 
								if($result.status != undefined && $result.status == true) {
									window.location.href='?r=channel/add';
									} else if($result.status != undefined && $result.status == false && $result.error != undefined) {
										alert($result.error);
									} else {
										window.href='';
								}
							}
						}

						var cropContainerPreload = new Croppic('channelpicCrop', croppicContainerPreloadOptions);				
						cropContainerPreload.destroy();
						cropContainerPreload.reset();
					} 
				}
			}
		}

		$('.clkchanneledit').on('click', function () {
			$channelName = $("#channelName").val();
			$editId = "<?php echo isset($editId) ? $editId : '';?>";
			croppicContainerPreloadOptions.cropData.channelName = $channelName;
			croppicContainerPreloadOptions.cropData.editId = $editId;
			$('.cropControlCrop').click();
		});

	</script>

  </div>