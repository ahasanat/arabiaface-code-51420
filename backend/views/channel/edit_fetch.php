<?php 
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');

if(isset($data) && !empty($data)) { 
	
	$id = (string)$data['_id'];
    $name = $data['name'];
    $profile = $data['profile'];

    if(!(file_exists('../frontend/web/uploads/channel/thumb_'.$profile))) {
    	$profile = 'eventdefault.png';
    }
?>
<form>
	<div class="form-group">
		<label class="control-label">Channel Name</label>
		<input type="text" class="form-control" id="editchannelName" value="<?=$name?>" placeholder="Enter channel name">
	</div>
	<div class="form-group">
		<div class="uploadProfile-stuff">
			<p class="grayp">Upload channel profile picture</p>
			<div class="uploadChannelpic-cropping">
				<div class="crop-holder">
					<div id="editchannelpicCrop">
						<img src="../frontend/web/uploads/channel/thumb_<?=$profile?>" style="width: 100%;">
					</div>
				</div>		
			</div>
			<div class="fakeFileButton">
				<img src="<?=$baseUrl?>/images/upload-green.png" /> &nbsp; Upload a photo from your computer
				
				<input type="file" id="editcoverpic-upload" value="<?=$profile?>" onchange="previewFiles('<?=$id?>')" accept="image/*">
			
			</div>
		</div>
	</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary clkbackcover">Save</button>
</div>	
</form>

<?php 
}
exit;?>							