<html>
	<head>
		<link href="croppic.css" rel="stylesheet">
		<script type="text/javascript" src="jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="croppic.js"></script>
		<style type="text/css">
			#yourId {
			width: 200px;
			height: 150px;
			position:relative; /* or fixed or absolute */
		}
			
		</style>
	</head>
	<body>
		<div id="yourId"></div>
		<script type="text/javascript">
			var cropperOptions = {
				uploadUrl:'process.php'
			}		
			
			var cropperHeader = new Croppic('yourId', cropperOptions);
		</script>
	</body>
</html>