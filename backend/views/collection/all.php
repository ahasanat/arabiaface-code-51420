<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
$this->title = 'Collections';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Groups</h1>
		<!--<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=collection/all">Collections</a></li>
		</ol>--> 
    </section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Group List</h3>
					</div>
					<div class="box-body">
						<table id="collectionlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Name</th>
								  <th>Owner</th>
								  <th>Created Date</th>
								  <th>Followers</th>
								  <th>Post</th>
								  <th>Status</th>
								  <th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($collections as $collection)
								{
									$id = $collection['_id'];
									$fcount = CollectionFollow::getcollectionfollowcount($id);
									$user_id = $collection['user_id'];
									$owner = $this->context->getuserdata($user_id,'fullname');
									if($collection['is_deleted']=='1'){$status = 'Active';}else if($collection['is_deleted']=='2'){$status= 'Flagged';} else {$status= 'Inactive';}
								?>
									<tr>
										<td><?= $collection['name'];?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $user_id;?>"><?= $owner;?></a></td>
										<td><?= date('d-M-Y',$collection['created_date']);?></td>
										<td><a href="?r=collection/followers&collection_id=<?= $id;?>"><?= $fcount;?>	</a></td>
										<td><?= $this->context->getpostcount($id ,'collection_id');?></td>
										<?php /*?><td><a  id = "update_<?= $id;?>" href="javascript:void(0)" onclick="update('<?= $id;?>','<?= $status;?>')" ><?= $status;?></a></td><?php */?>
										<td><?= $status;?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=collection/detail&col_id=<?= $id;?>">View</a> <?php /*?>/ <a  id = <?= $id;?> href="javascript:void(0)" onclick="remove('<?= $id;?>')">Delete</a><?php */?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this collection?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=collection/collectionremove', 
			type: 'POST',
			data: 'id=' + id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#collectionlist').dataTable().fnDeleteRow(row);
			}
		});
	}
}

function update(id,status)
{
	
	if(status == 'Active')
	{
		var statuss = 'Inactive';
	}else{
		var statuss = 'Active';
	}
	var r = confirm("Are you sure to "+statuss+" this collection?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=collection/collectionupdate', 
			type: 'POST',
			data: 'id=' + id + '&status='+status,
			success: function (data)
			{
				$('#update_'+id).html(statuss);
			}
		});
	}
}
</script>