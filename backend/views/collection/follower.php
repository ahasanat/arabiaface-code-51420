<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
$this->title = 'Collection followers';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Groups follower</h1>
	</section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Group follower List</h3>
					</div>
					<div class="box-body">
						<table id="collectionlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Follower Name</th>
								  <th>Followed Date</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($followers as $follower)
								{
									$user_id = $follower['user_id'];
									$user_name = $this->context->getuserdata($user_id,'fullname');
								?>
									<tr>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $user_id;?>"><?= $user_name;?></a></td>
										<td><?= date('d-M-Y',$follower['modified_date']);?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>