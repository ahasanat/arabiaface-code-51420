<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl;
$this->title = 'Collection Image';
?>
<div class="content-wrapper">
    <section class="content-header">
		<h1>Groups Image</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Group Image</h3>
						
						<?php if(count($defaultimages)< 16){?>
							<div class="right">
								<div class="overlayUploader"><input type="file" id="uploadphoto"/><a href="javascript:void(0);" class="popup-modal"><button class="btn btn-primary btn-sm btn-flat" type="button">Add Image</button></a></div>
							</div>
						<?php }?>
					</div>
					
					<div>
					
					<div class="box-body">
						<table id="collectionimglist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Image</th>
								  <th>Created Date</th>
								  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($defaultimages as $defaultimage)
								{
									$id = $defaultimage['_id'];
									?>
									<tr>
										<td><img height="100px" src="<?= $defaultimage['image'];?>"></td>
										<td><?= date('d-M-Y',$defaultimage['updated_date']);?></td>
										<td><a  id = <?= $id;?> href="javascript:void(0)" onclick="removedefaultimage('<?= $id;?>')">Delete</a></td>
										<?php /*<td><div class="overlayUploader"><input type="file" id="uploadphoto"/><a href="javascript:void(0);" class="popup-modal">Update</a></div></td> */?>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<div id="collection-crop" class="mfp-hide white-popup-block popup-area pic-change-popup photo-crop">
	<div class="popup-title">
		<div class="uploadphoto pc-title"><h3>Upload photo</h3></div>
		
		<a class="popup-modal-dismiss close-popup openBootstrapPopup" href="javascript:void(0)">
			<img src="<?=$baseUrl?>/images/cross-icon.png"/>
		</a>
	</div>
	<div class="popup-content">
	
		<div class="popupdata">
			<div class="uploadphoto pc-section">
				<!-- crop section -->
				<div class="crop-holder">
					<div id="collectionCrops"></div>
				</div>
				<div class="btn-holder">
					<a  class="popup-modal-dismiss btn btn-primary btn-sm close-popup" data-dismiss="modal" onclick="cropTriggerClkBywebCam()" href="javascript:void(0)">Save</a>
					<a href="javascript:void(0)" class="btn btn-primary btn-sm close-popup">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>	
<script type="text/javascript">
	document.getElementById('uploadphoto').onchange = function (evt) {
		var tgt = evt.target || window.event.srcElement,
		files = tgt.files;
		// FileReader support
		var fr = new FileReader();
		fr.onload = function (e) {	
			$.magnificPopup.open({
				items: {
					src: '#collection-crop'
				},
				closeMarkup: '',
				type: 'inline'
			});
			collectionCrops(e.target.result);				
		}
		fr.readAsDataURL(files[0]);
	}
	
	function collectionCrops($imgPath) 
	{
		if($imgPath != '') {
			var profileCropOptions = {
				cropUrl:'?r=collection/image-crop',
				loadPicture:$imgPath,
				onAfterImgCrop:function(response){
					location.reload();
				}
			}
			var collectionCrops = new Croppic('collectionCrops', profileCropOptions);
		}
	}
	function cropTriggerClkBywebCam() {
		$( ".cropControlCrop" ).trigger( "click" );
	}
	function removedefaultimage(id)
	{
		var r = confirm("Are you sure to delete this Image?");
		if (r == false)
		{
			return false;
		}
		else 
		{
			$.ajax({
				url: '?r=collection/removedefaultimage', 
				type: 'POST',
				data: 'id=' + id,
				success: function (data){
					location.reload();
				}
			});
		}
	}
</script>