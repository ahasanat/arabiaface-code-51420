<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Collection;
$this->title = 'Collection Posts';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Groups Posts</h1>
	</section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Group Post List</h3>
					</div>
					<div class="box-body">
						<table id="collectionpostlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Collection Name</th>
								  <th>Owner</th>
								  <th>Posted Date</th>
								  <th>Reported Count</th>
								  <th>Flag</th>
								  <th>Status</th>
								  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Collection</td>
									<td>Romesh Gupta</td>
									<td>05/04/2017</td>
									<td><a href="javascript:void(0)">05</a></td>
									<td>Yes</td>
									<td><a href="javascript:void(0)">Inactive</a></td>
									<td>view/Delete</td>
								</tr>
								<tr>
									<td>Collection</td>
									<td>Romesh Gupta</td>
									<td>05/04/2017</td>
									<td><a href="javascript:void(0)">03</a></td>
									<td>No</td>
									<td><a href="javascript:void(0)">Active</a></td>
									<td>view/Delete</td>
								</tr>
							</tbody>
							<?php /*<tbody>
								<?php foreach($posts as $post)
								{
									$postid = $post['_id'];
									$post_user_id = $post['post_user_id'];
									$owner_name = $this->context->getuserdata($post_user_id,'fullname');
									$collection_id = $post['collection_id'];
									$cl_details = Collection::find()->Where(['_id'=>"$collection_id"])->asarray()->one();
									$cl_name = $cl_details['name'];
								?>
									<tr id="page_<?=$postid?>">
										<td><a target="_blank" href="<?= $front_url;?>?r=event/detail&e=<?= $collection_id;?>"><?= $cl_name;?></a></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $post_user_id;?>"><?= $owner_name;?></a></td>
										<td><?= $post['post_text'];?></td>
										<td><?= date('d-M-Y',$post['post_created_date']);?></td>
										<td>
											<a target="_blank" href="<?= $front_url;?>?r=site/travpost&postid=<?= $postid;?>">View</a> / <a id="<?= $postid;?>" href="javascript:void(0)" onclick="remove('<?= $postid;?>')">Delete</a>
										</td>
									</tr>
								<?php } ?>
							</tbody><?php */ ?>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this post?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=page/removepost', 
			type: 'POST',
			data: 'post_id='+id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#collectionpostlist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
</script>