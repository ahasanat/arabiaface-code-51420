<?php 
use yii\helpers\Html;
use frontend\models\Collection;
use frontend\models\CollectionReport;
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
$this->title = 'Reported Flag Collection';
?>
<div class="content-wrapper">
    <section class="content-header">
		<h1>Reported Flag Collection</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Reported Flag Collection List</h3>
					</div>
					<div class="box-body">
						<table id="flag_collections" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Collection Name</th>
									<th>Owner</th>   
									<th>Created Date</th>
									<th>Reported Count</th>
									<th>Flag</th>
									<th>Flag date</th>
									<th>Status</th>
									<th>Status date</th>
									<th>Actions</th>  
								</tr>
							</thead>
							<tbody>
							<?php foreach($reportcollections as $collection){
								
								if(Collection::getcollectiondata($collection,'is_deleted')=='1'){$status = 'Active';} else {$status= 'Inactive';}
								if(Collection::getcollectiondata($collection,'flagger')=='1') {$flag = 'Yes';} else {$flag = 'No';}
								$flag_date = Collection::getcollectiondata($collection,'flagger_date');
								if($flag_date){$flag_date = date('d-m-Y',$flag_date); } else {$flag_date = '---';}
								$status_date = Collection::getcollectiondata($collection,'status_date');
								if($status_date){$status_date = date('d-m-Y',$status_date); } else {$status_date = '---';}
								$user_id = Collection::getcollectiondata($collection,'user_id');
								
								?>
								<tr>
									<td><?= Collection::getcollectiondata($collection,'name');?></td>
									<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $user_id;?>"><?= $this->context->getuserdata($user_id,'fullname');?></a></td>
									<td><?= date('d-M-Y',Collection::getcollectiondata($collection,'created_date'));?></td>
									<td><a href="?r=collection/reporter&collection_id=<?= $collection;?>"><?= CollectionReport::getreportcount($collection);?></a></td>
									<td><?= $flag;?></td>
									<td><?= $flag_date;?></td>
									<td><a id = "update_<?= $collection;?>" href="javascript:void(0)" onclick="update('<?= $collection;?>','<?= $status;?>')"><?= $status;?></a></td>
									<td><?= $status_date;?></td>
									<td><a target="_blank" href="<?= $front_url;?>?r=collection/detail&col_id=<?= $collection;?>">View</a> 
									<?php if($status == 'Inactive'){
										$date1=date_create($status_date);
										$date2= date_create(date('d-m-Y'));
										$diff= date_diff($date1,$date2);
										$date_diff = $diff->format("%a");
										if($date_diff > 30){
										?>	
											/ <a  id = <?= $collection;?> href="javascript:void(0)" onclick="remove('<?= $collection;?>')">Delete</a></td>
										<?php 
										}
									}?>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this collection?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=collection/collectionremove', 
			type: 'POST',
			data: 'id=' + id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#flag_collections').dataTable().fnDeleteRow(row);
			}
		});
	}
}
function update(id,status)
{
	
	if(status == 'Active')
	{
		var statuss = 'Inactive';
	}else{
		var statuss = 'Active';
	}
	var r = confirm("Are you sure to "+statuss+" this collection?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=collection/collectionupdate', 
			type: 'POST',
			data: 'id=' + id + '&status='+status,
			success: function (data)
			{
				$('#update_'+id).html(statuss);
			}
		});
	}
}
</script>