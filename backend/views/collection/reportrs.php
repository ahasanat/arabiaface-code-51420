<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Collection;
$this->title = 'Collection Report';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Groups report</h1>
	</section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><?= Collection::getcollectiondata($collection_id,'name');?> report List</h3>
					</div>
					<div class="box-body">
						<table id="collectionlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Name</th>
								  <th>Reason</th>
								  <th>Reported Date</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($reportrs as $reporter)
								{
									$user_id = $reporter['reporter_id'];
								?>
									<tr>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $user_id;?>"><?= $this->context->getuserdata($user_id,'fullname');?></a> </td>
										<td><?= $reporter['reason'];?></td>
										<td><?= date('d-M-Y',$reporter['created_date']);?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>