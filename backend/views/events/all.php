<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\EventVisitors;
$this->title = 'Events';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Events</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=events/all">Events</a></li>
		</ol> 
    </section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Event List</h3>
					</div>
					<div class="box-body">
						<table id="eventlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Name</th>
								  <th>Orgnizer</th>
								  <th>Created Date</th>
								  <th>Event Date</th>
								  <th>Location</th>
								  <th>Attending</th>
								  <th>Take Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($events as $event)
								{ 
									$id = $event['_id'];
									$event_count = EventVisitors::getEventCounts((string)$id);
									if($event['is_deleted']=='1'){$status = 'Active';}else{$status= 'Inactive';}
								?>
								<tr>
									<td><?= $event['event_name'];?></td>
									<td><?= $event['address'];?></td>
									<td><?= date('d/m/Y',$event['created_date']);?></td>
									<td><?= $event['event_date'];?></td>
									<td><?= $event['address'];?></td>
									<td><?= $event_count;?></td>
									<td><a target="_blank" href="<?= $front_url;?>?r=event/detail&e=<?= $id;?>">View</a><!-- / <a  id = <?= $id;?> href="javascript:void(0)" onclick="remove('<?= $id;?>')">Delete</a> / <a id = "update_<?= $id;?>" href="javascript:void(0)" onclick="update('<?= $id;?>','<?= $status;?>')" ><?= $status;?></a>--></td>
								</tr>
								<?php } ?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this event?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=events/eventremove',
			type: 'POST',
			data: 'id=' + id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#eventlist').dataTable().fnDeleteRow(row);
			}
		});
	}
}

function update(id,status)
{
	if(status == 'Active')
	{
		var statuss = 'Inactive';
	}else{
		var statuss = 'Active';
	}
	var r = confirm("Are you sure to "+statuss+" this event?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=events/eventupdate',
			type: 'POST',
			data: 'id=' + id + '&status='+status,
			success: function (data)
			{
				$('#update_'+id).html(statuss);
			}
		});
	}
}
</script>