<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\GroupMember;
use frontend\models\GroupPhoto;
$this->title = 'Groups';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Groups</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=groups/all">Groups</a></li>
		</ol> 
    </section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Group List</h3>
					</div>
					<div class="box-body">
						<table id="grouplist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Name</th>
								  <th>Location</th>
								  <th>Group Admin</th>
								  <th>Members</th>
								  <th>Photos</th>
								  <th>Created Date</th>
								  <th>Status</th>
								  <th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($groups as $group)
								{
									$id = $group['_id'];	
									$user_id = $group['user_id'];
									if($group['is_deleted']=='1'){$status = 'Active';}else{$status= 'Inactive'; }
									$admin_name = $this->context->getuserdata($user_id,'fullname');
									$member = GroupMember::getgroupmembercount($id);
									$photo = count(GroupPhoto::getphoto($id));
								?>
									<tr>
										<td><?= $group['name'];?></td>
										<td><?= $group['location'];?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $user_id;?>"><?= $admin_name;?></a></td>
										<td><a href="?r=groups/members&group_id=<?= $id;?>"><?= $member;?></a></td>
										<td><a href="?r=groups/photos&group_id=<?= $id;?>"><?= $photo;?></a> </td>
										<td><?= date('d-M-Y',$group['created_date']);?></td>
										<td><a  id = "update_<?= $id;?>" href="javascript:void(0)" onclick="update('<?= $id;?>','<?= $status;?>')" ><?= $status;?></a></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=groups/detail&group_id=<?= $id;?>">View</a> <span id="hide_<?= $id;?>"><?php if($status =='Inactive'){?>/ <a  id = <?= $id;?> href="javascript:void(0)" onclick="remove('<?= $id;?>')">Delete</a><?php } ?></span></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this group?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=groups/groupremove', 
			type: 'POST',
			data: 'id=' + id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#grouplist').dataTable().fnDeleteRow(row);
			}
		});
	}
}

function update(id,status)
{
	
	if(status == 'Active')
	{
		var statuss = 'Inactive';
	}else{
		var statuss = 'Active';
	}
	var r = confirm("Are you sure to "+statuss+" this collection?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=groups/groupupdate', 
			type: 'POST',
			data: 'id=' + id + '&status='+status,
			success: function (data)
			{
				$('#update_'+id).html(statuss);
				if(statuss == 'Active'){
					$("#hide_"+id).hide();
				}else{
					var da = '/ <a  id = '+id+' href="javascript:void(0)" onclick="remove(\''+id+'\')">Delete</a>';
					$("#hide_"+id).html(da);
				}
				/*var row = $("#"+id).parents('tr');
				$('#collectionlist').dataTable().reload;*/
			}
		});
	}
}
</script>