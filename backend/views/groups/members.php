<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Group;
use frontend\models\PostForm;
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
if(isset($group_id))
{
	$group_names = Group::find()->select(['name'])->Where(['_id'=>"$group_id"])->one();
	$group_name = ucfirst($group_names['name']);
}
else
{
	$group_name = 'Group';
}	
$this->title = $group_name." members";
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?= ucfirst($group_name);?>'s members</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=groups/all">Groups</a></li>
		</ol> 
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><?= $group_name;?>'s members List</h3>
					</div>
					<div class="box-body">
						<table id="groupmemberlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>User Name</th>
								  <th>Joined Date</th>
								  <!--<th>Post</th>-->
								  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($members as $member)
								{
									$id = $member['_id'];
									$user_id = $member['user_id'];
									$groups_id = $member['group_id']; 
									$user_name = $this->context->getuserdata($user_id,'fullname');
								?>
									<tr>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $user_id;?>"><?= $user_name;?></a></td>	
										<td><?= date('d-M-Y',$member['created_date']);?></td>
										<td><a  id = <?= $id;?> href="javascript:void(0)" onclick="remove('<?= $id;?>')">Delete</a></td>
									</tr>
								<?php 	
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this member?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=groups/removemember', 
			type: 'POST',
			data: 'id=' + id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#groupmemberlist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
</script>