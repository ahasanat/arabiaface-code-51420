<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Group;
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
$group_names = Group::find()->select(['name'])->Where(['_id'=>"$group_id"])->one();
$group_name = ucfirst($group_names['name']);
$this->title = $group_name." photos";
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?= ucfirst($group_name);?>'s members</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=groups/all">Groups</a></li>
		</ol> 
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><?= $group_name;?>'s members List</h3>
					</div>
					<div class="box-body">
						<table id="groupphotolist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Photo Name</th>
								  <th>Photo</th>
								  <th>Created Date</th>
								  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($photos as $photo)
								{
									$id = $photo['_id'];
								?>
									<tr>
										<td><?= $photo['name'];?></td>
										<td><img width="50px" src="<?= $front_url;?><?= $photo['image'];?>"></td>
										<td><?= date('d-M-Y',$photo['created_date']);?></td>
										<td><a  id = <?= $id;?> href="javascript:void(0)" onclick="remove('<?= $id;?>')">Delete</a></td>
									</tr>
								<?php 	
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this member?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=groups/removephoto', 
			type: 'POST',
			data: 'id=' + id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#groupphotolist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
</script>