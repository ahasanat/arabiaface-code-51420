<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Group;
$this->title = 'Group Posts';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Group Posts</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=groups/all">Groups</a></li>
			<li><a href="?r=groups/post">Posts</a></li>
		</ol> 
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Groups Post List</h3>
					</div>
					<div class="box-body">
						<table id="grouppostlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Group Name</th>
								  <th>Posted By</th>
								  <th>Post Comment</th>
								  <th>Posted Date</th>
								  <th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($posts as $post)
								{
									$postid = $post['_id'];
									$post_user_id = $post['post_user_id'];
									$owner_name = $this->context->getuserdata($post_user_id,'fullname');
									$group_id = $post['group_id'];
									$group_details = Group::find()->Where(['_id'=>"$group_id"])->asarray()->one();
									$group_name = $group_details['name'];
								?>
									<tr id="page_<?=$postid?>">
										<td><a target="_blank" href="<?= $front_url;?>?r=groups/detail&group_id=<?= $group_id;?>"><?= $group_name;?></a></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $post_user_id;?>"><?= $owner_name;?></a></td>
										<td><?= $post['post_text'];?></td>
										<td><?= date('d-M-Y',$post['post_created_date']);?></td>
										<td>
											<a target="_blank" href="<?= $front_url;?>?r=site/travpost&postid=<?= $postid;?>">View</a> / <a id="<?= $postid;?>" href="javascript:void(0)" onclick="remove('<?= $postid;?>')">Delete</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this post?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=page/removepost', 
			type: 'POST',
			data: 'post_id='+id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#grouppostlist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
</script>