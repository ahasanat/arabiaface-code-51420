<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\PageEvents;
$this->title = 'Event Posts';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Event Posts</h1>
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Event Post List</h3>
					</div>
					<div class="box-body">
						<table id="pageeventist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Event Name</th>
								  <th>Page Name</th>
								  <th>Posted By</th>
								  <th>Posted Date</th>
								  <th>Post Comment</th>
								  <th>Take Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($eventposts as $post)
								{
									$postid = $post['_id'];
									$post_user_id = $post['post_user_id'];
									$owner_name = $this->context->getuserdata($post_user_id,'fullname');
									$event_id = $post['event_id'];
									$ev_details = PageEvents::find()->Where(['_id'=>"$event_id"])->asarray()->one();
									$ev_name = $ev_details['event_name'];
									$page_id = $ev_details['parent_id'];
									$page_name = $this->context->getuserdata($page_id,'fullname');
								?>
									<tr id="page_<?=$postid?>">
										<td><a target="_blank" href="<?= $front_url;?>?r=event/detail&e=<?= $event_id;?>"><?= $ev_name;?></a></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=page/index&id=<?= $page_id;?>"><?= $page_name;?></a></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $post_user_id;?>"><?= $owner_name;?></a></td>
										<td><?= date('d-M-Y',$post['post_created_date']);?></td>
										<td><?= $post['post_text'];?></td>
										<td>
											<a target="_blank" href="<?= $front_url;?>?r=site/travpost&postid=<?= $postid;?>">View</a> / <a  href="javascript:void(0)" onclick="remove('<?= $postid;?>')">Delete</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this post?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=page/removepost', 
			type: 'POST',
			data: 'post_id='+id,
			success: function (data){
				$('#page_'+id).html('');
				$('#page_'+id).remove();
				var row = $("#"+id).parents('tr');
				$('#pageeventist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
</script>