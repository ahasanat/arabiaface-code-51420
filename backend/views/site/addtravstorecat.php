<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Travstore Category';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper addbuscat-admin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>Travstore Category</h1>
	</section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Add Business Category</h3>
					</div>
					<div class="box-body">
						<form id="frm" class="form-horizontal">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-2 control-label">Business category name</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="name" id="name" required/><span class="name_notice" style="display: none">
									</div>
								</div>
							</div>
							<div class="box-footer">
								<input type="reset" name="clear" value="Clear"  class="btn btn-default"/>  
								<input type="button" name="add" value="Add" onclick="addtravcat()" class="btn btn-info right"/>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Business Category Listing</h3>
					</div>
					<div class="box-body">
						<table id="storecatlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Name</th>
								  <th>Delete</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($trav_cats as $trav_cat)
								{ ?>
								<tr>
									<td><?= $trav_cat['name'];?></td>
									<td><a id="<?= $trav_cat['_id'];?>" style="cursor: pointer;" onclick="removetravcat('<?= $trav_cat['_id'];?>')">Delete</a></td>
								</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>	
			</div>
        </div>
    </section>
</div>
<script>
   function addtravcat(){
	var name = $('#name').val();
	if(name == '')
	{
		$('.name_notice').html('Please enter travstore category name');
		$('.name_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
		$("#name").focus();
		return false;
	}
	else
	{
		$.ajax({
			url: '?r=site/addtravstorecat', 
			type: 'POST',
			data: 'name=' + name,
			success: function (data) 
			{
				if(data == 'insert')
				{
					$("#frm")[0].reset();
				}
				else
				{
					$('.name_notice').html('This travstory category already exist');
					$('.name_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
				}
			}
		});
	}
}
</script> 

<script>
function removetravcat(id)
{
	var r = confirm("Are you sure to delete this travstore category?");
	if (r == false) {
		return false;
	}
	else 
	{
		$.ajax({
				url: '?r=site/removetravcat', 
				type: 'POST',
				data: 'id=' + id,
				success: function (data) {
					var row = $("#"+id).parents('tr');
					$('#storecatlist').dataTable().fnDeleteRow(row);					
				}
			});
	}
}
</script>
 
