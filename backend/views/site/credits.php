<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Credits Plans';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper credits-admin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Credit Plans</h1>
     <?php  $session =
                    Yii::$app->session;
           echo  $email =
                    $session->get('username'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Add Credits Plans</h3>
            </div>
            <div class="box-body">
				  <form id="frm" class="topform">
					<div class="frow">
						<label>Please add amount </label>&nbsp;
						<input type="text" name="amount" id="amount" required/><span class="amnt_notice" style="display: none"></span><br/>
					</div>
					<div class="frow">
						<label>Please add credits </label>&nbsp;
						<input type="text" name="credits" id="credits" placeholder="" required/><span class="mnth_notice" style="display: none"></span><br/>
					</div>
					<div class="frow">
						<label>Save Dollars </label>&nbsp;
						<input type="text" name="percentage" id="percentage" placeholder="" required/><span class="percentage_notice" style="display: none"></span><br/>
					</div>
				<div class="frow">
						<label>Plan Type </label>&nbsp;
						<select name="plan_type" id="plan_type">
							<option value="">select Plans</option>
							<!--<option value="Most Popular">Most Popular</option>-->
							<option value="Popular">Popular</option>
							<option value="Best Value">Best Value</option>
						</select>
						<span class="plan_type_notice" style="display: none"></span><br/>
				</div>
					<div class="frow">
						<input type="button" name="add" value="add" onclick="addcreditsplans()" class="btn btn-primary"/>  
						<input type="reset" name="clear" value="clear" class="btn btn-primary"/>  
					</div>
				  </form>
            </div>
			<script>
				function addcreditsplans(){
					var amount = $('#amount').val();
					var credits = $('#credits').val();
					var percentage = $('#percentage').val();
					var plan_type = $('#plan_type').val();
					var reg_amt = /^[0-9]{1}/;
					var reg_mnt = /^[0-9]{1}/;
					
					if(!reg_amt.test(amount))
					{
						$('.amnt_notice').html('Please enter valid amount in dollar only');
					    $('.amnt_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#amount").focus();
						return false;
					}
					if(!reg_mnt.test(credits))
					{
						$('.mnth_notice').html('Please enter valid credits in digit only');
					    $('.mnth_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#credits").focus();
						return false;
					}
					/*if(!reg_mnt.test(percentage))
					{
						$('.percentage_notice').html('Please enter valid percentage in digit only');
					    $('.percentage_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#percentage").focus();
						return false;
					} */
					else {
						$.ajax({
							url: '?r=site/addcreditsplans', 
							type: 'POST',
							data: 'amount=' + amount+'&credits='+credits+'&percentage='+percentage+'&plan_type='+plan_type,
							success: function (data) {
								 $("#frm")[0].reset();
								 $("#creditplan").load(window.location + " #creditplan");	
								 
							}
						});
					}
				}
			</script>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="creditplan" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Amount</th>
                  <th>Credits</th>
                  <th>Save Dollars</th>
                  <th>Plans Type</th>
                  <th>Delete</th>
			   </tr>
                </thead>
                <tbody>
    <?php foreach($credits_plans as $credit_plan){ ?>
            <tr>
                <td><?= $credit_plan['amount'];?></td>
                <td><?= $credit_plan['credits'];?></td>
                <td><?= $credit_plan['percentage'];?></td>
                <th><?= $credit_plan['plan_type'];?></th>
                <td id="<?=$credit_plan['_id']?>"><a onclick="remove_credits_plan('<?=$credit_plan['_id']?>')" style="cursor: pointer;">Delete</a></td>
			</tr>

            <?php }?>
                
                </tbody>
               
              </table>
            </div>
			<script>
				function remove_credits_plan(id){
					var r = confirm("Are you sure to delete this Creedit Plan?");
					if (r == false) {
						return false;
					}
					else
					{	
						$.ajax({
								url: '?r=site/removecreditsplans', 
								type: 'POST',
								data: 'id=' + id,
								success: function (data) {
									 
									 $("#"+id).parents('tr').remove();	
									 
								}
							});
					}	
				}
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <!-- <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="javascript:void(0)" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
          </div>
        </div> -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 
