<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
use frontend\models\Notification;
$this->title = 'Flag Collection';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>Flag Collection</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=collection/all">Collections</a></li>
			<li><a href="?r=site/flagcollection">Report</a></li>
		</ol> 
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Collection List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="flag_collections" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>View</th>
				  <th>Name</th>   
                  <th>Privacy</th>
                  <th>Reason</th>
				  <th>Publish</th>   
				  <th>Delete</th>   
                </tr>
                </thead>
                <tbody>
    <?php foreach($collectionlists as $collection){
	$reason = Notification::find()->where(['post_id' => (string)$collection['_id']])->one();
	?>
	
            <tr>
				<td><a href="../frontend/web/index.php?r=collection/detail&col_id=<?= $collection['_id'];?>" target='_blank'>View</a></td>
                <td><?= $collection['name'];?></td>
				<td><?= $collection['privacy'];?></td>
                <td><?= $reason['flag_reason'];?></td>
				<td><a id="<?= $collection['_id'];?>" onclick="publish_collection('<?= $collection['_id'];?>')">Publish</a></td>
				<td><a id="<?= $collection['_id'];?>" onclick="delete_collection('<?= $collection['_id'];?>')">Delete</a></td>
			</tr>

            <?php }?>
                
                </tbody>
				
				<!--<thead>
					<tr>
						<th>Collection Name</th>
						<th>Owner</th>   
						<th>Created Date</th>
						<th>Reported Count</th>
						<th>Flag</th>
						<th>Flag date</th>
						<th>Status</th>
						<th>Status date</th>
						<th>Actions</th>  
					</tr>
                </thead>
                <tbody>
					<tr>
					<td>Test Collection</td>
					<td>Rom Gupta</td>
					<td>05/01/2017</td>
					<td><a href="javascript:void(0)">10</a></td>
					<td>Yes</td>
					<td>07/02/2017</td>
					<td><a href="javascript:void(0)">Active</a></td>
					<td>---</td>
					<td><a href="javascript:void(0)">View</a></td>
					</tr>
					<tr>
					<td>Test1 Collection</td>
					<td>Alap Shah</td>
					<td>01/04/2017</td>
					<td><a href="javascript:void(0)">15</a></td>
					<td>No</td>
					<td>09/02/2017</td>
					<td><a href="javascript:void(0)">Inactive</a></td>
					<td>27/02/2017</td>
					<td><a href="javascript:void(0)">View/Delete</a></td>
					</tr>
				</tbody>-->
					
              </table>
            </div>
			<script>
			function delete_collection(col_id)
			{
				var r = confirm("Are you sure to delete this collection?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
						type: 'POST',
						url: '?r=site/deletecollection',
						data: 'col_id='+col_id,
						success: function(data)
						{
							$("#"+col_id).parents('tr').remove();
						}
					});
				}
			}
			
			function publish_collection(col_id)
			{
				var r = confirm("Are you sure to publish this collection?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
						type: 'POST',
						url: '?r=site/publishcollection',
						data: 'col_id='+col_id,
						success: function(data)
						{
							$("#"+col_id).parents('tr').remove();
						}
					});
				}	
			}
			
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
