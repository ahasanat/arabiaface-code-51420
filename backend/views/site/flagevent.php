<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
use frontend\models\Notification;
$this->title = 'Flag Event';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Flag Event</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Event List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="flag_events" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>View</th>
				  <th>Name</th>   
                  <th>created by</th>
                  <th>Reason</th>
				  <th>Publish</th>   
				  <th>Delete</th>   
                </tr>
                </thead>
                <tbody>
    <?php foreach($eventlists as $event){
	$reason = Notification::find()->where(['post_id' => (string)$event['_id']])->one();
	$user = LoginForm::find()->where(['_id' => $event['created_by']])->one();
	?>
	
            <tr>
				<td><a href="../frontend/web/index.php?r=event/detail&e=<?= $event['_id'];?>" target='_blank'>View</a></td>
                <td><?= $event['event_name'];?></td>
				<td><?= $user['fullname'];?></td>
                <td><?= $reason['flag_reason'];?></td>
				<td><a id="<?= $event['_id'];?>" onclick="publish_event('<?= $event['_id'];?>')">Publish</a></td>
				<td><a id="<?= $event['_id'];?>" onclick="delete_event('<?= $event['_id'];?>')">Delete</a></td>
			</tr>

            <?php }?>
                
                </tbody>
              </table>
            </div>
			<script>
			function delete_event(event_id)
			{
				var r = confirm("Are you sure to delete this event?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
						type: 'POST',
						url: '?r=site/deleteevent',
						data: 'event_id='+event_id,
						success: function(data)
						{
							$("#"+event_id).parents('tr').remove();
						}
					});
				}
			}
			
			function publish_event(event_id)
			{
				var r = confirm("Are you sure to publish this event?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
						type: 'POST',
						url: '?r=site/publishevent',
						data: 'event_id='+event_id,
						success: function(data)
						{
							$("#"+event_id).parents('tr').remove();
						}
					});
				}	
			}
			
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
