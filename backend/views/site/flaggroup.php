<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
use frontend\models\Notification;
$this->title = 'Flag Group';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>Flag Group</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=groups/all">Groups</a></li>
			<li><a href="?r=site/flaggroup">Report</a></li>
		</ol> 
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Group List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="flag_groups" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>View</th>
				  <th>Name</th>   
                  <th>created by</th>
                  <th>Reason</th>
				  <th>Publish</th>   
				  <th>Delete</th>   
                </tr>
                </thead>
                <tbody>
    <?php foreach($grouplists as $group){
	$reason = Notification::find()->where(['post_id' => (string)$group['_id']])->one();
	$user = LoginForm::find()->where(['_id' => $group['user_id']])->one();
	?>
	
            <tr>
				<td><a href="../frontend/web/index.php?r=groups/detail&group_id=<?= $group['_id'];?>" target='_blank'>View</a></td>
                <td><?= $group['name'];?></td>
				<td><?= $user['fullname'];?></td>
                <td><?= $reason['flag_reason'];?></td>
				<td><a id="<?= $group['_id'];?>" onclick="publish_group('<?= $group['_id'];?>')">Publish</a></td>
				<td><a id="<?= $group['_id'];?>" onclick="delete_group('<?= $group['_id'];?>')">Delete</a></td>
			</tr>

            <?php }?>
                
                </tbody>
              </table>
            </div>
			<script>
			function delete_group(group_id)
			{
				var r = confirm("Are you sure to delete this group?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
						type: 'POST',
						url: '?r=site/deletegroup',
						data: 'group_id='+group_id,
						success: function(data)
						{
							$("#"+group_id).parents('tr').remove();
						}
					});
				}
			}
			
			function publish_group(group_id)
			{
				var r = confirm("Are you sure to publish this group?");
				if (r == false) {
					return false;
				}
				else 
				{
					$.ajax({
						type: 'POST',
						url: '?r=site/publishgroup',
						data: 'group_id='+group_id,
						success: function(data)
						{
							$("#"+group_id).parents('tr').remove();
						}
					});
				}	
			}
			
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
