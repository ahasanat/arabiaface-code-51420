<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
$this->title = 'Payment History';
$user = LoginForm::find()->where(['_id'=> $_GET['user_id']])->one();
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Payment History</h1>
    </section>

    <!-- Main content -->
    <section class="content">
		
		
		<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
			<h3 class="box-title"><?=$user['fullname']?>'s Payment History</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Transaction Id</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Currency</th>
                  <th>Detail</th>
                  <th>Order Type</th>
                </tr>
                </thead>
                <tbody>
    <?php foreach($userdatas as $order){
			?>
            <tr>
                <td><?= $order['transaction_id'];?></td>
                <td><?= $order['amount'];?></td>
                <td><?= $order['status'];?></td>
                <td><?= $order['curancy'];?></td>
                <td><?= $order['detail'];?></td>
                <td><?php 
					if($order['order_type']== 'joinvip')
					{ 
						echo 'Become a Vip Member';
					}
					else if($order['order_type']== 'buycredits')
					{ 
						echo 'Purchase Credits';
					}
					else if($order['order_type']== 'verify')
					{ 
						echo 'Become a Verify Member';
					}
					else
					{ 
						echo '---';
					}
					
					?></td>
                
                </tr>

            <?php }?>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>