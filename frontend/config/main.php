<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => '34.217.21.181',
                'port' => 6379,
                'database' => 0,
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'request' => [
            'enableCsrfValidation'=>false,
        ],
        'EphocTime' => [
 
            'class' => 'common\components\EphocTime',
 
            ],
        'GenCls' => [
 
            'class' => 'common\components\GenCls',
 
            ],
         'CDbCriteria' => [
 
            'class' => 'common\components\CDbCriteria',
 
            ],
       'assetManager' => [
       // 'linkAssets' => true,
	    'linkAssets' => false,
        ], 
        // Fcaebook App Details
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
             'facebook' => [
                'class' => 'yii\authclient\clients\Facebook',
                'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',

                // BH Facebook Attached...
                'clientId' => '1405757779596660',
                'clientSecret' => 'b9e0a2b090317ed2d20eb5af7521d0bf',
		 
              ],'google' => [
                'class' => 'yii\authclient\clients\GoogleOAuth',
                'returnUrl' => 'http://localhost/arabiaface-code/frontend/web/index.php?r=site/abc',
                'clientId' => '215771007590-u2qkpstdnlupna88ehp2sli3u0q82nvt.apps.googleusercontent.com',
                'clientSecret' => 'YCnTDmoPMgwgZcXj9glahSMg',
            ],
            ],
         ],
         'i18n' => [
            'translations' => [
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ],
        ],
        //google+ login settings start
         'eauth' => [
            'class' => 'nodge\eauth\EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],
            'services' => [ // You can change the providers and their classes.
                'google' => [
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'Google',
                ],
                        //google+ login settings start
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
         'session' => [
            'class' => 'yii\mongodb\Session',
            'timeout' => 3600 * 72,
            'name' => 'PHPFRONTSESSID',
            'savePath' => __DIR__ . '/../tmp',
        ],
    ],   
    'params' => $params,
];
