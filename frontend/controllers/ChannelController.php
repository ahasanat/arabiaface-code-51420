<?php
namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\mongodb\ActiveRecord;
use frontend\models\UserForm;
use frontend\models\Channel;
use frontend\models\ChannelSubscriber;
use frontend\models\PostForm;
use frontend\assets\AppAsset;

class ChannelController extends Controller
{
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
        
    /**
    * Create auth action
    *
    * @return mixed
    */
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
                'captcha' => [
                'class' => 'yii\captcha\CaptchaAction', 
            ],
        ];           
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionSubscribeUpdate() { 
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $data = 'checkuserauthclassg';
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                if(isset($_POST['$channel_id']) && $_POST['$channel_id'] != '') {
                    $channel_id = $_POST['$channel_id']; 
                    $data = ChannelSubscriber::subscribeUpdate($user_id, $channel_id);              
                }
            } else {
                $data = $checkuserauthclass;
            }
        }
        return $data;
    }

    public function actionSubscribedList() {
        if(isset($_POST['$lazyhelpcount'])) {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');

            $data = array();
            $checkuserauthclass = 'checkuserauthclassg';

            $lazyhelpcount = $_POST['$lazyhelpcount'];
            $start = $lazyhelpcount * 12;
            if(isset($user_id) && $user_id != '') {
                $checkuserauthclass = UserForm::isUserExistByUid($user_id);
                if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                    $data = ChannelSubscriber::subscribedList($start, $user_id);
                }
            }

            return $this->render('subscribed',['data' =>$data, 'start' => $start, 'checkuserauthclass' => $checkuserauthclass]);
        }

        $url = Yii::$app->urlManager->createUrl(['channel']);
        Yii::$app->getResponse()->redirect($url);
    }

    public function actionSuggestedList() {
        if(isset($_POST['$lazyhelpcount'])) {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');

            if(isset($user_id) && $user_id != '') {
            	$checkuserauthclass = UserForm::isUserExistByUid($user_id);
            } else {
            	$checkuserauthclass = 'checkuserauthclassg';
            }

            $lazyhelpcount = $_POST['$lazyhelpcount'];
            $start = $lazyhelpcount * 12;
            $data = Channel::suggestedDisplay($start);
            $data = json_decode($data, true);
            return $this->render('suggested',['data' => $data, 'start' => $start, 'checkuserauthclass' => $checkuserauthclass]);
        } 

        $url = Yii::$app->urlManager->createUrl(['channel']);
        Yii::$app->getResponse()->redirect($url);
    }

    public function actionPopularList() {
        if(isset($_POST['$lazyhelpcount'])) {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');

            if(isset($user_id) && $user_id != '') {
            	$checkuserauthclass = UserForm::isUserExistByUid($user_id);
            } else {
            	$checkuserauthclass = 'checkuserauthclassg';
            }
            
            $lazyhelpcount = $_POST['$lazyhelpcount'];
            $start = $lazyhelpcount * 12;
            $data = Channel::getPopular($start);
            $data = json_decode($data, true);            
            return $this->render('popular',['data' =>$data, 'start' => $start, 'checkuserauthclass' => $checkuserauthclass]);
        } 

        $url = Yii::$app->urlManager->createUrl(['channel']);
        Yii::$app->getResponse()->redirect($url);
    }

    public function actionDetail() 
    {
        if (isset($_GET['col_id']) && $_GET['col_id'] != '') {
            if (isset($_GET['rcdno']) && $_GET['rcdno'] != '') {
        		$session = Yii::$app->session;
            	$user_id = (string)$session->get('user_id');
            
    	        if(isset($user_id) && $user_id != '') {
    	        	$checkuserauthclass = UserForm::isUserExistByUid($user_id);
    	        } else {
    	        	$checkuserauthclass = 'checkuserauthclassg';
    	        }

            	$channel_id = $_GET['col_id'];
                $count = $_GET['rcdno'];
                $channeldetail = Channel::channeldetail($channel_id);
                if(!empty($channeldetail)) {
                    $channeldetail['count'] = $count;
                    if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                        $posts =  PostForm::getChannelPosts($channel_id);
                    } else {
                        $posts =  PostForm::getChannelPosts($channel_id);
                    }

                    $col_follow_user = ChannelSubscriber::getchannelsubscriber($channel_id);
                    return $this->render('detail',array('posts' => $posts, 'channeldetail'=>$channeldetail, 'follow_user'=>$col_follow_user, 'checkuserauthclass' => $checkuserauthclass));
                }
            } 
        }

        $url = Yii::$app->urlManager->createUrl(['channel']);
        Yii::$app->getResponse()->redirect($url);
    }
}
?>