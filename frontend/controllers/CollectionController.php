<?php
namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter; 
use yii\filters\AccessControl;
use yii\helpers\Url;

use yii\mongodb\ActiveRecord;
use frontend\components\ExSession;
use frontend\models\Collection;
use frontend\models\CollectionFollow;
use frontend\models\CollectionReport;
use frontend\models\PostForm;
use frontend\models\UserForm;
use frontend\models\Friend;
use frontend\models\Credits;
use frontend\models\Notification;

class CollectionController
        extends Controller {

    public function behaviors() 
	{
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
	
	public function beforeAction($action)
	{
        $this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

    public function actions() 
	{
        return [ 
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this,
                    'oAuthSuccess'],
            ],
            'captcha' => [
                //'class' => 'yii\captcha\CaptchaAction',
                'class' => 'mdm\captcha\CaptchaAction',
                'level' => 1,
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function actionIndex() 
	{
		$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
        	$checkuserauthclass = UserForm::isUserExistByUid($user_id);
        } else {
        	$checkuserauthclass = 'checkuserauthclassg';
        }

		return $this->render('collection', array('checkuserauthclass' => $checkuserauthclass));
    }
	
	public function actionAll()
	{
		if(isset($_POST['$lazyhelpcount'])) {
			$session = Yii::$app->session;
	        $user_id = (string)$session->get('user_id');
	        
	        if(isset($user_id) && $user_id != '') {
	        	$checkuserauthclass = UserForm::isUserExistByUid($user_id);
	        } else {
	        	$checkuserauthclass = 'checkuserauthclassg';
	        }

	        $lazyhelpcount = $_POST['$lazyhelpcount'];
            $start = $lazyhelpcount * 12;
			$allcollection = Collection::allcollection($start);
			$result = json_decode($allcollection, true);
			return $this->render('all_collection',array('allcollection'=>$result, 'start' => $start,'checkuserauthclass' => $checkuserauthclass));
		}

		$url = Yii::$app->urlManager->createUrl(['collection']);
        Yii::$app->getResponse()->redirect($url);
	}
	
	
	/*Add Collection Function*/
	
	public function actionAdd() 
	{ 
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $time = time() . rand(9999,  9999999);
        
        if(isset($user_id) && $user_id != '') {
        	$authstatus = UserForm::isUserExistByUid($user_id);
        	$checkuserauthclass = $authstatus;
        } else {
        	$checkuserauthclass = 'checkuserauthclassg';
        }
		
		if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { 
			$date = time(); 
			$privacy = $_POST['visible'];
			$collection = new Collection();
			$collection->user_id = $user_id;
            $collection->name = $_POST['title']; 
            $collection->tagline = $_POST['tagline'];
			$collection->color = $_POST['color'];
			$collection->privacy = $privacy;
			
			if($privacy == 'Custom') {
				if(isset($_POST['custom']) && !empty($_POST['custom'])) {
					$custom = $_POST['custom'];
					$custom = implode(',', $custom);
					$collection->customids = $custom;
				}
			} else {
				$collection->customids = '';
			}
			
			if(isset($_POST['images']) && (!empty($_POST['images'])))
			{
				$rawImageString = $_POST['images'];
				$filterString = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $rawImageString));
				$imageName = $time.'.png';

				$filepath = "uploads/collection/";
				if(file_put_contents($filepath.$imageName, $filterString)) {
					$collection->collection_photo = 'ORI_'.$imageName;
					$collection->collection_thumb = $imageName;
				}

			}	
            $collection->created_date = "$date";
			$collection->modified_date = "$date";
			$collection->is_deleted = "1";
			$collection->entity_collection= "1";
            $collection->insert();
            $last_insert_id = $collection->_id;
            if($last_insert_id)
            {	
            	// default follow ower
            	$collectionfollow = new CollectionFollow();
				$collectionfollow->user_id = $user_id;
				$collectionfollow->collection_id = (string)$last_insert_id;
				$collectionfollow->is_deleted = "1";
				$collectionfollow->created_date = "$date";
				$collectionfollow->modified_date = "$date";
				$collectionfollow->insert();

                $data['msg'] = 'success';
                $data['collection_id'] = "$last_insert_id";
				
				$cre_amt = 5;
                $cre_desc = 'addcollection';
                $status = '1';
                $details = $last_insert_id;
                $credit = new Credits();
                $credit = $credit->addcredits($user_id,$cre_amt,$cre_desc,$status,"$details");
				
            }
            else
            {
                $data['msg'] = 'fail';
            }
    	} else {
    		$data['auth'] = $checkuserauthclass;
    	}
        return json_encode($data);
	}
	
	
	public function actionUpdate() 
	{
		$session = Yii::$app->session;
        $userid = (string)$session->get('user_id');
		$time = time() . rand(9999,  9999999);
		if($session->get('user_id'))
        {
            $date = time(); 
			$collection = new Collection();
			$privacy = $_POST['visible'];

			$col_id = $_POST['col_id'];
			$update = Collection::find()->where(['_id' => "$col_id"])->one();
            
			
            $update->user_id = $userid;
            $update->name = $_POST['title']; 
            $update->tagline = $_POST['tagline'];
			$update->color = $_POST['color']; 
			$update->privacy = $privacy;
			if($privacy == 'Custom') {
				if(isset($_POST['custom']) && !empty($_POST['custom'])) {
					$custom = $_POST['custom'];
					$custom = implode(',', $custom);
					$update->customids = $custom;
				}
			} else {
				$update->customids = '';
			}
			if(isset($_POST['images']) && $_POST['images'] != '' &&  $_POST['images'] != 'undefined')
			{
				$rawImageString = $_POST['images'];
				$filterString = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $rawImageString));
				$imageName = $time.'.png';

				$filepath = "uploads/collection/";
				if(file_put_contents($filepath.$imageName, $filterString)) {
					if(isset($update['collection_thumb'])) {
						if(file_exists('uploads/collection/'.$update['collection_thumb'])) {
							unlink('uploads/collection/'.$update['collection_thumb']);
						}
					}

					$update->collection_photo = $imageName;
					$update->collection_thumb = $imageName;
				}
			}
            $update->modified_date = "$date";
			$update->is_deleted = "1";
            $update->update();
			
				$collection_flager_id = $update['collection_flager_id'];
				
			/* Insert Notification For The Owner of Post For Flagging*/
				$notification = new Notification();
				$notification->post_id = "$col_id";
				$notification->user_id = "$collection_flager_id";
				$notification->collection_owner_id = "$userid";
				$notification->notification_type = 'editcollectionuser';
				$notification->is_deleted = '0';
				$notification->status = '1';
				$notification->created_date = "$date";
				$notification->updated_date = "$date";
				$notification->insert();
			
			
			
        }
        else
        {
            return $this->goHome();
        }
		
	}
	
	public function actionDelete(){
		$session = Yii::$app->session;
        $userid = (string)$session->get('user_id');
		if($session->get('user_id'))
        {
			$col_id = $_POST['col_id'];
			$mt_exist = Collection::find()->where(['_id' => "$col_id" ])->one();
			if(!empty($mt_exist)) {
				if($mt_exist->delete())
				{
					CollectionFollow::deleteAll(['collection_id'=>$col_id]);
					return true;
				}
			}
			return false;
		} else {
			 return $this->goHome();
		}	
	}
	
	public function actionFlag()
	{
		$session = Yii::$app->session;
        $userid = (string)$session->get('user_id');
        $collection_id = $_POST['c_id'];
        $collection_owner_id = $_POST['c_uid'];
        $desc = $_POST['desc'];
		
		$date = time(); 
		$update = Collection::find()->where(['_id' => "$collection_id"])->one();
		// $update->is_deleted = "2";
		//$update->modified_date = "$date";
		$update->flagger = "1";
		$update->collection_flager_id = "$userid";
		$update->flagger_date = "$date";
		if($update->update())
		{
			CollectionFollow::updateAll(['is_deleted' => '2'], ['collection_id' => $collection_id]);
			/* Insert Notification For The Owner of Collection For Flagging*/
			$notification =  new Notification();
			$notification->post_id = "$collection_id";
			$notification->user_id = "$collection_owner_id";
			$notification->notification_type = 'deletecollectionadmin';
			$notification->is_deleted = '0';
			$notification->status = '0';
			$notification->created_date = "$date";
			$notification->updated_date = "$date";
			$notification->flag_reason = "$desc";
			$notification->insert();
			
			return "1";
		}
	
	}
	
	public function actionPublishCollection() 
	{
		$c_id = $_POST['c_id'];
		$date = time();
		$record = Collection::find()->where(['_id' => $c_id,'is_deleted' => '2'])->one();
		$record->is_deleted = '1';
		$record->update();
		
		
		/* Insert Notification For The Owner of Post For Publishing his/her post*/
			
		$collection_owner_id = $record['user_id'];
	
		$notification =  new Notification();
		$notification->post_id = "$c_id";
		$notification->user_id = "$collection_owner_id";
		$notification->notification_type = 'publishcollection';
		$notification->is_deleted = '0';
		$notification->status = '1';
		$notification->created_date = "$date";
		$notification->updated_date = "$date";
		$notification->insert();
		return true;
	}
	
	/*Your Collection Funcion*/
	
	public function actionYours()
	{
		if(isset($_POST['$lazyhelpcount'])) {
			$session = Yii::$app->session;
	        $user_id = (string)$session->get('user_id');
	        
	        if(isset($user_id) && $user_id != '') {
	        	$authstatus = UserForm::isUserExistByUid($user_id);
	        	$checkuserauthclass = $authstatus;
	        } else {
	        	$checkuserauthclass = 'checkuserauthclassg';
	        }

	        $lazyhelpcount = $_POST['$lazyhelpcount'];
	        if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') {
	        	$result = array();
            	$start = 0;
	        } else {
	        	$start = $lazyhelpcount * 12;
	        	$yourscollection = Collection::yourscollection($user_id, $start);
	        	$result = json_decode($yourscollection, true);	
	        }

			return $this->render('yours_collection',array('yourscollection'=>$result, 'start' => $start, 'checkuserauthclass' => $checkuserauthclass));
		}

		$url = Yii::$app->urlManager->createUrl(['collection']);
        Yii::$app->getResponse()->redirect($url);

	}
	
	
	/*Popular Collection Function*/
	
	public function actionPopular()
	{
		$session = Yii::$app->session;
        $userid = (string)$session->get('user_id');
		if($session->get('user_id'))
		{
			$popularcollection = Collection::popularcollection();
			return $this->render('popular_collection',array('popularcollection'=>$popularcollection));
		}	
	}
	
	
	/*Following Collection Function*/
	
	public function actionFollowing()
	{
		$start = 0;
		if(isset($_POST['$lazyhelpcount'])) {
			$session = Yii::$app->session;
	        $userid = (string)$session->get('user_id');
			if($session->get('user_id'))
			{
				$lazyhelpcount = $_POST['$lazyhelpcount'];
            	$start = $lazyhelpcount * 12;
				$followingcollection = Collection::followingcollection($userid, $start);
				$result = json_decode($followingcollection, true);	
			} else {
				$result = array();
			}	
			
			return $this->render('following_collection', array('followingcollection'=>$result, 'start' => $start));
		}

		$url = Yii::$app->urlManager->createUrl(['collection']);
        Yii::$app->getResponse()->redirect($url);

	}
	
	Public function actionFollow()    
	{
		$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
        	$authstatus = UserForm::isUserExistByUid($user_id);
        	if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') {
        		$datas['auth'] = $authstatus;
        	} else {
	        	$collectionfollow = new CollectionFollow();
				$col_id = $_POST['col_id'];
				$date = time();
		
				$usercolexist = CollectionFollow::find()->where(['user_id' => $user_id,'collection_id' => $col_id])->one();
				if ($usercolexist)
				{

					// check user is owner or not..........
					$isOwner =  Collection::find()->Where([(string)'_id' => $col_id, 'user_id' => $user_id])->one();
		            if(!empty($isOwner)) {
		                $datas['msg'] = '7';
						return json_encode($datas);
		            }

					if($usercolexist['is_deleted']=='0'){
						$usercolexist->is_deleted = "1";
						$usercolexist->modified_date = "$date";
						if ($usercolexist->update())
						{
							$datas['msg'] = '1';
						}
						else
						{
							$datas['msg'] = '0';
						}	
					}else{
						if ($usercolexist->delete())
						{
							$datas['msg'] = '2';
						}
						else
						{
							$datas['msg'] = '0';
						}
					}
					
				} else {
					$collectionfollow->user_id = $user_id;
					$collectionfollow->collection_id = $col_id;
					$collectionfollow->is_deleted = "1";
					$collectionfollow->created_date = "$date";
					$collectionfollow->modified_date = "$date";
					if ($collectionfollow->insert())
					{
						/* Insert Notification For Follow Collection */
						
							$notification =  new Notification();
							$notification->collection_id =   "$col_id";
							$notification->user_id = "$user_id";
							$notification->notification_type = 'followcollection';
							$notification->is_deleted = '0';
							$notification->status = '1';
							$notification->created_date = "$date";
							$notification->updated_date = "$date";
							$collection_details = Collection::find()->where(['_id' => $col_id,'is_deleted' => '1'])->one();
							$notification->collection_owner_id = $collection_details['user_id'];
							$notification->insert();
						
						/* Insert Notification For Follow Collection */
						
						$datas['msg'] = '1';
					}
					else
					{
						$datas['msg'] = '0'; 
					}
				}

				$f_count = CollectionFollow::find()->where(['is_deleted' => "1",'collection_id' => $col_id])->count();
				$datas['f_count'] = $f_count;
				$follower_names = CollectionFollow::getFollowerUserNames((string)$col_id);
				$datas['follower_names'] = $follower_names;
			}
		} else {
			$datas['auth'] = 'checkuserauthclassg';
		}

		return json_encode($datas);		
	}
	
	/*Collection Detail page function*/
	
	public function actionDetail() 
	{
		if (isset($_GET['col_id']) && $_GET['col_id'] != '') {
			$session = Yii::$app->session;
        	$user_id = (string)$session->get('user_id');
        
	        if(isset($user_id) && $user_id != '') {
	        	$checkuserauthclass = UserForm::isUserExistByUid($user_id);
	        } else {
	        	$checkuserauthclass = 'checkuserauthclassg';
	        } 
		

			$collection_id = $_GET['col_id'];

			$collectiondetail = Collection::collectiondetail($collection_id);

			$col_created_by =$collectiondetail['user_id'];
			$col_privacy = $collectiondetail['privacy'];


	        if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
	        	$posts =  PostForm::getCollectionPosts($collection_id);
			} else {
				$posts =  PostForm::getCollectionPosts($collection_id);
			} 

			if(empty($collectiondetail))
            {
				$this->redirect(Url::toRoute(['collection/index']));
            } else if($col_created_by == $user_id) {
            	$col_follow_user = CollectionFollow::getcollectionfollowuser($collection_id);
					return $this->render('detail',array('posts' => $posts,'collectiondetail'=>$collectiondetail,'follow_user'=>$col_follow_user, 'checkuserauthclass' => $checkuserauthclass));
            } else if(trim($col_privacy) == 'Public') {
            		$col_follow_user = CollectionFollow::getcollectionfollowuser($collection_id);
					return $this->render('detail',array('posts' => $posts,'collectiondetail'=>$collectiondetail,'follow_user'=>$col_follow_user, 'checkuserauthclass' => $checkuserauthclass));
            } else if(trim($col_privacy) == 'Friends') {
            	if($user_id != $col_created_by) {
	            	$is_friend = Friend::find()->where(['from_id' => "$user_id",'to_id' => "$col_created_by",'status' => '1'])->one();
	            	if(!empty($is_friend)) {
	            		$col_follow_user = CollectionFollow::getcollectionfollowuser($collection_id);
						return $this->render('detail',array('posts' => $posts,'collectiondetail'=>$collectiondetail,'follow_user'=>$col_follow_user, 'checkuserauthclass' => $checkuserauthclass));
	            	} else {
	            		$this->redirect(Url::toRoute(['collection/index']));
	            	}
            	} else {
            		$col_follow_user = CollectionFollow::getcollectionfollowuser($collection_id);
					return $this->render('detail',array('posts' => $posts,'collectiondetail'=>$collectiondetail,'follow_user'=>$col_follow_user, 'checkuserauthclass' => $checkuserauthclass));
            	} 
            } else if(trim($col_privacy) == 'Private') {
				if($user_id == $col_created_by) {
					$col_follow_user = CollectionFollow::getcollectionfollowuser($collection_id);
					return $this->render('detail',array('posts' => $posts,'collectiondetail'=>$collectiondetail,'follow_user'=>$col_follow_user, 'checkuserauthclass' => $checkuserauthclass));
				} else {
            		$this->redirect(Url::toRoute(['collection/index']));
				}
			} else if(trim($col_privacy) == 'Custom') {
				$customids = array();
			    $customids = isset($collectiondetail['customids']) ? $collectiondetail['customids'] : '';
			    $customids = explode(',', $customids);

			    if(in_array($user_id, $customids)) {
					$col_follow_user = CollectionFollow::getcollectionfollowuser($collection_id);
					return $this->render('detail',array('posts' => $posts,'collectiondetail'=>$collectiondetail,'follow_user'=>$col_follow_user, 'checkuserauthclass' => $checkuserauthclass));
			    } else {
            		$this->redirect(Url::toRoute(['collection/index']));
				}
			} else {
            	$this->redirect(Url::toRoute(['collection/index']));
			}
		}
	}
	
	public function actionReportcollection() 
	{
		$session = Yii::$app->session;
        $user_id = (string) $session->get('user_id');

        if(isset($user_id) && $user_id != '') {
	        if (isset($_POST['pid']) && !empty($_POST['pid'])) 
			{
	            $date = time();
				$getdetails = CollectionReport::find()->where(['collection_id' => $_POST['pid'],'reporter_id' => $user_id])->one();
	            if (!$getdetails) 
				{
	                $report_post = new CollectionReport();
	                $report_post->collection_id = $_POST['pid'];
	                $report_post->reporter_id = $user_id;
	                $report_post->reason = $_POST['desc'];
	                $report_post->created_date = $date;
	                if ($report_post->insert()) 
					{
	                    print true;
	                } 
					else 
					{
	                    print false;
	                }
	            } 
				else 
				{
	                print false;
	            }
	        } 
			else 
			{
	            return $this->goHome();
	        }
	    }
    }
	
	public function actionCollectionImageCrop()
	{
		$imgpaths = '../web/uploads/collection';
			if (!file_exists($imgpaths))
			{
				mkdir($imgpaths, 0777, true);
		}
		$dt = time();

		$imgUrl = $_POST['imgUrl'];
		// original sizes
		$imgInitW = $_POST['imgInitW'];
		$imgInitH = $_POST['imgInitH'];
		// resized sizes
		$imgW = $_POST['imgW'];
		$imgH = $_POST['imgH'];
		// offsets
		$imgY1 = $_POST['imgY1'];
		$imgX1 = $_POST['imgX1'];
		// crop box
		$cropW = $_POST['cropW'];
		$cropH = $_POST['cropH'];
		// rotation angle
		$angle = $_POST['rotation'];

		$jpeg_quality = 100;


		$output_filename = "uploads/collection/".$dt;

		$what = getimagesize($imgUrl);

		switch(strtolower($what['mime']))
		{
			case 'image/png':
				$img_r = imagecreatefrompng($imgUrl);
				$source_image = imagecreatefrompng($imgUrl);
				$type = '.png';
				break;
			case 'image/jpeg':
				$img_r = imagecreatefromjpeg($imgUrl);
				$source_image = imagecreatefromjpeg($imgUrl);
				error_log("jpg");
				$type = '.jpeg';
				break;
			case 'image/gif':
				$img_r = imagecreatefromgif($imgUrl);
				$source_image = imagecreatefromgif($imgUrl);
				$type = '.gif';
				break;
			default: die('image type not supported');
		}



		// resize the original image to size of editor
		$resizedImage = imagecreatetruecolor($imgW, $imgH);
		imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
		// rotate the rezized image
		$rotated_image = imagerotate($resizedImage, -$angle, 0);
		// find new width & height of rotated image
		$rotated_width = imagesx($rotated_image);
		$rotated_height = imagesy($rotated_image);
		// diff between rotated & original sizes
		$dx = $rotated_width - $imgW;
		$dy = $rotated_height - $imgH;
		// crop rotated image to fit into original rezized rectangle
		$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
		imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
		imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
		// crop image into selected area
		$final_image = imagecreatetruecolor($cropW, $cropH);
		imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
		imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
		// finally output png image
		//imagepng($final_image, $output_filename.$type, $png_quality);
		imagejpeg($final_image, $output_filename.$type, $jpeg_quality);

		// Store For Original Image..
		$imageDataEncoded = base64_encode(file_get_contents($imgUrl));
		$imageData = base64_decode($imageDataEncoded);
		$source = imagecreatefromstring($imageData);
		$imageSave = imagejpeg($source,'uploads/collection/ORI_'.$dt.$type,100);
		imagedestroy($source);

		$response = Array( 
			"status" => 'success',
			"url" => $output_filename.$type
		);
		return json_encode($response);
	}

	public function actionCreateCollectionPrepare() {
        
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
        } else {
            $checkuserauthclass = 'checkuserauthclassg';
        } 
        
        return $this->render('create_collection', array('checkuserauthclass' => $checkuserauthclass));
    }

    public function actionEditCollectionPrepare() {
    	if(isset($_POST['$id']) && $_POST['$id'] != '') {
	        $session = Yii::$app->session;
	        $user_id = (string)$session->get('user_id');
	       	$id = $_POST['$id']; 
	        if(isset($user_id) && $user_id != '') {
	            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
	        } else {
	            $checkuserauthclass = 'checkuserauthclassg';
	        }

	        return $this->render('edit_collection', array('checkuserauthclass' => $checkuserauthclass, 'collection_id' => $id));
	    }
    }
}	