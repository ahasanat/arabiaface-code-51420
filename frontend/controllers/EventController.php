<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\mongodb\ActiveRecord;
use frontend\models\PageEvents;
use frontend\models\EventVisitors;
use frontend\models\EventPhoto;
use frontend\models\Friend;
use frontend\models\Notification;
use frontend\models\UserForm;
use frontend\models\LoginForm;
use frontend\models\PostForm;

class EventController extends Controller
{
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
        
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
                'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];           
    }
    
    public function actionIndex() 
    { 
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
        } else {
            $checkuserauthclass = 'checkuserauthclassg';
        }

        return $this->render('event', array('checkuserauthclass' => $checkuserauthclass));
    }
	
	public function actionAddNewEvent() 
    {
        $session = Yii::$app->session;
        $user_id = $session->get('user_id');
        $email = $session->get('email');
        if ($session->get('email'))
        {
			$events = $model->getEvents();	
            return $this->render('event',array('events'=>$events));
			
        }
        else
        {
            return $this->goHome();
        }
    }
    
    public function actionAll()  
    {   
        $search = $_POST['search'];
        $start = 0;
        if(isset($search) && !empty($search) && $search=='evall')
        {
            $lazyhelpcount = isset($_POST['$lazyhelpcount']) ? $_POST['$lazyhelpcount'] : 0;
            $start = $lazyhelpcount * 12;
            if(isset($_POST['location']) && trim($_POST['location']) != '') {
                $location = trim(urldecode($_POST['location']));
                $allevents = PageEvents::getAllEventswithlocation($location);
            } else {
                $allevents = PageEvents::getAllEvents($start);
            }
        }
        else if(isset($search) && !empty($search) && $search=='evup')
        {
            $date = strtotime(date('m/d/Y'));
            $allevents = PageEvents::find()->where(['is_deleted' => '1'])->andwhere(['event_date_ts'=> ['$gte'=>"$date"]])->orderBy(['event_date'=>SORT_ASC])->all();
        }
        else if(isset($search) && !empty($search) && $search=='evpo')
        {
			$ev = Yii::$app->mongodb->getCollection('event_visits');
			$allevents = $ev->aggregate(
				array( '$group' => array(
					'_id' => '$event_id',
					'count' => array('$sum' => 1)
				)) 
			);
			usort($allevents, function($a, $b) {
				return $b['count'] - $a['count'];
			});
        }
        else if(isset($search) && !empty($search) && $search=='evex')
        {
            $date = strtotime(date('m/d/Y'));
            $allevents = PageEvents::find()->where(['is_deleted' => '1'])->andwhere(['event_date_ts'=> ['$lt'=>"$date"]])->orderBy(['event_date'=>SORT_ASC])->all();
        }
        else if(isset($search) && !empty($search) && $search!='undefined')
        {
            $allevents = PageEvents::find()->where(['is_deleted' => '1'])->andwhere(['like','event_name',$search])->orderBy(['event_date'=>SORT_ASC])->all();
        }
        else
        {
            $allevents = PageEvents::getAllEvents();
        }

        return $this->render('all_event',array('allevents'=>$allevents, 'start' => $start));
        
    }
    
    public function actionGoing()
    {
        $session = Yii::$app->session;
        $user_id = $session->get('user_id');

        $goingevents = array();
        if(isset($user_id) && $user_id != '') {
            $authstatus = UserForm::isUserExistByUid($user_id);
            $checkuserauthclass = $authstatus;
            if($checkuserauthclass != 'checkuserauthclassnv' && $checkuserauthclass != 'checkuserauthclassg') {
                $lazyhelpcount = $_POST['$lazyhelpcount'];
                $start = $lazyhelpcount * 12;
                $goingevents = EventVisitors::getMyGoingEvents($user_id, $start);
                return $this->render('going_event',array('goingevents'=>$goingevents, 'start' => $start));
            }
        }
        else
        {
            return $this->render('going_event',array('goingevents'=>$goingevents, 'start' => ''));
        }
    }
    
    public function actionYours()
    {
        $session = Yii::$app->session;
        $user_id = $session->get('user_id');

        $myevents = array();
        if(isset($user_id) && $user_id != '') {
            $authstatus = UserForm::isUserExistByUid($user_id);
            $checkuserauthclass = $authstatus;
            if($checkuserauthclass != 'checkuserauthclassnv' && $checkuserauthclass != 'checkuserauthclassg') {
                $lazyhelpcount = $_POST['$lazyhelpcount'];
                $start = $lazyhelpcount * 12;
                $myevents = PageEvents::getMyEvents($user_id, $start);
                return $this->render('my_event',array('myevents'=>$myevents, 'checkuserauthclass' => $checkuserauthclass, 'start' => $start));
            }
        }
        else
        {
            return $this->render('my_event',array('myevents'=>$myevents, 'checkuserauthclass' => 'checkuserauthclassg', 'start' => ''));
        }
    }
    
    public function actionCreateevent()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $time = time() . rand(9999,  9999999);    
        if($session->get('user_id'))
        {
            $data = array();
            $date = time();
            $datein = date('Y-m-d h:i:s');
            $event_name = $_POST['en'];
            $event_tagline = $_POST['tl'];
            $short_desc = $_POST['sd'];
            $event_date = str_replace('-','/',$_POST['ed']);
            $event_time = $_POST['et'];
            $event_privacy = $_POST['ep'];
            $event_address = $_POST['ad'];
            $event_wu = $_POST['wu'];
            $event_tu = $_POST['tu'];
            $event_yu = $_POST['yu'];
            $event_tpiu = $_POST['tpiu'];
            $isasktojoin = $_POST['isasktojoin'];


            $_SERVER['HTTP_REFERER'];
            if(strstr($_SERVER['HTTP_REFERER'],'r=event') && !strstr($_SERVER['HTTP_REFERER'],'e='))
            {
                $pageevent = new PageEvents();
            }
            else
            {
                $url = $_SERVER['HTTP_REFERER'];
                $urls = explode('&',$url);
                $url = explode('=',$urls[1]); 
                $entity_id = $url[1];
                $pageevent = new PageEvents();
                $pageevent->parent_id = "$entity_id";
                if(strstr($_SERVER['HTTP_REFERER'],'r=page/index') || strstr($_SERVER['HTTP_REFERER'],'r=page%2Findex'))
                {
                    $pageevent->type = 'page';
                }
                else if(strstr($_SERVER['HTTP_REFERER'],'r=groups/detail') || strstr($_SERVER['HTTP_REFERER'],'r=groups%2Fdetail'))
                {
                    $pageevent->type = 'group';
                }
                else
                {
                    $pageevent = PageEvents::find()->where(['_id' => "$entity_id",'is_deleted' => '1'])->one();
                }
            }

            $pageevent->event_name = ucfirst($event_name);
            $pageevent->tagline = ucfirst($event_tagline);
            $pageevent->address = $event_address;
            $pageevent->short_desc = ucfirst($short_desc);
            $pageevent->event_date = "$event_date";
            //$pageevent->event_date = "";
            $event_date_ts = strtotime($event_date);
            $pageevent->event_date_ts = "$event_date_ts";
            $pageevent->event_time = "$event_time";
            //$pageevent->event_time = "";
            $pageevent->event_privacy = "$event_privacy";

            if($event_privacy == 'Custom') {
                if(isset($_POST['custom']) && !empty($_POST['custom'])) {
                    $customids = $_POST['custom'];
                    $pageevent->customids = $customids; 
                }
            } else {
                $pageevent->customids = '';
            }

            $pageevent->isasktojoin = "$isasktojoin";
            if((strstr($_SERVER['HTTP_REFERER'],'r=event') && !strstr($_SERVER['HTTP_REFERER'],'e=')) || strstr($_SERVER['HTTP_REFERER'],'r=page/index') || strstr($_SERVER['HTTP_REFERER'],'r=page%2Findex') || strstr($_SERVER['HTTP_REFERER'],'r=groups/detail') || strstr($_SERVER['HTTP_REFERER'],'r=groups%2Fdetail'))
            {
                $pageevent->created_date = "$date";
                $pageevent->created_in_date = "$datein";
                $pageevent->created_by = $user_id;
                $pageevent->created_from_ip = $_SERVER['REMOTE_ADDR'];
            }
            $pageevent->updated_date = "$date";
            $pageevent->updated_in_date = "$datein";
            $pageevent->updated_by = $user_id;
            $pageevent->updated_from_ip = $_SERVER['REMOTE_ADDR'];
            $pageevent->wu = "$event_wu";
            $pageevent->tu = "$event_tu";
            $pageevent->yu = "$event_yu";
            $pageevent->tpiu = "$event_tpiu";
            $pageevent->is_deleted = '1';
			
			if(isset($_POST['images']) && $_POST['images'] != '' &&  $_POST['images'] != 'undefined')
			{
                $image_parts = explode(";base64,", $_POST['images']);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];

                $rawImageString = str_replace(' ', '+', $image_parts[1]);
                $filterString = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $rawImageString));
                $imageName = $time.'.'.$image_type;

                $filepath = "uploads/events/";
                if(file_put_contents($filepath.$imageName, $filterString)) {
                    if(isset($pageevent['event_thumb'])) {
                        if(file_exists('uploads/events/'.$pageevent['event_thumb'])) {
                            unlink('uploads/events/'.$pageevent['event_thumb']);
                        }
                    }

                    $pageevent->event_photo = $imageName;
                    $pageevent->event_thumb = $imageName;
                }
			}

            if((strstr($_SERVER['HTTP_REFERER'],'r=event') && !strstr($_SERVER['HTTP_REFERER'],'e=')) || strstr($_SERVER['HTTP_REFERER'],'r=page/index') || strstr($_SERVER['HTTP_REFERER'],'r=page%2Findex') || strstr($_SERVER['HTTP_REFERER'],'r=groups/detail') || strstr($_SERVER['HTTP_REFERER'],'r=groups%2Fdetail'))
            {
                $pageevent->insert();
                $last_insert_id = $pageevent->_id;
                if($last_insert_id)
                {
                    $data['msg'] = 'success';
                    $data['event_id'] = "$last_insert_id";
                    $ev = new EventVisitors();
                    $ev->user_id = $user_id;
                    $ev->event_id = "$last_insert_id";
                    $ev->created_date = "$date";
                    $ev->is_deleted = '1';
                    $ev->insert();
                }
                else
                {
                    $data['msg'] = 'fail';
                }
            }
            else
            {
                if($user_id == $pageevent['created_by'])
                {
                    if($pageevent->update())
                    {
                        $data['msg'] = 'success';
                        $data['event_id'] = "$entity_id";
                    }
                    else
                    {
                        $data['msg'] = 'fail';
                    }
                }
                else
                {
                    $data['msg'] = 'fail';
                }
            }
            return json_encode($data);
        }
        else
        {
            return $this->goHome();
        }
    }
    
    public function actionJoin()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $authstatus = UserForm::isUserExistByUid($user_id);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') {
                $retdata['auth'] = $authstatus;
            } else {
                $eventid = $_POST['e'];
                $date = time();
                $retdata = array();

                $event_exist = PageEvents::find()->where(['_id' => "$eventid",'is_deleted' => '1'])->asarray()->one();
                if($event_exist)
                {
                    // check owner not allowed..............
                    $created_by = $event_exist['created_by'];
                    if($created_by == $user_id) {
                        $retdata['label'] = '7'; 
                        return json_encode($retdata);
                    }

                    // get isasktojoin is on or off 
                    $isasktojoin = EventVisitors::isasktojoin($eventid);
                    $ev = EventVisitors::find()->where(['event_id' => "$eventid",'user_id' => "$user_id"])->one();
                    if(!empty($ev))
                    {
                        if($ev->delete()) {
                            $event_details = PageEvents::getEventdetails($eventid);
                            $notification = Notification::find()->where(['user_id' => $user_id , 'event_id' => $eventid ,'notification_type' => 'attendevent'])->one();
                            if(!empty($notification)) {
                                $notification->delete();
                            }
                        }
                    } else {
                        $status = '1';
                        if($isasktojoin) {
                            $status = '2';
                        }

                        $ev = new EventVisitors();
                        $ev->user_id = $user_id;
                        $ev->event_id = $eventid;
                        $ev->status = $status;
                        $ev->created_date = "$date";
                        $ev->is_deleted = '1';
                        if($ev->insert()) {
                            //if($isasktojoin) {
                                $notification =  new Notification();
                                $notification->event_owner_id = $event_exist['created_by'];
                                $notification->user_id = "$user_id";
                                $notification->event_id = "$eventid";
                                $notification->notification_type = 'attendevent';
                                $notification->is_deleted = "0";
                                $notification->status = '1';
                                $notification->created_date = "$date";
                                $notification->updated_date = "$date";
                                $notification->insert();
                            //}
                        }
                    }
                }

                $evcount = EventVisitors::getEventCounts($eventid);
    			$retdata['goingcount'] = $evcount;
                
                $label = EventVisitors::getEventGoing($eventid);
                $retdata['label'] = $label;
            }
        }
        else
        {
            $retdata['auth'] = 'checkuserauthclassg';
        }

        return json_encode($retdata, true);
    }

    public function actionDetail() 
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
        } else {
            $checkuserauthclass = 'checkuserauthclassg';
        }

        $request = Yii::$app->request;
        $event_id = (string)$request->get('e');
        //$event_details = PageEvents::getEventdetails($event_id);
        $event_details = PageEvents::getEventdetailsshare($event_id);
        if(!$event_details)
        {
            $this->redirect(Url::toRoute(['event/index']));
        }  
        else
        {   
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                $eventposts =  PageEvents::getEventPost($event_id);
            } else {
                $eventposts =  PageEvents::getEventPost($event_id);
            }
            
            $eventpostcount = PageEvents::getEventPostCount($event_id);
            $eventcount = EventVisitors::getEventCounts($event_id);
            $eventusers = EventVisitors::getEventUsers($event_id);
            $isAttending = EventVisitors::getEventGoing($event_id);
            if($isAttending == 'Attending' || $isAttending == 'Organizer') {
                $isAttending = 'Y';
            } else {
                $isAttending = 'N';
            }
            return $this->render('detail',array('eventposts' => $eventposts,'eventpostcount'=>$eventpostcount,'eventusers'=>$eventusers,'eventcount'=>$eventcount,'event_details' => $event_details, 'isAttending' => $isAttending, 'checkuserauthclass' => $checkuserauthclass));
        }
    }
    
    public function actionMap() 
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if($user_id)
        {
            $event_id = (string)$_POST['e'];
            $event_details = PageEvents::getEventdetails($event_id);
            return $this->render('map',array('event_details' => $event_details));
        }
        else
        {
            return $this->goHome();
        }
    }
    
    public function actionInvites() 
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if($user_id)
        {
            $event_id = (string)$_POST['e'];
            $event_details = PageEvents::getEventdetails($event_id);
            if(!$event_details)
            {
                $this->getnolistfound('noaccesstoviewevent');
            }
            else
            {
                $eventusers = EventVisitors::getEventUsers($event_id);
                $eventcount = EventVisitors::getEventCounts($event_id);
                $invitedfriend = Friend::find()->where(['from_id' => "$user_id", 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->limit(6)->offset(0)->all();
				return $this->render('invitees',array('eventusers' => $eventusers,'eventcount' => $eventcount,'invitedfriend' => $invitedfriend,'event_details' => $event_details,'event_id' => $event_id,'user_id' => $user_id));
            } 
        }
        else
        {
            return $this->goHome();
        }
    }
    
    public function actionAttending() 
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if($user_id)
        {
            $event_id = (string)$_POST['e'];
            $event_details = PageEvents::getEventdetails($event_id);
            if(!$event_details)
            {
                $this->getnolistfound('noaccesstoviewevent');
            }
            else
            { 
                $eventusers = EventVisitors::getEventUsers($event_id);
                $eventuserscount = EventVisitors::getEventCounts($event_id);
				return $this->render('attending',array('eventusers' => $eventusers,'eventuserscount' => $eventuserscount,'event_details' => $event_details,'event_id' => $event_id,'user_id' => $user_id));
			}
        }
        else
        {
            return $this->goHome();
        }
    }

    public function actionInviteFriendsEvent()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $model = new \frontend\models\LoginForm();
        $word = '';
        $isEmpty = true;
        if(strstr($_SERVER['HTTP_REFERER'],'r=event'))
        {
            $url = $_SERVER['HTTP_REFERER'];
            $urls = explode('&',$url);
            $url = explode('=',$urls[1]);
            $event_id = $url[1];
        }

        $user_friends = Friend::find()->Where(['to_id'=> $user_id, 'status'=>'1'])->orWhere(['from_id'=> $user_id, 'status'=>'1'])->asarray()->all();
        $ids = array();
        if(!empty($user_friends)) { 
            foreach ($user_friends as $key => $user_friend) {
                if($user_friend['from_id'] == $user_id) {
                    $ids[] = $user_friend['to_id'];
                } else {
                    $ids[] = $user_friend['from_id'];
                }
            }
        }

        if (isset($_GET['key']) && trim($_GET['key']) != '') 
        {
            $word = trim($_GET['key']);
            $data = UserForm::find()->select(['fname', 'lname', 'fullname'])
            ->where(['like','fname', $word])
            ->orwhere(['like','lname', $word])
            ->orwhere(['like','fullname', $word])
            ->andwhere(['in', (string)'_id', $ids])
            ->andwhere(['status' => '1'])
            ->orderBy(['_id'=>SORT_DESC])->asarray()->all();
        } else {
            $data = UserForm::find()->select(['fname', 'lname', 'fullname'])->where(['in', (string)'_id', $ids])->orderBy(['_id'=>SORT_DESC])->asarray()->all();
        }
        ?>
            <div class="list-holder nice-scroll">
                <ul>
                <?php
                foreach($data as $sdata) {
                    $friendid = (string)$sdata['_id'];
                    $name = $sdata['fullname'];

                    
                    if($word != '') {
                        $fname = isset($sdata['fname']) ? $sdata['fname'] : '';
                        $lname = isset($sdata['lname']) ? $sdata['lname'] : '';
                        if (stripos($fname, $word) === 0 || stripos($lname, $word) === 0 || stripos($name, $word) === 0) {
                        } else {
                            continue;
                        }
                    }

                    $frndimg = $this->getimage($friendid,'thumb');
                    $attending = EventVisitors::find()->where(['event_id' => "$event_id", 'user_id' => "$friendid", 'is_deleted' => '1'])->one();
                    $invitaionsent = Notification::find()->where(['event_id' => "$event_id", 'status' => '1', 'event_invited_to_id' => "$friendid", 'user_id' => "$user_id", 'notification_type' => 'eventinvited'])->one();
                    $isEmpty = false;
                ?>
                <li class="invite_<?=$friendid?>">
                    <div class="invitelike-friend">
                        <div class="imgholder"><img src="<?=$frndimg?>"/></div>
                        <div class="descholder">
                            <h6><?=$name?></h6>
                            <div class="btn-holder events_<?=$friendid?>">
                                <?php if($attending) { ?>
                                    <label class="infolabel"><i class="zmdi zmdi-check"></i> Attending</label>
                                <?php } else if($invitaionsent) { ?>
                                    <label class="infolabel"><i class="zmdi zmdi-check"></i> Invited</label>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" class="btn-invite" onclick="sendinviteevent('<?=$friendid?>','<?=$event_id?>')">Invite</a>
                                    <a href="javascript:void(0)" class="btn-invite-close" onclick="cancelinviteevent('<?=$friendid?>')"><i class="mdi mdi-close"></i></a>
                                <?php } ?>
                            </div>
                            <div class="dis-none btn-holder sendinvitation_<?=$friendid?>">
                                <label class="infolabel"><i class="zmdi zmdi-check"></i> Invitation sent</label>
                            </div>
                        </div>
                    </div>
                </li>
                <?php } 
                    if($isEmpty == true) {
                        $this->getnolistfound('nofriendfound');
                    }
                ?>
                </ul>
            </div>
            <?php
    }

    public function actionSearchFriend()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $model = new \frontend\models\LoginForm();
        $word = '';
        $isEmpty = true;
     
        $user_friends = Friend::find()->Where(['to_id'=> $user_id, 'status'=>'1'])->orWhere(['from_id'=> $user_id, 'status'=>'1'])->asarray()->all();
        $ids = array();
        if(!empty($user_friends)) { 
            foreach ($user_friends as $key => $user_friend) {
                if($user_friend['from_id'] == $user_id) {
                    $ids[] = $user_friend['to_id'];
                } else {
                    $ids[] = $user_friend['from_id'];
                }
            }
        }

        if (isset($_GET['key']) && trim($_GET['key']) != '') 
        {
            $word = trim($_GET['key']);
            $data = UserForm::find()->select(['fname', 'lname', 'fullname'])
            ->where(['like','fname', $word])
            ->orwhere(['like','lname', $word])
            ->orwhere(['like','fullname', $word])
            ->andwhere(['in', (string)'_id', $ids])
            ->andwhere(['status' => '1'])
            ->orderBy(['_id'=>SORT_DESC])->asarray()->all();
        } else {
            $data = UserForm::find()->select(['fname', 'lname', 'fullname'])->where(['in', (string)'_id', $ids])->orderBy(['_id'=>SORT_DESC])->asarray()->all();
        }

        foreach($data as $sdata) {
            $friendid = (string)$sdata['_id'];
            $name = $sdata['fullname'];

            
            if($word != '') {
                $fname = isset($sdata['fname']) ? $sdata['fname'] : '';
                $lname = isset($sdata['lname']) ? $sdata['lname'] : '';
                if (stripos($fname, $word) === 0 || stripos($lname, $word) === 0 || stripos($name, $word) === 0) {
                } else {
                    continue;
                }
            }

            $frndimg = $this->getimage($friendid,'thumb');
            $isEmpty = false;
        ?>
        <li data-uid="<?=$friendid?>">
            <a class="search-link nothemecolor">
                <span class="display-box">
                    <span class="img-holder">
                        <img src="<?=$frndimg?>">
                    </span>
                    <span class="desc-holder nothemecolor">
                        <p class="unm"><?=$name?></p>
                    </span>
                </span>
            </a>
        </li>
        <?php } 
            if($isEmpty == true) {
                $this->getnolistfound('nofriendfound');
            }
    }
    
    public function actionSendinvite()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
		
		if(isset($user_id) && $user_id != '') {
		$authstatus = UserForm::isUserExistByUid($user_id);
		if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') {
			return $authstatus;
		} 
		else {

        $friend_id = (string)$_POST['fid'];
        $event_id = (string)$_POST['pid'];
        $date = time();

        $event_details = PageEvents::getEventdetails($event_id);
		$event_admin = $event_details['created_by'];
        if($event_details && $user_id)
        {
			if($user_id != $event_admin){
				$notification =  new Notification();
				$notification->event_owner_id = "$event_admin";
				$notification->event_invited_to_id = "$friend_id";
				$notification->user_id = "$user_id";
				$notification->event_id = "$event_id";
				$notification->notification_type = 'eventinvitedbyother';
				$notification->is_deleted = '0';
				$notification->status = '1';
				$notification->created_date = "$date";
				$notification->updated_date = "$date";
				$notification->insert();
			}
				
            $notification =  new Notification();
			$notification->event_owner_id = "$event_admin";
            $notification->event_invited_to_id = "$friend_id";
            $notification->user_id = "$user_id";
            $notification->event_id = "$event_id";
            $notification->notification_type = 'eventinvited';
            $notification->is_deleted = '0';
            $notification->status = '1';
            $notification->created_date = "$date";
            $notification->updated_date = "$date";
            if($notification->insert())
            {
                try
                {
                    $eventname = $event_details['event_name'];
                    $eventaddress = $event_details['address'];
                    $eventtagline = $event_details['tagline'];
                    $event_short_desc = $event_details['short_desc'];
                    $eventdate = $event_details['event_date'];
                    $eventtime = $event_details['event_time'];
                    $attend = EventVisitors::getEventCounts($event_id);
                    $ed = date("dS F Y", strtotime($eventdate)).' at '.$eventtime;
                    $frndname = $this->getuserdata($friend_id,'fullname');
                    $sendto = $this->getuserdata($friend_id,'email');
                    $fullname = $this->getuserdata($user_id,'fullname');
                    $ev_image = $this->geteventimage($event_id);
                    $ev_image = 'https://www.arabiaface.com/'.$ev_image;
                    $eventlink = "https://www.arabiaface.com/frontend/web/index.php?r=event/detail&e=$event_id";
                    $userlink = "https://www.arabiaface.com/frontend/web/index.php?r=userwall/index&id=$user_id";
                    $addresslink = "https://maps.google.it/maps?q=$eventaddress";
                      
                    $assetsPath = '../../vendor/bower/travel/images/';
                    
                    $test = Yii::$app->mailer->compose()
                            ->setFrom(array('csupport@arabiaface.com' => 'Arabiaface'))
                            ->setTo($sendto)
                            ->setSubject($fullname .' has invited you to an event '.$eventname.' in '.$eventaddress)
                            ->setHtmlBody('<html>
                                <head>
                                    <meta charset="utf-8" />
                                    <title>Arabiaface</title>
                                </head>
                                <body style="margin:0;padding:0;background:#dfdfdf;">
                                    <div style="color: #353535; float:left; font-size: 13px;width:100%; font-family:Arial, Helvetica, sans-serif;text-align:center;padding:40px 0 0;">
                                    <div style="width:600px;display:inline-block;">
                                        <table style="width:100%;">
                                            <tbody>
                                                <tr>
                                                  <td><img src="'.$assetsPath.'black-logo.png" style="margin:0 0 10px;width:130px;float:left;"></td>
                                                </tr>
                                                <tr style="background:#333;position:relative;width:100%;float:left;padding-top:10px;border-bottom:1px solid #e1e1e1;">
                                                  <td style="background:#fff;position:relative;width:100%;float:left;padding-top:10px;text-align:center;padding:0;">
                                                    <h4 style="color:#434343;font-size:14px;font-weight:normal;margin:0;padding:10px 15px;border-bottom:1px solid #e1e1e1;"><span style="font-weight:bold;">'.$fullname.'</span> has invited you to an event at <span style="font-weight:bold;">'.$eventaddress.'</span></h4>
                                                  </td>
                                                </tr>
                                                <tr style="background:#f5f5f5;position:relative;width:100%;float:left;padding:0;">
                                                  <td style="width:200px;padding:15px;padding-right:0;margin:0;float:left;"><img src="'.$ev_image.'" style="width:100%;"></td>
                                                  <td style="width:355px;padding:15px;vertical-align:top;float:right;padding-left:0;">
                                                    <div style="position:relative;width:100%;text-align:left;font-size:13px;">
                                                        <span style="color:#3399cc;margin: 0 0 5px;font-weight:bold;font-size:14px;display:inline-block;">'.$eventname.'</span>
                                                        <br />
                                                        <span style="margin: 0 0 15px;display:inline-block;">'.$eventtagline.'</span>
                                                        <br />
                                                        <span style="color:#999;margin: 0 0 5px;display:inline-block;">'.$ed.'</span>
                                                        <br />
                                                        <span style="color:#999;">'.$attend.' Attending</span>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr style="background:#fff;width:100%;float:left;padding:10px 0 20px;border-top:1px solid #e1e1e1;">
                                                  <td style="width:100%;float:left;padding:0;">
                                                    <div style="position:relative;width:100%;padding:0 20px;box-sizing: border-box;text-align:justify;color:#666;clear:both;float:left;font-size:13px;">
                                                        <p style="margin: 0 0 15px;">'.$event_short_desc.'</p>
                                                        <a href="'.$eventlink.'" style="background:#3399cc;font-size:13px;padding:8px 15px;border-radius:3px;float:right;clear:both;color:#fff;text-decoration:none;margin:10px 0 0;">View Event Details</a>
                                                    </div>	
                                                  </td>
                                                </tr>
                                                <tr style="width:100%;float:left;">
                                                    <td style="width:100%;float:left;padding-top:10px;text-align:center;padding:0;margin:10px 0 0;">
                                                        <div style="width:600px;display:inline-block;font-size:11px;">
                                                            <div style="color: #777;text-align: left;">&copy;  www.arabiaface.com All rights reserved.</div>
                                                            <div style="text-align: left;width: 100%;margin:5px  0 0;color:#777;">For support, you can reach us directly at <a href="csupport@arabiaface.com" style="color:#4083BF">csupport@arabiaface.com</a></div>
                                                       </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>')
                    ->send();
                }
                catch (ErrorException $e)
                {
                    return $e->getMessage();
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
		}
		}
		else {
			return 'checkuserauthclassg';
		}
    }
    
    public function actionSetorganizer() {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['id']) && $_POST['id'] !='') {
                    if(isset($_POST['event_id']) && $_POST['event_id'] !='') {
                        $id = $_POST['id'];
                        $event_id = $_POST['event_id'];
                        $result= PageEvents::setorganizer($user_id, $id, $event_id);
                        return $result;
                    }
                }
            }
        }
    }


    public function actionDelete()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if($user_id)
        {
            $eventid = (string)$_POST['e'];
            $event_exist = PageEvents::find()->where(['_id' => $eventid,'is_deleted' => '1'])->one();
            if($event_exist)
            {
                $date = time();
                $datein = date('Y-m-d h:i:s');
                $event_exist->is_deleted = "0";
                $event_exist->updated_date = "$date";
                $event_exist->updated_in_date = "$datein";
                if($event_exist->update())
                {
                    //Notification::updateAll(['is_deleted' => '1'], ['post_id' => $eventid]);
                    //EventVisitors::updateAll(['is_deleted' => '0'], ['event_id' => $eventid]);
                    Notification::deleteAll(['is_deleted' => '0','post_id' => $eventid,'notification_type' => 'eventgoing']);
                    Notification::deleteAll(['is_deleted' => '0','post_id' => $eventid,'notification_type' => 'eventinvited']);
                    EventVisitors::deleteAll(['is_deleted' => '1','event_id' => $eventid]);
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return $this->goHome();
        }	
    }
	
    public function actionFlagEvent()
    { 
        $session = Yii::$app->session;
        $event_id = (string)$_POST['event_id'];
        $event_owner_id = (string)$_POST['e_uid'];
        $user_id = (string)$session->get('uid');
        $desc = $_POST['desc'];
        $delete = new PageEvents();
        $delete = PageEvents::find()->where(['_id' => $event_id])->one();
        $delete->is_deleted = '2';
        $delete->update();
        /* Insert Notification For The Owner of Collection For Flagging*/
        $date = time(); 
        $notification =  new Notification();
        $notification->post_id = "$event_id";
        $notification->user_id = "$event_owner_id";
        $notification->notification_type = 'deleteeventadmin';
        $notification->is_deleted = '0';
        $notification->status = '0';
        $notification->created_date = "$date";
        $notification->updated_date = "$date";
        $notification->flag_reason = "$desc";
        $notification->insert();
        return true;
    }
	
    public function actionEventImageCrop()
    {
        $imgpaths = '../web/uploads/events';
        if (!file_exists($imgpaths))
        {
            mkdir($imgpaths, 0777, true);
        }
        $dt = time();

        $imgUrl = $_POST['imgUrl'];
        // original sizes
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        // resized sizes
        $imgW = $_POST['imgW'];
        $imgH = $_POST['imgH'];
        // offsets
        $imgY1 = $_POST['imgY1'];
        $imgX1 = $_POST['imgX1'];
        // crop box
        $cropW = $_POST['cropW'];
        $cropH = $_POST['cropH'];
        // rotation angle
        $angle = $_POST['rotation'];

        $jpeg_quality = 100;

        $output_filename = "uploads/events/".$dt;

        $what = getimagesize($imgUrl);

        switch(strtolower($what['mime']))
        {
            case 'image/png':
                $img_r = imagecreatefrompng($imgUrl);
                $source_image = imagecreatefrompng($imgUrl);
                $type = '.png';
                break;
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($imgUrl);
                $source_image = imagecreatefromjpeg($imgUrl);
                error_log("jpg");
                $type = '.jpeg';
                break;
            case 'image/gif':
                $img_r = imagecreatefromgif($imgUrl);
                $source_image = imagecreatefromgif($imgUrl);
                $type = '.gif';
                break;
            default: die('image type not supported');
        }

        // resize the original image to size of editor
        $resizedImage = imagecreatetruecolor($imgW, $imgH);
        imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
        // rotate the rezized image
        $rotated_image = imagerotate($resizedImage, -$angle, 0);
        // find new width & height of rotated image
        $rotated_width = imagesx($rotated_image);
        $rotated_height = imagesy($rotated_image);
        // diff between rotated & original sizes
        $dx = $rotated_width - $imgW;
        $dy = $rotated_height - $imgH;
        // crop rotated image to fit into original rezized rectangle
        $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
        imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
        imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
        // crop image into selected area
        $final_image = imagecreatetruecolor($cropW, $cropH);
        imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
        imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
        // finally output png image
        //imagepng($final_image, $output_filename.$type, $png_quality);
        imagejpeg($final_image, $output_filename.$type, $jpeg_quality);

        // Store For Original Image..
        $imageDataEncoded = base64_encode(file_get_contents($imgUrl));
        $imageData = base64_decode($imageDataEncoded);
        $source = imagecreatefromstring($imageData);
        $imageSave = imagejpeg($source,'uploads/events/ORI_'.$dt.$type,100);
        imagedestroy($source);

        $response = Array(
            "status" => 'success',
            "url" => $output_filename.$type
        );
        return json_encode($response);
    }
	
    /* DONE */  
    public function actionPhoto()
    {
        $session = Yii::$app->session; 
        $user_id = (string)$session->get('user_id');

        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['event_id']) && $_POST['event_id'] != '') {
                    $event_id = $_POST['event_id']; 
                    $photos = EventPhoto::getphoto($event_id);
                    $event_detail = PageEvents::getEventdetails($event_id);
                    return $this->render('photo',array('photo'=>$photos,'event_detail'=>$event_detail));
                }
            }
        }
    }

    /*Photo upload functionlaity*/
    
    public function actionAddphoto()
    {
        $session = Yii::$app->session; 
        $user_id = (string)$session->get('user_id');
        $model = new \frontend\models\LoginForm();
        $date = time();
        if($user_id)
        {
            if(isset($_FILES) && !empty($_FILES))
            {
                $imgcount = count($_FILES["imageFile1"]["name"]);
                $img = '';
                for($i=0; $i<$imgcount; $i++)
                {
                    if(isset($_FILES["imageFile1"]["name"][$i]) && $_FILES["imageFile1"]["name"][$i] != "") 
                    {
                        $url = '../web/uploads/';
                        $urls = '/uploads/';
                        $image_extn = explode('.',$_FILES["imageFile1"]["name"][$i]);
                        $image_extn = end($image_extn);
                        $rand = rand(111,999);
                        $img = $urls.$date.$rand.'.'.$image_extn;
                        move_uploaded_file($_FILES["imageFile1"]["tmp_name"][$i], $url.$date.$rand.'.'.$image_extn);

                        $event_id = $_POST['event_id'];

                        $userdata = UserForm::find()->select(['fullname'])->where([(string)'_id' => (string)$user_id])->asarray()->one(); 
                        if(!empty($userdata)) {
                            $post = new EventPhoto();
                            $post->image = $img;
                            $post->user_id = "$user_id" ; 
                            $post->event_id = "$event_id";
                            $post->privacy  = 'Public';
                            $post->created_date = "$date";
                            $post->modified_date = "$date";
                            $post->name = $userdata['fullname']; 
                            $post->is_deleted = '1';
                            if($post->insert()) {
                                $last_insert_id =  $post->_id;
                                $eventCreatedAt = PageEvents::find()->select(['created_by'])->where([(string)'_id' => (string)$event_id])->asarray()->one();

                                if(!empty($eventCreatedAt)) {
                                    $notification = new Notification();
                                    $notification->event_id = "$event_id";
                                    $notification->user_id = "$user_id";
                                    $notification->post_id = "$last_insert_id";
                                    $notification->event_owner_id = $eventCreatedAt['created_by'];
                                    $notification->notification_type = 'addphotoevent';
                                    $notification->is_deleted = '0';
                                    $notification->status = '1';
                                    $notification->created_date = "$date";
                                    $notification->updated_date = "$date";
                                    $notification->insert();
                                }
                            }
                        }
                    }
                }
                $last_ablum_id = $post->_id;
                return $last_ablum_id;
            }
           else { }
        }
        else
        {
            return $this->render('index', ['model' => $model,]);
        }
    }
    
    public function actionDeletephoto()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if($user_id)
        {
            $id = $_POST['photo_id'];
            $date = time(); 
            $update = EventPhoto::find()->where(['_id' => "$id"])->one();
            $update->is_deleted = "0";
            $update->modified_date = "$date";
            $update->update();
        }else{
             return $this->goHome();
        }   
    }

    public function actionPublishEvent() 
    {
        $e_id = $_POST['e_id'];
        $date = time();
        $record = PageEvents::find()->where(['_id' => $e_id,'is_deleted' => '2'])->one();
        $record->is_deleted = '1';
        $record->update();
        /* Insert Notification For The Owner of Post For Publishing his/her post*/
        $event_owner_id = $record['created_by'];
        $notification =  new Notification();
        $notification->post_id = "$e_id";
        $notification->user_id = "$event_owner_id";
        $notification->notification_type = 'publishevent';
        $notification->is_deleted = '0';
        $notification->status = '1';
        $notification->created_date = "$date";
        $notification->updated_date = "$date";
        $notification->insert();
        return true;
    }

    public function actionManagemembers() 
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $event_id = $_POST['event_id']; 
        if($session->get('user_id'))
        {
            $member_request = EventVisitors::geteventrequest($event_id);
            $event_member = EventVisitors::geteventmember($event_id);
            $event_detail = PageEvents::getEventdetails($event_id);
 
            return $this->render('manage_members',array('member_request'=>$member_request,'event_member'=>$event_member,'event_detail'=>$event_detail));
        }else{
            return $this->goHome();
        }   
    }

    public function actionAllmemberfororganizer()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $event_id = $_POST['event_id']; 
        if($session->get('user_id'))
        {
            $data = EventVisitors::getEventUsers($event_id);
            return $this->render('allmemberfororganizer',array('event_members'=>$data,'event_id'=>$event_id));
        }else{
            return $this->goHome();
        }   
    }

    public function actionMemberrequestorganizer()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $event_id = $_POST['event_id']; 
        if($session->get('user_id'))
        {
            $data = EventVisitors::eventuserrequest($user_id, $event_id);
            return $this->render('memberrequestorganizer',array('data'=>$data, 'event_id'=>$event_id));
        } else {
            return $this->goHome();
        }   
    }

    public function actionRemovemember()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $member_id = $_POST['member_id'];
        $datas  = array();
        if($session->get('user_id'))
        {
            $date = time(); 
            $update = EventVisitors::find()->where(['_id' => "$member_id"])->one();
            $update->status = '0'; 
            $update->modified_date = "$date";
            $update->update();
            $datas['msg'] = '1';
            return json_encode($datas);
        }else{
            return $this->goHome();
        }
    }   

    public function actionRequestresponcce()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $status = $_POST['status'];
        $id = $_POST['id'];
        $datas  = array();
        if($session->get('user_id'))
        {
            $date = time(); 
            $update = EventVisitors::find()->where(['_id' => "$id"])->one();
            $event_id = $update['event_id'];
            $friend_user_id = $update['user_id'];
            $update->status = $status; 
            $update->modified_date = "$date";
            $update->update();
            
            $notification =  new Notification();
            $notification->from_friend_id = "$friend_user_id";
            $notification->user_id = "$user_id";
            $notification->post_id = "$event_id";
            $notification->notification_type = 'eventjoin';
            $notification->is_deleted = '0';
            $notification->status = '1';
            $notification->created_date = "$date";
            $notification->updated_date = "$date";
            $notification->insert();
            $datas['msg'] = '1';
            return json_encode($datas);
        }
        else
        {
            return $this->goHome();
        }
    }
    
    public function actionLeftevent() {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['event_id']) && $_POST['event_id'] !='') {
                    $event_id = $_POST['event_id'];
                    $result = PageEvents::leftevent($user_id, $event_id);
                    return $result;
                }
            }
        }
    }

    public function actionAllrequests() {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['event_id']) && $_POST['event_id'] !='') {
                    $event_id = $_POST['event_id'];

                    $member_request = EventVisitors::geteventrequest($event_id);
                    $event_member = EventVisitors::geteventmember($event_id);
                    $event_detail = PageEvents::getEventdetails($event_id);
         
                    return $this->render('all_requests',array('member_request'=>$member_request,'event_member'=>$event_member,'event_detail'=>$event_detail));
                }
            }
        }
    } 

    public function actionAllmembers() {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['event_id']) && $_POST['event_id'] !='') {
                    $event_id = $_POST['event_id'];
                    $member_request = EventVisitors::geteventrequest($event_id);
                    $event_member = EventVisitors::geteventmember($event_id);
                    $event_detail = PageEvents::getEventdetails($event_id);
         
                    return $this->render('all_members',array('member_request'=>$member_request,'event_member'=>$event_member,'event_detail'=>$event_detail));
                }
            }
        }
    }

    public function actionGetprivacydata() 
    {
        $result = array();
        $url = $_SERVER['HTTP_REFERER'];
        $urls = explode('&',$url);
        $url = explode('=',$urls[1]); 
        $entity_id = $url[1];
        $result = array('status' => false);

        if($entity_id != '') {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');
            $data = PageEvents::getEventdetailsshare($entity_id);
            
            if(!empty($data)) {
                if(isset($data['event_privacy']) && $data['event_privacy'] == 'Custom') {
                    $customids = $data['customids'];
                    $customids = explode(',', $customids);
                    if(!empty($customids)) {
                        $result['ids'] = $customids; 
                        $result['status'] = true; 
                    }
                }
            }
        }

        return json_encode($result, true);

    }

}
?>