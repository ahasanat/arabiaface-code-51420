<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\filters\VerbFilter; 
use yii\filters\AccessControl;
use yii\helpers\Url;

use yii\mongodb\ActiveRecord;
use frontend\components\ExSession;  
use frontend\models\Group;
use frontend\models\GroupMember;
use frontend\models\GroupPhoto;
use frontend\models\PostForm;
use frontend\models\Friend;
use frontend\models\PageEvents;
use frontend\models\Preferences;
use frontend\models\UserForm;
use frontend\models\Abuse;
use frontend\models\LoginForm;
use frontend\models\Notification;
use frontend\models\EventVisitors;
class GroupsController extends Controller {

	public function behaviors() 
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],  
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}
	
	public function beforeAction($action)
	{   
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function actions() 
	{
		return [ 
			'auth' => [
				'class' => 'yii\authclient\AuthAction',
				'successCallback' => [$this,
					'oAuthSuccess'],
			],
			'captcha' => [
				'class' => 'mdm\captcha\CaptchaAction',
				'level' => 1,
			],
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}
	
	public function actionIndex() 
	{
		$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
        } else {
            $checkuserauthclass = 'checkuserauthclassg';
        }

        return $this->render('group', array('checkuserauthclass' => $checkuserauthclass));
	}
	
/*Group Add, update, delete, view functionality*/	

	public function actionGroupimagecrop()
	{
		$imgpaths = '../web/uploads/group';
			if (!file_exists($imgpaths))
			{
				mkdir($imgpaths, 0777, true);
		}
		$dt = time();

		$imgUrl = $_POST['imgUrl'];
		// original sizes
		$imgInitW = $_POST['imgInitW'];
		$imgInitH = $_POST['imgInitH'];
		// resized sizes
		$imgW = $_POST['imgW'];
		$imgH = $_POST['imgH'];
		// offsets
		$imgY1 = $_POST['imgY1'];
		$imgX1 = $_POST['imgX1'];
		// crop box
		$cropW = $_POST['cropW'];
		$cropH = $_POST['cropH'];
		// rotation angle
		$angle = $_POST['rotation'];

		$jpeg_quality = 100;


		$output_filename = "uploads/group/".$dt;

		$what = getimagesize($imgUrl);

		switch(strtolower($what['mime']))
		{
			case 'image/png':
				$img_r = imagecreatefrompng($imgUrl);
				$source_image = imagecreatefrompng($imgUrl);
				$type = '.png';
				break;
			case 'image/jpeg':
				$img_r = imagecreatefromjpeg($imgUrl);
				$source_image = imagecreatefromjpeg($imgUrl);
				error_log("jpg");
				$type = '.jpeg';
				break;
			case 'image/gif':
				$img_r = imagecreatefromgif($imgUrl);
				$source_image = imagecreatefromgif($imgUrl);
				$type = '.gif';
				break;
			default: die('image type not supported');
		}



		// resize the original image to size of editor
		$resizedImage = imagecreatetruecolor($imgW, $imgH);
		imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
		// rotate the rezized image
		$rotated_image = imagerotate($resizedImage, -$angle, 0);
		// find new width & height of rotated image
		$rotated_width = imagesx($rotated_image);
		$rotated_height = imagesy($rotated_image);
		// diff between rotated & original sizes
		$dx = $rotated_width - $imgW;
		$dy = $rotated_height - $imgH;
		// crop rotated image to fit into original rezized rectangle
		$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
		imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
		imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
		// crop image into selected area
		$final_image = imagecreatetruecolor($cropW, $cropH);
		imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
		imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
		// finally output png image
		//imagepng($final_image, $output_filename.$type, $png_quality);
		imagejpeg($final_image, $output_filename.$type, $jpeg_quality);

		// Store For Original Image..
		$imageDataEncoded = base64_encode(file_get_contents($imgUrl));
		$imageData = base64_decode($imageDataEncoded);
		$source = imagecreatefromstring($imageData);
		$imageSave = imagejpeg($source,'uploads/group/ORI_'.$dt.$type,100);
		imagedestroy($source);

		$response = Array(
			"status" => 'success',
			"url" => $output_filename.$type
		);
		return json_encode($response);
	}
	
/*Add Group Function*/
 	
	public function actionAdd() 
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		$time = time();
		if($session->get('user_id'))
		{
			$data 	= array();
			$date = time(); 

			$privacy = $_POST['visible'];
			$group = new Group();
			$group->user_id = $user_id;
			$group->name = $_POST['title']; 
			$group->tagline = $_POST['tagline'];
			$group->location = $_POST['location'];
			$group->description = $_POST['description'];
			$group->privacy = $privacy; 
			if($privacy == 'Custom') {
				if(isset($_POST['custom']) && !empty($_POST['custom'])) {
					$customids = $_POST['custom'];
					$group->customids = $customids; 
				}
			} else {
				$group->customids = '';
			}

			if(isset($_POST['review']) && $_POST['review'] == 'true') {
				$_POST['review'] = '1';
			}
			if(isset($_POST['isasktojoin']) && $_POST['isasktojoin'] == 'true') {
				$_POST['isasktojoin'] = '1';
			}
			$group->review = $_POST['review'];
			$group->isasktojoin = $_POST['isasktojoin'];
			if(isset($_POST['images']) && $_POST['images'] != '' &&  $_POST['images'] != 'undefined')
			{
				$image_parts = explode(";base64,", $_POST['images']);
				$image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];

                $rawImageString = str_replace(' ', '+', $image_parts[1]);
                $filterString = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $rawImageString));
                $imageName = $time.'.'.$image_type;

 				$filepath = "uploads/group/";
                if(file_put_contents($filepath.$imageName, $filterString)) {
                    $group->group_thumb = $imageName;
                    $group->group_photo = $imageName;
                }
			}
			
			$group->created_by = "$user_id";
			$group->created_date = "$date";
			$group->modified_date = "$date";
			$group->is_deleted = "1";
			$group->entity_group = "1";
			$group->insert();
			$last_insert_id = $group->_id;
			if($last_insert_id)
			{
				$data['msg'] = 'success';
				$data['group_id'] = "$last_insert_id";
				$groupmember = new GroupMember();
                $groupmember->user_id = $user_id;
				$groupmember->group_id = "$last_insert_id";
				$groupmember->status = "1";
				$groupmember->created_date = "$date";
				$groupmember->modified_date = "$date";
				$groupmember->insert();
			}
			else
			{
				$data['msg'] = 'fail';
			}
			return json_encode($data);
		}
		else
		{
			return $this->goHome();
		}
	}

/*update group functionality*/	
	public function actionUpdate() 
	{
		$session = Yii::$app->session;
		$time = time();  
		if($session->get('user_id'))
		{
			$privacy = $_POST['visible'];
			$collection = new Group();
			$group_id = $_POST['group_id'];
			$update = Group::find()->where(['_id' => "$group_id"])->one();
			
			$update->name = $_POST['title']; 
			$update->tagline = $_POST['tagline'];
			$update->location = $_POST['location'];
			$update->privacy = $privacy;

			if($privacy == 'Custom') {
				if(isset($_POST['custom']) && !empty($_POST['custom'])) {
					$customids = $_POST['custom'];
					$update->customids = $customids; 
				}
			} else {
				$update->customids = ''; 
			}

			if(isset($_POST['isasktojoin']) && $_POST['isasktojoin'] == 'true') {
				$_POST['isasktojoin'] = '1';	
			}
			if(isset($_POST['review']) && $_POST['review'] == 'true') {
				$_POST['review'] = '1';
			}
			
			$update->review = $_POST['review'];
			$update->isasktojoin = $_POST['isasktojoin'];
			$update->description = $_POST['description'];
			if(isset($_POST['images']) && $_POST['images'] != '' &&  $_POST['images'] != 'undefined')
			{
				$image_parts = explode(";base64,", $_POST['images']);
				$image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];

                $rawImageString = str_replace(' ', '+', $image_parts[1]);
                $filterString = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $rawImageString));
                $imageName = $time.'.'.$image_type;

 				$filepath = "uploads/group/";
                if(file_put_contents($filepath.$imageName, $filterString)) {
                	if(isset($update['group_thumb'])) {
                        if(file_exists('uploads/group/'.$update['group_thumb'])) {
                            unlink('uploads/group/'.$update['group_thumb']);
                        }
                    }

                    $update->group_thumb = $imageName;
                    $update->group_photo = $imageName;
                }
			}

			$update->modified_date = "$time";
			$update->update(); 
		}
		else
		{
			return $this->goHome();
		}
		
	}

/*Delete group functionality*/	
	public function actionDelete(){
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		if($session->get('user_id'))
		{
			$group_id = $_POST['group_id'];
			$date = time(); 
			$delete = Group::find()->where(['_id' => "$group_id"])->one();
			if(!empty($delete)) {
				if($delete->delete())
				{
					$group_event = PageEvents::find()->where(['type' => 'group','parent_id' => $group_id])->all();
					foreach($group_event as $group_events)
					{
						$eventid = (string)$group_events['_id'];
	                    Notification::deleteAll(['is_deleted' => '0','post_id' => $eventid,'notification_type' => 'eventgoing']);
	                    Notification::deleteAll(['is_deleted' => '0','post_id' => $eventid,'notification_type' => 'eventinvited']);
	                    EventVisitors::deleteAll(['is_deleted' => '1','event_id' => $eventid]);
					}
					PageEvents::deleteAll(['type' => 'group','parent_id' => $group_id]);
					GroupMember::deleteAll(['group_id'=>$group_id]);
					GroupPhoto::deleteAll(['group_id'=>$group_id]);
					return true;
				}
			}
		}else{
			 return $this->goHome();
		}	
	}
	
/*Group Detail page function*/
	public function actionDetail() 
	{
		if (isset($_GET['group_id']) && $_GET['group_id'] != '') {
			$session = Yii::$app->session;
			$user_id = (string)$session->get('user_id');
			$group_id = $_GET['group_id'];  

			if(isset($user_id) && $user_id != '') {
	        	$authstatus = UserForm::isUserExistByUid($user_id);
	        	$checkuserauthclass = $authstatus;
	        } else {
	        	$checkuserauthclass = 'checkuserauthclassg';
	        }

	        if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
				$posts =  PostForm::getGroupPosts($group_id, '', $user_id);
			} else {
				$posts =  PostForm::getGroupPosts($group_id);
				
			}

			if(isset($_GET['notification_id']) && $_GET['notification_id'] != '') {
				$notification_id = $_GET['notification_id'];
				$data = Notification::find()->where([(string)'_id' => $notification_id, "post_id" => $group_id, "notification_type" => "grouprequest"])->asarray()->one();
				if(!empty($data)) {
					$getUserId = $data['user_id'];
					$isFind = GroupMember::find()->where(['user_id' => $getUserId, 'group_id' => $group_id])->one();
					if(!empty($isFind)) {
						$isFind->status = '1';
						$isFind->update();
					}
				}
			}

			$model_group = new Group();
			$groupdetail = $model_group->groupdetail($group_id);
			
			$col_created_by =$groupdetail['user_id'];
			$col_privacy = $groupdetail['privacy'];


			if(empty($groupdetail)) {
				$this->redirect(Url::toRoute(['groups/index']));
			}

			if($user_id == $col_created_by) {
				$isWriteAllow = 'yes';
			} else if(($col_privacy == 'Public' || $status == '10') || ($col_privacy == 'Friends' && ($is_friend || $status == '10')) || ($col_privacy == 'Private')) {
				$isWriteAllow = 'yes';
			} else if($col_privacy == 'Custom') {
				$customids = array();
			    $customids = $groupdetail['customids'];
			    $customids = explode(',', $customids);

			    if(in_array($user_id, $customids)) {
					$isWriteAllow = 'yes';	    	
			    }
			} else if($col_privacy == 'Friend of friend') {
				$friendoffriendids = Friend::getuserFriendsIds($col_created_by);
				foreach ($friendoffriendids as $levelsingle) {
					$level = Friend::getuserFriendsIds($levelsingle);
					if(!empty($level)) {
						$friendoffriendids = array_merge($friendoffriendids, $level);	
					}
				}
				$friendoffriendids = array_unique($friendoffriendids);

				if(in_array($user_id, $friendoffriendids)) {
					$isWriteAllow = 'yes';	 	
				}
			}

            if($isWriteAllow == 'yes') {
            	$group_member = GroupMember::getgroupmember($group_id);
            	return $this->render('detail',array('posts'=>$posts, 'detail'=>$groupdetail,'group_member'=>$group_member, 'checkuserauthclass' => $checkuserauthclass));
            } else {
            	$this->redirect(Url::toRoute(['groups/index']));
            }
		} else {
			return $this->goHome();
		}	
	}
	
/*Group Member functionality*/
	Public function actionMember()
	{  
		$session = Yii::$app->session;
		$user_id  = (string)$session->get('user_id');

		if(isset($user_id) && $user_id != '') {
			$authstatus = UserForm::isUserExistByUid($user_id);
        	if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') {
        		$datas['auth'] = $authstatus;
        	} else {
				$group_id = $_POST['group_id'];
				$date = time();
				$datas 	= array();
				$group_detail = Group::groupdetail($group_id);
				$group_admin = $group_detail['user_id'];
				if($session->get('user_id'))
				{	
					// check user is admin or not..........
					$isAdmin =  Group::find()->Where([(string)'_id' => $group_id, 'user_id' => $user_id])->one();
		            if(!empty($isAdmin)) {
		                $datas['label'] = '7';
						return json_encode($datas);
		            }

					// get isasktojoin is on or off 
					$isasktojoin = Group::isasktojoin($group_id);		
					$groupmemberexist = GroupMember::find()->where(['user_id' => $user_id,'group_id' => $group_id])->one();
					if (!empty($groupmemberexist))
					{
						
						if($groupmemberexist->delete()) {
							$groupmembernot = Notification::find()->where(['group_owner_id' => $group_admin,'group_id' => $group_id, 'is_deleted' => '0'])->one();

							if(!empty($groupmembernot)) {
								$groupmembernot->delete();
							}
						}
					} else {

						$status = '1';
						if($isasktojoin) {
							$status = '2';
						}
						$groupmember = new GroupMember();
						$groupmember->user_id = $user_id;
						$groupmember->group_id = $group_id;
						$groupmember->status = $status;
						$groupmember->created_date = "$date";
						$groupmember->modified_date = "$date";
						if ($groupmember->insert()) {
							//if($isasktojoin) {
								$notification =  new Notification();
								$notification->group_owner_id = "$group_admin";
								$notification->user_id = "$user_id";
								$notification->group_id = "$group_id";
								$notification->notification_type = 'becomegroupmember';
								$notification->is_deleted = '0';
								$notification->status = '1';
								$notification->created_date = "$date";
								$notification->updated_date = "$date";
								$notification->insert();
							//}
						}
					}	
				}

				$member_count = GroupMember::find()->where(['status' => "1",'group_id' => $group_id])->count();
				$datas['member_count'] = $member_count;
		 		
		 		$label = GroupMember::getgroupmemerstatus($group_id);
		 		$datas['label'] = $label;
 			}
 		} else {
 			$datas['auth'] = 'checkuserauthclassg';
 		}

		return json_encode($datas);		
	}
	
    /*group all functionality*/	
	public function actionAll()
	{
		$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        $start = 0;
        if(isset($user_id) && $user_id != '') { 
        	$checkuserauthclass = UserForm::isUserExistByUid($user_id);
        } else {
        	$checkuserauthclass = 'checkuserauthclassg'; 
        }

        $lazyhelpcount = isset($_POST['$lazyhelpcount']) ? $_POST['$lazyhelpcount'] : 0;
        $start = $lazyhelpcount * 12;
		$allgroup = Group::allgroup($start);
		return $this->render('allgroup',array('allgroup'=>$allgroup, 'checkuserauthclass' => $checkuserauthclass, 'start' => $start));
	}

	public function actionSearchWithLocation()
	{
		if(isset($_POST['location']) && trim($_POST['location']) != '') {
			$session = Yii::$app->session;
	        $user_id = (string)$session->get('user_id');
	        
	        if(isset($user_id) && $user_id != '') { 
	        	$checkuserauthclass = UserForm::isUserExistByUid($user_id);
	        } else {
	        	$checkuserauthclass = 'checkuserauthclassg'; 
	        }

	        $location = $_POST['location'];
	        $location = urldecode($location);
			$allgroup = Group::searchwithlocation($location);
			return $this->render('allgroup', array('allgroup'=>$allgroup, 'checkuserauthclass' => $checkuserauthclass));
    	
    	} else {
    		$url = Yii::$app->urlManager->createUrl(['groups/index']);
            Yii::$app->getResponse()->redirect($url);
    	}
	}
	
/*Group Your Funcion*/
	public function actionYours()
	{
		$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        $start = 0;
        if(isset($user_id) && $user_id != '') {
        	$authstatus = UserForm::isUserExistByUid($user_id);
        	$checkuserauthclass = $authstatus;
        } else {
        	$checkuserauthclass = 'checkuserauthclassg';
        }

        $lazyhelpcount = isset($_POST['$lazyhelpcount']) ? $_POST['$lazyhelpcount'] : 0;
        if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') {
        	$yoursgroup = array();
        } else {
            $start = $lazyhelpcount * 12;
        	$yoursgroup = Group::yoursgroup($user_id, $start);	
        }

		return $this->render('yoursgroup',array('yoursgroup'=>$yoursgroup, 'checkuserauthclass' => $checkuserauthclass, 'start' => $start, 'lazyhelpcount' => $lazyhelpcount));
		
	}
	
/*Group join Function*/
	public function actionJoin()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');

		$start = 0;
		if(isset($user_id) && $user_id != '') {
        	$authstatus = UserForm::isUserExistByUid($user_id);
        	$checkuserauthclass = $authstatus;
        } else {
        	$checkuserauthclass = 'checkuserauthclassg';
        }

        if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') {
        	$joingroup = array();
        } else {
        	$lazyhelpcount = isset($_POST['$lazyhelpcount']) ? $_POST['$lazyhelpcount'] : 0;
            $start = $lazyhelpcount * 12;
        	$joingroup = Group::joingroup($user_id, $start);
        }
		
		return $this->render('joingroup',array('joingroup'=>$joingroup, 'start' => $start));
	}
	
/*Group about functionality*/	
	public function actionAbout()
	{
		if(isset($_POST['group_id']) && $_POST['group_id'] != '') {
			$group_id = $_POST['group_id']; 
			$groupdetail = Group::groupdetail($group_id);
			$group_member = GroupMember::getgroupmember($group_id);
			
			return $this->render('about',array('detail'=>$groupdetail,'group_member'=>$group_member));
		}
	}
		
	public function actionEvent()
	{	

		$session = Yii::$app->session; 
		$user_id = (string)$session->get('user_id');
		
		if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['group_id']) && $_POST['group_id'] != '') {
                	$group_id = $_POST['group_id']; 
                    $groupdetail 	= Group::groupdetail($group_id);
					$groupevent 	= PageEvents::getGroupEvent($group_id);
					return $this->render('event',array('groupevent'=>$groupevent,'groupdetail'=>$groupdetail));
                }
            }
        }
	}
	
	
	
	public function actionMembertab()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		$group_id = $_POST['group_id']; 
		if($session->get('user_id'))
		{
			$group_member = GroupMember::getgroupmember($group_id);
			return $this->render('member',array('group_member'=>$group_member));
			
		}else{
			return $this->goHome();
		}	
	}
	
	public function actionManagemembers()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		$group_id = $_POST['group_id']; 
		if($session->get('user_id'))
		{
			$member_request = GroupMember::getmemberrequest($group_id);
			$group_member = GroupMember::getgroupmember($group_id);
			$group_detail = Group::groupdetail($group_id);
			return $this->render('manage_members',array('member_request'=>$member_request,'group_member'=>$group_member,'group_admin'=>$group_detail['user_id']));
		} else {
			return $this->goHome();
		}	
	}

	public function actionAllmemberforadmin()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		$group_id = $_POST['group_id']; 
		if($session->get('user_id'))
		{
			$group_member = GroupMember::getgroupmember($group_id);
			return $this->render('allmemberforadmin',array('group_member'=>$group_member,'group_id'=>$group_id));
		} else {
			return $this->goHome();
		}	
	}

	public function actionMemberrequest()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		$group_id = $_POST['group_id']; 
		if($session->get('user_id'))
		{
			$data = Group::memberrequest($user_id, $group_id);
			return $this->render('memberrequest',array('data'=>$data, 'group_id'=>$group_id));
		} else {
			return $this->goHome();
		}	
	}
	
	public function actionRequestresponcce()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		$status = $_POST['status'];
		$id = $_POST['id'];
		$datas 	= array();
		if($session->get('user_id'))
		{
			$date = time(); 
			$update = GroupMember::find()->where(['_id' => "$id"])->one();
			$group_id = $update['group_id'];
			$friend_user_id = $update['user_id'];
			$update->status = $status; 
			$update->modified_date = "$date";
			$update->update();
			
			$notification =  new Notification();
			$notification->from_friend_id = "$friend_user_id";
			$notification->user_id = "$user_id";
			$notification->post_id = "$group_id";
			$notification->notification_type = 'groupjoin';
			$notification->is_deleted = '0';
			$notification->status = '1';
			$notification->created_date = "$date";
			$notification->updated_date = "$date";
			$notification->insert();
			$datas['msg'] = '1';
			return json_encode($datas);
		}
		else
		{
			return $this->goHome();
		}
	}
	
	public function actionRemovemember()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		$member_id = $_POST['member_id'];
		$datas 	= array();
		if($session->get('user_id'))
		{
			$date = time(); 
			$update = GroupMember::find()->where(['_id' => "$member_id"])->one();
			$update->status = '0'; 
			$update->modified_date = "$date";
			$update->update();
			$datas['msg'] = '1';
			return json_encode($datas);
		}else{
			return $this->goHome();
		}
	}	
			
	/* DONE */	
	public function actionPhoto()
	{
		$session = Yii::$app->session; 
		$user_id = (string)$session->get('user_id');

		if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['group_id']) && $_POST['group_id'] != '') {
                    $group_id = $_POST['group_id']; 
                    $photos = GroupPhoto::getphoto($group_id);
					$groupdetail = Group::groupdetail($group_id);
					return $this->render('photo',array('photo'=>$photos,'groupdetail'=>$groupdetail));
                }
            }
        }
	}
	
	/*Photo upload functionlaity*/
	
	public function actionAddphoto()
    {
		$session = Yii::$app->session; 
		$user_id = (string)$session->get('user_id');
		$model = new \frontend\models\LoginForm();
		$date = time();
		if($user_id)
        {
            if(isset($_FILES) && !empty($_FILES))
			{
				$imgcount = count($_FILES["imageFile1"]["name"]);
                $img = '';
                for($i=0; $i<$imgcount; $i++)
                {
					if(isset($_FILES["imageFile1"]["name"][$i]) && $_FILES["imageFile1"]["name"][$i] != "") 
					{
						$url = '../web/uploads/';
						$urls = '/uploads/';
						$image_extn = explode('.',$_FILES["imageFile1"]["name"][$i]);
                        $image_extn = end($image_extn);
						$rand = rand(111,999);
						$img = $urls.$date.$rand.'.'.$image_extn;
						move_uploaded_file($_FILES["imageFile1"]["tmp_name"][$i], $url.$date.$rand.'.'.$image_extn);

						$userdata = UserForm::find()->select(['fullname'])->where([(string)'_id' => (string)$user_id])->asarray()->one(); 
                        if(!empty($userdata)) {
							$post = new GroupPhoto();
							$post->image = $img;
							$group_id = $_POST['group_id'];
							$post->user_id = "$user_id" ; 
							$post->group_id = "$group_id";
							$post->privacy	= 'Public'; 
							$post->created_date = "$date";
							$post->modified_date = "$date";
							$post->name = $userdata['fullname']; 
							$post->is_deleted = '1';
							if($post->insert()) {

                                $eventCreatedAt = Group::find()->select(['user_id'])->where([(string)'_id' => (string)$group_id])->asarray()->one();

                                if(!empty($eventCreatedAt)) {
                                    $notification = new Notification();
                                    $notification->group_id = "$group_id";
                                    $notification->user_id = "$user_id";
                                    $notification->group_owner_id = $eventCreatedAt['user_id'];
                                    $notification->notification_type = 'addphotogroup';
                                    $notification->is_deleted = '0';
                                    $notification->status = '1';
                                    $notification->created_date = "$date";
                                    $notification->updated_date = "$date";
                                    $notification->insert();
                                }
                            }
						}
					}
                }
				$last_ablum_id = $post->_id;
				return $last_ablum_id;
            }
           else { }
        }
        else
        {
            return $this->render('index', ['model' => $model,]);
        }
    }
	
	public function actionDeletephoto()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		if($user_id)
		{
			$id = $_POST['photo_id'];
			$date = time(); 
			$update = GroupPhoto::find()->where(['_id' => "$id"])->one();
			$update->is_deleted = "0";
			$update->modified_date = "$date";
			$update->update();
		}else{
			 return $this->goHome();
		}	
	}
	
	public function actionInvites() 
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if($user_id)
        {
            $group_id = (string)$_POST['group_id'];
            $group_details = Group::groupdetail($group_id);
            if(!$group_details)
            {
            	return $this->goHome();
            }
            else
            {
                $groupusers = GroupMember::getgroupmember($group_id);
				$groupcount = GroupMember::getgroupmembercount($group_id);
				$invitedfriend = Friend::find()->where(['from_id' => "$user_id", 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->limit(6)->offset(0)->all();
				  
				$assetsPath = '../../vendor/bower/travel/images/';

			?>
                <div class="content-box bshadow inner-content-box">
                    <div class="cbox-title">
                        <h5>
                            <i class="mdi mdi-bullhorn"></i>
                            Invite Friends
                        </h5>
                    </div>
                    <div class="cbox-desc">
                        <div class="likes-summery">
                            <div class="friend-likes"> 
                                <h5><a href="javascript:void(0)"><?=$groupcount?> Member<?php if($groupcount > 1){ ?>s<?php } ?></a> of <?=$group_details['name']?> group</h5>
                                <?php if($groupcount > 1){ ?>
                                <ul>
                                    <?php foreach($groupusers as $groupuser){?>
                                    <li>
                                        <a href="<?=Url::to(['userwall/index', 'id' => $groupuser['user_id']]);?>">
                                            <img title="<?=$this->getuserdata($groupuser['user_id'],'fullname');?>" src="<?= $this->getimage($groupuser['user_id'],'thumb')?>">
                                        </a>
                                    </li>
                                    <?php 
									
									} ?>
                                </ul>										
                                <?php } else { ?>
                                <?php $this->getnolistfound('becomefirsttojoingroup');?>
                                <?php } ?>
                            </div>
                            <div class="invite-likes">
                                <?php if(count($invitedfriend) > 0) { ?>
                                <p>Invite your friends to join this group</p>
                                <?php } ?>
                                <div class="invite-holder">
                                    <form>
                                        <div class="tholder">
                                            <div class="sliding-middle-out anim-area underlined">
                                                <input placeholder="Type a friend's name" type="text" class="invite_friend_group" data-id="invite_friend_group">
												<a href="javascript:void(0)" onclick="removeinvitesearchinput(this);"><img src="<?=$assetsPath?>cross-icon.png"/></a>
                                            </div>
                                        </div>
                                        <!-- <input class="btn btn-primary" value="Search" type="button"> -->
                                    </form>
                                    <div class="list-holder blockinvite_friend_group">
                                        <ul>
                                            <?php 
                                            if(count($invitedfriend) > 0){
                                                foreach($invitedfriend as $invitedfriends){
                                                $friendid = (string)$invitedfriends['to_id'];
                                                $result = LoginForm::find()->where(['_id' => $friendid])->one();
                                                $frndimg = $this->getimage($friendid,'thumb');
                                                $attending = GroupMember::find()->where(['group_id' => "$group_id", 'user_id' => "$friendid", 'status' => '1'])->one();
                                                $invitaionsent = Notification::find()->where(['post_id' => "$group_id", 'status' => '1', 'from_friend_id' => "$friendid", 'user_id' => "$user_id", 'notification_type' => 'groupinvite'])->one();
                                            ?>
                                            <li class="invite_<?=$friendid?>">
                                                <div class="invitelike-friend">
                                                    <div class="imgholder"><img src="<?=$frndimg?>"></div>
                                                    <div class="descholder">
                                                        <h6><?=$result['fullname']?></h6>
                                                        <div class="btn-holder events_<?=$friendid?>">
                                                        <?php 
                                                        if($attending)
                                                        {
                                                        	?>
                                                            <label class="infolabel"><i class="zmdi zmdi-check"></i> Member</label>
                                                        	<?php
                                                        }
                                                        else if($invitaionsent) 
                                                        {
                                                        	?>
                                                            <label class="infolabel"><i class="zmdi zmdi-check"></i> Invited</label>
                                                        	<?php
                                                        }
                                                        else
                                                        { ?>
                                                            <a href="javascript:void(0)" class="btn-invite" onclick="sendinvitegroup('<?=$friendid?>','<?=$group_id?>')">Invite</a>
                                                            <a href="javascript:void(0)" class="btn-invite-close" onclick="cancelinvitegroup('<?=$friendid?>')"><i class="mdi mdi-close"></i></a>
                                                        <?php } ?>
                                                        </div>
                                                        <div class="btn-holder sendinvitation_<?=$friendid?> dis-none">
                                                            <label class="infolabel"><i class="zmdi zmdi-check"></i> Invitation sent</label>
                                                        </div>
                                                    </div>														
                                                </div>
                                            </li>
                                            <?php } } else { ?>
                                            <?php $this->getnolistfound('allfriendattendinggroup');?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
        }
        else
        {
            return $this->goHome();
        }
    }
	
	public function actionSendinvite()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
		$friend_id = (string)$_POST['fid'];
        $group_id = (string)$_POST['pid'];
        $date = time();
		$group_details = Group::groupdetail($group_id);
		$group_admin = $group_details['user_id'];
        if($group_details && $user_id)
        {
			if($user_id != $group_admin){
				$notification =  new Notification();
				$notification->group_owner_id = "$group_admin";
	            $notification->from_friend_id = "$friend_id";
	            $notification->user_id = "$user_id";
	            $notification->post_id = "$group_id";
	            $notification->notification_type = 'groupinvitedbyother';
	            $notification->is_deleted = '0';
	            $notification->status = '1';
	            $notification->created_date = "$date";
	            $notification->updated_date = "$date";
				$notification->insert();				
			}	

            $notification =  new Notification();
			$notification->group_owner_id = "$group_admin";
            $notification->from_friend_id = "$friend_id";
            $notification->user_id = "$user_id";
            $notification->post_id = "$group_id";
            $notification->notification_type = 'groupinvite';
            $notification->is_deleted = '0';
            $notification->status = '1';
            $notification->created_date = "$date";
            $notification->updated_date = "$date";
            if($notification->insert())
            {
                try
                {
                    $groupname = $group_details['name'];
                    $groupaddress = $group_details['location'];
                    $grouptagline = $group_details['tagline'];
                    $group_short_desc = $group_details['description'];
                    $attend = GroupMember::getgroupmembercount($group_id);
                    $frndname = $this->getuserdata($friend_id,'fullname');
                    $sendto = $this->getuserdata($friend_id,'email');
                    $fullname = $this->getuserdata($user_id,'fullname');
                    $ev_image = $this->getgroupimage($group_id);
					$ev_image = 'https://www.arabiaface.com/'.$ev_image;
                    $grouplink = "https://www.arabiaface.com/frontend/web/index.php?r=groups/detail&group_id=$group_id";
                    $userlink = "https://www.arabiaface.com/frontend/web/index.php?r=userwall/index&id=$user_id";
                    $addresslink = "https://maps.google.it/maps?q=$groupaddress";
                    
                    $test = Yii::$app->mailer->compose()
                            ->setFrom(array('csupport@arabiaface.com' => 'Arabiaface'))
                            ->setTo($sendto)
                            ->setSubject($fullname .' has invited you to join a group '.$groupname.' at '.$groupaddress)
                            ->setHtmlBody('<html>
                                <head>
                                    <meta charset="utf-8" />
                                    <title>Arabiaface</title>
                                </head>
                                <body style="margin:0;padding:0;background:#dfdfdf;">
									<div style="color: #353535; float:left; font-size: 13px;width:100%; font-family:Arial, Helvetica, sans-serif;text-align:center;padding:40px 0 0;">
										<div style="width:600px;display:inline-block;">
										<table style="width:100%;">
										<tbody>
											<tr>
											  <td><img src="https://www.arabiaface.com/frontend/web/images/black-logo.png" style="margin:0 0 10px;width:130px;float:left;"></td>
											</tr>
											<tr style="background:#333;position:relative;width:100%;float:left;padding-top:10px;border-bottom:1px solid #e1e1e1;">
											  <td style="background:#fff;position:relative;width:100%;float:left;padding-top:10px;text-align:center;padding:0;"><h4 style="color:#434343;font-size:14px;font-weight:normal;margin:0;padding:10px 15px;"><span style="font-weight:bold;">'.$fullname.'</span> has invited you to join a group at <span style="font-weight:bold;">'.$groupaddress.'</span></h4></td>
											</tr>
											<tr style="background:#f5f5f5;position:relative;width:100%;float:left;padding:0;">
											  <td style="width:200px;padding:15px;padding-right:0;margin:0;float:left;"><img src="'.$ev_image.'" style="width:100%;"></td>
											  <td style="width:350px;padding:15px;vertical-align:top;float:right;">
												<div style="position:relative;width:100%;text-align:left;font-size:13px;">
												  <span style="color:#3399cc;margin: 0 0 5px;font-weight:bold;font-size:14px;display:inline-block;">'.$groupname.'</span>
												  <br>
												  <span style="margin: 0 0 15px;display:inline-block;">'.$grouptagline.'</span>
												  <br>
												  <span style="color:#999;">'.$attend.' Members</span>
												</div>
											  </td>
											</tr>
											<tr style="background:#fff;width:100%;float:left;padding:10px 0 20px;border-top:1px solid #e1e1e1;">
											  <td style="width:100%;float:left;padding:0;">
												<div style="position:relative;width:100%;padding:0 20px;box-sizing: border-box;text-align:justify;color:#666;clear:both;float:left;font-size:13px;">
													<p style="margin: 0 0 15px;">'.$group_short_desc.'</p>
													<a href="'.$grouplink.'" style="background:#3399cc;font-size:13px;padding:8px 15px;border-radius:3px;float:right;clear:both;color:#fff;text-decoration:none;margin:10px 0 0;">View Group Details</a>
												</div>	
											  </td>
											</tr>
											<tr style="width:100%;float:left;">
												<td style="width:100%;float:left;padding-top:10px;text-align:center;padding:0;margin:10px 0 0;">
													<div style="width:600px;display:inline-block;font-size:11px;">
														   <div style="color: #777;text-align: left;">&copy;  www.arabiaface.com All rights reserved.</div>
														   <div style="text-align: left;width: 100%;margin:5px  0 0;color:#777;">For support, you can reach us directly at <a href="csupport@arabiaface.com" style="color:#4083BF">csupport@arabiaface.com</a></div>
												   </div>
											   </td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</body>
                    </html>')
                            ->send();
                }
                catch (ErrorException $e)
                {
                    return $e->getMessage();
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
	
	public function actionInviteFriendsGroup()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $model = new \frontend\models\LoginForm();
        $word = '';
        $isEmpty = true;
        if(strstr($_SERVER['HTTP_REFERER'],'r=groups'))
        {
            $url = $_SERVER['HTTP_REFERER'];
            $urls = explode('&',$url);
            $url = explode('=',$urls[1]);
            $group_id = $url[1];
        }

        $user_friends = Friend::find()->Where(['to_id'=> $user_id, 'status'=>'1'])->orWhere(['from_id'=> $user_id, 'status'=>'1'])->asarray()->all();
        $ids = array();
        if(!empty($user_friends)) { 
            foreach ($user_friends as $key => $user_friend) {
                if($user_friend['from_id'] == $user_id) {
                    $ids[] = $user_friend['to_id'];
                } else {
                    $ids[] = $user_friend['from_id'];
                }
            }
        }

        if (isset($_GET['key']) && trim($_GET['key']) != '') 
        {
            $word = trim($_GET['key']);
            $data = UserForm::find()->select(['fname', 'lname', 'fullname'])
            ->where(['like','fname', $word])
            ->orwhere(['like','lname', $word])
            ->orwhere(['like','fullname', $word])
            ->andwhere(['in', (string)'_id', $ids])
            ->andwhere(['status' => '1'])
            ->orderBy(['_id'=>SORT_DESC])->asarray()->all();
        } else {
            $data = UserForm::find()->select(['fname', 'lname', 'fullname'])->where(['in', (string)'_id', $ids])->orderBy(['_id'=>SORT_DESC])->asarray()->all();
        }
        ?>
            <div class="list-holder nice-scroll">
            <ul>
                <?php 
                 foreach($data as $sdata) {
                    $friendid = (string)$sdata['_id'];
                    $name = $sdata['fullname'];
                    if($word != '') {
                        $fname = isset($sdata['fname']) ? $sdata['fname'] : '';
                        $lname = isset($sdata['lname']) ? $sdata['lname'] : '';
                        if (stripos($fname, $word) === 0 || stripos($lname, $word) === 0 || stripos($name, $word) === 0) {
                        } else {
                            continue;
                        }
                    }
                    $frndimg = $this->getimage($friendid,'thumb');
                    $attending = GroupMember::find()->where(['group_id' => "$group_id", 'user_id' => "$friendid", 'is_deleted' => '1'])->one();
                    $invitaionsent = Notification::find()->where(['post_id' => "$group_id", 'status' => '1', 'from_friend_id' => "$friendid", 'user_id' => "$user_id", 'notification_type' => 'groupinvite'])->one();
                    $isEmpty = false;
                ?>
                <li class="invite_<?=$friendid?>">
                    <div class="invitelike-friend">
                        <div class="imgholder"><img src="<?=$frndimg?>"/></div>
                        <div class="descholder">
                            <h6><?=$name?></h6>
                            <div class="btn-holder events_<?=$friendid?>">
                                <?php if($attending)
                                {
                                	?>
                                    <label class="infolabel"><i class="zmdi zmdi-check"></i> Member</label>
                                	<?php
                                }
                                else if($invitaionsent)
                                {
                                	?>
                                    <label class="infolabel"><i class="zmdi zmdi-check"></i> Invited</label>
                                	<?php
                                }
                                else
                                { ?>
                                    <a href="javascript:void(0)" class="btn-invite" onclick="sendinvitegroup('<?=$friendid?>','<?=$group_id?>')">Invite</a>
                                    <a href="javascript:void(0)" class="btn-invite-close" onclick="cancelinvitegroup('<?=$friendid?>')"><i class="mdi mdi-close"></i></a>
                                <?php } ?>
                            </div>
                            <div class="btn-holder sendinvitation_<?=$friendid?> dis-none">
                                <label class="infolabel"><i class="zmdi zmdi-check"></i> Invitation sent</label>
                            </div>
                        </div>
                    </div>
                </li>
               <?php } 
                    if($isEmpty == true) {
                       $this->getnolistfound('nofriendfound');
                    }
                ?>
                </ul>
            </div>
            <?php
    }

    public function actionIsMember() {
    	
    	$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['$group_id']) && $_POST['$group_id'] !='') {
		           if(isset($_POST['pageName']) && $_POST['pageName'] !='') {
                		$pagename = $_POST['pageName'];
		                $id = $_POST['$group_id'];
		                if($pagename == 'commevents-details') {
                			$result= EventVisitors::isgroupmemberwithreason($user_id, $id);
		        			return $result;
                		}

                		if($pagename == 'groups-details') {
		                    $result= GroupMember::isgroupmemberwithreason($user_id, $id);
		        			return $result;
                		}
                	}

                }
            }
        }

    } 

    public function actionSetadmin() {
    	$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
		        if(isset($_POST['id']) && $_POST['id'] !='') {
		        	if(isset($_POST['group_id']) && $_POST['group_id'] !='') {
			        	$id = $_POST['id'];
			        	$group_id = $_POST['group_id'];
			        	$result= Group::setadmin($user_id, $id, $group_id);
			        	return $result;
			        }
		        }
		    }
		}
    }

    public function actionLeftgroup() {
    	$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {        
                if(isset($_POST['group_id']) && $_POST['group_id'] !='') {
                    $group_id = $_POST['group_id'];
                    $result = Group::leftgroup($user_id, $group_id);
        			return $result;
                }
            }
        }
    }

    public function actionReportAbuse() {
    	if(isset($_POST['id']) && $_POST['id'] != '') {
    	    if(isset($_POST['label']) && $_POST['label'] != '') {
		    	$session = Yii::$app->session;
		        $user_id = (string)$session->get('user_id');
		        $data = array();
		        $postid = $_POST['id'];
		        $action = $_POST['label'];
		        $action = ucwords(strtolower($action));

		        if(isset($user_id) && $user_id != '') {
		            $data = UserForm::getUserName($user_id);
		            $data = json_decode($data, true);
		        }
		        return $this->render('report_abuse',array('data'=>$data, 'action' => $action, 'id' => $postid));
	    	}
	    }
    } 

    public function actionReportAbuseStored() {
    	$result = false;
    	if(isset($_POST['$id']) && $_POST['$id'] != '') {
    		if(isset($_POST['$reason']) && $_POST['$reason'] != '') {
    			if(isset($_POST['$type']) && $_POST['$type'] != '') {
			    	$session = Yii::$app->session;
			        $user_id = (string)$session->get('user_id');

			        if(isset($user_id) && $user_id != '') {
			        	$post = $_POST;   
			        	$result = Abuse::abuseReportUniq($post, $user_id);
					}    
				}
		    }
		}

		return $result;
    }

    public function actionPreferences() {
    	if(isset($_POST['id']) && $_POST['id'] != '') {
    	    if(isset($_POST['label']) && $_POST['label'] != '') { 
		    	$session = Yii::$app->session;
		        $user_id = (string)$session->get('user_id');
		        $data = array();
		        $postid = $_POST['id']; 
		        $action = $_POST['label']; 
		        $action = strtolower($action);

		        if(isset($user_id) && $user_id != '') { 
		            $data = Preferences::getinfo($user_id, $action, $postid);
		            $data = json_decode($data, true);
		        }

		        return $this->render('preferences',array('data'=>$data, 'postid' => $postid, 'action' => $action));
	    	}
	    }
    }  

    public function actionPreferencesSave() {
    	$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
		    	if(isset($_POST) && !empty($_POST)) {
			    	$session = Yii::$app->session;
			        $user_id = (string)$session->get('user_id');
			        $post = $_POST;
					
					$result = Preferences::preferencessave($user_id, $post);
			        return $result;
			    }
		    } 
		}
	}

    public function actionPreferencesStored() {
    	$result = false;
    	if(isset($_POST['$id']) && $_POST['$id'] != '') {
    		if(isset($_POST['$reason']) && $_POST['$reason'] != '') {
    			if(isset($_POST['$type']) && $_POST['$type'] != '') {
			    	$session = Yii::$app->session;
			        $user_id = (string)$session->get('user_id');

			        if(isset($user_id) && $user_id != '') {
			        	$post = $_POST;   
			        	$result = Abuse::abuseReportUniq($post, $user_id);
					}    
				}
		    }
		}

		return $result;
    }

    public function actionFlagGroup()
    { 
        $session = Yii::$app->session;
        $group_id = (string)$_POST['group_id'];
        $group_owner_id = (string)$_POST['g_uid'];
        $user_id = (string)$session->get('uid');
        $desc = $_POST['desc'];
        $delete = new Group();
        $delete = Group::find()->where(['_id' => $group_id])->one();
        $delete->is_deleted = '2';
        $delete->update();
		
		$date = time(); 
		$notification =  new Notification();
		$notification->post_id = "$group_id";
		$notification->user_id = "$group_owner_id";
		$notification->notification_type = 'deletegroupadmin';
		$notification->is_deleted = '0';
		$notification->status = '0';
		$notification->created_date = "$date";
		$notification->updated_date = "$date";
		$notification->flag_reason = "$desc";
		$notification->insert();
		return true;
    }

    public function actionPublishGroup() 
	{
		$g_id = $_POST['g_id'];
		$date = time();
		$record = Group::find()->where(['_id' => $g_id,'is_deleted' => '2'])->one();
		$record->is_deleted = '1';
		$record->update();
		
		$group_owner_id = $record['user_id'];
		$notification =  new Notification();
		$notification->post_id = "$g_id";
		$notification->user_id = "$group_owner_id";
		$notification->notification_type = 'publishgroup';
		$notification->is_deleted = '0';
		$notification->status = '1';
		$notification->created_date = "$date";
		$notification->updated_date = "$date";
		$notification->insert();
		return true;
	}

	public function actionNewCreateGroup() 
    {
    	return $this->render('create_group');
    }
    public function actionNewCreateEvent() 
    {
    	return $this->render('create_event');
    }

    public function actionEditGroup() 
    {
    	if(isset($_POST['$id']) && $_POST['$id'] != '') {
	    	$session = Yii::$app->session;
	        $user_id = (string)$session->get('user_id');
	     	$id = $_POST['$id'];   
	        if(isset($user_id) && $user_id != '') {
	            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
	            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
	            	return $this->render('editgroup', array('group_id' => $id, 'checkuserauthclass' => $checkuserauthclass));
	            }
	        }
	    }
    }

    public function actionGetprivacydata() 
    {
    	$result = array();
    	$url = $_SERVER['HTTP_REFERER'];
        $urls = explode('&',$url);
        $url = explode('=',$urls[1]); 
        $entity_id = $url[1];
        $result = array('status' => false);
        if($entity_id != '') {
        	$session = Yii::$app->session;
        	$user_id = (string)$session->get('user_id');
        	$data = Group::find()->where([(string)'_id' => $entity_id, 'is_deleted' => '1'])->one();
        	if(!empty($data)) {
        		if(isset($data['privacy']) && $data['privacy'] == 'Custom') {
        			$customids = $data['customids'];
        			$customids = explode(',', $customids);
        			if(!empty($customids)) {
        				$result['ids'] = $customids; 
        				$result['status'] = true; 
        			}
        		}
        	}
        }

        return json_encode($result, true);

	}
}	

