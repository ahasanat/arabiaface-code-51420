<?php
namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use frontend\models\LoginForm;
use frontend\models\Like;
use frontend\models\Page;
use frontend\models\Notification;
use frontend\models\ReadNotification;
use frontend\models\HideNotification;
use frontend\models\EventVisitors;
use frontend\models\Friend;
use frontend\models\UserForm;
use frontend\models\Group;
use frontend\models\PageEvents;

class NotificationController extends Controller
{
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
	
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionNewNotificationCount() {
        $session = Yii::$app->session;
        $userid = (string)$session->get('user_id');
        $uxi = array('status' => false, 'notcount' => '', 'frdcount' => '');

        if($userid != '' && $userid != 'undefined') {
            $uxi = array('status' => true);
            $notInfo = Notification::getUserPostBudge();
            if($notInfo > 0) {
                $uxi['notcount'] = $notInfo;
            }
            $frdInfo = Friend::friendRequestbadge();
            if($frdInfo > 0) {
                $uxi['frdcount'] = $frdInfo;
            }
        }

        return json_encode($uxi, true);
    }
 
    public function actionNewNotification()
    {    
        $session = Yii::$app->session;
        $userid = (string)$session->get('user_id');
        $assetsPath = '../../vendor/bower/travel/images/';

        if($userid != '' && $userid != 'undefined') {
            $budge = 0;
            $budge = Notification::getUserPostBudge();
            $notifications = Notification::getAllNotification();
            ?>
            <input type="hidden" name="new_budge" id= "new_budge" value="<?=$budge;?>"/>
            <span class="not-title">Notifications</span> 
            <?php if(!empty($notifications) && count($notifications)>0){ ?>
            <div class="not-resultlist nice-scroll">
                <ul class="noti-listing">
                    <?php $notcnt = 0;
                        foreach($notifications as $notification)
                        {
                            $notificationId = (string)$notification["_id"];
                            $liked = Like::getPageLike($notification['page_id']);
                            if((isset($notification['page_id']) && !empty($notification['page_id']) && $liked && $notification['notification_type'] == 'post' && $liked['updated_date'] < $notification['updated_date']) || (($notification['notification_type'] == 'likepost' || $notification['notification_type'] == 'comment' || $notification['notification_type'] == 'pagereview' || $notification['notification_type'] == 'onpagewall') && $notification['page_id'] != null) || $notification['page_id'] == null) {
                            $friend_user_id = $notification['user_id'];
                            $is_friend = Friend::find()->where(['from_id' => "$userid",'to_id' => "$friend_user_id",'status' => '1'])->one();
                            $friendon = $is_friend['updated_date'];
                            $nottime = $notification['updated_date'];
                            $xcollection_owner_id = $notification['collection_owner_id'];
                            $xcollection_id = $notification['collection_id'];
                            $xevent_owner_id = $notification['event_owner_id'];
                            $xevent_id = $notification['event_id'];
                            $group_owner_id = $notification['group_owner_id'];
                            $xgroup_id = $notification['group_id'];

                            if($friendon <= $nottime) {
                            if($notification['notification_type'] == 'sharepost')
                            {
                                $not_img = $this->getimage($notification['shared_by'],'thumb');
                            }
                            else if($notification['notification_type'] == 'deletepostadmin' || $notification['notification_type'] == 'publishpost' || $notification['notification_type'] == 'deletecollectionadmin' || $notification['notification_type'] == 'publishcollection' || $notification['notification_type'] == 'deletepageadmin' || $notification['notification_type'] == 'publishpage' || $notification['notification_type'] == 'deleteeventadmin' || $notification['notification_type'] == 'publishevent' || $notification['notification_type'] == 'deletegroupadmin' || $notification['notification_type'] == 'publishgroup')
                            {
                                $not_img = $this->getimage('admin', 'thumb');
                            }
                            else if($notification['notification_type'] == 'editpostuser')
                            {
                                $not_img = $this->getimage($notification['post']['post_user_id'],'thumb');
                            }
                            else if ($notification['notification_type'] == 'editcollectionuser')
                            {
                                 $not_img = $this->getimage($notification['collection_owner_id'],'thumb');
                            }
                            else if($notification['notification_type'] == 'onpagewall')
                            {
                                $not_img = $this->getimage($notification['user_id'],'thumb');
                            }
                            else if($notification['notification_type'] == 'low_credits')
                            {
                                $not_img = $this->getimage($notification['user_id'],'thumb');
                            }
                            else if($notification['notification_type'] == 'followcollection' || $notification['notification_type'] == 'sharecollection' || $notification['notification_type'] == 'shareevent' || $notification['notification_type'] == 'addpostevent' || $notification['notification_type'] == 'addphotoevent'|| $notification['notification_type'] == 'attendevent' || $notification['notification_type'] == 'eventinvited' || $notification['notification_type'] == 'sharegroup' || $notification['notification_type'] == 'addpostgroup' || $notification['notification_type'] == 'addphotogroup' || $notification['notification_type'] == 'becomegroupmember' || $notification['notification_type'] == 'groupinvite')
                            {
                                $not_img = $this->getimage($notification['user_id'],'thumb');
                            }
                            else if($notification['notification_type'] == 'groupinvitedbyother')
                            {
                                $not_img = $this->getimage($notification['from_friend_id'],'thumb');
                            }
                            else if($notification['notification_type'] == 'eventinvitedbyother')
                            {
                                $not_img = $this->getimage($notification['from_friend_id'],'thumb');
                            }
                            else if ($notification['notification_type'] == 'groupadmin')
                            {
                                $not_img = $this->getimage($notification['from_friend_id'],'thumb');
                            }
                            else if ($notification['notification_type'] == 'groupadminrequestresult')
                            {
                                $not_img = $this->getimage($notification['from_friend_id'], 'thumb');
                            }
                            else if ($notification['notification_type'] == 'eventorganizer')
                            {
                                $not_img = $this->getimage($notification['from_friend_id'],'thumb');
                            }
                            else if ($notification['notification_type'] == 'eventorganizerrequestresult')
                            {
                                $not_img = $this->getimage($notification['from_friend_id'], 'thumb');
                            }
                            else
                            {
                                if(isset($notification['page_id']) && !empty($notification['page_id']) && $notification['notification_type'] == 'post')
                                {
                                    if($_SERVER['HTTP_HOST'] == 'localhost')
                                    {
                                        $baseUrll = '/arabiaface-code/frontend/web';
                                    }
                                    else
                                    {
                                        $baseUrll = '/frontend/web/assets/baf1a2d0';
                                    }
                                    $not_img = $this->getpageimage($notification['page_id']);
                                }
                                else
                                {
                                    $not_img = $this->getimage($notification['user']['_id'],'thumb');
                                }
                            }
                            if(isset($notification['post']['post_text']) && !empty($notification['post']['post_text'])) {
                                if(strlen($notification['post']['post_text']) > 20){
                                    $notification['post']['post_text'] = substr($notification['post']['post_text'],0,20);
                                    $notification['post']['post_text'] = substr($notification['post']['post_text'], 0, strrpos($notification['post']['post_text'], ' '));
                                }
                                else{
                                    $notification['post']['post_text'] = $notification['post']['post_text'];
                                }
                            }
                            if(empty($notification['post']['post_text']) && $notification['notification_type'] != 'likereply') {
                                if($notification['notification_type']!='friendrequestaccepted' && $notification['notification_type']!='friendrequestdenied' && $notification['notification_type']!='pageinvite' && $notification['notification_type']!='eventinvite' && $notification['notification_type']!='eventgoing' && $notification['notification_type']!='pageinvitereview' && $notification['notification_type']!='likepage')
                                {
                                    //$notification['post']['post_text'] = 'View Post';
                                }
                            } 
                            if($notification['notification_type'] == 'tag_friend' || $notification['notification_type'] == 'eventinvited' || $notification['notification_type'] == 'groupinvite')
                            {
                                $name = 'You';
                            }
                            else if($notification['notification_type'] == 'groupinvitedbyother')
                            {
                                 $name = $this->getuserdata($notification['from_friend_id'],'fullname');
                            }
                            else if($notification['notification_type'] == 'eventinvitedbyother')
                            {
                                 $name = $this->getuserdata($notification['from_friend_id'],'fullname');
                            }
                            else if($notification['notification_type'] == 'low_credits' || $notification['notification_type'] == 'followcollection' || $notification['notification_type'] == 'sharecollection' || $notification['notification_type'] == 'shareevent' || $notification['notification_type'] == 'addpostevent' || $notification['notification_type'] == 'addphotoevent' || $notification['notification_type'] == 'attendevent' || $notification['notification_type'] == 'sharegroup' || $notification['notification_type'] == 'addpostgroup' || $notification['notification_type'] == 'addphotogroup' || $notification['notification_type'] == 'becomegroupmember')
                            {
                                $name = 'Your';
                            }
                            else if($notification['notification_type'] == 'deletepostadmin' || $notification['notification_type'] == 'publishpost' || $notification['notification_type'] == 'deletecollectionadmin' || $notification['notification_type'] == 'publishcollection' || $notification['notification_type'] == 'deletepageadmin' || $notification['notification_type'] == 'publishpage' || $notification['notification_type'] == 'deleteeventadmin' || $notification['notification_type'] == 'publishevent' || $notification['notification_type'] == 'deletegroupadmin' || $notification['notification_type'] == 'publishgroup')
                            {
                                $name = 'Arabiaface Admin';
                            }
                            else if($notification['notification_type'] == 'editpostuser')
                            {
                                $name = $this->getuserdata($notification['post']['post_user_id'],'fullname');
                            }
                            else if ($notification['notification_type'] == 'editcollectionuser')
                            {
                                $name = $this->getuserdata($notification['collection_owner_id'],'fullname');
                            }
                            else if($notification['notification_type'] == 'sharepost')
                            {
                                $usershare = LoginForm::find()->where(['_id' => $notification['user_id']])->one();
                                $usershare_id = $usershare['_id'];
                                if($notification['user_id'] == $userid){$user_name = 'Your';}else{ $user_name = $usershare['fullname']; }

                                $post_owner_id = LoginForm::find()->where(['_id' => $notification['post_owner_id']])->one();
                                $post_owner_id_name_id = $post_owner_id['_id'];
                                if($notification['post_owner_id'] == $userid){$post_owner_id_name = 'Your';}else{ $post_owner_id_name = $post_owner_id['fullname'].'\'s'; }

                                $shared_by = LoginForm::find()->where(['_id' => $notification['shared_by']])->one();
                                $shared_by_name_id = $shared_by['_id'];
                                if($notification['shared_by'] == $userid){$shared_by_name = 'You';}else{ $shared_by_name = $shared_by['fullname']; }
                                $name = "";
                                $name .= "<span class='btext'>";
                                $name .= $shared_by_name;
                                $name .= "</span> Shared <span class='btext'>";
                                $name .= $post_owner_id_name;
                                $name .= "</span> Post on <span class='btext'>";
                                $name .= $user_name;
                                $name .= "</span> Wall: ";
                            }
                            else
                            {
                                if(isset($notification['page_id']) && !empty($notification['page_id']) && $notification['notification_type'] == 'post')
                                {
                                    $page_id = Page::Pagedetails($notification['page_id']);
                                    $name = $page_id['page_name'];
                                }
                                else
                                {
                                    $name = ucfirst($notification['user']['fname']).' '.ucfirst($notification['user']['lname']);
                                }
                            }
                            $notification_time = Yii::$app->EphocTime->time_elapsed_A(time(),$notification['updated_date']);
                            $npostid = $notification['post_id'];
                            $fromid = $notification['user_id'];
                    ?>
                    <li id="noti_<?=$notificationId?>">
                        <div class="noti-holder">
                            <?php if($notification['notification_type'] == 'friendrequestaccepted') { ?>
                                <a href="<?=Url::to(['userwall/index', 'id' => "$fromid"])?>">
                            <?php } 
                            
                            if($notification['notification_type'] == 'low_credits') { ?>
                                <a href="<?=Url::to(['site/credits']);?>">
                            <?php } else if($notification['notification_type'] == 'followcollection') { ?>
                                <a href="<?=Url::to(['collection/detail', 'col_id'=> "$xcollection_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'sharecollection') { ?>
                                <a href="<?=Url::to(['collection/detail', 'col_id'=> "$xcollection_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'shareevent') { ?>
                                <a href="<?=Url::to(['event/detail', 'e'=> "$xevent_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'addpostevent') { ?>
                                <a href="<?=Url::to(['site/travpost', 'postid'=> $notification['post_id'] ]);?>">
                            <?php } else if($notification['notification_type'] == 'addphotoevent') { ?>
                                <a href="<?=Url::to(['event/detail', 'e'=> "$xevent_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'attendevent') { ?>
                                <a href="<?=Url::to(['event/detail', 'e'=> "$xevent_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'eventinvited') { ?>
                                <a href="<?=Url::to(['event/detail', 'e'=> "$xevent_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'sharegroup') { ?>
                                <a href="<?=Url::to(['groups/detail', 'group_id'=> "$xgroup_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'addpostgroup') { ?>
                                <a href="<?=Url::to(['site/travpost', 'postid'=> $notification['post_id'] ]);?>">
                            <?php } else if($notification['notification_type'] == 'addphotogroup') { ?>
                                <a href="<?=Url::to(['groups/detail', 'group_id'=> "$xgroup_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'becomegroupmember') { ?>
                                <a href="<?=Url::to(['groups/detail', 'group_id'=> "$xgroup_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'groupinvite') { ?>
                                <a href="<?=Url::to(['groups/detail', 'group_id'=> "$xgroup_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'groupinvitedbyother') { ?>
                                <a href="<?=Url::to(['groups/detail', 'group_id'=> "$xgroup_id" ]);?>">
                            <?php } else if($notification['notification_type'] == 'eventinvitedbyother') { ?>
                                <a href="<?=Url::to(['event/detail', 'e'=> "$xevent_id" ]);?>">
                            <?php }
                            else if($notification['notification_type'] == 'pageinvite' || $notification['notification_type'] == 'pageinvitereview' || $notification['notification_type'] == 'likepage' || $notification['entity'] == 'page' || $notification['notification_type'] == 'page_role_type') {
                                if($notification['entity'] == 'page') { $npostid = $notification['page_id']; } ?>
                                <a href="<?=Url::to(['page/index', 'id' => "$npostid"]);?>">
                            <?php } else if($notification['notification_type'] == 'eventinvite' || $notification['notification_type'] == 'eventgoing') { ?>
                                <a href="<?=Url::to(['event/detail', 'e' => "$npostid"]);?>">
                            <?php } else if($notification['notification_type'] == 'deletecollectionadmin' || $notification['notification_type'] == 'editcollectionuser' || $notification['notification_type'] == 'publishcollection') { ?>
                                <a href="<?=Url::to(['collection/detail', 'col_id' => "$npostid"]);?>">
                            <?php } else if($notification['notification_type'] == 'deletepageadmin' || $notification['notification_type'] == 'editpageuser' || $notification['notification_type'] == 'publishpage') { ?>
                                <a href="<?=Url::to(['page/index', 'id' => "$npostid"]);?>">    
                            <?php } else if($notification['notification_type'] == 'deleteeventadmin' || $notification['notification_type'] == 'editeventuser' || $notification['notification_type'] == 'publishevent') { ?>
                                <a href="<?=Url::to(['event/detail', 'e' => "$npostid"]);?>">   
                            <?php } else if($notification['notification_type'] == 'deletegroupadmin' || $notification['notification_type'] == 'editgroupuser' || $notification['notification_type'] == 'publishgroup') { ?>
                                <a href="<?=Url::to(['groups/detail', 'group_id' => "$npostid"]);?>">   
                            <?php } else if($notification['notification_type'] == 'groupadmin') { $frnid = $notification['from_friend_id'];?>
                                <a href="javascript:void(0)">
                            <?php } else if($notification['notification_type'] == 'groupadminrequestresult') { $frnid = $notification['from_friend_id'];?>
                                <a href="<?=Url::to(['userwall/index', 'id' => "$frnid"]);?>">
                             <?php } else if($notification['notification_type'] == 'eventorganizer') { $frnid = $notification['from_friend_id'];?>
                                <a href="javascript:void(0)">
                             <?php } else if($notification['notification_type'] == 'eventorganizerrequestresult') { $frnid = $notification['from_friend_id'];?>
                                <a href="<?=Url::to(['userwall/index', 'id' => "$frnid"]);?>">
                            <?php } else { ?>
                                <a href="<?=Url::to(['site/travpost', 'postid' => "$npostid"]);?>">
                            <?php } ?>
                                <span class="img-holder">
                                    <img class="img-responsive" src="<?= $not_img ?>">
                                </span>
                                <span class="desc-holder">
                                    <span class="desc">
                                        <?php if($notification['notification_type'] != 'sharepost' && $notification['notification_type'] != 'groupadmin' && $notification['notification_type'] != 'groupadminrequestresult' && $notification['notification_type'] != 'eventorganizerrequestresult' && $notification['notification_type'] != 'eventorganizer') { ?> <span class="btext"><?=$name;?></span><?php } ?>
                                        <?php if($notification['notification_type']=='likepost' || $notification['notification_type']== 'like'){ ?> Likes your post: <?=$notification['post']['post_text'];?>
                                        <?php } else if($notification['notification_type']=='likecomment'){ ?> Likes your comment: View Post
                                        <?php } else if($notification['notification_type'] == 'sharepost'){ ?> <?=$name;?> <?=$notification['post']['post_text'];?>
                                        <?php } else if($notification['notification_type'] == 'comment'){  
                                                     if($notification['post_owner_id'] == "$userid"){ ?> Commented on your post: <?php } else {  ?>Commented on the post you are Tagged in: <?=$notification['post']['post_text']; } ?>
                                        <?php } else if($notification['notification_type'] == 'tag_friend'){ ?> Tagged in the post: <?=$notification['post']['post_text'];?>
                                        <?php } else if($notification['notification_type'] == 'post'){ ?>
                                        Added new post: <?=$notification['post']['post_text'];?>
                                        <?php } else if($notification['notification_type'] == 'commentreply'){ ?> Replied on your comment: <?=$notification['post']['post_text'];?>
                                        <?php } else if($notification['notification_type'] == 'friendrequestaccepted'){ ?> Accepted your friend request.
                                        <?php } else if($notification['notification_type'] == 'friendrequestdenied'){ ?> Denied your friend request.
                                        <?php } else if($notification['notification_type'] == 'onwall'){ ?> Write on your wall.
                                        <?php } else if($notification['notification_type'] == 'pageinvitereview'){
                                        $page_info = Page::Pagedetails($npostid);
                                        ?> Invited to review <?=$page_info['page_name']?> page.
                                        <?php } else if($notification['notification_type'] == 'low_credits'){
                                        ?> Credit is Tipping low.
                                        <?php } else if($notification['notification_type'] == 'followcollection'){
                                            $name_collection_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Collection is Followed By <?=$name_collection_owner?>.
                                        <?php } else if($notification['notification_type'] == 'sharecollection'){
                                            $name_collection_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Collection is Shared By <?=$name_collection_owner?>.
                                        <?php } else if($notification['notification_type'] == 'shareevent'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Event is Shared By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'addpostevent'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Event has New Post By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'addphotoevent'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Event has New Photo By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'attendevent'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Event is Attending By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'eventinvited'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Are Invited To Attend Event By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'sharegroup'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Group is Shared By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'addpostgroup'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Group has New Post By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'addphotogroup'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Group has New Photo By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'becomegroupmember'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Group is Joined By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'groupinvite'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Are Invited to Join The Group By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'groupinvitedbyother'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Is Invited to Join Your Group By <?=$name_event_owner?>.
                                        <?php } else if($notification['notification_type'] == 'eventinvitedbyother'){
                                            $name_event_owner = $this->getuserdata($notification['user_id'],'fullname');
                                        ?> Is Invited to Join Your Event By <?=$name_event_owner?>.
                                        <?php }
                                        else if($notification['notification_type'] == 'pagereview'){
                                        $page_info = Page::Pagedetails($npostid);
                                        ?> Reviewed <?=$page_info['page_name']?> page.
                                        <?php } else if($notification['notification_type'] == 'pageinvite'){
                                        $page_info = Page::Pagedetails($npostid);
                                        ?> Invited to like <?=$page_info['page_name']?> page.
                                        <?php } else if($notification['notification_type'] == 'eventinvite'){
                                        $page_info = PageEvents::getEventdetails($npostid);
                                        ?> Invited to attend <?=$page_info['event_name']?> event.
                                        <?php } else if($notification['notification_type'] == 'eventgoing'){
                                        $page_info = PageEvents::getEventdetails($npostid);
                                        ?> will attend <?=$page_info['event_name']?> event.
                                        <?php } else if($notification['notification_type'] == 'likepage'){
                                        $page_info = Page::Pagedetails($npostid);
                                        ?> Liked <?=$page_info['page_name']?> page.
                                        <?php } else if($notification['notification_type'] == 'onpagewall'){
                                        $page_info = Page::Pagedetails($npostid);
                                        ?> Write on <?=$page_info['page_name']?> page.
                                        <?php } else if($notification['notification_type'] == 'grouprequest'){
                                        $page_info = Group::groupdetail($npostid); ?>
                                        <?php } else if($notification['notification_type'] == 'groupadmin') {
                                        
                                            $isflag = false;
                                            $flag_reason = '';
                                            if($notification['status'] == '0') {
                                                $isflag = true;
                                                $flag_reason = $notification['flag_reason'];
                                            }

                                            $page_info = Group::groupadminrequesthtml((string)$notification['_id'], $npostid, $notification['from_friend_id'], $isflag, $flag_reason);
                                            ?>
                                            <?=$page_info?>
                                        <?php } else if($notification['notification_type'] == 'groupadminrequestresult'){
                                            $page_info = Group::groupadminrequestresult((string)$notification['_id'], $npostid, $notification['from_friend_id'], $notification['flag_reason']);
                                            ?>
                                            <?=$page_info;?>
                                        <?php } else if($notification['notification_type'] == 'eventorganizer'){
                                            $isflag = false;
                                            $flag_reason = '';
                                            if($notification['status'] == '0' || $notification['status'] == false) {
                                                $isflag = true;
                                                $flag_reason = $notification['flag_reason'];
                                            }

                                            $page_info = EventVisitors::eventorganizerrequesthtml((string)$notification['_id'], $npostid, $notification['from_friend_id'], $isflag, $flag_reason);
                                            ?>
                                            <?=$page_info?>
                                        <?php } else if($notification['notification_type'] == 'eventorganizerrequestresult'){
                                            $page_info = EventVisitors::eventorganizerrequestresult((string)$notification['_id'], $npostid, $notification['from_friend_id'], $notification['flag_reason']);
                                            ?>
                                            <?=$page_info?>
                                        <?php } else if($notification['notification_type'] == 'groupjoin'){
                                        $page_info = Group::groupdetail($npostid);
                                        ?> Accepted <?=$page_info['name']?> group request.
                                        <?php } else if($notification['notification_type'] == 'deletepostadmin'){
                                        ?> Flaged your post for <?=$notification['flag_reason']; ?>.
                                        <?php } else if($notification['notification_type'] == 'deletecollectionadmin'){
                                        ?> Flaged your collection for <?=$notification['flag_reason']; ?>.
                                        <?php } else if($notification['notification_type'] == 'deletepageadmin'){
                                        ?> Flaged your page for <?=$notification['flag_reason']; ?>.
                                        <?php } else if($notification['notification_type'] == 'deleteeventadmin'){
                                        ?> Flaged your event for <?=$notification['flag_reason']; ?>.
                                        <?php } else if($notification['notification_type'] == 'deletegroupadmin'){
                                        ?> Flaged your group for <?=$notification['flag_reason']; ?>.
                                        <?php } else if($notification['notification_type'] == 'editpostuser'){
                                        ?> has edited flaged post.
                                        <?php } else if($notification['notification_type'] == 'editcollectionuser'){
                                        ?> has edited flaged collection.
                                        <?php } else if($notification['notification_type'] == 'publishpost'){
                                        ?> Approved your post.
                                        <?php } else if($notification['notification_type'] == 'publishcollection'){
                                        ?> Approved your collection.
                                        <?php } else if($notification['notification_type'] == 'publishpage'){
                                        ?> Approved your Page.
                                        <?php } else if($notification['notification_type'] == 'publishevent'){
                                        ?> Approved your Event.
                                        <?php } else if($notification['notification_type'] == 'publishgroup'){
                                        ?> Approved your Group.
                                        <?php } else if($notification['notification_type'] == 'page_role_type'){
                                            $page_info = Page::Pagedetails($npostid);
                                            if($notification['status'] == '0'){$lblrole = 'Removed';}else{$lblrole = 'Added';}
                                        ?> <?=$lblrole?> you as <?=$notification['page_role_type']?> for <?=$page_info['page_name']?> page.
                                        <?php } else{ ?> Likes post<?php } ?>
                                    </span>
                                    <span class="notif-icon">
                                        <?php if($notification['notification_type']=='likepost' || $notification['notification_type']== 'like' || $notification['notification_type']== 'likepage'){ ?><img class="img-responsive" src="<?=$assetsPath?>post-like.png">
                                        <?php } else if($notification['notification_type']== 'comment') {?> <img class="img-responsive" src="<?=$assetsPath?>post-comment.png">
                                        <?php }else if($notification['notification_type'] == 'sharepost'){ ?><i class="mdi mdi-share-variant"></i> 
                                        <?php }else if($notification['notification_type'] == 'pageinvite' || $notification['notification_type'] == 'eventinvite'){ ?><img class="img-responsive" src="<?=$assetsPath?>post-like.png"> 
                                        <?php }else if($notification['notification_type'] == 'pageinvitereview'){ ?><i class="mdi mdi-pencil"></i> 
                                        <?php }else if($notification['notification_type'] == 'pagereview'){ ?><i class="mdi mdi-pencil-square"></i> 
                                        <?php }else if($notification['notification_type'] == 'eventgoing'){ ?><i class="mdi mdi-calendar-check-o"></i> 
                                        <?php }else { ?><img class="img-responsive" src="<?=$assetsPath?>post-common.png"> <?php }?> 
                                        
                                        <span class="time-stamp">
                                            <?= $notification_time;?>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </li>
            <?php $notcnt++; } } } ?>
            </ul>        
            </div>
            <?php if($notcnt > 0){ ?>
            <span class="not-result bshadow seemorenoti">
                <a href="<?=Url::to(['site/travnotifications']); ?>">See More Notifications <i class="mdi mdi-menu-right"></i></a>
            </span>
            <?php } else { ?>
                <?php $this->getnolistfound('nonotificationfound');?>
            <?php } ?>
            <?php } else { ?>
                <?php $this->getnolistfound('nonotificationfound');?>
            <?php }
        }
    }
    
    public function actionViewNotification()
    { 
        $session = Yii::$app->session;
        $uid = $session->get('user_id');
       
        $date = time();
        $data = array();
        $user_data = LoginForm::find()->where(['_id' => "$uid"])->one();
               
        if(!empty($user_data)) {
            $date = time();
            $user_data->last_logout_time = "$date";
            $user_data->update();
        }
        return 'done';
    }
    
    public function actionMarkasread()
    {
        if (isset($_POST['nid']) && !empty($_POST['nid'])) {
            $not_id = (string) $_POST["nid"];
            $session = Yii::$app->session;
            $user_id = (string) $session->get('user_id');
            $readnot = new ReadNotification();
            $userexist = ReadNotification::find()->where(['user_id' => $user_id])->one();
            if ($userexist)
            {
                if (strstr($userexist['notification_ids'], $not_id))
                {
                    $readnot = ReadNotification::find()->where(['user_id' => $user_id])->one();
                    $readnot->notification_ids = str_replace($not_id.',', '', $userexist['notification_ids']);
                    if ($readnot->update())
                    {
                        if(strlen($readnot['notification_ids'] < 11))
                        {
                            $readnot->delete();
                        }
                        print true;
                    }
                    else
                    {
                        print false;
                    }
                }
                else
                {
                    $readnot = ReadNotification::find()->where(['user_id' => $user_id])->one();
                    $readnot->notification_ids = $userexist['notification_ids'] . $not_id . ',';
                    if ($readnot->update())
                    {
                        print true;
                    }
                    else
                    {
                        print false;
                    }
                }
            }
            else
            {
                $readnot->user_id = $user_id;
                $readnot->notification_ids = $not_id . ',';
                if ($readnot->insert())
                {
                    print true;
                }
                else
                {
                    print false;
                }
            }
        }
    }
    
    public function actionHidenotification()
    {
        if (isset($_POST['nid']) && !empty($_POST['nid'])) { 
            $not_id = (string) $_POST["nid"];
            $session = Yii::$app->session;
            $user_id = (string) $session->get('user_id');
            $hidenot = new HideNotification(); 
            $userexist = HideNotification::find()->where(['user_id' => $user_id])->one();
            if (!empty($userexist)) {
                $notificationArray = $userexist['notification_ids'];
                $notificationArray = explode(',', $notificationArray);
                $notificationArray = array_filter($notificationArray);
                
                if(in_array($not_id, $notificationArray)) {
                    return true;
                } else {
                    $notificationArray[] = $not_id;
                    $hidenot = HideNotification::find()->where(['user_id' => $user_id])->one();
                    $hidenot->notification_ids = implode(",", $notificationArray);
                    if ($hidenot->update()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                $hidenot->user_id = $user_id;
                $hidenot->notification_ids = $not_id;
                if ($hidenot->insert()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
    
    public function actionGroupadminrequestdelete() {
        if(isset($_POST['$id']) && $_POST['$id'] != '') {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');
            
            if(isset($user_id) && $user_id != '') {
                $checkuserauthclass = UserForm::isUserExistByUid($user_id);
                if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { 
                    $id = $_POST['$id'];
                    $data = Group::groupadminrequestdelete($id, $user_id);
                    return $data;
                }
            }
        }
    }    

    public function actionGroupadminrequestaccept() {
        if(isset($_POST['$id']) && $_POST['$id'] != '') {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');
            
            if(isset($user_id) && $user_id != '') {
                $checkuserauthclass = UserForm::isUserExistByUid($user_id);
                if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { 
                    $id = $_POST['$id'];
                    $data = Group::groupadminrequestaccept($id, $user_id);
                    return $data;
                }
            }
        }
    }    

    public function actionEventorganizerrequestdelete() {
        if(isset($_POST['$id']) && $_POST['$id'] != '') {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');
            
            if(isset($user_id) && $user_id != '') {
                $checkuserauthclass = UserForm::isUserExistByUid($user_id);
                if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { 
                    $id = $_POST['$id'];
                    $data = EventVisitors::eventorganizerrequestdelete($id, $user_id);
                    return $data;
                }
            }
        }
    }    

    public function actionEventorganizerrequestaccept() {
        if(isset($_POST['$id']) && $_POST['$id'] != '') {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');
            
            if(isset($user_id) && $user_id != '') {
                $checkuserauthclass = UserForm::isUserExistByUid($user_id);
                if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { 
                    $id = $_POST['$id'];
                    $data = EventVisitors::eventorganizerrequestaccept($id, $user_id);
                    return $data;
                }
            }
        }
    }    
}
