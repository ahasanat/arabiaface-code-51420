<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\mongodb\ActiveRecord;
use frontend\models\Snapit;

class SnapitController extends Controller
{
   public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
        
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
                'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];           
    }
 
    public function actionIndex()  
    { 
        return $this->render('index');
    }

    public function actionAddgallery()  {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $result = array('success' => false);
        if(isset($user_id) && $user_id != '') {
            if(isset($_POST['photoUpload']) && !empty($_POST['photoUpload'])) {
                $photoUpload = json_decode($_POST['photoUpload'], true);
                if(isset($_FILES['imageFile1']['name']) && !empty($_FILES['imageFile1']['name'])) {
                    $files = $_FILES['imageFile1'];
                    $filesCount = count($files['name']);

                    $place = isset($_POST['place']) ? $_POST['place'] : '';
                    $placetitle = isset($_POST['placetitle']) ? $_POST['placetitle'] : '';

                    $filterGalleryArray = array();

                    for ($i=0; $i < $filesCount; $i++) { 
                        $filterGalleryArray[$i] = array();

                        if(isset($photoUpload[$i])) {
                            $uploadpopupJIDSphototitle = isset($photoUpload[$i]['$uploadpopupJIDSphototitle']) ? $photoUpload[$i]['$uploadpopupJIDSphototitle'] : '';
                            $uploadpopupJIDSdescription = isset($photoUpload[$i]['$uploadpopupJIDSdescription']) ? $photoUpload[$i]['$uploadpopupJIDSdescription'] : '';
                            $uploadpopupJIDSlocation = isset($photoUpload[$i]['$uploadpopupJIDSlocation']) ? $photoUpload[$i]['$uploadpopupJIDSlocation'] : '';
                            $uploadpopupJIDStaggedfriends = isset($photoUpload[$i]['$uploadpopupJIDStaggedfriends']) ? $photoUpload[$i]['$uploadpopupJIDStaggedfriends'] : array();
                            if(!empty($uploadpopupJIDStaggedfriends)) {
                                $uploadpopupJIDStaggedfriends = implode(',', $uploadpopupJIDStaggedfriends);
                            } else {
                                $uploadpopupJIDStaggedfriends = '';
                            }

                            $uploadpopupJIDSvisibleto = isset($photoUpload[$i]['$uploadpopupJIDSvisibleto']) ? $photoUpload[$i]['$uploadpopupJIDSvisibleto'] : '';
                            $uploadpopupJIDScategories = isset($photoUpload[$i]['$uploadpopupJIDScategories']) ? $photoUpload[$i]['$uploadpopupJIDScategories'] : '';

                            for ($k=0; $k <= $i; $k++) { 
                                if($uploadpopupJIDSphototitle != '') {
                                    if(isset($filterGalleryArray[$k]['$uploadpopupJIDSphototitle'])) {
                                        if($filterGalleryArray[$k]['$uploadpopupJIDSphototitle'] == '') {
                                            $filterGalleryArray[$k]['$uploadpopupJIDSphototitle'] = $uploadpopupJIDSphototitle;
                                        }
                                    } else {
                                        $filterGalleryArray[$k]['$uploadpopupJIDSphototitle'] = $uploadpopupJIDSphototitle;
                                    }
                                }

                                if($uploadpopupJIDSdescription != '') {
                                    if(isset($filterGalleryArray[$k]['$uploadpopupJIDSdescription'])) {
                                        if($filterGalleryArray[$k]['$uploadpopupJIDSdescription'] == '') {
                                            $filterGalleryArray[$k]['$uploadpopupJIDSdescription'] = $uploadpopupJIDSdescription;
                                        }
                                    } else {
                                        $filterGalleryArray[$k]['$uploadpopupJIDSdescription'] = $uploadpopupJIDSdescription;
                                    }
                                }

                                if($uploadpopupJIDSlocation != '') {
                                    if(isset($filterGalleryArray[$k]['$uploadpopupJIDSlocation'])) {
                                        if($filterGalleryArray[$k]['$uploadpopupJIDSlocation'] == '') {
                                            $filterGalleryArray[$k]['$uploadpopupJIDSlocation'] = $uploadpopupJIDSlocation;
                                        }
                                    } else {
                                        $filterGalleryArray[$k]['$uploadpopupJIDSlocation'] = $uploadpopupJIDSlocation;
                                    }
                                }

                                if($uploadpopupJIDStaggedfriends != '') {
                                    if(isset($filterGalleryArray[$k]['$uploadpopupJIDStaggedfriends'])) {
                                        if($filterGalleryArray[$k]['$uploadpopupJIDStaggedfriends'] == '') {
                                            $filterGalleryArray[$k]['$uploadpopupJIDStaggedfriends'] = $uploadpopupJIDStaggedfriends;
                                        }
                                    } else {
                                        $filterGalleryArray[$k]['$uploadpopupJIDStaggedfriends'] = $uploadpopupJIDStaggedfriends;
                                    }
                                }

                                if($uploadpopupJIDSvisibleto != '') {
                                    if(isset($filterGalleryArray[$k]['$uploadpopupJIDSvisibleto'])) {
                                        if($filterGalleryArray[$k]['$uploadpopupJIDSvisibleto'] == '') {
                                            $filterGalleryArray[$k]['$uploadpopupJIDSvisibleto'] = $uploadpopupJIDSvisibleto;
                                        }
                                    } else {
                                        $filterGalleryArray[$k]['$uploadpopupJIDSvisibleto'] = $uploadpopupJIDSvisibleto;
                                    }
                                }

                                if($uploadpopupJIDScategories != '') {
                                    if(isset($filterGalleryArray[$k]['$uploadpopupJIDScategories'])) {
                                        if($filterGalleryArray[$k]['$uploadpopupJIDScategories'] == '') {
                                            $filterGalleryArray[$k]['$uploadpopupJIDScategories'] = $uploadpopupJIDScategories;
                                        }
                                    } else {
                                        $filterGalleryArray[$k]['$uploadpopupJIDScategories'] = $uploadpopupJIDScategories;
                                    }
                                }
                            }
                        }

                        $imageName = $files['name'][$i];
                        $imageType = $files['type'][$i];
                        $imageTmp_name = $files['tmp_name'][$i];
                        $imageError = $files['error'][$i];
                        $imageSize = $files['size'][$i];

                        $imageArray = array(
                            'name' => $imageName,
                            'type' => $imageType,
                            'tmp_name' => $imageTmp_name,
                            'error' => $imageError,
                            'size' => $imageSize
                        );

                        $filterGalleryArray[$i]['image'] = $imageArray;
                    }


                    if(!empty($filterGalleryArray)) {
                        for ($m=0; $m < count($filterGalleryArray) ; $m++) { 

                            $newuploadpopupJIDSphototitle = isset($filterGalleryArray[0]['$uploadpopupJIDSphototitle']) ? $filterGalleryArray[0]['$uploadpopupJIDSphototitle'] : '';
                            if(isset($filterGalleryArray[$m]['$uploadpopupJIDSphototitle'])) {
                                if($filterGalleryArray[$m]['$uploadpopupJIDSphototitle'] == '') {
                                    $filterGalleryArray[$m]['$uploadpopupJIDSphototitle'] = $newuploadpopupJIDSphototitle;
                                }
                            } else {
                                $filterGalleryArray[$m]['$uploadpopupJIDSphototitle'] = $newuploadpopupJIDSphototitle;
                            }

                            $newuploadpopupJIDSdescription = isset($filterGalleryArray[0]['$uploadpopupJIDSdescription']) ? $filterGalleryArray[0]['$uploadpopupJIDSdescription'] : '';
                            if(isset($filterGalleryArray[$m]['$uploadpopupJIDSdescription'])) {
                                if($filterGalleryArray[$m]['$uploadpopupJIDSdescription'] == '') {
                                    $filterGalleryArray[$m]['$uploadpopupJIDSdescription'] = $newuploadpopupJIDSdescription;
                                }
                            } else {
                                $filterGalleryArray[$m]['$uploadpopupJIDSdescription'] = $newuploadpopupJIDSdescription;
                            }
                        
                            $newuploadpopupJIDSlocation = isset($filterGalleryArray[0]['$uploadpopupJIDSlocation']) ? $filterGalleryArray[0]['$uploadpopupJIDSlocation'] : '';
                            if(isset($filterGalleryArray[$m]['$uploadpopupJIDSlocation'])) {
                                if($filterGalleryArray[$m]['$uploadpopupJIDSlocation'] == '') {
                                    $filterGalleryArray[$m]['$uploadpopupJIDSlocation'] = $newuploadpopupJIDSlocation;
                                }
                            } else {
                                $filterGalleryArray[$m]['$uploadpopupJIDSlocation'] = $newuploadpopupJIDSlocation;
                            }
                            
                            $newuploadpopupJIDStaggedfriends = isset($filterGalleryArray[0]['$uploadpopupJIDStaggedfriends']) ? $filterGalleryArray[0]['$uploadpopupJIDStaggedfriends'] : '';
                            if(isset($filterGalleryArray[$m]['$uploadpopupJIDStaggedfriends'])) {
                                if($filterGalleryArray[$m]['$uploadpopupJIDStaggedfriends'] == '') {
                                    $filterGalleryArray[$m]['$uploadpopupJIDStaggedfriends'] = $newuploadpopupJIDStaggedfriends;
                                }
                            } else {
                                $filterGalleryArray[$m]['$uploadpopupJIDStaggedfriends'] = $newuploadpopupJIDStaggedfriends;
                            }

                            $newuploadpopupJIDSvisibleto = isset($filterGalleryArray[0]['$uploadpopupJIDSvisibleto']) ? $filterGalleryArray[0]['$uploadpopupJIDSvisibleto'] : '';
                            if(isset($filterGalleryArray[$m]['$uploadpopupJIDSvisibleto'])) {
                                if($filterGalleryArray[$m]['$uploadpopupJIDSvisibleto'] == '') {
                                    $filterGalleryArray[$m]['$uploadpopupJIDSvisibleto'] = $newuploadpopupJIDSvisibleto;
                                }
                            } else {
                                $filterGalleryArray[$m]['$uploadpopupJIDSvisibleto'] = $newuploadpopupJIDSvisibleto;
                            }

                            $newuploadpopupJIDScategories = isset($filterGalleryArray[0]['$uploadpopupJIDScategories']) ? $filterGalleryArray[0]['$uploadpopupJIDScategories'] : '';
                            if(isset($filterGalleryArray[$m]['$uploadpopupJIDScategories'])) {
                                if($filterGalleryArray[$m]['$uploadpopupJIDScategories'] == '') {
                                    $filterGalleryArray[$m]['$uploadpopupJIDScategories'] = $newuploadpopupJIDScategories;
                                }
                            } else {
                                $filterGalleryArray[$m]['$uploadpopupJIDScategories'] = $newuploadpopupJIDScategories;
                            }
                        }
                    }
                     
                    $add = Snapit::addGallery($filterGalleryArray, $place, $placetitle, $user_id);
                    $result = array('success' => true);
                    return json_encode($result, true);
                }
            }
        }

        $result = array('success' => false);
        return json_encode($result, true);
    }

    public function actionEditgallery()  {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        $result = array('success' => false);
        if(isset($user_id) && $user_id != '') {
            if(isset($_POST) && !empty($_POST)) {
                $post = $_POST;

                $id = isset($post['id']) ? $post['id'] : '';
                if($id) {
                    $title = isset($post['$uploadpopupJIDSphototitleedit']) ? $post['$uploadpopupJIDSphototitleedit'] : '';
                    $description = isset($post['$uploadpopupJIDSdescriptionedit']) ? $post['$uploadpopupJIDSdescriptionedit'] : '';
                    $location = isset($post['$uploadpopupJIDSlocationedit']) ? $post['$uploadpopupJIDSlocationedit'] : '';
                    $taggedfriends = isset($post['$uploadpopupJIDStaggedfriendsedit']) ? $post['$uploadpopupJIDStaggedfriendsedit'] : array();
                    
                    if(!is_array($taggedfriends)) {
                        $taggedfriends = explode(',', $taggedfriends);
                        $taggedfriends = array_values(array_filter($taggedfriends));
                    }

                    if(!empty($taggedfriends)) {
                        $taggedfriends = implode(',', $taggedfriends);
                    } else {
                        $taggedfriends = '';
                    }

                    $visibleto = isset($post['$uploadpopupJIDSvisibletoedit']) ? $post['$uploadpopupJIDSvisibletoedit'] : '';
                    
                    $url = '../web/uploads/gallery/';
                    $date = uniqid().'_'.rand(9999, 99999).'_'.time();
                   
                    $Gallery = Gallery::find()->where([(string)'_id' => $id, 'user_id' => $user_id])->one();

                    if(!empty($Gallery)) {
                        
                        if(isset($_FILES['image']) && !empty($_FILES['image'])) {
                            $image = $_FILES['image'];
                            if(!empty($image)) {
                                if(isset($image['name']) && $image['name'] != '') {
                                    $unlink = $Gallery->image;
                                    if(file_exists($unlink)) {
                                        unlink($unlink);
                                    }
                                    $name = $image["name"]; 
                                    $tmp_name = $image["tmp_name"];
                                    move_uploaded_file($tmp_name, $url . $date . $name);
                                    $img = $url . $date . $name;
                                    $Gallery->image = $img;
                                }
                            }
                        }
                        
                        if(trim($visibleto) == 'Custom') {
                            if(isset($post['customids']) && !empty($post['customids'])) {
                                $ids = $post['customids'];
                                if(is_array($ids)) {
                                    $ids = implode(',', $ids);
                                }
                                $Gallery->customids = $ids;
                            }
                        } else {
                            $Gallery->customids = '';
                        }
                                 
                        $Gallery->title = $title;
                        $Gallery->description = $description;
                        $Gallery->location = $location;
                        $Gallery->tagged_friends = $taggedfriends;
                        $Gallery->visible_to = $visibleto;
                        $Gallery->modified_at = time();
                        $Gallery->update(); 

                        $result = array('success' => true);
                        return json_encode($result, true);
                    }
                }
            }
        }

        $result = array('success' => false);
        return json_encode($result, true);
    }

    public function actionGetcategorysnapits()
    {
        if(isset($_POST['category'])) {
            $category = $_POST['category'];
            if($category != '') {
                return $this->render('categorysearch', ['category' => $category]);
            } else {
                return $this->render('recent');
            }
        }

        return $this->render('recent');
    }

    public function actionGetsnapitsbysearch()
    {
        if(isset($_POST['value'])) {
            $value = $_POST['value'];
            if($value != '') {
                return $this->render('getsnapitsbysearch', ['value' => $value]);
            } else {
                return $this->render('recent');
            }
        }

        return $this->render('recent');
    }

    public function actionFetchgalleryrecent()
    {
        return $this->render('recent');
    }

    public function actionFetchgallerypopular()
    {
        return $this->render('popular');
    }

    public function actionFetchgalleryyour()
    {
        return $this->render('your');
    }

    public function actionRemovesnapit()  
    { 
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $result = array();

        if(isset($_POST['$id']) && $_POST['$id'] != '') {
            $id = $_POST['$id'];
            $data = Snapit::find()->where([(string)'_id' => $id])->one();

            if(!empty($data)) {
                $data->delete();
                $result = array('success' => true);
            }
        }

        return json_encode($result, true);
    }
}
?>