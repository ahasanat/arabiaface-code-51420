<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter; 
use yii\filters\AccessControl;
use yii\mongodb\ActiveRecord;

use frontend\components\ExSession;
use frontend\models\UserForm;
use frontend\models\LoginForm;
use frontend\models\SecuritySetting;
use frontend\models\Friend;
use frontend\models\Whoisaround;

class WhoisaroundController
        extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
	
	public function beforeAction($action)
	{   
        $this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

    public function actions() {
        return [ 
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this,
                    'oAuthSuccess'],
            ],
            'captcha' => [
                //'class' => 'yii\captcha\CaptchaAction',
                'class' => 'mdm\captcha\CaptchaAction',
                'level' => 1,
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
    public function actionIndex() {
        return $this->render('index');
    }
	
    public function actionAll() {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $authstatus = UserForm::isUserExistByUid($user_id);
            $checkuserauthclass = $authstatus;
        } else {
            $checkuserauthclass = 'checkuserauthclassg';
        }
        $post = array(); 

        $getUsers = UserForm::find()->with('isvip')->select(['fullname', 'photo', 'thumbnail', 'gender', 'city', 'country','vip_flag'])->where(['status' => '1'])->orderBy(['_id'=>SORT_DESC])->asarray()->all();
        $vipArray = array();
        $simpleArray = array();
        if(!empty($getUsers)) {
            foreach ($getUsers as $key => $sdata) {
                if(isset($sdata['vip_flag']) && $sdata['vip_flag'] != '0' && $sdata['vip_flag'] != '') {
                    $vipArray[] = $sdata;
                } else {
                    $simpleArray[] = $sdata;
                }
            }
            $getUsers = array_merge($vipArray, $simpleArray);
        }
 
        $this->getUserGridLayout($getUsers, 'whoisaround', 'all');

        //return $this->render('all', array('getUsers' => $getUsers, 'online' => $post, 'checkuserauthclass' => $checkuserauthclass));
    }

    public function actionAllfriends() {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if(isset($user_id) && $user_id != '') {
            $post = isset($_POST['$result']) ? $_POST['$result'] : array();
            $post = array_flip($post);
            $getUsers = Whoisaround::getAllFriendUsers($user_id);
            $getUsers = json_decode($getUsers, true);
            $this->getUserGridLayout($getUsers, 'whoisaround', 'friends');

            //return $this->render('friends', array('data' => $data, 'online' => $post));
        } else {
            return $this->goHome();
        }
    }
    
    public function actionSearch() { 
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        if(isset($user_id) && $user_id != '') {
            $authstatus = UserForm::isUserExistByUid($user_id);
            $checkuserauthclass = $authstatus;
        } else {
            $checkuserauthclass = 'checkuserauthclassg';
        }


        $post = isset($_POST['$result']) ? $_POST['$result'] : array();
        $searchUserName = (isset($post['whoisaroundsearchname']) && $post['whoisaroundsearchname'] != '') ? $post['whoisaroundsearchname'] : '';
        $searchUserName = trim($searchUserName);
        $gender= trim($post['gender']);
        if($gender != 'Male' && $gender != 'Female') {
            $gender = array('Male', 'Female');
        } else {
            $gender = array($gender);
        }

        $onlineIds = isset($post['onlineIdsList']) ? $post['onlineIdsList'] : array(); 
        $data = Whoisaround::search($post, $searchUserName, $gender, $onlineIds);
        $data = json_decode($data, true);  
        return $this->render('search', array('data' => $data, 'post' => $post, 'checkuserauthclass' => $checkuserauthclass));
    }

	public function actionOnlineuser() {
		$session = Yii::$app->session;
        $user_id = $session->get('user_id');
		$email = $session->get('email');
		$search_value = $_POST['search'];
		if ($session->get('email'))
        {
            $model_aroundme = new Whoisaround();
			$online_users = $model_aroundme->userlist($search_value);
			return $this->render('onlineuser',array('onlineuser' => $online_users));
		}
        else
        {
            return $this->goHome();
        }
    }
	
	public function actionPopularuser() {
		$session = Yii::$app->session;
        $user_id = $session->get('user_id');
		$email = $session->get('email');
		$search_value = $_POST['search'];
		if ($session->get('email'))
        {
            $model_aroundme = new Whoisaround();
			$userpopularlist = $model_aroundme->userpopularlist();
			$popular_users = array();
			foreach($userpopularlist as $userpopularlists){
				if($user_id !=$userpopularlists['_id']){
					$users_alls = $userpopularlists['_id'];
					//$data = UserForm::getSelectedUsersData($userpopularlists['_id']);
					if($search_value == 'undefined'){
						$data = LoginForm::find()->where(['in','_id',$users_alls])->andwhere(['status'=>'1'])->all();
					}else{
						$data = LoginForm::find()->where(['in','_id',$users_alls])->andwhere(['status'=>'1'])->andwhere(['like','fullname', $search_value])->all();
					}
					foreach ($data as $key => $value) {
						$id = (string)$value['_id'];
						$dp = $this->getimage($id,'photo');
						if(!isset($value['city']) && empty($value['city'])){$value['city'] = 'Not added';}
						$popular_users[] = 	array(	'id' => $id,
													'full_name' => $value['fullname'],
													'thumb' => $dp ,
													'vip_flag'=> $value['vip_flag'],
													'credits' => $userpopularlists['credits'],
													'city'=> $value['city'],
													'friend_count'=> count(Friend::getuserFriends($id)),
													'mutual_count'=> Friend::mutualfriendcount($id)
													);
					}
				}
			}
			return $this->render('popularuser',array('popularuser' => $popular_users));
		}
        else
        {
            return $this->goHome();
        }
    }
	
	public function actionAroundmeuser() {
		$session = Yii::$app->session;
        $user_id = $session->get('user_id');
		$search_value = $_POST['search'];
		$email = $session->get('email');
		if ($session->get('email'))
        {
            $model_aroundme = new Whoisaround();
			$useraroundmelist = $model_aroundme->useraroundmelist($search_value);
			return $this->render('aroundmeuser',array('aroundmeuser' => $useraroundmelist));
		}
        else
        {
            return $this->goHome();
        }
    }
	
	public function actionFindFriends() {
		$session = Yii::$app->session;
        $user_id = $session->get('user_id');
		$email = $session->get('email');
		if ($session->get('email'))
        {
            return $this->render('findfriends');
		}
        else
        {
            return $this->goHome();
        }
    }
    
    public function actionFriendRequest() {
        if(isset($_POST)) {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');
            
            if(isset($user_id) && $user_id != '') {
                $checkuserauthclass = UserForm::isUserExistByUid($user_id);
                if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                    if(isset($_POST['$id']) && $_POST['$id']) {
                        $id = $_POST['$id'];
                        $data = Friend::friendrequestupdate($user_id, $id);
                        return $data;
                    }
                }
            }   
        }
    }

    public function actionIsBlock() {
        if(isset($_POST)) {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');
            
            if(isset($user_id) && $user_id != '') {
                $checkuserauthclass = UserForm::isUserExistByUid($user_id);
                if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                    if(isset($_POST['$id']) && $_POST['$id']) {
                        $id = $_POST['$id']; 
                        if($user_id != $id) {
                            $data = SecuritySetting::isBlocked($id, $user_id);
                            $data = json_encode($data, true);
                            return $data;
                        } else {
                            $data = array('status' => false);
                            $data = json_encode($data, true);
                            return $data;
                        }
                    }
                }
            }   
        }
    }

    public function actionBlockUser() {
        if(isset($_POST)) {
            $session = Yii::$app->session;
            $user_id = (string)$session->get('user_id');
            
            if(isset($user_id) && $user_id != '') {
                $checkuserauthclass = UserForm::isUserExistByUid($user_id);
                if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                    if(isset($_POST['$id']) && $_POST['$id']) {
                        $id = $_POST['$id']; 
                        if($user_id != $id) {
                            $data = SecuritySetting::blockuser($id, $user_id);
                            if($data == 'unblock') {
                                $data = 'Block';
                            } else {
                                $data = 'Unblock';
                            }
                            return $data;
                        }
                    }
                }
            }   
        }
    } 

    public function actionSendMessageContent() {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                if(isset($_POST['$id']) && $_POST['$id']) {
                    $id = $_POST['$id']; 
                    if($user_id != $id) {
                        $data = Whoisaround::sendmessagecontent($user_id, $id);

                        if(!empty($data)) {
                            return $this->render('sendmessage',array('data'=>$data));
                        }
                    }
                }
            }
        }   
    }

    public function actionSendMessage() {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
            if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') {
                if(isset($_POST['$id']) && $_POST['$id']) {
                    if(isset($_POST['$message']) && trim($_POST['$message'])) {
                        $id = $_POST['$id']; 
                        if($user_id != $id) {
                            $message = $_POST['$message']; 
                            $data = Whoisaround::sendmessage($user_id, $id, $message);
                            return $data;
                        }
                    }
                }
            }   
        }
        return false;
    }
}	
