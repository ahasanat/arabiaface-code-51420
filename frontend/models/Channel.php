<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;
use yii\helpers\ArrayHelper;
use frontend\models\PostForm;

class Channel extends ActiveRecord
{
    public static function collectionName()
    {
        return 'channel';
    }

    public function attributes()
    {
         return ['_id', 'name', 'profile', 'created_at', 'created_by', 'updated_at'];
    }

    public function suggestedDisplay($start) {
        if($start == 0 || $start == '') {
            $start = 0;
        } 

        $result =  Channel::find()->where(['status' => true])->orderBy(['created_at'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
        return json_encode($result, true);
    } 

    public function channeldetail($channel_id)
    {
        $channeldetail = Channel::find()->where(['_id'=>$channel_id, 'status' => true])->asarray()->one();
        return $channeldetail;
    }

    public function isActive($channel_id)
    {
        $channeldetail = Channel::find()->where(['_id'=>$channel_id, 'status' => true])->asarray()->one();
        if(!empty($channeldetail)) {
            return true;
        }
        return false;
    }

    public function postcount($channel_id)
    {
        $data = PostForm::find()->where(['channel_id'=>$channel_id])->count();
        if($data >0) {
            return $data . ' posts';
        } else {
            return '0 post';
        }
    }

    public function getPopular($start)
    { 
        $result = array();
        $channelIds = ArrayHelper::map(Channel::find()->select([(string)'_id'])->orderBy(['created_at'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all(), function($id) { return (string)$id['_id'];}, '1');

        if(!empty($channelIds)) {
            $channelIds = array_keys($channelIds);
            $data = PostForm::find()->select(['channel_id'])->where(['in', 'channel_id', $channelIds])->asarray()->all();
            $filterData = array();
            if(!empty($data)) {
                foreach ($data as $key => $sdata) {
                    $channel_id = $sdata['channel_id'];
                    $filterData[$channel_id][] = 1;
                }
            }

            $newfilterData = array();
            if(!empty($filterData)) {
                foreach ($filterData as $key => $sfilterData) {
                    $newfilterData[$key] = count($sfilterData);
                }
            }
            
            arsort($newfilterData);
            foreach ($newfilterData as $key => $value) {
                $data = Channel::find()->where(['_id' => $key])->asarray()->one();
                if(!empty($data)) {
                    $result[] = $data;
                }
            }
        }

        return json_encode($result, true);
    }
}