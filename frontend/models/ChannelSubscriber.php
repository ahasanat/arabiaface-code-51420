<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;

class ChannelSubscriber extends ActiveRecord
{
    public static function collectionName()
    {
        return 'channel_subscriber';
    }

    public function attributes()
    {
         return ['_id', 'user_id', 'channel_id', 'created_at'];
    }

    public function isSubscriber($user_id, $channel_id) {
        $result =  ChannelSubscriber::find()->where(['user_id' => (string)$user_id, 'channel_id' => (string)$channel_id])->asarray()->one();
        if(!empty($result)) {
            return true;
        }
        return false;
    }

    public function subscribeUpdate($user_id, $channel_id) {
        $result =  ChannelSubscriber::find()->where(['user_id' => $user_id, 'channel_id' => $channel_id])->one();
        if(!empty($result)) {
            $result->delete();
            return 'U';
        } else {
            $result = new ChannelSubscriber();
            $result->user_id = $user_id;
            $result->channel_id = $channel_id;
            $result->created_at = strtotime('now');
            $result->save();
            return 'S';
        }
    }

    public function getchannelInfo() {
        return $this->hasOne(Channel::className(), ['_id' => 'channel_id'])->andWhere(['status' => true]);
    }

    public function subscribedList($start, $user_id) {
        $result =  ChannelSubscriber::find()->with('channelInfo')->where(['user_id' => $user_id])->orderBy(['created_at'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
        if(!empty($result)) {
            return $result;
        }

        $result = array();
        return $result;
    }

    public function getchannelsubscriber($channel_id)
    {
        $col_follow_user =  ChannelSubscriber::find('user_id')->Where(['channel_id'=> $channel_id])->asarray()->all();
        return $col_follow_user;
    }
    
    public function getchannelsubscribercount($channel_id)
    {
        $col_follow =  ChannelSubscriber::find()->Where(['channel_id'=> (string)$channel_id])->count();

        if($col_follow <= 0) {
            return '0';
        } else {
            return $col_follow;
        }
    }
    
}