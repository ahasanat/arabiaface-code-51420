<?php 
namespace frontend\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\mongodb\Query;
use yii\mongodb\ActiveRecord;
use frontend\models\UserForm;
use frontend\models\CollectionFollow;

class Collection extends ActiveRecord 
{
    public static function collectionName()
    {
        return 'collection';
    }

     public function attributes()
    {
        return ['_id', 'user_id', 'name', 'tagline', 'collection_photo', 'collection_thumb', 'privacy', 'color', 'entity_collection', 'is_deleted','status_date','flagger','flagger_date', 'created_date', 'modified_date','collection_flager_id', 'customids'];
    }
	
    public function getUser()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'user_id']);
    }

	public function scenarios()
    {
        $scenarios = parent::scenarios();     
        return $scenarios;
    }
       
    public function allcollection($start=0)
	{
		if($start == 0 || $start == '') {
            $start = 0;
        } 

		$allcollection = Collection::find()->where(['is_deleted'=>"1"])->orderBy(['created_date'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
		return json_encode($allcollection, true);
	}
	
	public function allcollectionadmin()
	{
		$allcollection = Collection::find()->orderBy(['created_date'=>SORT_DESC])->all();
		return $allcollection;
	}
	
	public function yourscollection($user_id, $start)
	{
		if($start == 0 || $start == '') {
            $start = 0;
        } 
		$yourscollection = Collection::find()->where(['user_id'=>$user_id,'is_deleted'=>"1"])->orderBy(['created_date'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
		return json_encode($yourscollection, true);
	}
	
	public function yourscollectionuserwall($user_id)
	{
		$yourscollection = Collection::find()->where(['user_id'=>$user_id,'is_deleted'=>"1"])->orderBy(['created_date'=>SORT_DESC])->asarray()->all();
		return $yourscollection;
	}
	
	public function popularcollection()
	{
		$ev = Yii::$app->mongodb->getCollection('collection_follow');
		$popularcollection = array();
		/*$popularcollection = $ev->aggregate(
			array( '$group' => array(
                            '_id' => '$collection_id',
                            'count' => array('$sum' => 1)
			))
		);*/
		usort($popularcollection, function($a, $b) {
                    return $b['count'] - $a['count'];
                });
		return $popularcollection;
	}
	
	public function followingcollection($user_id, $start)
	{
		if($start == 0 || $start == '') {
            $start = 0;
        } 
		$followingcollection = CollectionFollow::find()->with('collectiondata')->where(['is_deleted'=>"1",'user_id'=>$user_id])->orderBy(['created_date'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
		return json_encode($followingcollection, true);
	}
	
	public function collectiondetail($collection_id)
	{
		$collectiondetail = Collection::find()->where(['_id'=>"$collection_id"])->one();
		return $collectiondetail;
	}
	
	public function getFlagcollections()
	{
		return Collection::find()->where(['is_deleted'=>"2"])->orderBy(['created_date'=>SORT_DESC])->all();
	}
	
	public function getcollectiondata($id,$type)
    {
        $result = Collection::find()->where(['_id' => $id])->one();
        $datas = $result[$type];
        return $datas;
    }
}