<?php 
namespace frontend\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Query;
use yii\helpers\Url;
use frontend\models\Collection;

class CollectionFollow extends ActiveRecord 
{
	public static function collectionName()
    {
        return 'collection_follow';
    }
	
    public function attributes()
    {
        return ['_id', 'user_id', 'collection_id', 'is_deleted', 'created_date', 'modified_date'];
    }
	
	public function getUser()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'user_id']);
    }

	public function getCollectiondata()
    {
        return $this->hasOne(Collection::className(), ['_id' => 'collection_id']);
    }
	
	public function getcollectionfollow($col_id ,$userid)
    {
		$col_follow =  CollectionFollow::find()->Where(['user_id'=> $userid, 'collection_id'=> $col_id, 'is_deleted' => '1'])->one();
		if(!empty($col_follow)) {
            $isOwner =  Collection::find()->Where([(string)'_id' => $col_id, 'user_id' => $userid])->one();
            if(!empty($isOwner)) {
                $follow = 'Owner';
            } else {
                $follow = 'Following';
            }
		} else {
			$follow = 'Follow';
		}		
		return $follow;
    }
	
	public function getcollectionfollowcount($collection_id)
    {
		$col_follow =  CollectionFollow::find()->Where(['is_deleted'=>'1','collection_id'=> "$collection_id"])->count();
		return $col_follow;
    }
	
	public function getcollectionfollowuser($collection_id)
	{
		$col_follow_user =  CollectionFollow::find('user_id')->Where(['is_deleted'=>'1','collection_id'=> "$collection_id"])->all();
		return $col_follow_user;
	}
	
	public function getFollowerUserNames($collection_id)
    {
       
		$session = Yii::$app->session;
        $followers_buddy_counts = 0;
        $uid = $session->get('user_id');
		$data = array();
        $followers_buddy_names = CollectionFollow::find()->with('user')->where(['collection_id' => "$collection_id",'is_deleted' => '1'])->andwhere(['not in','user_id',array("$uid")])->orderBy(['created_date'=>SORT_DESC])->limit(3)->all();
		
        if(count($followers_buddy_names) == 3)
            $offset = 2;
        else 
            $offset = count($followers_buddy_names)-3;
        
		if($offset >=1)
        {
            $followers_buddy_counts = CollectionFollow::find()->with('user')->where(['collection_id' => "$collection_id",'is_deleted' => '1'])->andwhere(['not in','user_id',array("$uid")])->orderBy(['created_date'=>SORT_DESC])->offset($offset)->all();
        } 
        $is_follower_login = CollectionFollow::find()->with('user')->where(['collection_id' => "$collection_id",'is_deleted' => '1','user_id'=> "$uid"])->orderBy(['modified_date'=>SORT_DESC])->one();
       
        if(!empty($is_follower_login))
        {
            if(!empty($followers_buddy_names))
            {
                $id = Url::to(['userwall/index', 'id' => "$uid"]);
                $names = "You, ";
            }
            else
            {
                $names = ucfirst($is_follower_login['user']['fname']).' '.ucfirst($is_follower_login['user']['lname']);
            }
            $ctr = count($followers_buddy_counts)-1;
        }
        else
        {
            $names = '';
            $ctr = count($followers_buddy_counts);
        }
        $like_count = CollectionFollow::getcollectionfollowcount($collection_id);
       $start = 0;
       foreach($followers_buddy_names AS $follower_buddy_name)
       {
            if($start < 2)
            {
                $lid = $follower_buddy_name['user']['_id'];
                $id = Url::to(['userwall/index', 'id' => "$lid"]);
                if((string)$uid == (string)$lid)
                {
                    $name = 'You';
                }
                else
                {
                    $name = ucfirst($follower_buddy_name['user']['fname']). ' '.ucfirst($follower_buddy_name['user']['lname']);
                }
                if($start != 1)
                {
                    $names .= $name.", ";
                }
                else
                {
                    $names .= $name." ";
                }
                if($like_count > 3 )
                {
                    $val = $like_count - 3; 
                    $counter = $val.' others'; 
                    $counter = ' and '.$counter;
                }
                else {$counter = '';}
            }
            $start++;
       }
       if($like_count > 3 )
        {
       $names = $names . $counter;
        }
        else
        {
        $names = $names;
        }
       $data['count'] = $ctr; 
       $data['like_ctr'] = count($followers_buddy_names);
       $data['names'] =  trim($names, ", ");
       $data['login_user_details'] = $is_follower_login;
       return $data['names'];
    }
	
    public function isFollwer($userId, $id) {
        if(isset($userId) && $userId != '') {
            if(isset($id) && $id != '') {
                $isFollwer = CollectionFollow::find()->where(['user_id' => $userId, 'collection_id' => $id])->asarray()->one();
                if(!empty($isFollwer)) {
                    return true;
                } else {  //check collection is self ower;
                    $isFollwer = Collection::find()->where(['user_id' => $userId, '_id' => $id])->asarray()->one();
                    if(!empty($isFollwer)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}