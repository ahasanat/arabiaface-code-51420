<?php 
namespace frontend\models;

use Yii;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Query;
use yii\db\ActiveQuery;
use yii\db\Expression;

class CollectionReport extends ActiveRecord 
{
	public static function collectionName()
    {
        return 'collection_report';
    }
	
    public function attributes()
    {
        return ['_id', 'collection_id', 'reporter_id', 'reason', 'created_date',];
    }
	
	public function getreportabuse($user_id, $collection_id)
	{
		$report = CollectionReport::find()->where(['reporter_id'=>"$user_id",'collection_id'=>"$collection_id"])->one();
		if($report){
			return true;
		}else{
			return false;
		}
	}
	
	public function getreportcount($id)
	{
		return CollectionReport::find()->where(['collection_id'=>"$id"])->count();
	}
	
	public function getreportdata($id)
	{
		return CollectionReport::find()->where(['collection_id'=>"$id"])->all();
	}
}