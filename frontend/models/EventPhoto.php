<?php 
namespace frontend\models;
use Yii;
use yii\db\Expression;
use yii\db\ActiveQuery;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Query;

class EventPhoto extends ActiveRecord 
{
	public static function collectionName()
    {
        return 'event_photo';
    }
	
    public function attributes()
    {
        return ['_id', 'user_id', 'event_id', 'name', 'image', 'is_deleted', 'privacy' , 'created_date', 'modified_date'];
    }
	
	public function getphoto($event_id)
	{
		$event_photo =  EventPhoto::find()->Where(['is_deleted'=>'1','event_id'=> $event_id])->orderBy(['created_date'=>SORT_DESC])->all();
		return $event_photo;
	}
}