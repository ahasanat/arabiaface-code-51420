<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;
use frontend\models\UserForm;
use frontend\models\PageEvents;

class EventVisitors extends ActiveRecord
{
    public static function collectionName()
    {
        return 'event_visits';
    }

    public function attributes()
    {
        return ['_id', 'page_id', 'user_id', 'event_id', 'status', 'created_date', 'modified_date', 'is_deleted'];
    }

    public function getEvent()
    {
        return $this->hasOne(PageEvents::className(), ['_id' => 'event_id']);
    }
    
    public function getUser()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'user_id']);
    }

    public function getMyGoingEvents($user_id, $start=0)
    {
        if($start == 0 || $start == '') {
            $start = 0;
        } 
        return EventVisitors::find()->with('event')->where(['user_id' => "$user_id",'is_deleted' => '1'])->orderBy(['created_date'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
    }

    public function getEventUsers($event_id)
    {
        return EventVisitors::find()->with('user')->where(['event_id' => $event_id,'is_deleted' => '1'])->orderBy(['created_date'=>SORT_DESC])->asarray()->all();
    }

    public function getEventCounts($event_id)
    {
        return EventVisitors::find()->where(['event_id' => $event_id,'is_deleted' => '1'])->count();
    }
    
    public function getEventGoing($event_id)
    {
        $session = Yii::$app->session;
        $userid = (string)$session->get('user_id');
        $pageevent = PageEvents::find()->where([(string)'_id' => $event_id])->asarray()->one();

        if(!empty($pageevent)) {
            // check user is Organizer...
            $created_by = $pageevent['created_by'];
            if($created_by == $userid) {
                return 'Organizer';
            } else {
                $EventVisitors =  EventVisitors::find()->Where(['user_id'=>"$userid",'event_id'=> "$event_id",'is_deleted' => '1'])->asarray()->one();
                if(!empty($EventVisitors)) {
                    if(isset($EventVisitors['status']) && $EventVisitors['status'] == 1) {
                        return 'Attending';
                    } elseif(isset($EventVisitors['status']) && $EventVisitors['status'] == 2) {  
                        return 'Request Sent';
                    } else {
                        $isasktojoin = isset($pageevent['isasktojoin']) ? $pageevent['isasktojoin'] : '0';
                        if($isasktojoin == '1') {
                            return 'Ask to Join';
                        } else {
                            return 'Attending';
                        }
                    }
                } else {
                    $isasktojoin = isset($pageevent['isasktojoin']) ? $pageevent['isasktojoin'] : '0';
                    if($isasktojoin == '1') {
                        return 'Ask to Join';
                    } else {
                        return 'Join';
                    }
                }
            }
        }
    }

    public function isasktojoin($id) {
        $data = PageEvents::find()->where([(string)'_id' => $id])->asarray()->one();
        if(!empty($data)) {
            $isasktojoin = isset($data['isasktojoin']) ? $data['isasktojoin'] : '0';
            if($isasktojoin == '1') {
                return true;
            }
        }
        return false; 
    }

    public function geteventrequest($event_id)
    {
        return EventVisitors::find()->Where(['status'=>'2','event_id'=> $event_id])->asarray()->all();
    }

    public function geteventmember($event_id)
    {
        $event_member =  EventVisitors::find()->Where(['status'=>'1','event_id'=> $event_id])->asarray()->all();
        return $event_member;
    }

    public function isgroupmemberwithreason($user_id, $event_id)
    {
        $result = array('status' => false);
        $data = EventVisitors::find()->Where(['event_id'=> $event_id, 'user_id'=> $user_id])->asarray()->one();
        if(!empty($data))
        {
            $status = isset($data['status']) ? $data['status'] : '1';
            if($status == '1') {
                $result = array('status' => true);
            } else if($status == '2') {
                $result['reason'] = '2';
            }
        }
        return json_encode($result, true);
    }

    public function eventuserrequest($user_id, $event_id) {

        $data = Notification::find()->where(['from_friend_id' => $user_id, 'post_id' => $event_id,  'notification_type' => 'eventorganizer', 'is_deleted' => '0'])->asarray()->one();

        if(!empty($data)) {
            return $data;
        }

        $result = array();
        return $result;
    }

    public function eventorganizerrequesthtml($notification_id, $post_id, $fromid, $isflag, $flag_reason)
    {
        $PageEvents =  PageEvents::find()->where([(string)'_id' => $post_id, 'created_by' => $fromid])->asArray()->one();
        if(!empty($PageEvents)) {
            $detail = Group::getUserInfo($fromid);
            $name = $detail['fullname'];
            $groupname = $PageEvents['event_name'];

            if($isflag) {
                $btn = '<label class="infolabel">'.$flag_reason.'</label>';
            } else {
                $btn = '<button class="btn btn-primary btn-sm btn-gray" onclick="eventorganizerrequestdelete(\''.$notification_id.'\');">Delete</button> <button class="btn btn-primary btn-sm" onclick="eventorganizerrequestaccept(\''.$notification_id.'\');">Confirm</button>';
            }

            $html = '<span class="btext">'.$name.'</span> wants you to be <span class="btext">Organizer</span> for the <span class="btext">'.$groupname.'</span> event <div class="btn-holder">'.$btn.'</div>';
        
            return $html;   
        }
    } 

    public function eventorganizerrequestresult($notification_id, $post_id, $fromid, $label)
    {
        $PageEvents =  PageEvents::find()->where([(string)'_id' => $post_id])->asArray()->one();
        if(!empty($PageEvents)) {
            $detail = Group::getUserInfo($fromid);
            $name = $detail['fullname'];
            $groupname = $PageEvents['event_name'];
            if($label == 'Rejected') {
                $label = 'reject';
            } else {
                $label = 'accept';
            }

            $html = '<span class="desc"><span class="btext">'.$name.'</span> '.$label.' your <span class="btext">'.$groupname.'</span> event organizer request</span>';
        
            return $html;   
        }
    }

    public function eventorganizerrequestdelete($id, $user_id) {
        $Notification = Notification::find()->where([(string)'_id' => (string)$id, 'user_id' => $user_id])->one();
        $result = array('status' => false);
        if(!empty($Notification)) {
            $Notification->status = '0';
            $Notification->flag_reason = 'Rejected';
            if($Notification->update()) {
                $date = time();
                $addNotification = new Notification();
                $addNotification->from_friend_id = $Notification['user_id'];
                $addNotification->user_id = $Notification['from_friend_id'];
                $addNotification->post_id = $Notification['post_id'];
                $addNotification->notification_type = 'eventorganizerrequestresult';
                $addNotification->is_deleted = '0';
                $addNotification->status = '1';
                $addNotification->created_date = "$date";
                $addNotification->flag_reason = 'Rejected';
                $addNotification->save();

                $result = array('status' => true);
            }
        }
        return json_encode($result, true);
    }

    public function eventorganizerrequestaccept($id, $user_id) {
        $Notification = Notification::find()->where([(string)'_id' => $id, 'user_id' => $user_id])->one();
        $result = array('status' => false);
        if(!empty($Notification)) {
            $post_id = $Notification['post_id'];
            $PageEvents = PageEvents::find()->where([(string)'_id' => $post_id])->one();
            if(!empty($PageEvents)) {
                $PageEvents->created_by = $user_id;
                if($PageEvents->update()) {
                    $Notification->status = '0';
                    $Notification->flag_reason = 'Accepted';
                    if($Notification->update()) {
                        $date = time();
                        $addNotification = new Notification();
                        $addNotification->from_friend_id = $Notification['user_id'];
                        $addNotification->user_id = $Notification['from_friend_id'];
                        $addNotification->post_id = $Notification['post_id'];
                        $addNotification->notification_type = 'eventorganizerrequestresult';
                        $addNotification->is_deleted = '0';
                        $addNotification->status = '1';
                        $addNotification->created_date = "$date";
                        $addNotification->flag_reason = 'Accepted';
                        $addNotification->save();

                        $result = array('status' => true);
                    }
                }   
            }
        }
        return json_encode($result, true);
    }
}