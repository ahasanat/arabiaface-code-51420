<?php 
namespace frontend\models;
use Yii;
use yii\helpers\ArrayHelper;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Query;
use yii\db\ActiveQuery;
use yii\db\Expression;
use frontend\models\LoginForm;
use yii\helpers\Url;
use backend\models\Googlekey;

class Friend extends ActiveRecord 
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
	
    public static function collectionName()
    {
        return 'friend';
    }

     public function attributes()
    {
        return ['_id', 'from_id', 'to_id', 'status', 'action_user_id','created_date','updated_date'];
    }
    
    public function getUserdata()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'from_id']);
    }
       
    public function scenarios()
    {
        $scenarios = parent::scenarios();     
        return $scenarios;
    }
   
    public function getuserFriends($user_id)
    {
      $user_friends =  Friend::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$user_id"])->asarray()->all();
      return $user_friends;
    }

    public function getuserFriendsIds($user_id)
    {   
      $ids = ArrayHelper::map(Friend::find()->where(['to_id' => (string)$user_id, 'status' => '1'])->all(), 'from_id', 'to_id');
  		if(!empty($ids)) {
        $friendids = array_keys($ids);
        return $friendids;
      }  
  	}
	
	   public function getuserFriendscount($user_id)
    {
		
		$user_friends = array();
		$user_friends1 =  Friend::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$user_id"])->all();
		
		foreach($user_friends1 as $user_friends2)
		{
			$result = UserForm::find()->where(['_id' => $user_friends2['from_id']])->one();
			if(empty($result))
			{
				continue;
			}
			$user_friends[] = $user_friends2; 	
		}
		return $user_friends;
	}

     public function getuserFriendsWithLike($user_id, $like)
     {
        $user_friends =  Friend::find()
            ->with('userdata')
            ->andWhere(['to_id'=> "$user_id"])
            ->andWhere(['status'=>'1'])
            ->all();
            
            return $user_friends;
     }
   
    public function friendPendingRequests()
    {
		$session = Yii::$app->session;
		$uid = $session->get('user_id');
		$pending_request =  Friend::find()->with('userdata')->Where(['not','action_user_id', "$uid"])->andwhere(['status'=>'0'])->andwhere(['to_id'=>"$uid"])->andWhere(['not','from_id', "$uid"])->all();
        return $pending_request;
    }
	
	public function friendPendingRequestsAPI($uid)
    {
        $pending_request =  Friend::find()->with('userdata')->Where(['not','action_user_id', "$uid"])->andwhere(['status'=>'0'])->andwhere(['to_id'=>"$uid"])->andWhere(['not','from_id', "$uid"])->all();
        return $pending_request;
    }
   
    public function friendRequestbadge()
     {
            $session = Yii::$app->session;
            $uid = $session->get('user_id');
            $array = [$uid];  
            $result_requests = Friend::find()->Where(['not','action_user_id', "$uid"])->andwhere(['status'=>'0'])->andwhere(['to_id'=>"$uid"])->andWhere(['not','from_id', "$uid"])->all();
         
          $count = count($result_requests);
        
          return $count;  
     }
      
     public function userlistFirstfive($uid)
     {
		$array = [$uid];
		$exist_ids = $pending_ids = array();   
		$requestexists =  Friend::find()->where(['from_id'=>"$uid"])->all();
		if(!empty($requestexists) && count($requestexists)>0)
		{
			foreach($requestexists AS $requestexist)
			{
				$exist_ids[] = $requestexist['from_id'];
			}
		}
		$requestpendings =  Friend::find()->where(['to_id'=>"$uid"])->all();
		 if(!empty($requestpendings) && count($requestpendings)>0)
		{
			foreach($requestpendings AS $requestpending)
			{
				$pending_ids[] = $requestpending['from_id'];
			}
		}
                 
		$array_final = array_unique (array_merge ($exist_ids, $pending_ids,$array));
		$result_friends = LoginForm::find()->where(['not in','_id',$array_final])->andwhere(['status'=>'1'])->orderBy(['ontop' => SORT_DESC,
        'rand()' => SORT_DESC,])->limit(3)->all();
		$count = count($result_friends);
		return $result_friends;      
     }
	 
    public function userlist()
    {
      $session = Yii::$app->session;
      $uid = (string)$session->get('user_id');
      $friendids = Friend::getfriendids($uid);
      $friendids[] = $uid;

      $result_friends = LoginForm::find()->where(['not in', '_id', $friendids])->andwhere(['status'=>'1'])->orderBy(['_id'=>SORT_DESC])->all();
      return $result_friends;
    }
	 
	   public function getmyfriend()
	   {

      $session = Yii::$app->session;
      $uid = (string)$session->get('user_id');
      $newData = array();
      $result_friends = Friend::find()->where(['to_id' => $uid, 'status' => '1'])->orwhere(['from_id' => $uid, 'status' => '1'])->asArray()->all();
      if(!empty($result_friends)) {
        foreach($result_friends as $frd) {
          $id = $frd['from_id'];
          if($id == $uid) {
            $id = $frd['to_id'];
          }
          if($id != $uid) {
            $fnm = UserForm::find()->select(['fname', 'lname', 'fullname'])->where([(string)'_id' => $id])->asarray()->one();
            if(!empty($fnm)) {
              $fullname = $fnm['fullname'];
              $newData[$id] = $fullname;
            }
          }
        }
      }
      
      return $newData;
			exit;
	   }

    public function getmyfriendcloseaccount()
    {
      $session = Yii::$app->session;
      $uid = (string)$session->get('user_id');
      $newData = array();
      $result_friends = Friend::find()->where(['to_id' => $uid, 'status' => '1'])->orwhere(['from_id' => $uid, 'status' => '1'])->asArray()->all();
      if(!empty($result_friends)) {
        foreach($result_friends as $frd) {
          $id = $frd['from_id'];
          if($id == $uid) {
            $id = $frd['to_id'];
          }
          if($id != $uid) {
            if(!in_array($id, $alreaystored)) {
              $alreaystored[] = $id;
              $fnm = UserForm::find()->select(['fname', 'lname', 'fullname', 'city', 'country'])->where([(string)'_id' => $id])->asarray()->one();
              if(!empty($fnm)) {
                $thumb = Yii::$app->GenCls->getimage($id,'thumb');  
                $fnm['thumb'] = $thumb; 
                $newData[] = $fnm;
              }
            }
          }
        }
      }
      
      return $newData;
      exit;
    }
     
     public function searchfriend()
     {

		$session = Yii::$app->session;
		$email = $session->get('name');

		$result_friends = LoginForm::find()->where(['like','email',$email])
				->orwhere(['like','fname',$email])
				->orwhere(['like','lname',$email])
				->orwhere(['like','photo',$email])
				->orwhere(['like','phone',$email])
				->orwhere(['like','fullname',$email])
				->andwhere(['status'=>'1'])->orderBy(['_id'=>SORT_DESC])->all();


		return $result_friends;
        
     }
	 
     function userlistsuggetions($sug)
     {
        if (\Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }
        else
        {
            return LoginForm::find()->where(['like','username',$sug,false])->andwhere(['status'=>'1'])->orderBy(['_id'=>SORT_DESC])->limit(10)->all();
        }
     }
     
     
     public function getMutualFriend($id)
     {
        $session = Yii::$app->session;
        $uid = $session->get('user_id');
    
        $friends_of_a = Friend::find()->where(['from_id'=>"$uid",'status'=>'1'])->all();
        $friends_of_b  = Friend::find()->where(['from_id'=>"$id",'status'=>'1'])->all();
       
        $arr_a = $arr_b = array();
        foreach($friends_of_a as $t)
        {
            $arr_a[$t->to_id] = $t->attributes;
        }

        foreach($friends_of_b as $t1)
        {
            $arr_b[$t1->to_id] = $t1->attributes;
        }
        $result_mutual = array_intersect_key($arr_a,$arr_b);
        $rmData = array();

        if(!empty($result_mutual)) {
          foreach ($result_mutual as $rm) {
            $id = $rm['from_id'];
            if($id == $id) {
              $id = $rm['to_id'];
            }

            $usrDta = LoginForm::find()->where([(string)'_id' => $id])->andwhere(['status'=>'1'])->asArray()->one();
            if(!empty($usrDta)) {
              $rm['otherid'] = $id;
              $rm['userdata'] = $usrDta;
              $rmData[] = $rm;
            }
          }
        }
        
        return $rmData;
     }

     public function getMutualFriendIds($id)
     {
        $session = Yii::$app->session;
        $uid = $session->get('user_id');
    
        $friends_of_a = Friend::find()->where(['from_id'=>"$uid",'status'=>'1'])->all();
        $friends_of_b  = Friend::find()->where(['from_id'=>"$id",'status'=>'1'])->all();
       
        $arr_a = $arr_b = array();
        foreach($friends_of_a as $t)
        {
            $arr_a[$t->to_id] = $t->attributes;
        }

        foreach($friends_of_b as $t1)
        {
            $arr_b[$t1->to_id] = $t1->attributes;
        }
        $result_mutual = array_intersect_key($arr_a,$arr_b);

        $ids = array_keys($result_mutual);

        return $ids;
     }

     public function mutualfriendcount($id)
     {
        $session = Yii::$app->session;
        $uid = $session->get('user_id');
		
        $friends_of_a = Friend::find()->where(['from_id'=>"$uid",'status'=>'1'])->all();
        $friends_of_b  = Friend::find()->where(['from_id'=>"$id",'status'=>'1'])->all();
       
        $arr_a = $arr_b = array();
        foreach($friends_of_a as $t)
        {
            $arr_a[$t->to_id] = $t->attributes;
        }

        foreach($friends_of_b as $t1)
        {
            $arr_b[$t1->to_id] = $t1->attributes;
        }
        $result_mutual = array_intersect_key($arr_a,$arr_b);
        $count = count($result_mutual);
        return $count;
     }
	 
	public function mutualfriendcountAPI($id,$uid)
	{
		$friends_of_a = Friend::find()->where(['from_id'=>"$uid",'status'=>'1'])->all();
		$friends_of_b  = Friend::find()->where(['from_id'=>"$id",'status'=>'1'])->all();
		$arr_a = $arr_b = array();
		foreach($friends_of_a as $t)
		{
			$arr_a[$t->to_id] = $t->attributes;
		}

		foreach($friends_of_b as $t1)
		{
			$arr_b[$t1->to_id] = $t1->attributes;
		}
		$result_mutual = array_intersect_key($arr_a,$arr_b);
		$count = count($result_mutual);
		return $count;
	}
     
     public function requestexists($id)
     {
        $session = Yii::$app->session;
        $uid = $session->get('user_id');
        
        $result_exists = Friend::find()->where(['from_id'=>"$id",'to_id'=>"$uid"])->all();
        $count = count($result_exists);
        return $count;
     }
      
     public function requestalreadysend($id)
     {
        $session = Yii::$app->session;
        $uid = $session->get('user_id');
  
        $result_exists = Friend::find()->where(['from_id'=>"$uid",'to_id'=>"$id"])->all();
        $count = count($result_exists);

        return $count;
     }
   
    public function getFriendsCity($uid)
     {
        $user_friends =  Friend::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $cities = '';
        foreach($user_friends AS $user_friend)
        {
			$city = Personalinfo::getCity($user_friend['userdata']['_id']);
			$cities .=  "'".$user_friend['userdata']['city']."',";
        }
        return $cities;   
     }
   
    public function getFriendsNames($uid)
     {
        $user_friends =  Friend::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $names = '';
        foreach($user_friends AS $user_friend)
        {
           $names .=  "'".$user_friend['userdata']['fname'].' '.$user_friend['userdata']['lname']."',";
        }
		return $names;
     }
   
    public function getFriendsLinks($uid)
	{
		$user_friends =  Friend::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
		$names = '';
		foreach($user_friends AS $user_friend)
		{
		   $names .=  "'".$user_friend['userdata']['_id']."',";
		}
		return $names;
	}
	  
    public function getFriendsImages($uid,$from)
     {
        $user_friends =  Friend::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $images = '';
        foreach($user_friends AS $user_friend)
        {
            if($from == 'wallajax')
            {
                $mapdp = Yii::$app->GenCls->getimage($user_friend['userdata']['_id'],'thumb');
            }
            else
            {
                $mapdp = Yii::$app->GenCls->getimage($user_friend['userdata']['_id'],'thumb');
            }
           $images .=  "'".$mapdp."',";
        }
        return $images;  
     }
	 
     function friends_or_tobe_friends($to_id,$from_id)
     {
        $is_friends =  Friend::find()->with('userdata')->Where(['from_id'=>"$from_id",'to_id'=> "$to_id"])->orWhere(['from_id'=> "$to_id",'to_id'=>"$from_id"])->all();
        
        if(count($is_friends)>0)
            return true;
        else
            return false;
     }
     
     public function getFriendsCityValue($uid)
     {
        $user_friends =  Friend::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $cities = '';
        foreach($user_friends AS $user_friend)
        {
            $prepAddr = str_replace(' ','+',$user_friend['userdata']['city']);
            $prepAddr = str_replace("'",'',$prepAddr);
			$citylat = $user_friend['userdata']['citylat'];
		    $citylong = $user_friend['userdata']['citylong'];
			if(isset($citylat) && !empty($citylat) && isset($citylong) && !empty($citylong))
			{
				$latitude = $citylat;
				$longitude = $citylong;
				$cities .=  "['".$prepAddr."',".$latitude.",".$longitude."],";
			}
			if(isset($prepAddr) && !empty($prepAddr) && !isset($citylat) && empty($citylat) && !isset($citylong) && empty($citylong))
            {
				$connected = @fsockopen("www.google.com", 80); //website, port  (try 80 or 443)
				if($connected)
				{
          $GApiKeyL = $GApiKeyP = Googlekey::getkey();
					$geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?key='.$GApiKeyL.'&address='.$prepAddr.'&sensor=false');
					$output = json_decode($geocode);
					if(!empty($output)){
						$latitude = @$output->results[0]->geometry->location->lat;
						$longitude = @$output->results[0]->geometry->location->lng;	
					}
					else
					{
						$latitude = '';
						$longitude = '';
					}
					
					$cities .=  "['".$prepAddr."',".$latitude.",".$longitude."],";
				}
				else
				{
					$cities = '[],';
				}
            }
        }
        return substr($cities,0,-1);   
     }
     
     public function getFriendsMapDetails($uid)
     {
        $user_friends =  Friend::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $fredetails = '';
        foreach($user_friends AS $user_friend)
        {
            $freid = $user_friend['userdata']['_id'];
            $frename = $user_friend['userdata']['fullname'];
      			$prepAddr = str_replace(' ','+',$user_friend['userdata']['city']);

            $mapdp = Yii::$app->GenCls->getimage($freid,'thumb');  
            $frelink = Url::to(['userwall/index', 'id' => "$freid"]);
      			if(isset($prepAddr) && !empty($prepAddr))
      			{
      				$friend_content = '\'<img height="18" width="18" src="'.$mapdp.'"/> <a href="'.$frelink.'">'.$frename.'</a>\'';
      				$fredetails .=  "[$friend_content],";
      			}
        }
        return substr($fredetails,0,-1);   
    }
 
    public function isFriend($user_id, $id) {
        $isFriend = Friend::find()->where(['from_id' => $user_id, 'to_id' => $id])->orwhere(['from_id' => $id, 'to_id' => $user_id])->one();
        if(!empty($isFriend)) {
            return $isFriend;
        }
        return false;
    }
 
    public function friendrequestupdate($user_id, $id) {
        $data = Friend::find()->where(['from_id' => $user_id, 'to_id' => $id])->orwhere(['from_id' => $id, 'to_id' => $user_id])->one();
        if(!empty($data)) {
            $data->delete();
            return 'AF';
        } else {
            $date = time();
            $data = new Friend();
            $data->from_id = $user_id;
            $data->to_id = $id;
            $data->action_user_id = $user_id;
            $data->status = '0';
            $data->created_date = $date;
            $data->updated_date = $date;
            $data->insert();
            return 'CFR';
        }
    }

    public function getfriendids($user_id) {
        $data = Friend::find()->where(['from_id' => $user_id])->orwhere(['to_id' => $user_id])->asarray()->all();
        $ids = array();
        if(!empty($data)) {
            foreach ($data as $key => $value) {
                if($value['from_id'] == $user_id) {
                    $ids[] = $value['to_id'];
                } else {
                    $ids[] = $value['from_id'];
                }
            }
        }
        return array_unique($ids);
    }

    public function AddUserForTag($user_id, $start) {
        $getfriendids = Friend::getfriendids($user_id);
        $results = array();
        if(!empty($getfriendids)) {
          $results = UserForm::find()->select(['fullname'])->where(['in', '_id', $getfriendids])->orderBy(['created_at'=>SORT_DESC])->asarray()->all();
        }
        return json_encode($results, true);
    }

    public function AddUserForTagSearch($user_id, $key, $start, $isall=false) {
        $getfriendids = Friend::getfriendids($user_id);
        $results = array();

        if($isall) {
          $results = UserForm::find()
            ->select(['fname', 'lname', 'fullname'])
            ->where(['like','fullname',$key])
            ->orwhere(['like','fname',$key])
            ->orwhere(['like','lname',$key])
            ->andwhere(['status'=>'1'])->orderBy(['_id'=>SORT_DESC])
            ->asarray()->all();
        } else if(!empty($getfriendids)) {
          if($key != '') {
            $results = UserForm::find()
            ->select(['fname', 'lname', 'fullname'])
            ->where(['like','fullname',$key])
            ->orwhere(['like','fname',$key])
            ->orwhere(['like','lname',$key])
            ->andwhere(['in', '_id', $getfriendids])
            ->andwhere(['status'=>'1'])->orderBy(['_id'=>SORT_DESC])
            ->asarray()->all();
          } else {
            $results = UserForm::find()->select(['fullname'])->where(['in', '_id', $getfriendids])->orderBy(['created_at'=>SORT_DESC])->asarray()->all(); 
          }
        }

        return json_encode($results, true);
    }

    public function frdandfrdoffrdIDS() {
      $session = Yii::$app->session;
      $user_id = (string)$session->get('user_id');
      $result = array();

      if($user_id) {
        $friend = Friend::find()->select(['from_id', 'to_id'])->where(['from_id' => $user_id])->orwhere(['to_id' => $user_id])->asarray()->all();
        
        if(!empty($friend)) {
          foreach ($friend as $key => $value) {
            $tempResult = array(); 
            $id = $value['to_id'];
            if($value['from_id'] == $user_id) {
              $id = $value['to_id'];
            }
            $result[] = $id;

            $subFriend = ArrayHelper::map(Friend::find()->select(['from_id', 'to_id'])->where(['from_id' => $user_id, 'to_id' => $id])->orwhere(['from_id' => $id, 'to_id' => $user_id])->asArray()->all(), 'from_id', 'to_id');
            if(!empty($subFriend)) {
              $T_ = array_keys($subFriend);
              $TT_ = array_values(array_unique($subFriend));

              $T_Merge = array_merge($T_,  $TT_);  
              $result = array_merge($result, $T_Merge); 
            }
          }
        }

        $result = array_values(array_unique($result));
        if (($key = array_search($user_id, $result)) !== false) {
            unset($result[$key]);
        }

        if(!empty($result)) {
          foreach ($result as $resultSingle) {
              $subfriendID = $resultSingle;
              $subfriend = Friend::find()->select(['from_id', 'to_id'])->select(['from_id', 'to_id'])->where(['from_id' => $subfriendID])->orwhere(['to_id' => $subfriendID])->asarray()->all();
      
              if(!empty($subfriend)) { 
                foreach ($subfriend as $s_subfriend) {
                  $s_subfriendID = $s_subfriend['from_id'];
                  if($user_id == $s_subfriendID) {
                    $s_subfriendID = $s_subfriend['to_id'];
                  }
                  $result[] = $s_subfriendID;
                }
              }
          }
        }

        $result = array_values(array_unique($result));

        return $result;
      }
    }

    public function getfrdandfrdoffrd($user_id, $itretion=0) {
      $start = $itretion * 20;
      $end = 20;
      $ids = Friend::frdandfrdoffrdIDS();
      if(!empty($ids)) {
        $idsCount = count($ids);
        if($start >= $idsCount) {
            echo 'FINISH';
        }
        $slice_ids = array_slice($ids, $start, $end);
        if(!empty($slice_ids)) {
          $usrDta = LoginForm::find()->select(['photo', 'thumbnail', 'fullname'])->where(['in', (string)'_id', $slice_ids])->andwhere(['status'=>'1'])->asArray()->all();
          foreach ($usrDta as $singleusrDta) {
            $subfid = (string)$singleusrDta['_id']; 
            $dp = Yii::$app->GenCls->filtergetimage($singleusrDta);
            $fullname = $singleusrDta['fullname'];
            ?>
            <a href="javascript:void(0)" id="msg_<?=$subfid?>" onclick="openMessage(this)" class="add_to_group_container">
              <span class="add_to_group_personprofile imgholder">
                <img src="<?=$dp?>"/>
              </span>
              <div class="add_to_group__personlabel">
                <h6 class="group_person_name" id="checkPerson0"><?=$fullname?></h6>
              </div>          
            </a>
            <?php
          }
        }
      }
    }

    public function getfrdandfrdoffrdsearch($user_id, $searchkey) {
      $usrDta = LoginForm::find()->select(['photo', 'thumbnail', 'fullname', 'fname', 'lname'])->where(['status'=>'1'])->andwhere(['not', (string)'_id', $user_id])->asArray()->all();
      foreach ($usrDta as $singleusrDta) {
        $fullname = $singleusrDta['fullname'];
        $fname = $singleusrDta['fname'];
        $lname = $singleusrDta['lname'];
        if (stripos($fname, $searchkey) === 0) {
        } else if(stripos($lname, $searchkey) === 0) {
        } else if(stripos($fullname, $searchkey) === 0) {
        } else {
          continue;
        }  

        $subfid = (string)$singleusrDta['_id'];
        $dp = Yii::$app->GenCls->filtergetimage($singleusrDta);
        ?>
         <a href="javascript:void(0)" id="msg_<?=$subfid?>" onclick="openMessage(this)" class="add_to_group_container">
          <span class="add_to_group_personprofile imgholder">
            <img src="<?=$dp?>"/>
          </span>
          <div class="add_to_group__personlabel">
            <h6 class="group_person_name" id="checkPerson0"><?=$fullname?></h6>
          </div>          
        </a>
        <?php
      }
    }
}
