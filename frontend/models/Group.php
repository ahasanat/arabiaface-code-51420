<?php 
namespace frontend\models;
use Yii;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Query;
use yii\db\ActiveQuery;
use yii\db\Expression;
use frontend\models\PostForm;
use frontend\models\Notification;

class Group extends ActiveRecord 
{
    public static function GroupName()
    {
        return 'group';
    }

     public function attributes()
    {

        return ['_id', 'user_id', 'name', 'tagline', 'location', 'description', 'group_photo', 'group_thumb', 'privacy','review', 'isasktojoin', 'entity_group', 'is_deleted', 'created_by', 'created_date', 'modified_date', 'customids'];
    }
	
   public function scenarios()
    {
        $scenarios = parent::scenarios();     
        return $scenarios;
    }
   
    public function allgroup($start=0)
	{
		if($start == 0 || $start == '') {
            $start = 0;
        }

		$allgroup = Group::find()->where(['is_deleted'=>"1"])->orderBy(['created_date'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
		if(!empty($allgroup)) {
			array_rand($allgroup);
		}
		return $allgroup;
	}
	
	public function searchwithlocation($location)
	{
		$allgroup = Group::find()->where(['like', 'location', $location])->andwhere(['is_deleted'=>"1"])->orderBy(['created_date'=>SORT_DESC])->asarray()->all();
		if(!empty($allgroup)) {
			array_rand($allgroup);
		}
		return $allgroup;
	}

	public function allgroupnew($address, $start, $limit)
	{
		$allgroup = Group::find()->where(['is_deleted'=>"1"])->andwhere(['like', 'location', $address])->orderBy(['created_date'=>SORT_DESC])->limit($limit)->offset($start)->asarray()->all();
		if(!empty($allgroup)) {
			array_rand($allgroup);
		}
		return $allgroup;
	}
	
	public function allgroupadmin()
	{
		return Group::find()->orderBy(['created_date'=>SORT_DESC])->asarray()->all();
	}
	
	public function yoursgroup($user_id, $start=0)
	{
		if($start == 0 || $start == '') {
            $start = 0;
        }

		$yoursgroup = Group::find()->where(['user_id'=>$user_id,'is_deleted'=>"1"])->orderBy(['created_date'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
		return $yoursgroup;
	}
	
	public function joingroup($user_id, $start=0)
	{
		if($start == 0 || $start == '') {
            $start = 0;
        }
        
		$joingroup = GroupMember::find()->with('groupdata')->where(['status'=>"1",'user_id'=>$user_id])->orderBy(['created_date'=>SORT_DESC])->limit(12)->offset($start)->asarray()->all();
		return $joingroup; 
	}
	
	public function groupdetail($group_id)
	{
		$groupdetail = Group::find()->where(['_id'=>"$group_id"])->asarray()->one();
		return $groupdetail;
	}
	
	public function grouppostcount($group_id)
	{
		$grouppostcount = PostForm::find()->where(['group_id'=>"$group_id",'is_deleted'=>"0"])->count();
		return $grouppostcount;
	}
	
	public function getFlaggroup()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        return Group::find()->where(['is_deleted' => '2'])->orderBy(['created_date'=>SORT_DESC])->all();
    }

    public function isasktojoin($group_id) {
		$data = Group::find()->where([(string)'_id' => $group_id])->asarray()->one();

		if(!empty($data)) {
			$isasktojoin = isset($data['isasktojoin']) ? $data['isasktojoin'] : '0';
			if($isasktojoin == '1') {
				return true;
			}
		}

		return false;
	}

	public function isRequestSend($id, $group_id) {
		$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        $data = Notification::find()->where(['from_friend_id' => $user_id, 'user_id' => $id, 'post_id' => $group_id,  'notification_type' => 'groupadmin', 'is_deleted' => '0'])->one();

        if(!empty($data)) {
        	return true;
        }

        return false;
	}

	public function setadmin($user_id, $id, $group_id) {
		$data = Group::find()->where(['user_id' => $user_id, (string)'_id' => $group_id])->one();
		if(!empty($data)) {
			$data = Notification::find()->where(['from_friend_id' => $user_id, 'post_id' => $group_id, 'notification_type' => 'groupadmin', 'is_deleted' => '0'])->one();
			if(!empty($data)) {
				if($data['user_id'] != $id) { 
					return 'P';
				}
			}

			if(!empty($data)) {
				$data->is_deleted = '1';
				$data->status = '0';
				$data->update();
				return 'A';
			} else {
				$date = time();
				$data =  new Notification();
				$data->from_friend_id = $user_id;
				$data->user_id = $id;
				$data->post_id = $group_id;
				$data->notification_type = 'groupadmin';
				$data->is_deleted = '0';
				$data->status = '1';
				$data->created_date = "$date";
				$data->updated_date = "$date";
				$data->insert();
				return 'C';
			}
		}
	}
	
	public function memberrequest($user_id, $group_id) {
        $data = Notification::find()->where(['from_friend_id' => $user_id, 'post_id' => $group_id,  'notification_type' => 'groupadmin', 'is_deleted' => '0'])->asarray()->one();
        if(!empty($data)) {
        	return $data;
        }
        $result = array();
        return $result;
	}

	public function leftgroup($user_id, $group_id) {
		$data = Group::find()->where([(string)'_id' => $group_id, 'user_id' => $user_id])->one();
		if(!empty($data)) {
			return 'S';
		} else {
			$data = GroupMember::find()->where(['user_id' => $user_id, 'group_id' => $group_id])->one();
			if(!empty($data)) {
				$data->delete();
			}
		}
	}

	public function getUserInfo($id)
    {
        $data =  UserForm::find()->select(['fullname'])->where([(string)'_id' => $id])->asArray()->one();
        return $data;
    } 
 
    public function groupadminrequesthtml($notification_id, $post_id, $fromid, $isflag, $flag_reason)
    {
        $data =  Group::find()->where([(string)'_id' => $post_id, 'user_id' => $fromid])->asArray()->one();
        if(!empty($data)) {
        	$detail = Group::getUserInfo($fromid);
        	$name = $detail['fullname'];
        	$groupname = $data['name'];

        	if($isflag) {
        		$btn = '<label class="infolabel">'.$flag_reason.'</label>';
        	} else {
        		$btn = '<button class="btn btn-primary btn-sm btn-gray" onclick="groupadmindelete(\''.$notification_id.'\', this);">Delete</button> <button class="btn btn-primary btn-sm" onclick="groupadminaccept(\''.$notification_id.'\', this);">Confirm</button>';
        	}
        	$html = '<span class="btext">'.$name.'</span> wants you to be <span class="btext">Admin</span> for the <span class="btext">'.$groupname.'</span> group <div class="btn-holder">'.$btn.'</div>';
        
        	return $html;	
        }
    } 

    public function groupadminrequestresult($notification_id, $post_id, $fromid, $label)
    {
        $data =  Group::find()->where([(string)'_id' => $post_id])->asArray()->one();
        if(!empty($data)) {
        	$detail = Group::getUserInfo($fromid);
        	$name = $detail['fullname'];
        	$groupname = $data['name'];
        	if($label == 'Rejected') {
        		$label = 'reject';
        	} else {
        		$label = 'accept';
        	}

        	$html = '<span class="desc"><span class="btext">'.$name.'</span> '.$label.' your <span class="btext">'.$groupname.'</span> group admin request</span>';
        
        	return $html;	
        }
    }

    public function groupadminrequestdelete($id, $user_id) {
    	$Notification = Notification::find()->where([(string)'_id' => (string)$id, 'user_id' => $user_id])->one();
    	$result = array('status' => false);
		if(!empty($Notification)) {
    		$Notification->status = '0';
    		$Notification->flag_reason = 'Rejected';
    		if($Notification->update()) {
    			$date = time();
    			$addNotification = new Notification();
    			$addNotification->from_friend_id = $Notification['user_id'];
    			$addNotification->user_id = $Notification['from_friend_id'];
    			$addNotification->post_id = $Notification['post_id'];
    			$addNotification->notification_type = 'groupadminrequestresult';
    			$addNotification->is_deleted = '0';
    			$addNotification->status = '1';
    			$addNotification->created_date = "$date";
    			$addNotification->flag_reason = 'Rejected';
    			$addNotification->save();

    			$result = array('status' => true);
    		}
    	}
    	return json_encode($result, true);
    }

    public function groupadminrequestaccept($id, $user_id) {
    	$Notification = Notification::find()->where([(string)'_id' => $id, 'user_id' => $user_id])->one();
    	$result = array('status' => false);
    	if(!empty($Notification)) {
    		$post_id = $Notification['post_id'];
    		$Group = Group::find()->where([(string)'_id' => $post_id])->one();
    		if(!empty($Group)) {
    			$Group->user_id = $user_id;
    			if($Group->update()) {
		    		$Notification->status = '0';
		    		$Notification->flag_reason = 'Accepted';
		    		if($Notification->update()) {
		    			$date = time();
		    			$addNotification = new Notification();
		    			$addNotification->from_friend_id = $Notification['user_id'];
    					$addNotification->user_id = $Notification['from_friend_id'];
		    			$addNotification->post_id = $Notification['post_id'];
		    			$addNotification->notification_type = 'groupadminrequestresult';
		    			$addNotification->is_deleted = '0';
		    			$addNotification->status = '1';
		    			$addNotification->created_date = "$date";
		    			$addNotification->flag_reason = 'Accepted';
		    			$addNotification->save();

		    			$result = array('status' => true);
		    		}
    			}	
    		}
    	}
		return json_encode($result, true);
    }

}