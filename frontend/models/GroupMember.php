<?php 
namespace frontend\models;
use Yii;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Query;
use yii\db\ActiveQuery;
use yii\db\Expression;

class GroupMember extends ActiveRecord 
{
	public static function collectionName()
    {
        return 'group_member';
    }
	
   public function attributes()
    {
        return ['_id', 'user_id', 'group_id', 'status', 'created_date', 'modified_date'];
    }
	
	public function getGroupdata()
    {
        return $this->hasOne(Group::className(), ['_id' => 'group_id']);
    }
	
	public function getgroupmemerstatus($group_id)
    {
		$session = Yii::$app->session;
		$userid = (string)$session->get('user_id');
		$group =  Group::find()->Where([(string)'_id'=> (string)$group_id])->asarray()->one();
		$group_member =  GroupMember::find()->Where(['user_id'=> $userid,'group_id'=> $group_id])->asarray()->one();
		if($group_member) {
			$groupUid = $group['user_id'];
			if($groupUid == $userid) {
				$status = 'Admin';
			} else {
	 			if($group_member['status'] == 1){
					$status = 'Member';
				}elseif($group_member['status'] == 2){  
					$status = 'Request Sent';
				}else{

					$isasktojoin = isset($group['isasktojoin']) ? $group['isasktojoin'] : '0';
					if($isasktojoin == '1') {
						$status = 'Ask to Join';
					} else {
						$status = 'Join';
					}
				}
			}
		} else {
			$isasktojoin = isset($group['isasktojoin']) ? $group['isasktojoin'] : '0';
			if($isasktojoin == '1') {
				$status = 'Ask to Join';
			} else {
				$status = 'Join';
			}
		}
		return $status;
    }
	
	public function getgroupmembercount($group_id)
    {
		$group_member =  GroupMember::find()->Where(['status'=>'1','group_id'=> $group_id])->count();
		return $group_member;
    }
	
	public function getgroupmember($group_id)
	{
		$group_member =  GroupMember::find()->Where(['status'=>'1','group_id'=> $group_id])->all();
		return $group_member;
	}
	
	public function getallgroupmember()
	{
		return GroupMember::find()->Where(['status'=>'1'])->all();
	}
	
	public function getmemberrequest($group_id)
	{
		return GroupMember::find()->Where(['status'=>'2','group_id'=> $group_id])->all();
	}
	
	public function isgroupmember($user_id, $group_id)
	{
		$groupmember = GroupMember::find()->Where(['status'=>'1','group_id'=> $group_id,'user_id'=>"$user_id"])->one();
		if(!empty($groupmember))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function isgroupmemberwithreason($user_id, $group_id)
	{
		$result = array('status' => false);
		$groupmember = GroupMember::find()->Where(['group_id'=> $group_id,'user_id'=>"$user_id"])->one();
		if(!empty($groupmember))
		{
			$status = $groupmember['status'];
			if($status == '1') {
				$result = array('status' => true);
			} else if($status == '2') {
				$result['reason'] = '2';
			}
		}
		return json_encode($result, true);
	}
}