<?php 
namespace frontend\models;
use Yii;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Query;
use yii\db\ActiveQuery;
use yii\db\Expression;

class GroupPhoto extends ActiveRecord 
{
    public static function collectionName()
    {
        return 'group_photo';
    }
	
	public function attributes()
    {
        return ['_id', 'user_id', 'group_id', 'name', 'image', 'is_deleted', 'privacy' , 'created_date', 'modified_date'];
    }
	
	public function getphoto($group_id)
	{
		$group_photo =  GroupPhoto::find()->Where(['is_deleted'=>'1','group_id'=> $group_id])->orderBy(['created_date'=>SORT_DESC])->all();
		return $group_photo;
	}
}