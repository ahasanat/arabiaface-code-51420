<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;

class MuteFriend extends ActiveRecord
{
    public static function collectionName()
    {
        return 'user_mute';
    }

    public function attributes()
    {
        return ['_id', 'user_id', 'mute_ids'];
    }
    
    public function getMutefriendsIds($user_id)
    {
        return MuteFriend::find()->where(['user_id'=>"$user_id"])->one();
    }    
}