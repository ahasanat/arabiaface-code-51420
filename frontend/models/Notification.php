<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;

class Notification extends ActiveRecord
{ 
    public static function collectionName()
    {
        return 'notification';
    }

    public function attributes()
    {
        return ['_id', 'user_id', 'post_id','post_owner_id','shared_by','share_id','from_friend_id', 'comment_content','comment_id','reply_comment_id','like_id','page_id','event_id','notification_type','status','created_date','updated_date','ip','is_deleted','review_setting','entity','page_id','flag_reason','page_role_type','collection_owner_id','tag_id','credits','collection_id','event_owner_id','group_owner_id', 'group_id','event_invited_to_id'];
    }
   
    public function getUser()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'user_id']);
    }
    
    public function getPost()
    {
        return $this->hasOne(PostForm::className(), ['_id' => 'post_id']);
    }
    
    public function getLike()
    {
        return $this->hasMany(Like::className(), ['_id' => 'like_id']);
    }
    
    public function getComment()
    {
        return $this->hasMany(Comment::className(), ['_id' => 'comment_id']);
    }
	
     public function getUserPostBudge()
     {
        $session = Yii::$app->session;
        $uid = (string)$session->get('user_id');
        
        $friends =  Friend::getuserFriends($uid);
        $ids = array();
        foreach($friends as $friend)
        {
            $ids[]= $friend['from_id'];
        }
        $mute_ids = MuteFriend::getMutefriendsIds($uid);
        $mute_friend_ids =  (explode(",",$mute_ids['mute_ids']));
        $login_user = LoginForm::find()->where(['_id' => "$uid"])->one();
        $view_noti_time =  $login_user->last_logout_time;
        $notification_settings = NotificationSetting::find()->where(['user_id' => (string)$uid])->one();
       
           $notificationpost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_friend_ids])->andwhere(['not','notification_type','tag_friend'])->andwhere(['not','notification_type','friendrequestaccepted'])->andwhere(['not','notification_type','followcollection'])->andwhere(['not','notification_type','sharecollection'])->andwhere(['not','notification_type','attendevent'])->andwhere(['not','notification_type','shareevent'])->andwhere(['not','notification_type','addpostevent'])->andwhere(['not','notification_type','addphotoevent'])->andwhere(['not','notification_type','eventinvited'])->andwhere(['not','notification_type','eventinvitedbyother'])->andwhere(['not','notification_type','sharegroup'])->andwhere(['not','notification_type','addpostgroup'])->andwhere(['not','notification_type','addphotogroup'])->andwhere(['not','notification_type','becomegroupmember'])->andwhere(['not','notification_type','groupinvite'])->andwhere(['not','notification_type','friendrequestdenied'])->andwhere(['not','notification_type','comment'])->andwhere(['not','notification_type','likepost'])->andwhere(['not','notification_type','onpagewall'])->andwhere(['not','notification_type','page_role_type'])->andwhere(['not','notification_type','likecomment'])->andwhere(['not','notification_type','commentreply'])->andwhere(['not','notification_type','sharepost'])->andwhere(['not','notification_type','pageinvite'])->andwhere(['not','notification_type','eventinvite'])->andwhere(['not','notification_type','pageinvitereview'])->andwhere(['not','notification_type','likepage'])->andwhere(['not','notification_type','pagereview'])->andwhere(['not','notification_type','eventgoing'])->andwhere(['not','notification_type','groupinvite'])->andwhere(['not','notification_type','grouprequest'])->andwhere(['not','notification_type','groupjoin'])->andwhere(['not','notification_type','deletepostadmin'])->andwhere(['not','notification_type','editpostuser'])->andwhere(['not','notification_type','editcollectionuser'])->andwhere(['not','notification_type','publishpost'])->andwhere(['not','notification_type','publishcollection'])->andwhere(['not','notification_type','publishpage'])->andwhere(['not','notification_type','publishgroup'])->andwhere(['not','notification_type','publishevent'])->andwhere(['not','notification_type','deletecollectionadmin'])->andwhere(['not','notification_type','deletepageadmin'])->andwhere(['not','notification_type','deletegroupadmin'])->andwhere(['not','notification_type','groupadminrequestresult'])->andwhere(['not','notification_type','eventorganizerrequestresult'])->andwhere(['not','notification_type','groupadmin'])->andwhere(['not','notification_type','deleteeventadmin'])->andwhere(['not','notification_type','eventorganizer'])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

			$notificationtag = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'tag_friend','user_id'=>(string)$uid])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		   
			$notificationfriendaccepted = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'friendrequestaccepted','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

			$notificationfrienddenied = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'friendrequestdenied','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
		   
		   
			$notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
				
			$notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
				
			$notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
				
				
			$notificationlikecomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likecomment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

			$notificationcommentreply = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'commentreply','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
				
			$notificationlowcredits = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'low_credits','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
				
				
			$notificationfollowcollection = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'followcollection','collection_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
				
			$notificationsharecollection = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharecollection','collection_owner_id'=>(string)$uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationshareevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'shareevent','event_owner_id'=>(string)$uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationaddpostevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'addpostevent','event_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationaddphotoevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'addphotoevent','event_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
				
				
			$notificationattendevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'attendevent','event_owner_id'=> (string) $uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			
			$notificationinvitedforevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'eventinvited','event_invited_to_id'=> (string) $uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationeventownerforinvitedbyother = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventinvitedbyother','event_owner_id'=>(string)$uid])->andwhere(['not', 'user_id',$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			
			$notificationsharegroup = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharegroup','group_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
				
			$notificationaddpostgroup = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'addpostgroup','group_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
				
			$notificationaddphotogroup = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'addphotogroup','group_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
				
			$notificationabecomegroupmember = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'becomegroupmember','group_owner_id'=> (string) $uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			
			$notificationinvitedforgroup = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'groupinvite','from_friend_id'=> (string) $uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationsharepostowner = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
		   
			$notificationpageinvitation = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pageinvite','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationpageinvitationreview = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pageinvitereview','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
		
			$notificationpagelikes = Notification::find()->with('user')->with('like')->where(['notification_type'=>'likepage','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

			$notificationpagereviews = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pagereview','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationeventinvitation = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventinvite','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationeventgoing = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventgoing','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationgroupinvitation = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupinvite','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

			$notificationgroupownerforinvitedbyother = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupinvitedbyother','group_owner_id'=>(string)$uid])->andwhere(['not', 'user_id',$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationgrouprequest = Notification::find()->with('user')->with('like')->where(['notification_type'=>'grouprequest','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

			$notificationgroupjoin = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupjoin','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationgroupadmin = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationeventorganizer = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventorganizer','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
		   
			$groupadminrequestresult = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupadminrequestresult','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$eventorganizerrequestresult = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventorganizerrequestresult','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationdeletebyadmin = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletepostadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_flag_collection = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletecollectionadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_flag_page = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletepageadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_flag_group = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletegroupadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

			$n_flag_event = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deleteeventadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationeditbyadmin = Notification::find()->with('user')->with('like')->where(['notification_type'=>'editpostuser','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_edit = Notification::find()->with('user')->with('like')->where(['notification_type'=>'editcollectionuser','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

			$notificationpublishpost = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishpost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_publish_collection = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishcollection','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_publish_page = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishpage','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_publish_group = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishgroup','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_publish_event = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishevent','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationpageroles = Notification::find()->with('user')->with('like')->where(['notification_type'=>'page_role_type','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationpagewall = Notification::find()->with('user')->with('like')->where(['notification_type'=>'onpagewall','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
			
			if(isset($notification_settings) && !empty($notification_settings))
			{
				if($notification_settings['friend_activity'] == 'No')
				{
					$notificationfriendaccepted = array();
					$notificationfrienddenied = array();
					$notificationlikepost = array();
					$notificationcomment = array();
					$notificationsharepost = array();
					$notificationfollowcollection = array();
					$notificationsharecollection = array();
					$notificationshareevent = array();
					$notificationaddpostevent = array();
					$notificationaddphotoevent = array();
					$notificationattendevent = array();
					$notificationinvitedforevent = array();
					$notificationsharegroup = array();
					$notificationaddpostgroup = array();
					$notificationaddphotogroup = array();
					$notificationabecomegroupmember = array();
					$notificationinvitedforgroup = array();
				}
				else if($notification_settings['friend_request'] == 'No')
				{
					$notificationfriendaccepted = array();
					$notificationfrienddenied = array();
				}
				else if($notification_settings['is_like'] == 'No')
				{
					$notificationlikepost = array();
				}
				else if($notification_settings['is_comment'] == 'No')
				{
					$notificationcomment = array();
				}
				else if($notification_settings['is_share'] == 'No')
				{
					$notificationsharepost = array();
				}
				else if($notification_settings['follow_collection'] == 'No')
				{
					$notificationfollowcollection = array();
				}
				else if($notification_settings['share_collection'] == 'No')
				{
					$notificationsharecollection = array();
				}
				else if($notification_settings['share_event'] == 'No')
				{
					$notificationshareevent = array();
				}
				else if($notification_settings['add_post_event'] == 'No')
				{
					$notificationaddpostevent = array();
				}
				else if($notification_settings['add_photo_event'] == 'No')
				{
					$notificationaddphotoevent = array();
				}
				else if($notification_settings['attend_event'] == 'No')
				{
					$notificationattendevent = array();
				}
				else if($notification_settings['invited_for_event'] == 'No')
				{
					$notificationinvitedforevent = array();
				}
				else if($notification_settings['share_group'] == 'No')
				{
					$notificationsharegroup = array();
				}
				else if($notification_settings['add_post_group'] == 'No')
				{
					$notificationaddpostgroup = array();
				}
				else if($notification_settings['add_photo_group'] == 'No')
				{
					$notificationaddphotogroup = array();
				}
				else if($notification_settings['become_member_of_your_group'] == 'No')
				{
					$notificationabecomegroupmember = array();
				}
				else if($notification_settings['invited_for_group'] == 'No')
				{
					$notificationinvitedforgroup = array();
				}
				else if($notification_settings['member_invited_for_your_group'] == 'No')
				{
					$notificationgroupownerforinvitedbyother = array();
				}
				else if($notification_settings['member_invited_for_your_event'] == 'No')
				{
					$notificationeventownerforinvitedbyother = array();
				}
			}

			$notification = array_merge_recursive($notificationpost,$notificationtag,$notificationfriendaccepted,$notificationfrienddenied,$notificationcomment,$notificationlikepost,$notificationlikecomment,$notificationcommentreply,$notificationsharepost,$notificationsharepostowner,$notificationpageinvitation,$notificationpageinvitationreview,$notificationpagelikes,$notificationpagereviews,$notificationeventinvitation,$notificationeventgoing,$notificationgroupinvitation,$notificationgrouprequest,$notificationgroupjoin, $notificationgroupadmin, $groupadminrequestresult, $notificationdeletebyadmin,$notificationeditbyadmin,$notificationpublishpost,$notificationpageroles,$notificationpagewall,$n_flag_collection,$n_edit,$n_publish_collection,$n_flag_page,$n_publish_page,$n_flag_event,$n_publish_event,$n_publish_group,$n_flag_group, $notificationeventorganizer, $eventorganizerrequestresult,$notificationlowcredits,$notificationfollowcollection,$notificationsharecollection,$notificationshareevent,$notificationaddpostevent,$notificationaddphotoevent,$notificationattendevent,$notificationinvitedforevent,$notificationsharegroup,$notificationaddpostgroup,$notificationaddphotogroup,$notificationabecomegroupmember,$notificationinvitedforgroup,$notificationgroupownerforinvitedbyother,$notificationeventownerforinvitedbyother);
			
			return count($notification);
     }
    
    public function getAllNotification()
    {
        $session = Yii::$app->session;
        $uid = $session->get('user_id');
        $friends =  Friend::getuserFriends($uid);
        $ids = array();
        foreach($friends as $friend)
        {
            $ids[]= $friend['from_id'];
        }
        $mute_ids = MuteFriend::getMutefriendsIds($uid);
        $mute_friend_ids =  (explode(",",$mute_ids['mute_ids']));
        
        $notification_settings = NotificationSetting::find()->where(['user_id' => (string)$uid])->one();

		$notificationpost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_friend_ids])->andwhere(['not','notification_type','friendrequestaccepted'])->andwhere(['not','notification_type','followcollection'])->andwhere(['not','notification_type','sharecollection'])->andwhere(['not','notification_type','attendevent'])->andwhere(['not','notification_type','shareevent'])->andwhere(['not','notification_type','addpostevent'])->andwhere(['not','notification_type','addphotoevent'])->andwhere(['not','notification_type','eventinvited'])->andwhere(['not','notification_type','eventinvitedbyother'])->andwhere(['not','notification_type','sharegroup'])->andwhere(['not','notification_type','addpostgroup'])->andwhere(['not','notification_type','addphotogroup'])->andwhere(['not','notification_type','becomegroupmember'])->andwhere(['not','notification_type','groupinvite'])->andwhere(['not','notification_type','tag_friend'])->andwhere(['not','notification_type','page_role_type'])->andwhere(['not','notification_type','comment'])->andwhere(['not','notification_type','likepost'])->andwhere(['not','notification_type','pagereview'])->andwhere(['not','notification_type','onpagewall'])->andwhere(['not','notification_type','likecomment'])->andwhere(['not','notification_type','commentreply'])->andwhere(['not','notification_type','sharepost'])->andwhere(['not','notification_type','pageinvite'])->andwhere(['not','notification_type','eventinvite'])->andwhere(['not','notification_type','pageinvitereview'])->andwhere(['not','notification_type','likepage'])->andwhere(['not','notification_type','eventgoing'])->andwhere(['not','notification_type','groupinvite'])->andwhere(['not','notification_type','grouprequest'])->andwhere(['not','notification_type','groupadmin'])->andwhere(['not','notification_type','groupadminrequestresult'])->andwhere(['not','notification_type','groupjoin'])->andwhere(['not','notification_type','deletepostadmin'])->andwhere(['not','notification_type','deletecollectionadmin'])->andwhere(['not','notification_type','deletepageadmin'])->andwhere(['not','notification_type','deletegroupadmin'])->andwhere(['not','notification_type','deleteeventadmin'])->andwhere(['not','notification_type','editpostuser'])->andwhere(['not','notification_type','editcollectionuser'])->andwhere(['not','notification_type','publishpost'])->andwhere(['not','notification_type','publishcollection'])->andwhere(['not','notification_type','publishpage'])->andwhere(['not','notification_type','publishgroup'])->andwhere(['not','notification_type','publishevent'])->andwhere(['not','notification_type','eventorganizer'])->andwhere(['not','notification_type','eventorganizerrequestresult'])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		

		$notificationtag = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'tag_friend','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

   		$notificationfriendaccepted = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'friendrequestaccepted','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$notificationfrienddenied = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'friendrequestdenied','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
              
		$notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
					
		$notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
					
		$notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
					
		$notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
					
		$notificationlikecomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likecomment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$notificationcommentreply = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'commentreply','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
	
		$notificationlowcredits = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'low_credits','user_id'=> (string)$uid])->orderBy(['created_date'=>SORT_DESC])->all();
		
		
		$notificationfollowcollection = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'followcollection','collection_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
        $notificationsharecollection = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharecollection','collection_owner_id'=>(string)$uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
					
		$notificationshareevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'shareevent','event_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
		$notificationaddpostevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'addpostevent','event_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
					
		$notificationaddphotoevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'addphotoevent','event_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
					
					
		$notificationattendevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'attendevent','event_owner_id'=> (string) $uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
				
		$notificationinvitedforevent = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'eventinvited','event_invited_to_id'=> (string) $uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$notificationeventownerforinvitedbyother = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventinvitedbyother','event_owner_id'=>(string)$uid])->andwhere(['not', 'user_id',$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
		$notificationsharegroup = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharegroup','group_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
					
				
		$notificationaddpostgroup = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'addpostgroup','group_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
					
		$notificationaddphotogroup = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'addphotogroup','group_owner_id'=> (string) $uid])->andwhere(['not','user_id',(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
					
				
		$notificationabecomegroupmember = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'becomegroupmember','group_owner_id'=> (string) $uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
					
		$notificationinvitedforgroup = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'groupinvite','from_friend_id'=> (string) $uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
		$notificationsharepostowner = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
              
		$notificationpageinvitation = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pageinvite','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                
		$notificationpageinvitationreview = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pageinvitereview','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
		$notificationpagelikes = Notification::find()->with('user')->with('like')->where(['notification_type'=>'likepage','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                
		$notificationpagereviews = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pagereviews','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                
		$notificationeventinvitation = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventinvite','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                
		$notificationeventgoing = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventgoing','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
		$notificationgroupinvitation = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupinvite','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$notificationgroupownerforinvitedbyother = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupinvitedbyother','group_owner_id'=>(string)$uid])->andwhere(['not', 'user_id',$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$notificationgrouprequest = Notification::find()->with('user')->with('like')->where(['notification_type'=>'grouprequest','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$notificationgroupjoin = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupjoin','from_friend_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$notificationgroupadmin = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$eventorganizerrequestresult = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventorganizerrequestresult','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$notificationeventorganizer = Notification::find()->with('user')->with('like')->where(['notification_type'=>'eventorganizer','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$groupadminrequestresult = Notification::find()->with('user')->with('like')->where(['notification_type'=>'groupadminrequestresult','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$notificationadmindelete = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletepostadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_flag_collection_adminn = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletecollectionadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_flag_page_adminn = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletepageadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_flag_group_adminn = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletegroupadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_flag_event_adminn = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deleteeventadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$notificationeditpost = Notification::find()->with('user')->with('like')->where(['notification_type'=>'editpostuser','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_editcollection = Notification::find()->with('user')->with('like')->where(['notification_type'=>'editcollectionuser','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

		$notificationpublishpost = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishpost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_publish_collection3 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishcollection','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_publish_page3 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishpage','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_publish_group3 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishgroup','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$n_publish_event3 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishevent','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$notificationpageroles = Notification::find()->with('user')->with('like')->where(['notification_type'=>'page_role_type','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		$notificationpagewall = Notification::find()->with('user')->with('like')->where(['notification_type'=>'onpagewall','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
		
		if(isset($notification_settings) && !empty($notification_settings))
		{
			if($notification_settings['friend_activity'] == 'No')
			{
				$notificationfriendaccepted = array();
				$notificationfrienddenied = array();
				$notificationlikepost = array();
				$notificationcomment = array();
				$notificationsharepost = array();
				$notificationfollowcollection = array();
				$notificationsharecollection = array();
				$notificationshareevent = array();
				$notificationaddpostevent = array();
				$notificationaddphotoevent = array();
				$notificationattendevent = array();
				$notificationinvitedforevent = array();
				$notificationsharegroup = array();
				$notificationaddpostgroup = array();
				$notificationaddphotogroup = array();
				$notificationabecomegroupmember = array();
				$notificationinvitedforgroup = array();
			}
			else if($notification_settings['friend_request'] == 'No')
			{
				$notificationfriendaccepted = array();
				$notificationfrienddenied = array();
			}
			else if($notification_settings['is_like'] == 'No')
			{
				$notificationlikepost = array();
			}
			else if($notification_settings['is_comment'] == 'No')
			{
				$notificationcomment = array();
			}
			else if($notification_settings['is_share'] == 'No')
			{
				$notificationsharepost = array();
			}
			else if($notification_settings['follow_collection'] == 'No')
			{
				$notificationfollowcollection = array();
			}
			else if($notification_settings['share_collection'] == 'No')
			{
				$notificationsharecollection = array();
			}
			else if($notification_settings['share_event'] == 'No')
			{
				$notificationshareevent = array();
			}
			else if($notification_settings['add_post_event'] == 'No')
			{
				$notificationaddpostevent = array();
			}
			else if($notification_settings['add_photo_event'] == 'No')
			{
				$notificationaddphotoevent = array();
			}
			else if($notification_settings['attend_event'] == 'No')
			{
				$notificationattendevent = array();
			}
			else if($notification_settings['invited_for_event'] == 'No')
			{
				$notificationinvitedforevent = array();
			}
			else if($notification_settings['share_group'] == 'No')
			{
				$notificationsharegroup = array();
			}
			else if($notification_settings['add_post_group'] == 'No')
			{
				$notificationaddpostgroup = array();
			}
			else if($notification_settings['add_photo_group'] == 'No')
			{
				$notificationaddphotogroup = array();
			}
			else if($notification_settings['become_member_of_your_group'] == 'No')
			{
				$notificationabecomegroupmember = array();
			}
			else if($notification_settings['invited_for_group'] == 'No')
			{
				$notificationinvitedforgroup = array();
			}
			else if($notification_settings['member_invited_for_your_group'] == 'No')
			{
				$notificationgroupownerforinvitedbyother = array();
			}
			else if($notification_settings['member_invited_for_your_event'] == 'No')
			{
				$notificationeventownerforinvitedbyother = array();
			}
		}
				
		$notification = array_merge_recursive($notificationpost,$notificationtag,$notificationfriendaccepted,$notificationfrienddenied,$notificationcomment,$notificationlikepost,$notificationlikecomment,$notificationcommentreply,$notificationsharepost,$notificationsharepostowner,$notificationpageinvitation,$notificationpageinvitationreview,$notificationpagelikes,$notificationpagereviews,$notificationeventinvitation,$notificationeventgoing,$notificationgroupinvitation,$notificationgrouprequest,$notificationgroupjoin,$notificationgroupadmin, $groupadminrequestresult, $notificationadmindelete,$notificationeditpost,$notificationpublishpost,$notificationpageroles,$notificationpagewall,$n_flag_collection_adminn,$n_editcollection,$n_publish_collection3,$n_flag_page_adminn,$n_publish_page3,$n_flag_event_adminn,$n_publish_event3,$n_publish_group3,$n_flag_group_adminn, $notificationeventorganizer, $eventorganizerrequestresult,$notificationlowcredits,$notificationfollowcollection,$notificationsharecollection,$notificationshareevent,$notificationaddpostevent,$notificationaddphotoevent,$notificationattendevent,$notificationinvitedforevent,$notificationsharegroup,$notificationaddpostgroup,$notificationaddphotogroup,$notificationabecomegroupmember,$notificationinvitedforgroup,$notificationgroupownerforinvitedbyother,$notificationeventownerforinvitedbyother);
		foreach ($notification as $key)
		{
			$sortkeys[] = $key["created_date"];
		}

		if(count($notification))
		{
			array_multisort($sortkeys, SORT_DESC, SORT_STRING, $notification);
		}

		return $notification;
    }
    
}