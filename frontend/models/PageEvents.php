<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;
use frontend\models\PostForm;
use frontend\models\EventVisitors;

class PageEvents extends ActiveRecord
{
    public static function collectionName()
    {
        return 'page_events';
    }

    public function attributes()
    {
        return ['_id', 'type', 'parent_id', 'event_name', 'event_address', 'tagline', 'short_desc', 'event_photo', 'event_thumb', 'isasktojoin','address', 'event_date', 'event_date_ts', 'event_time', 'event_privacy', 'created_date', 'updated_date', 'created_in_date', 
            'updated_in_date', 'created_by', 'updated_by', 'created_from_ip', 'updated_from_ip', 'is_deleted', 'wu', 'tu', 'yu', 
            'tpiu','customids'];
    }
    
    public function getMyEvents($user_id, $start=0)
    {
        if($start == 0 || $start == '') {
            $start = 0;
        } 
        return PageEvents::find()->where(['created_by' => "$user_id",'is_deleted' => '1'])->orderBy(['event_date'=>SORT_ASC])->limit(12)->offset($start)->asarray()->all();
    }
	
    public function getAllEvents($start=0)
    {
        if($start == 0 || $start == '') {
            $start = 0;
        } 
        return PageEvents::find()->where(['is_deleted' => '1'])->orderBy(['event_date'=>SORT_ASC])->limit(12)->offset($start)->asarray()->all();
    }

    public function getAllEventswithlocation($location)
    {
        return PageEvents::find()->where(['like', 'address', $location])->andwhere(['is_deleted' => '1'])->orderBy(['event_date'=>SORT_ASC])->asarray()->all();
    }
	
	public function getAllEventsAdmin()
    {
        return PageEvents::find()->all();
    }
	
    public function getGroupEvent($group_id)
    {
        return PageEvents::find()->where(['is_deleted' => '1','parent_id' => "$group_id",'type' => 'group'])->orderBy(['event_date'=>SORT_DESC])->all();
    }
    
    public function getUpcomingEvents($page_id)
    {
        $date = time();
        return PageEvents::find()->where(['parent_id' => "$page_id", 'is_deleted' => '1'])->andwhere(['created_date'=> ['$gte'=>"$date"]])->orderBy(['event_date'=>SORT_DESC])->all();
    }
    
    public function getUpcomingEventsCount($page_id)
    {
        $date = date('m/d/Y');
        return PageEvents::find()->where(['is_deleted' => '1','parent_id' => "$page_id",'type' => 'page'])->andwhere(['event_date'=> ['$gte'=>"$date"]])->orderBy(['event_date'=>SORT_ASC])->count();
    }
    
    public function getUpcomingLastSixEvents($page_id)
    {
        $date = date('m/d/Y');
        return PageEvents::find()->where(['is_deleted' => '1','parent_id' => "$page_id",'type' => 'page'])->andwhere(['event_date'=> ['$gte'=>"$date"]])->orderBy(['event_date'=>SORT_DESC])->limit(6)->offset(0)->all();
    }
    
    public function getEventdetails($event_id)
    {
        return PageEvents::find()->where(['_id' => "$event_id",'is_deleted'=>"1"])->asarray()->one();
    }

    public function getUser()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'created_by']);
    }

    public function getEventdetailsshare($event_id)
    {
        return PageEvents::find()->with('user')->where(['_id' => "$event_id"])->asarray()->one();
    }

    public function getEventPost($event_id, $start='')
    {
        if($start == 0 || $start == '') {
            $start = 0;
        } 

        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        $eventpost = PostForm::find()->where(['event_id'=>"$event_id",'is_deleted'=>"0"])->orderBy(['post_created_date'=>SORT_DESC])->limit(7)->offset($start)->asarray()->all();
        if($user_id == '') {
            $data = Preferences::find()->where(['user_id' => $user_id, 'object_id' => $event_id])->asarray()->one();
            if(!empty($data)) {
               $sort = $data['sort'];
               if($sort == 'Popular') {
                    $newpostsarray = array();
                    foreach ($posts as $key => $post) {
                        $postId = (string)$post['_id'];
                        $count = Like::getLikePostCount($postId);
                        if($count == '') {
                            $count = 0;
                        }
                        $post['likecount'] = $count;
                        $newpostsarray[] = $post;
                    }

                    usort($newpostsarray, function($a, $b) {
                        if ($a['likecount'] == $b['likecount']) {
                            return 0;
                        }

                        return $b['likecount'] < $a['likecount'] ? -1 : 1;
                    });

                    return $newpostsarray;
               }
            }
        }
        
        return $eventpost;
    }

    public function getEventPostCount($event_id)
    {
        $eventpostcount = PostForm::find()->where(['event_id'=>"$event_id",'is_deleted'=>"0"])->count();
        return $eventpostcount;
    }
    
    public function getPageEvents($page_id)
    {
        return PageEvents::find()->where(['is_deleted' => '1','parent_id' => "$page_id",'type' => 'page'])->orderBy(['event_date'=>SORT_ASC])->all();
    }
    
    public function getPageEventsCount($page_id)
    {
        return PageEvents::find()->where(['is_deleted' => '1','parent_id' => "$page_id",'type' => 'page'])->orderBy(['event_date'=>SORT_ASC])->count();
    }
	
    public function getUpcomingPlaceEvents($place)
    {
        $date = strtotime(date('m/d/Y'));
        return PageEvents::find()->where(['is_deleted' => '1','event_privacy' => 'Public'])->andwhere(['like','address',$place])->andwhere(['event_date_ts'=> ['$gte'=>"$date"]])->orderBy(['event_date'=>SORT_DESC])->all();
    }

    public function getUpcomingPlaceEventsNew($place, $start, $limit)
    {
        $date = strtotime(date('m/d/Y'));
        return PageEvents::find()->where(['is_deleted' => '1','event_privacy' => 'Public'])->andwhere(['like','address',$place])->orderBy(['event_date'=>SORT_DESC])->limit($limit)->offset($start)->asarray()->all();
    }
	
	public function getEvents()
    {
        return PageEvents::find()->where(['is_deleted' => '1'])->orderBy(['event_date'=>SORT_ASC])->all();
    }
	
	public function getFlagevent()
    {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        return PageEvents::find()->where(['is_deleted' => '2'])->orderBy(['created_date'=>SORT_DESC])->all();
    }

    public function isRequestSend($id, $event_id) {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');

        $data = Notification::find()->where(['from_friend_id' => $user_id, 'user_id' => $id, 'post_id' => $event_id,  'notification_type' => 'eventorganizer', 'is_deleted' => '0'])->one();

        if(!empty($data)) {
            return true;
        }

        return false;
    }

    public function setorganizer($user_id, $id, $event_id) {
        
        // check user is must admin of group
        $data = PageEvents::find()->where(['created_by' => $user_id, (string)'_id' => $event_id])->one();
        if(!empty($data)) {
            // check is any pending request 
            $data = Notification::find()->where(['from_friend_id' => $user_id, 'post_id' => $event_id, 'notification_type' => 'eventorganizer', 'is_deleted' => '0'])->one();
            if(!empty($data)) {
                if($data['user_id'] != $id) { 
                    return 'P';
                }
            }

            if(!empty($data)) {
                $data->is_deleted = '1';
                $data->status = '0';
                $data->update();
                return 'A';
            } else {
                $date = time();
                $data =  new Notification();
                $data->from_friend_id = $user_id;
                $data->user_id = $id;
                $data->post_id = $event_id;
                $data->notification_type = 'eventorganizer';
                $data->is_deleted = '0';
                $data->status = '1';
                $data->created_date = "$date";
                $data->updated_date = "$date";
                $data->insert();
                return 'C';
            }
        }
    }

    public function leftevent($user_id, $event_id) {

        $data = PageEvents::find()->where([(string)'_id' => $event_id, 'created_by' => $user_id])->one();
        if(!empty($data)) {
            return 'S';
        } else {
            $data = EventVisitors::find()->where(['user_id' => $user_id, 'event_id' => $event_id])->one();
            if(!empty($data)) {
                $data->delete();
            }
        }
    }
}