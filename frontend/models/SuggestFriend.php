<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;

class SuggestFriend extends ActiveRecord
{
    public static function collectionName()
    {
        return 'suggestedfriends';
    }

    public function attributes()
    {
        return ['_id', 'user_id', 'friend_id', 'suggest_to', 'created_at', 'status'];
    }
}