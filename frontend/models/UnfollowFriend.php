<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;

class UnfollowFriend extends ActiveRecord
{
    public static function collectionName()
    {
        return 'user_mute_post';
    }

    public function attributes()
    {
        return ['_id', 'user_id', 'unfollow_ids'];
    }

     public function  getUnfollowfriendsIds($uid)
     {
       return UnfollowFriend::find()->where(['user_id'=>"$uid"])->one();  
     }
}