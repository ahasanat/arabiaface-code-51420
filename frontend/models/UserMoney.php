<?php
namespace frontend\models;
use Yii;
use yii\base\Model;
use yii\mongodb\ActiveRecord;

class UserMoney extends ActiveRecord
{
    public static function collectionName()
    {
        return 'user_money';
    }

    public function attributes()
    {
        return ['_id', 'user_id', 'amount'];
    }
	
	public function usertotalmoney()
	{
		$session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
		$money = Yii::$app->mongodb->getCollection('user_money');
		$record = array();
		/*$record = $money->aggregate(
			array( '$match' => array( 'user_id' => $user_id ) ),
			array( '$group' => array(
				'_id' => '$user_id',
				'totalmoney' => array( '$sum' => '$amount' )
			))
		);*/
		return $record;
	}
}