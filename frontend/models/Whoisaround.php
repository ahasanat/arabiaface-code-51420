<?php 
namespace frontend\models;
use Yii;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Query;
use yii\db\ActiveQuery;
use yii\db\Expression;
use frontend\models\LoginForm;
use frontend\models\UserForm;
use frontend\models\Messages;
use frontend\models\MessageBlock;

class Whoisaround extends ActiveRecord 
{
    public static function collectionName()
    {
        return 'user';
    }

     public function attributes()
    {

        return ['_id', 'fb_id', 'username', 'fname','lname','fullname', 'password', 'con_password', 'pwd_changed_date', 'email','alternate_email','photo','thumbnail','cover_photo', 'birth_date','gender','created_date','updated_date','created_at','updated_at','status','phone','isd_code','country','country_code','city','citylat','citylong','captcha','vip_flag','member_type','last_login_time','forgotcode','forgotpassstatus','lat','long','login_from_ip'];

    }
	
    public function getUserdata()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'from_id']);
    }
       
    public function scenarios()
    {
        $scenarios = parent::scenarios();     
        return $scenarios;
    }

    public function getAllUsers() {
    	$data = UserForm::find()->with('isvip')->select(['fullname', 'photo', 'thumbnail', 'gender', 'city', 'country','vip_flag'])->where(['status' => '1'])->orderBy(['_id'=>SORT_DESC])->asarray()->all();
    	$vipArray = array();
    	$simpleArray = array();
    	if(!empty($data)) {
    		foreach ($data as $key => $sdata) {
    			if(isset($sdata['vip_flag']) && $sdata['vip_flag'] != '0' && $sdata['vip_flag'] != '') {
    				$vipArray[] = $sdata;
    			} else {
    				$simpleArray[] = $sdata;
    			}
    		}

    		$data = array_merge($vipArray, $simpleArray);
    		return json_encode($data, true);

    	}
    }
    
    public function getAllFriendUsers($user_id) {
		$user_friends = Friend::find()->Where(['to_id'=> $user_id, 'status'=>'1'])->orWhere(['from_id'=> $user_id, 'status'=>'1'])->asarray()->all();
		$defaultArray = array('<div class="no-listcontent">No record found.</div>');
		if(!empty($user_friends)) { 

			$ids = array();
			foreach ($user_friends as $key => $user_friend) {
				if($user_friend['from_id'] == $user_id) {
					$ids[] = $user_friend['to_id'];
				} else {
					$ids[] = $user_friend['from_id'];
				}
			}

			$data = UserForm::find()->select(['fullname', 'photo', 'thumbnail', 'gender', 'city', 'country', 'vip_flag'])->where(['in', (string)'_id', $ids])->orderBy(['_id'=>SORT_DESC])->asarray()->all();
			$vipArray = array();
    		$simpleArray = array();
	    	if(!empty($data)) {
	    		foreach ($data as $key => $sdata) {
	    			if(isset($sdata['vip_flag']) && $sdata['vip_flag'] != '0' && $sdata['vip_flag'] != '') {
	    				$vipArray[] = $sdata;
	    			} else {
	    				$simpleArray[] = $sdata;
	    			}
	    		}
	    		
	    		$data = array_merge($vipArray, $simpleArray);
	    		return json_encode($data, true);
	    	} 
	    }
	    return json_encode($defaultArray, true);
    }

    public function search($post, $searchName = '', $gender, $onlineIds) {
    	if($searchName != '') {
    		if(isset($post['online']) && $post['online'] == 'true') {
    			$data = UserForm::find()
	    		->where(['like','fname', $searchName])
	    		->orwhere(['like','lname', $searchName])
	            ->orwhere(['like','fullname', $searchName])
	            ->andwhere(['status' => '1'])
	            ->andwhere(['in', 'gender', $gender])
	            ->andwhere(['in', '_id', $onlineIds])
	            ->orderBy(['_id'=>SORT_DESC])->asarray()->all();
    		} else {
	    		$data = UserForm::find()
	    		->where(['like','fname', $searchName])
	    		->orwhere(['like','lname', $searchName])
	            ->orwhere(['like','fullname', $searchName])
	            ->andwhere(['status' => '1'])
	            ->andwhere(['in', 'gender', $gender])
	            ->orderBy(['_id'=>SORT_DESC])->asarray()->all();
        	}
    	} else {
    		if(isset($post['online']) && $post['online'] == 'true') {
    			$data = UserForm::find()->where(['in', '_id', $onlineIds])->andwhere(['in', 'gender', $gender])->orderBy(['_id'=>SORT_DESC])->asarray()->all();
    		} else {
				$data = UserForm::find()->where(['in', 'gender', $gender])->orderBy(['_id'=>SORT_DESC])->asarray()->all();
    		}
    	}

    	$vipArray = array();
    	$simpleArray = array();
    	if(!empty($data)) {
    		foreach ($data as $key => $sdata) {
    			if(isset($sdata['vip_flag']) && $sdata['vip_flag'] != '0' && $sdata['vip_flag'] != '') {
    				$vipArray[] = $sdata;
    			} else {
    				$simpleArray[] = $sdata;
    			}
    		}

    		$data = array_merge($vipArray, $simpleArray);
	    	return json_encode($data, true);
    	}
    }

    public function userlist($search_value)
	{
		$session = Yii::$app->session;
		$uid = (string)$session->get('user_id');
		
		$result_member = LoginForm::find()->where(['not in','_id',$uid])->andwhere(['status'=>'1'])->orderBy(['vip_flag'=>SORT_DESC])->all();
		
		foreach($result_member as $result_members){
				$users_all[] = (string)$result_members['id'];
		}
		
		$users_alls =array();
		$result_all_online = Session::getAllOnlineUsers($users_all);
		foreach($result_all_online as $result_all_onlines){
				$users_alls[] = $result_all_onlines['id'];
		}
		$date = time();
		$beforeoneday = strtotime('-1 day', $date);
		if($search_value == 'undefined'){
			$result_members = LoginForm::find()->where(['in','_id',$users_alls])->andwhere(['status'=>'1'])->andwhere(['last_login_time'=> ['$gte'=>"$beforeoneday"]])->orderBy(['vip_flag'=>SORT_DESC])->all();	
		}else{
			$result_members = LoginForm::find()->where(['in','_id',$users_alls])->andwhere(['like','fullname', $search_value])->andwhere(['status'=>'1'])->andwhere(['last_login_time'=> ['$gte'=>"$beforeoneday"]])->orderBy(['vip_flag'=>SORT_DESC])->all();
		}
		return $result_members;
	}
	
	public function userpopularlist()
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		$ans = array();
		$collection = Yii::$app->mongodb->getCollection('users_credits');
		$results = array();
		/*$results = $collection->aggregate(
			array( '$group' => array(
				'_id' => '$user_id',
				'credits' => array( '$sum' => '$credits' )
			))
		);
		asort($results);*/
		return $results;
	}
	
	public function useraroundmelist($search_value)
	{
		$session = Yii::$app->session;
		$user_id = (string)$session->get('user_id');
		
		$select_member = LoginForm::find('city')->where(['_id'=>$user_id])->one();
		$user_city = $select_member['city'];
		if($user_city== null || empty($user_city) || !isset($user_city)){
			$user_city = 'xyz';
		}
		if($search_value == 'undefined'){
			$selected_member = LoginForm::find('city')->where(['like','city', $user_city])->andwhere(['not in','_id',$user_id])->andwhere(['status'=>'1'])->orderBy(['vip_flag'=>SORT_DESC])->all();;
		}else{
			$selected_member = LoginForm::find('city')->where(['like','city', $user_city])->andwhere(['like','fullname', $search_value])->andwhere(['not in','_id',$user_id])->andwhere(['status'=>'1'])->orderBy(['vip_flag'=>SORT_DESC])->all();;
		}
		return $selected_member;
	}	

	public function sendmessagecontent($user_id, $id)
	{
		return UserForm::find()->select(['fullname'])->where(['_id' => $id])->asarray()->one();
	}

	public function sendmessage($user_id, $id, $message)
	{
		$isexist = UserForm::find()->select(['fullname'])->where(['_id' => $id])->asarray()->one();
		if(!empty($isexist)) {
			$data = MessageBlock::find()->where(['from_id' => $user_id, 'to_id' => $id])->orwhere(['from_id' => $id, 'to_id' => $user_id])->asarray()->one();
			if(!empty($data)) {
				$con_id = $data['con_id'];
			} else {
				$data = MessageBlock::find()->where(['from_id' => $user_id, 'to_id' => $id])->orwhere(['from_id' => $id, 'to_id' => $user_id])->asarray()->orderBy(['_id'=>SORT_DESC])->one();
				if(!empty($data)) {
					$con_id = $data['con_id'] + 1;
				} else {
					$con_id = 0;
					$socket = new MessageBlock();
					$socket->from_id = $user_id;
					$socket->to_id = $id;
					$socket->con_id = 0;
					$socket->created_at = strtotime('now'); 
					if(!($socket->insert())) {
						return false;
					}
				}
			}

			$socket = new Messages();
			$socket->type = 'text';
			$socket->from_id = $user_id;
			$socket->to_id = $id;
			$socket->con_id = $con_id;
			$socket->category = 'inbox';
			$socket->created_at = strtotime('now');
			$socket->to_id_del = 0;
			$socket->from_id_del = 0;
			$socket->to_id_read = 0;
			$socket->from_id_read = 0;
			$socket->to_id_flush = 0;
			$socket->from_id_flush = 0;
			$socket->is_read = 0;
			if($socket->insert()) {
				return true;
			}
		}
		return false;
	}	
}