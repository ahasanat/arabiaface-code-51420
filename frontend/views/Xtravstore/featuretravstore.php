<div class="title"><h4>Featured <span class="featured_cat"></span></h4></div>
<div class="post-list">
	<?php 
	if((count($featuredposts)) == 0)
	{
		$this->context->getnolistfound('norecordfound'); 
	}
	$lp = 1; 
	foreach($featuredposts as $featuredpost)
	{ 
		$existing_posts = '1';
		$cls = '';
		if(count($featuredposts)==$lp) {
		  $cls = 'lazyloadscroll'; 
		}

		$this->context->display_last_post($featuredpost['_id'],$existing_posts, '', $cls);
		$lp++;
	}?>
</div>
<div class="clear"></div>
<?php exit();?>