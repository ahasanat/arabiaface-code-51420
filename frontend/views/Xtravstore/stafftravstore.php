<div class="title"><h4>Staff Picks <span class="featured_cat"></span></h4></div>
<div class="post-list">
	<?php 
	if((count($staffposts)) == 0)
	{
		 $this->context->getnolistfound('norecordfound');
	}
	$lp = 1; 
	foreach($staffposts as $staffpost)
	{ 
		$existing_posts = '1';
		$cls = '';
		if(count($staffposts)==$lp) {
		  $cls = 'lazyloadscroll'; 
		}

		$this->context->display_last_post($staffpost['_id'],$existing_posts, '', $cls);
		$lp++;
	}?>
</div>
<div class="clear"></div>
<?php exit();?>