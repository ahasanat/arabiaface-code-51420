<?php   

use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
use frontend\models\LoginForm;
use frontend\models\Friend;
use backend\models\Googlekey;
 
$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session;
$email = $session->get('email');
$user_id = $session->get('user_id');

$this->title = 'Trav Store';
$defaultimage = $baseUrl.'/images/additem-groups.png';

// Friend list
$user_tags =  Friend::getuserFriends($user_id);
$usrfrdlist = array();
foreach($user_tags AS $ud)
{
    $id = (string)$ud['userdata']['_id'];
    $fbid = isset($ud['userdata']['fb_id']) ? $ud['userdata']['fb_id'] : '';
    $dp = $this->context->getimage($ud['userdata']['_id'],'thumb');

    $nm = (isset($ud['userdata']['fullname']) && !empty($ud['userdata']['fullname'])) ? $ud['userdata']['fullname'] : $ud['userdata']['fname'].' '.$ud['userdata']['lname'];
    $usrfrdlist[] = array('id' => $id, 'fbid' => $fbid, 'name' => $nm, 'text' => $nm, 'thumb' => $dp);
}
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
<div class="page-wrapper">
	<div class="header-section">
		<?php include('../views/layouts/header.php'); ?>
	</div>
			
	<div class="floating-icon">
		<div class="scrollup-btnbox anim-side btnbox scrollup-float">
			<div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>			
		</div>
		<?php include('../views/layouts/theme_color.php'); ?>
	</div>
	<div class="clear"></div>
	<?php include('../views/layouts/leftmenu.php'); ?>
	<div class="main-content with-lmenu general-page generaldetails-page travelstore-page main-page">
		<div class="combined-column">
			<div class="content-box">
				<div class="cbox-title nborder">						
					<i class="mdi mdi-shopping"></i>
					Travel Store
				</div>					
				<div class="cbox-desc">
					<div class="fake-title-area divided-nav">							
						<ul class="tabs">								
							<li class="feacturedtrav active tab"><a href="#tstore-featured" data-toggle="tab" aria-expanded="true">Featured</a></li>
							<li class="stafftrav tab"><a href="#tstore-staff" data-toggle="tab" aria-expanded="false">Staff Picks</a></li>								
							<li class="yourtrav tab"><a href="#tstore-yours" data-toggle="tab" aria-expanded="false" onclick="nestedTabCalled('main','groups-yours')">Yours</a></li>
						</ul>
					</div>
					<div class="friends-search">
						<div class="fsearch-form">
							<input type="text" class="travitemsearch" placeholder="Search for a product"/>
							<a href="javascript:void(0)"><i class="zmdi zmdi-search"></i></a>
						</div>
					</div>
					<div class="tab-content view-holder grid-view">		
						<div class="travelstore-details">
							<div class="row">
								<div class="travelstore-summery">
									<h4>Catgories</h4>
									<div class="main-catlist">
										<ul class="cat-list">
											<li class="cat_li all_cat active"><a href="javascript:void(0)" class="cate_click" tabname="" onclick="category_change('undefined','all_cat')">All Category</a></li>
											<?php foreach ($trav_cat as $trav_cats){?>
												<li class="cat_li <?= $trav_cats['_id'];?>"><a href="javascript:void(0)" class="cate_click" tabname="" onclick="category_change('<?= $trav_cats['name'];?>','<?= $trav_cats['_id'];?>')"><?= $trav_cats['name'];?></a></li>
											<?php }?>	
										</ul>
									</div>
									<div class="mbl-catlist">
										<div class="sliding-middle-out anim-area underlined fullwidth">
											<select class="select2">
												<?php foreach ($trav_cat as $trav_cats){?>
													<option value="<?= $trav_cats['name'];?>" ><?= $trav_cats['name'];?></option>
												<?php }?>
											</select>
										</div>
									</div>
								</div>
								<div class="post-column">
									<div class="tab-content">
										<div class="tab-pane fade main-pane active in" id="tstore-featured">
											
										</div>
										<div class="tab-pane fade main-pane" id="tstore-staff">		
											
										</div>
										<div class="tab-pane fade main-pane" id="tstore-yours">
											<div class="title"><h4>Yours <span class="yours_cat"></span></h4></div>
											<div class="new-post" id="viacall_new_post">
												<?= \Yii::$app->view->renderFile('@app/views/layouts/travblock.php'); ?>
											</div>
											<div id="tstore-yoursss">
											
											<div class="clear"></div>
											</div>
										</div>										
									</div>										
								</div>										
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="chatblock">
						<div class="float-chat anim-side">
							<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
							</div>
						</div>
					</div>
	</div>
		<?php include('../views/layouts/footer.php'); ?>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>
<script>
var data1=<?php echo json_encode($usrfrdlist); ?>;

//Seach function
$( document ).on( 'keyup', '.travitemsearch', function(e) 
{
	var tabname = $('.cate_click').attr('tabname');
	var textValue = $(this).val();
	if(tabname == 'featured'){
		feacturedtrav('undefined',textValue);
	}else if(tabname == 'yours'){
		yourtrav('undefined',textValue);
	}
});

//Category change function
function category_change(cat_name,cat_id)
{
	$('.cat_li').removeClass('active');
	$('.'+cat_id).addClass('active');
	var tabname = $('.cate_click').attr('tabname');
	if(tabname == 'featured'){
		feacturedtrav(cat_name);
	}else if(tabname == 'yours'){
		yourtrav(cat_name);
	}
}

//window load function
$(document).ready(function(){
	feacturedtrav();
	$('.feacturedtrav').click(function() {
		$('.cate_click').attr('tabname','featured');
		$('.cat_li').removeClass('active');
		$('.all_cat').addClass('active');
		$('.travitemsearch').val('');
		feacturedtrav();		
    });
	$('.stafftrav').click(function() {
		$('.cate_click').attr('tabname','yours');
        $('.cat_li').removeClass('active');
		$('.all_cat').addClass('active');
		$('.travitemsearch').val('');
		stafftrav();
    });
	$('.yourtrav').click(function() {
		$('.cate_click').attr('tabname','yours');
        $('.cat_li').removeClass('active');
		$('.all_cat').addClass('active');
		$('.travitemsearch').val('');
		yourtrav();		
    });
	
});

//featured function
function feacturedtrav(cat_name,searchval)
{
	$('.cate_click').attr('tabname','featured');
	var baseUrl ='<?php echo (string) $baseUrl; ?>';
    $.ajax({
        url: '?r=travstore/feacturedtrav',  
        type: 'POST',
        data: "baseUrl="+baseUrl+"&cat_name="+cat_name+"&searchval="+searchval,
        success: function(data)
        {
			$("#tstore-featured").html(data);
			if(cat_name != 'undefined' && cat_name != undefined){
				$('.featured_cat').html('('+cat_name+')');
			}else{
				$('.featured_cat').html('');
			}
			setTimeout(function(){fixImageUI();},300);
		}
    });
}

//Staff function
function stafftrav(cat_name,searchval)
{
	$('.cate_click').attr('tabname','featured');
	var baseUrl ='<?php echo (string) $baseUrl; ?>';
    $.ajax({
        url: '?r=travstore/stafftrav',  
        type: 'POST',
        data: "baseUrl="+baseUrl+"&cat_name="+cat_name+"&searchval="+searchval,
        success: function(data)
        {
			$("#tstore-staff").html(data);
			if(cat_name != 'undefined' && cat_name != undefined){
				$('.staff_cat').html('('+cat_name+')');
			}else{
				$('.staff_cat').html('');
			}
			setTimeout(function(){fixImageUI();},300);
		}
    });
}

//yourtrav function
function yourtrav(cat_name,searchval){
	var baseUrl ='<?php echo (string) $baseUrl; ?>';
    $.ajax({
        url: '?r=travstore/yourtrav',  
        type: 'POST',
        data: "baseUrl="+baseUrl+"&cat_name="+cat_name+"&searchval="+searchval,
        success: function(data)
        {
            $("#tstore-yoursss").html(data);
			if(cat_name!= 'undefined' && cat_name != undefined){
				$('.yours_cat').html('('+cat_name+')');
			}else{
				$('.yours_cat').html('');
			}
			setTimeout(function(){fixImageUI();},300);
        }
    });
}

//add post code start
$("body").on('input propertychange', '#textInput', function()
{
	toggleAbilityTravButton();
});
function toggleAbilityTravButton() 
{
	if($("#title").val().trim().length > 0 || $("#textInput").val().trim().length > 0 || storedFiles.length > 0)
	{
	$(".posttravstore").attr("disabled",false);
	}   
	else if($("#title").val().trim().length == 0 && $("#textInput").val().trim().length == 0 && storedFiles.length == 0)
	{
	$(".posttravstore").attr("disabled",true);
	}
	else
	{
	$(".posttravstore").attr("disabled",true);
	}
}

$("body").on("click", ".posttravstore", function(){
	var col = $(this).closest('form').attr('id');
	var getparntcls = $("#"+col).parent().attr('id');
	if(getparntcls)
	{
		addTravstore("#"+getparntcls);
	}
})

function addTravstore(id) 
{
	var status = $("#user_status").val();
	if(status == 0)
	{
		return false;
	}
	//var reg = /<(.|\n)*?>/g;
	var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
	var title = $("#title").val();

	chk = 'off';
	if($('#customchk:checkbox:checked').length > 0) {
		chk = 'on';
	}
	
	var status = document.getElementById('textInput').value;
	if (status != "" && reg.test(status) == true)
	{
		 status = status.replace(reg, " ");
	}
	if (title != "" && reg.test(title) == true)
	{
		 title = title.replace(reg, " ");
	}
	var count = document.getElementById('hiddenCount').value;
	var formdata;
	formdata = new FormData($('form')[1]);
	for(var i=0, len=storedFiles.length; i<len; i++) {
		formdata.append('imageFile1[]', storedFiles[i]); 
	}
	if(title === '')
	{
		return false;
	}
	
	if (/^\s+$/.test(status) && document.getElementById('imageFile1').value == "" )
	{
		toggleAbilityPostButton(HIDE);
		$('#textInput').val("");
		$("#textInput").focus();
		return false;
	}
	else if (status == "" && document.getElementById('imageFile1').value == "")
	{
		$("#textInput").focus();
		return false;
	} else if (status != "" && document.getElementById('imageFile1').value == "")
	{
		formdata.append("test", status);
		formdata.append("imageFile1", "");

	} else if (document.getElementById('imageFile1').value != "" && status == "")
	{
		var ofile = document.getElementById('imageFile1').files;
		formdata.append("imageFile1", ofile);
		formdata.append("test", "");

	} else if (document.getElementById('imageFile1').value != "" && status != "")
	{
		var ofile = document.getElementById('imageFile1').files;
		formdata.append("imageFile1", ofile);
		formdata.append("test", status);
	} else
	{
		return false;
	}
	$(".post_loadin_img").css("display","inline-block");
	formdata.append("trav_price" ,$('#trav_pricce').val());
	formdata.append("trav_cat" ,$('#trav_cat').html());
	  
	formdata.append("sharewith",$('#customin1').val());
	formdata.append("sharenot",$('#customin3').val());
	formdata.append("customchk", chk);
	formdata.append("posttags" ,$('#taginput').val());

	formdata.append("current_location",$('#cur_loc').val());
	formdata.append("post_privacy",$('#post_privacy').val());
	formdata.append("link_title",$('#link_title').val());
	formdata.append("link_url",$('#link_url').val());
	formdata.append("link_description",$('#link_description').val());
	formdata.append("link_image",$('#link_image').val());
	formdata.append("title",$('#title').val());
	formdata.append("share_setting",$('#share_setting').val());
	formdata.append("comment_setting",$('#comment_setting').val());
	formdata.append("pagename",$('#pagename').val());
	formdata.append("tlid",$('#tlid').val());
	$('.post-loader').addClass('show');
	managePostButton('HIDE');
	 // formdata.append("newpost_flag",'1');
	// alert($('#link_image').val());
	$.ajax({
		url: '?r=site/upload',  
		type: 'POST',
		data:formdata,
		async:false,        
		processData: false,
		contentType: false,
		success: function(data) {
			newct = 0;
			lastModified = [];
			storedFiles = [];
			storedFiles.length = 0;
			$("#taginput").val('').trigger('change');

			$(data).hide().prependTo(".post-list").fadeIn(3000);
			$(id + ' ' + '.post-bcontent').slideUp();
			managePostButton('SHOW');
			load_last_post(data);        	
		},
		complete: function(){
		   $('.post-loader').removeClass('show');
		}
	}).done(function(){
	   setTimeout(function(){fixImageUI('newpost');},500);
  });

	return true;
}	
/* FUN end page review functions */
</script>	