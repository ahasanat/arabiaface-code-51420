<?php   

use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
use frontend\models\LoginForm;
use frontend\models\Friend;

$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session;
$email = $session->get('email');
$user_id = $session->get('user_id');
$defaultimage = $baseUrl.'/images/additem-groups.png';
?>
<div class="post-list">
	<?php 
	/*if((count($yourposts)) == 0)
	{
		echo '<div class="no-listcontent">No trav items added by you.</div>';
	}*/
	$lp = 1; 
	foreach($yourposts as $yourpost)
	{ 
		$existing_posts = '1';
		$cls = '';
		if(count($yourposts)==$lp) {
		  $cls = 'lazyloadscroll'; 
		}

		$this->context->display_last_post($yourpost['_id'],$existing_posts, '', $cls);
		$lp++;
	}?>
</div>
<div class="post-holder bshadow">						
	<div class="joined-tb">
		<i class="mdi mdi-home"></i>								
		<h4>Welcome to the Travel Store</h4>
		<p>Add your ad here at the arabiaface store and increase your sale</p>
	</div>						
</div>
<?php exit;?>