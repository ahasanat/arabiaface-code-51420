<?php   
use frontend\assets\AppAsset;
use frontend\models\ChannelSubscriber;
use backend\models\Googlekey;
 
$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$request = Yii::$app->request;
$col_id = (string)$request->get('col_id');
$this->title = 'Channel Details';
$fcount = ChannelSubscriber::getchannelsubscribercount($channeldetail['_id']);
if($fcount==0) { 
	$fcount = "0";	
} 

$profile = (isset($channeldetail['profile']) && $channeldetail['profile'] != '') ? $channeldetail['profile'] : '';
if($profile == '' || !file_exists('uploads/channel/thumb_'.$profile)) {
	$profile = $baseUrl.'/images/additem-channels.png';
}
$isSubscriber = ChannelSubscriber::isSubscriber($user_id, $channeldetail['_id']);
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
<div class="page-wrapper menutransheader-wrapper menuhideicons-wrapper">
	<div class="header-section">
        <?php include('../views/layouts/header.php'); ?>
    </div>
	<div class="floating-icon">
		<div class="scrollup-btnbox anim-side btnbox scrollup-float">
			<div class="scrollup-button float-icon">
				<span class="icon-holder ispan">
					<i class="mdi mdi-arrow-up-bold-circle"></i>
				</span>
			</div>			
		</div>			
	</div>
	<div class="clear"></div>
	<div class="container page_container fulltab">
		<?php include('../views/layouts/leftmenu.php'); ?>			
		<div class="fixed-layout ipad-mfix">			
			<div class="main-content with-lmenu general-page generaldetails-page channels-page channeldetails-page main-page">
				<div class="combined-column">
					<div class="content-box">
						<div class="cbox-title nborder">
							<i class="mdi mdi-view-grid-large"></i>
							Channels
							<a href="?r=channel" class="backbtn"><i class="mdi mdi-menu-left"></i> Back to channels</a>
						</div>
						<div class="cbox-desc">							
							<div class="tab-content view-holder">								
								<div class="general-details">									
									<div class="gdetails-summery">
										<div class="main-info">
											<div class="imgholder"> 
												<img src="uploads/channel/thumb_<?= $profile;?>"/>
												<div class="back-link">
													<a href="?r=channel" class="waves-effect waves-theme"><i class="mdi mdi-arrow-left"></i></a>
												</div>
												<div class="action-links item_detail_dropdown"> 
													<?php if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { ?>
														<a href="javascript:void(0)" class="waves-effect waves-theme orglink <?=$checkuserauthclass?> directcheckuserauthclass share-it">
															<i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
														</a>													
													<?php } else if($isSubscriber == true) { ?>
														<a href="javascript:void(0)" class="waves-effect waves-theme orglink sharepostmodalAction share-it" data-sharepostid="channel_<?=$col_id;?>">
															<i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
														</a>													
													<?php }?>
													<div class="settings-icon">
														<?php if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { ?>
															<a class="dropdown-button waves-effect waves-theme <?=$checkuserauthclass?> directcheckuserauthclass" href="javascript:void(0)" data-activates="detail_setting">
																<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
															</a>
														<?php } else { ?>
															<a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting">
																<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
															</a>
															<ul id="detail_setting" class="dropdown-content custom_dropdown">
															<?php if($isSubscriber == true) { ?>
																<li><a href="javascript:void(0)" onclick="preferencesopenpopup('<?= $channeldetail['_id'];?>', 'channel');">Preferences</a></li>
																<li><a class="<?=$checkuserauthclass?>" onclick="channelSubscribeDetail('<?=$channeldetail['_id'];?>',this)">Unsubscribe channel</a></li>
															<?php } ?>
																<li><a href="javascript:void(0)" onclick="reportabuseopenpopup('<?= $channeldetail['_id'];?>', 'Channel');">Report abuse</a></li>
															</ul>
														<?php } ?>
													</div>
												</div>
											</div>
											<div class="content-holder dateholder">
												<div class="countinfo">
													<?=$channeldetail['count']?>
												</div>													
											</div>													
											<div class="content-holder gdetails-moreinfo expandable-holder">
												<a href="javascript:void(0)" class="expandable-link invertsign" onclick="mng_expandable(this)"><i class="mdi mdi-chevron-down"></i></a>
												<div class="expandable-area">												
													<div class="username">
														<span><?= $this->context->getuserdata((string)$channeldetail['_id'],'fullname');?></span>
													</div>
													<h4><?= ucfirst($channeldetail['name']);?></h4>
													<div class="username tagline">
														<span></span>
													</div>
													<div class="icon-line">
														<span class="fcount">
														<?php 
															if($fcount>1) {
																echo $fcount .' subscribers';
															} else {
																echo $fcount .' subscriber';
															}
														?>
														 </span>
														<span>-</span>
														<span><?= $this->context->getpostcount($col_id,'channel_id');?> posts</span>
													</div>													
													<div class="action-btns">	
														<a class="<?=$checkuserauthclass?>" onclick="channelSubscribeDetail('<?=$channeldetail['_id'];?>',this)">
															<?php
															if($isSubscriber == true) {
																echo 'Subscribed';
															} else {
																echo 'Subscribe';
															}
															?>	
															</a>
													</div>
												</div>
											</div>
										</div>
										<div class="sideboxes">
											<?php include('../views/layouts/recently_joined.php'); ?>
											<?php include('../views/layouts/travads.php'); ?>
										</div>
									</div>
									<div class="post-column">
										<?php 
										$lp = 1; 
										if(isset($isSubscriber) && $isSubscriber == true) {
										$checkposttime = ChannelSubscriber::find()->select(['created_at'])->where(['channel_id' => (string)$channeldetail['_id'], 'user_id' => $user_id])->asarray()->one();
										$joinedTime = $checkposttime['created_at'];

										 ?>
										<div class="row">
											<div class="col s12 m12"> 
												<div class="new-post base-newpost">
													<form action="">
														<div class="npost-content">
															<div class="post-mcontent">
																<i class="mdi mdi-pencil-box-outline main-icon"></i>
																<div class="desc">									
																	<div class="input-field comments_box">
																		<input placeholder="What's new?" type="text" class="validate commentmodalAction_form" />
																	</div>
																</div>
															</div>
														</div>				
														
													</form>
													<div class="overlay" id="composetoolboxAction"></div>
												</div>
											</div>
										</div>
										<div class="post-list margint15">
											<div class="row">
												<?php 
												$templp = 1;
												$lp = 1;
												foreach($posts as $post)
												{ 
													$postTime = $post['post_created_date'];
													if($postTime < $joinedTime) {
														continue;
													}
													
													$existing_posts = '1';
													$cls = '';
													if(count($posts)==$templp) {
													  $cls = 'lazyloadscroll'; 
													}

													$postid = (string)$post['_id'];
													$postownerid = (string)$post['post_user_id'];
													$postprivacy = $post['post_privacy'];

													$isOk = $this->context->filterDisplayLastPost($postid, $postownerid, $postprivacy);
													if($isOk == 'ok2389Ko') {
														if(($lp%8) == 0) {
															$ads = $this->context->getad(true); 
															if(isset($ads) && !empty($ads))
															{
																$ad_id = (string) $ads['_id'];	
																$this->context->display_last_post($ads['_id'], $existing_posts, '', $cls);
																$lp++;
															} else {
																$lp++;
															}
														} else {
															$this->context->display_last_post((string)$postid, $existing_posts, '', $cls);
															$lp++;	
														}						
													}
													$templp++;	
												}?>
											</div>
										</div>
										<div class="clear"></div>
										<?php 
										}
										if($lp <= 1){
											$this->context->getwelcomebox("channel");
										} ?>
										<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100 dis-none"> <div></div> </div></div></center>
									</div>									
								
								</div>
								
							</div>
						</div>
					</div>
				
				</div>
			</div>	
			<div id="chatblock">
				<div class="float-chat anim-side">
					<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
					</div>
				</div>
			</div>
			 <?php if($isSubscriber == true) { ?>
			 <div class="new-post-mobile clear">
				<a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
			</div>			
			 <?php } ?>
		</div>
	</div>	
	<?php include('../views/layouts/footer.php'); ?>
</div>	
<input type="hidden" name="pagename" id="pagename" value="channel" />
<input type="hidden" name="tlid" id="tlid" value="<?=$col_id?>" />
<input type="hidden" name="baseurl" id="baseurl" value="<?=$baseUrl?>" />

<?php include('../views/layouts/preferences.php'); ?>

<div id="compose_tool_box" class="modal compose_tool_box post-popup custom_modal main_modal">
</div>

<div id="composeeditpostmodal" class="modal compose_tool_box edit_post_modal post-popup main_modal custom_modal compose_edit_modal">
</div>

<div id="sharepostmodal" class="modal sharepost_modal post-popup main_modal custom_modal">
</div>

<!-- Post detail modal -->
<div id="postopenmodal" class="modal modal_main compose_tool_box custom_modal postopenmodal_main postopenmodal_new">	
</div>

<!--post comment modal for xs view-->
<div id="comment_modal_xs" class="modal comment_modal_xs">
</div> 

<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
	<?php include('../views/layouts/mapmodal.php'); ?>
</div> 

<?php include('../views/layouts/addpersonmodal.php'); ?>
<?php include('../views/layouts/editphotomadol.php'); ?>

<script>
$(document).ready(function(){
	$(".discard_md_modal .modal_discard").attr('data-id','<?=$col_id?>');
	var channelName = '<?= ucfirst($channeldetail['name']);?>';
	$('.page-name.mainpage-name').html(channelName);
});
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>
<script type="text/javascript" src="<?=$baseUrl?>/js/channel.js"></script> 
<script src="<?=$baseUrl?>/js/post.js"></script>