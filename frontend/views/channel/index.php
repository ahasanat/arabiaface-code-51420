<?php   
use frontend\assets\AppAsset; 
use backend\models\Googlekey; 
$baseUrl = AppAsset::register($this)->baseUrl; 
$this->title = 'Channels';
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
<link href="<?=$baseUrl?>/css/animate.css" rel="stylesheet">

<div class="page-wrapper gen-pages">
	<div class="header-section">
		<?php include('../views/layouts/header.php'); ?>
	</div>       
	<div class="floating-icon">
		<div class="scrollup-btnbox anim-side btnbox scrollup-float">
			<div class="scrollup-button float-icon">
				<span class="icon-holder ispan">
					<i class="mdi mdi-arrow-up-bold-circle"></i>
				</span>
			</div>          
		</div>           
	</div>
	<div class="clear"></div>
	<div class="container page_container channel_container">
		<?php include('../views/layouts/leftmenu.php'); ?>
		<div class="fixed-layout ipad-mfix">
			<div class="main-content with-lmenu channels-page main-page grid-view general-page">
				<div class="combined-column comsepcolumn">
					<div class="content-box nbg">
						<div class="cbox-desc md_card_tab">
							<div class="fake-title-area divided-nav mobile-header">
								<ul class="tabs">
									<li class="tab col s3" onclick="suggestedList();">
										<a class="active" href="#channels-suggested" data-toggle="tab" aria-expanded="true">Suggested</a>
									</li> 
									<li class="tab col s3" onclick="subscribedList();">
										<a href="#channels-subscribed" data-toggle="tab" aria-expanded="false">Subscribed</a>
									</li>
									<li class="tab col s3" onclick="popularList();">
										<a href="#channels-popular" data-toggle="tab" aria-expanded="false">Popular</a>
									</li>
								</ul>									
							</div>
							<div class="tab-content view-holder grid-view">								
								<div class="tab-pane fade main-pane active in" id="channels-suggested">
									<div class="generalbox-list channels-list">								
										<div class="row">
											<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
										</div>
									</div>
								</div>
								<div class="tab-pane fade main-pane dis-none" id="channels-subscribed">
									<div class="generalbox-list channels-list">			
										<div class="row"></div>
									</div>
								</div>
								<div class="tab-pane fade main-pane general-yours dis-none" id="channels-popular">
									<div class="generalbox-list channels-list">					
										<div class="row"></div>
									</div>
								</div>				 			
							</div>
						</div>
					</div>
				</div>
				<div id="chatblock">
					<div class="float-chat anim-side">
						<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
						</div>
					</div>
				</div>
			</div>
		</div> 
	</div>	 
	<?php include('../views/layouts/footer.php'); ?>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>

<script type="text/javascript" src="<?=$baseUrl?>/js/channel.js"></script>  
