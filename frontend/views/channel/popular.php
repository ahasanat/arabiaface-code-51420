<?php   
use frontend\assets\AppAsset; 
use frontend\models\ChannelSubscriber;
use frontend\models\Channel;

$baseUrl = AppAsset::register($this)->baseUrl; 
$session = Yii::$app->session; 
$user_id = (string)$session->get('user_id');
$count = 1;
if(isset($data) && !empty($data)) { 
	foreach ($data as $key => $sdata) {	
		$channel_id = array_values($sdata['_id'])[0];				
		$postcount = Channel::postcount($channel_id);

		$profile = (isset($sdata['profile']) && $sdata['profile'] != '') ? $sdata['profile'] : '';
		if($profile == '' || !file_exists('uploads/channel/thumb_'.$profile)) {
			$profile = $baseUrl.'/images/additem-channels.png';
		} else {
			$profile = 'uploads/channel/thumb_'.$profile;
		}

		$name = $sdata['name'];	
    	$isSubscribe = ChannelSubscriber::isSubscriber($user_id, $channel_id);
		$label = 'Subscribe';
		if($isSubscribe == 'S') {
			$label = 'Subscribed';	
		} 

		$isDefaultProfile = $imgclass = '';
		if (strpos($profile, 'additem-channels.png') !== false) {
		    $isDefaultProfile ='defaultprofile';
		} else {
			$val = getimagesize($profile);
	        if($val[0] > $val[1]) { 
	            $imgclass = 'himg';
	        } else if($val[1] > $val[0]) { 
	            $imgclass = 'vimg';
	        } else {
	            $imgclass = 'himg';
	        }
	    }
		?>
		<div class="col s6 m4 l3 gridBox127 channelbox channelbox_<?=$channel_id?>">
			<div class="card hoverable channelCard animated fadeInUp">
				<a href="javascript:void(0);" onclick="channelSubscribe(event, '<?=$channel_id?>', this, true, <?=$count?>)" class="general-box">
					<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
						<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
					</div>
					<div class="content-holder">
						<h4><?=$name?></h4>
						<div class="icon-line">
							<span>Tagline goes here</span>
						</div>	
						<div class="countinfo">
							<?=$count?>
						</div>														
						<div class="posts-line">
							<?=$postcount?>
						</div>														
						<div class="action-btns">
							<span class="noClick <?=$checkuserauthclass?>" onclick="channelSubscribe(event, '<?=$channel_id?>', this, false, <?=$count?>)"><?=$label?></span>
						</div>
					</div>
				</a>
			</div>
		</div>
		<?php 
		$count++;
	}
}
exit;