<?php 
use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
use frontend\models\Friend;
$baseUrl = AppAsset::register($this)->baseUrl;  
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id'); 
$status = $session->get('status');
if(isset($allcollection) && !empty($allcollection)) { 
	foreach($allcollection as $allcollections) {
		$isWriteAllow = 'no';
		$idArray = array_values($allcollections['_id']);
		$collection_id = $idArray[0];
		//$collection_id = (string)$allcollections['_id']['$id'];
		$dp = $this->context->getimage($allcollections['user_id'],'thumb');
		$follow_count = CollectionFollow::getcollectionfollowcount($collection_id);
		$follow_name = CollectionFollow::getFollowerUserNames((string)$collection_id);
		$color = isset($allcollections['color']) ? $allcollections['color'] : '';
		$tagline = isset($allcollections['tagline']) ? $allcollections['tagline'] : '';
		if($follow_count == 0){
			$follow_count = "No";
			$follow_name  = "Become a first to follow this";
		} else {
			$follow_name = $follow_name." followed this";
		} 
		$col_created_by =$allcollections['user_id'];
		$col_privacy = $allcollections['privacy'];

		$is_friend = Friend::find()->where(['from_id' => "$user_id",'to_id' => "$col_created_by",'status' => '1'])->one();

		if($user_id == $col_created_by) {
			$isWriteAllow = 'yes';
		} else if(($col_privacy == 'Public' || $status == '10') || ($col_privacy == 'Friends' && ($is_friend || $status == '10')) || ($col_privacy == 'Private')) {
			$isWriteAllow = 'yes';
		} else if($col_privacy == 'Custom') {
			$customids = array();
		    $customids = isset($allcollections['customids']) ? $allcollections['customids'] : '';
		    $customids = explode(',', $customids);

		    if(in_array($user_id, $customids)) {
				$isWriteAllow = 'yes';	    	
		    }
		} else if($col_privacy == 'Friend of friend') {
			$friendoffriendids = Friend::getuserFriendsIds($col_created_by);
			foreach ($friendoffriendids as $levelsingle) {
				$level = Friend::getuserFriendsIds($levelsingle);
				if(!empty($level)) {
					$friendoffriendids = array_merge($friendoffriendids, $level);	
				}
			}
			$friendoffriendids = array_unique($friendoffriendids);

			if(in_array($user_id, $friendoffriendids)) {
				$isWriteAllow = 'yes';	 	
			}
		}

		if($isWriteAllow == 'yes')
		{


		$profile = $this->context->getcollectionimage($collection_id);

		$isDefaultProfile = $imgclass = '';
		if (strpos($profile, 'additem-collections.png') !== false) {
		    $isDefaultProfile ='defaultprofile';
		} else {
			$val = getimagesize($profile);
	        if($val[0] > $val[1]) { 
	            $imgclass = 'himg';
	        } else if($val[1] > $val[0]) { 
	            $imgclass = 'vimg';
	        } else {
	            $imgclass = 'himg';
	        }
	    }

		?> 
		<div class="col s6 m4 l3 gridBox127 collectionbox collectionbox_<?=$collection_id?>"> 
			<div class="card hoverable collectionCard">
				<a href="javascript:void(0);" class="general-box <?=$color;?>" onclick="collectionFollows(event,'<?=$collection_id;?>',this, true)">
					<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
						<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
					</div>
					<div class="content-holder">
						<h4><?= ucfirst($allcollections['name']);?></h4>
						<div class="icon-line">
							<span><?= $tagline;?></span>
						</div>						
						<div class="userinfo">
							<img src="<?= $dp;?>"/>
						</div>
						<div class="username">
							<span><?= $this->context->getuserdata($allcollections['user_id'],'fullname');?></span>
						</div>													
						<div class="action-btns">						
							<span class="noClick <?=$checkuserauthclass?>" onclick="collectionFollows(event,'<?=$collection_id;?>',this, false)"><?= CollectionFollow::getcollectionfollow($collection_id, $user_id);?></span>
						</div>
					</div>
				</a>
			</div>
		</div>
		<?php 
		}
	}
}
exit;?>					

		