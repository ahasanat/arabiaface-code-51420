<?php   
use frontend\assets\AppAsset;
use backend\models\Googlekey; 
$baseUrl = AppAsset::register($this)->baseUrl;
$this->title = 'Collections';
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
<link href="<?=$baseUrl?>/css/animate.css" rel="stylesheet">
<div class="page-wrapper gen-pages">
	<div class="header-section">
        <?php include('../views/layouts/header.php'); ?>
    </div>
	<div class="floating-icon">		
		<div class="scrollup-btnbox anim-side btnbox scrollup-float">
			<div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>			
		</div>
	</div>
	<div class="clear"></div>
	<div class="container page_container collection_container">
		<?php include('../views/layouts/leftmenu.php'); ?>
		<div class="fixed-layout ipad-mfix">
			<div class="main-content with-lmenu collections-page main-page grid-view general-page">
				<div class="combined-column comsepcolumn">
					<div class="content-box nbg">
						<div class="cbox-desc md_card_tab">
							<div class="fake-title-area divided-nav mobile-header">
								<ul class="tabs">
									<li class="tab col s3 allcollection active">
										<a class="active" href="#collections-suggested" data-toggle="tab" aria-expanded="true">Suggested</a>
									</li>
									<li class="tab col s3 following">
										<a href="#collections-following" data-toggle="tab" aria-expanded="false">Following</a>
									</li>
									<li class="tab col s3 yours">
										<a href="#collections-yours" data-toggle="tab" aria-expanded="false">Yours</a>
									</li>
								</ul>
							</div>
							
							<div class="tab-content view-holder grid-view">											 
								<div class="tab-pane fade main-pane active in" id="collections-suggested">
									<div class="generalbox-list">
										<div class="row">
											<center>
												 <div class="lds-css ng-scope">
												    <div class="lds-rolling lds-rolling100">
												        <div></div>
												    </div>
												 </div>
											</center>
										</div>
									</div>
								</div>
								<div class="tab-pane fade main-pane" id="collections-following">	
									<div class="generalbox-list animated fadeInUp">		
										<div class="row"></div>
									</div>
								</div>
								<div class="tab-pane fade main-pane collections-yours general-yours" id="collections-yours">	
									<div class="generalbox-list animated fadeInUp">
										<div class="row"></div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div id="chatblock">
					<div class="float-chat anim-side">
						<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<?php include('../views/layouts/footer.php'); ?>
</div>	
<!--add collection modal-->

<?php include('../views/layouts/addpersonmodal.php'); ?>

<div id="add_collection_modal" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new">
</div>

<!--discard collection  modal-->
<?php include('../views/layouts/custom_modal.php'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>
<script src="<?=$baseUrl?>/js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/collection.js"></script>
