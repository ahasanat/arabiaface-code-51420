<?php   
use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
use frontend\models\CollectionReport;
use backend\models\Googlekey; 
$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session;
$status = $session->get('status');
$user_id = $session->get('user_id');
$request = Yii::$app->request;
$col_id = (string)$request->get('col_id');
$this->title = 'Collection Details';
$fcount = CollectionFollow::getcollectionfollowcount($collectiondetail['_id']);
if($fcount==0){
	$fcount = "No";	
}
$col_image = $this->context->getcollectionimage($collectiondetail['_id']);
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>

<div class="page-wrapper menutransheader-wrapper menuhideicons-wrapper">
	<div class="header-section">
        <?php include('../views/layouts/header.php'); ?>
    </div>
	<div class="floating-icon">
		<div class="scrollup-btnbox anim-side btnbox scrollup-float">
			<div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>		
		</div>
	</div>
	<div class="clear"></div>
	<div class="container page_container fulltab">
		<?php include('../views/layouts/leftmenu.php'); ?>
		<div class="fixed-layout ipad-mfix">			
			<div class="main-content with-lmenu general-page generaldetails-page collections-page collectiondetails-page main-page">
			
				<div class="combined-column">
					<div class="content-box">
						<div class="cbox-title nborder">
							<i class="mdi mdi-folder-open"></i>
							Collections
							<a href="?r=collection" class="backbtn"><i class="mdi mdi-menu-left"></i> Back to collections</a>
						</div>
						<div class="cbox-desc">
							
							<div class="tab-content view-holder">								
								<div class="general-details">									
									<div class="gdetails-summery <?php //echo $collectiondetail['color'];?>">
										<div class="main-info">
											<div class="imgholder">
												<img src="<?= $col_image;?>"/>
												<div class="back-link">
													<a href="?r=collection" class="waves-effect waves-theme"><i class="mdi mdi-arrow-left"></i></a>
												</div>
												<div class="action-links item_detail_dropdown">
													<?php if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { ?>
														<a href="javascript:void(0)" class="waves-effect waves-theme orglink <?=$checkuserauthclass?> directcheckuserauthclass share-it">
															<i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
														</a>			
													<?php } else { 
														if(CollectionFollow::getcollectionfollow((string)$collectiondetail['_id'], (string)$user_id) == 'Following' || CollectionFollow::getcollectionfollow((string)$collectiondetail['_id'], (string)$user_id) == 'Owner') { ?>
															<a href="javascript:void(0)" class="waves-effect waves-theme orglink sharepostmodalAction share-it" data-sharepostid="collection_<?=$col_id;?>">
																<i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
															</a>
														<?php } ?>
														<div class="settings-icon collection_detail_dropdown">
															<a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting">
																<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
															</a>
															<ul id="detail_setting" class="dropdown-content custom_dropdown">
																<?php if($status == '10') { ?>
																	<?php if($collectiondetail['is_deleted'] != '2'){ ?>	
																	<li>
																		<a href="#reportpost-popup-<?php echo $collectiondetail['_id'];?>" data-reportpostid="<?php echo $collectiondetail['_id'];?>" class="customreportpopup-modal reportpost-link">Flag collection</a>
																	</li>
																	<?php } ?>
																	<?php if($collectiondetail['is_deleted'] == '2'){ ?>	
																	<li id="publish_collection"><a href="javascript:void(0)" onclick ="publish_collection('<?=$collectiondetail['_id']?>')">Publish collection</a>
																	</li>
																	<?php } ?>
																<?php 
																} elseif(CollectionFollow::getcollectionfollow((string)$collectiondetail['_id'], (string)$user_id) == 'Owner') { ?>
																	<li><a href="javascript:void(0)" onclick="composeEditCollectionBox('<?=(string)$collectiondetail['_id']?>')">Edit collection</a></li>
																	<li><a href="javascript:void(0)" onclick="CollectionFollowers()" class="add-collection">Collection followers</a></li>
																	<li><a href="javascript:void(0)" onclick="preferencesopenpopup('<?= $collectiondetail['_id'];?>', 'collection');">Preferences</a></li>
																	<li><a href="javascript:void(0)" onclick="delete_collecction()">Delete collection</a></li>
																<?php } elseif(CollectionFollow::getcollectionfollow((string)$collectiondetail['_id'], (string)$user_id) == 'Following') { ?>
																	<li><a href="javascript:void(0)" onclick="preferencesopenpopup('<?= $collectiondetail['_id'];?>', 'collection');">Preferences</a></li>
															
																	<li><a class="<?=$checkuserauthclass?>" onclick="collectionFollowsnew('<?=$collectiondetail['_id'];?>', this)">Unfollow collection</a></li>
																	<li><a href="javascript:void(0)" onclick="reportabuseopenpopup('<?= $collectiondetail['_id'];?>', 'Collection');">Report abuse</a></li>

																<?php } else {  ?>
																	<?php if(CollectionReport::getreportabuse($user_id,$collectiondetail['_id'])){?> 
																		<li><a href="javascript:void(0)">Reported</a></li>
																	<?php } else { ?>
																		<li><a href="javascript:void(0)" onclick="reportabuseopenpopup('<?= $collectiondetail['_id'];?>', 'Collection');">Report abuse</a></li>
																	<?php }?>
																		
																<?php	} ?>
															</ul>

														</div>
													<?php } ?>
												</div>
											</div>
											<div class="content-holder dateholder">
												<div class="userinfo">
													<img src="<?= $this->context->getimage($collectiondetail['user_id'],'thumb');?>">
												</div>													
											</div>													
											<div class="content-holder gdetails-moreinfo expandable-holder">
												<a href="javascript:void(0)" class="expandable-link invertsign" onclick="mng_expandable(this)"><i class="mdi mdi-chevron-down"></i></a>
												<div class="expandable-area">
													<div class="username">
														<span><?= $this->context->getuserdata($collectiondetail['user_id'],'fullname');?></span>
													</div>
													<h4><?= ucfirst($collectiondetail['name']);?></h4>
													<div class="username tagline">
														<span><?= $collectiondetail['tagline'];?></span>
													</div>
													<div class="icon-line">
														<span class="fcount"><?= $fcount;?> followers</span>
														<span>-</span>
														<span><?= $this->context->getpostcount($col_id,'collection_id');?> posts</span>
														<span>-</span>
														<span><?= $collectiondetail['privacy'];?></span>
													</div>													
													<div class="action-btns">	
														<a class="<?=$checkuserauthclass?>"  onclick="collectionFollowsnew('<?=$collectiondetail['_id'];?>',this)"><?= CollectionFollow::getcollectionfollow((string)$collectiondetail['_id'], (string)$user_id);?></a>
													</div>
												</div>
											</div>          
										</div>
										<div class="sideboxes">
											<?php include('../views/layouts/recently_joined.php'); ?>
											<?php include('../views/layouts/travads.php'); ?>
										</div>
									
									</div>
									<div class="post-column">
										<?php 
										if(CollectionFollow::getcollectionfollow((string)$collectiondetail['_id'], (string)$user_id) == 'Owner') { ?>
										<div class="row">
											<div class="col s12 m12">
												<div class="new-post base-newpost">
													<form action="">
														<div class="npost-content">
															<div class="post-mcontent">
																<i class="mdi mdi-pencil-box-outline main-icon"></i>
																<div class="desc">				
																	<div class="input-field comments_box">
																		<input placeholder="What's new?" type="text" class="validate commentmodalAction_form" />
																	</div>
																</div>
															</div>
														</div>				
														
													</form>
													<div class="overlay" id="composetoolboxAction"></div>
												</div>
											</div>
										</div>
										<?php } ?>
										<div class="post-list margint15">
											<div class="row">
												<?php 
												$templp = 1;
												$lp = 1;
												foreach($posts as $post)
												{ 
													$existing_posts = '1';
													$cls = ''; 
													if(count($posts)==$templp) {
													  $cls = 'lazyloadscroll'; 
													}
	 												
	 												$postid = (string)$post['_id'];
													$postownerid = (string)$post['post_user_id'];
													$postprivacy = $post['post_privacy'];

													$isOk = $this->context->filterDisplayLastPost($postid, $postownerid, $postprivacy);
													if($isOk == 'ok2389Ko') {
														if(($lp%8) == 0) {
															$ads = $this->context->getad(true); 
															if(isset($ads) && !empty($ads))
															{
																$ad_id = (string) $ads['_id'];	
																$this->context->display_last_post($ads['_id'], $existing_posts, '', $cls);
																$lp++;
															} else {
																$lp++;
															}
														} else {
															$this->context->display_last_post((string)$postid, $existing_posts, '', $cls);
															$lp++;	
														}						
													}
													$templp++;
												}?>
											</div>
										</div>
										<div class="clear"></div>
										<?php 
										if($lp <=1){ 
											$this->context->getwelcomebox("collection");
										} ?>
										<center><div class="lds-css ng-scope dis-none"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
									</div>																
								</div>								
							</div>
						</div>
					</div>
				
				</div>
			</div>	
			<div id="chatblock">
				<div class="float-chat anim-side">
					<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
					</div>
				</div>
			</div>
	<?php 
	if($collectiondetail['user_id'] == $user_id) { ?>
		<div class="new-post-mobile clear">
			<a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
		</div>	
	<?php } ?>

		</div>
	</div>	
	<?php include('../views/layouts/footer.php'); ?>
	
</div>	

<input type="hidden" name="pagename" id="pagename" value="collection" />
<input type="hidden" name="tlid" id="tlid" value="<?=$col_id?>" />
<input type="hidden" name="baseurl" id="baseurl" value="<?=$baseUrl?>" />

<div id="compose_tool_box" class="modal compose_tool_box post-popup custom_modal main_modal">
</div>

<div id="composeeditpostmodal" class="modal compose_tool_box edit_post_modal post-popup main_modal custom_modal compose_edit_modal">
</div>

<div id="sharepostmodal" class="modal sharepost_modal post-popup main_modal custom_modal">
</div>

<!-- Post detail modal -->
<div id="postopenmodal" class="modal modal_main compose_tool_box custom_modal postopenmodal_main postopenmodal_new">	
</div>

<!--post comment modal for xs view-->
<div id="comment_modal_xs" class="modal comment_modal_xs">
</div> 
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
	<?php include('../views/layouts/mapmodal.php'); ?>
</div>
<?php include('../views/layouts/addpersonmodal.php'); ?>
<?php include('../views/layouts/editphotomadol.php'); ?>


	
<div id="add_collection_modal" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new">
</div>

<!--collection followers modal -->
<div id="collection_followers" class="modal item_members">
	<div class="modal_header">
		<button class="close_btn custom_modal_close_btn close_modal waves-effect">
		  <i class="mdi mdi-close mdi-20px	"></i>
		</button>
		<h3>Collection followers</h3>
	</div>
	<div class="custom_modal_content modal_content">
		<div class="follower-container">
			<ul>
				<?php if(empty($follow_user)){
					echo "No followers found";	
				}else{
					foreach($follow_user as $follow_users){
				?>
					<li>				
						<div class="follwer_profile">						
							<img src="<?= $this->context->getimage($follow_users['user_id'],'thumb');?>">
						</div>													
						<div class="follwer_name">
							<span><?= $this->context->getuserdata($follow_users['user_id'],'fullname');?></span>
						</div>						
					</li>
				<?php }
				}?>
			</ul>
			
		</div>
	</div>
		
</div>

<?php include('../views/layouts/preferences.php'); ?>

<div id="upload-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="edit-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="userwall_tagged_users" class="modal modalxii_level1">
	<div class="content_header">
		<button class="close_span waves-effect">
			<i class="mdi mdi-close mdi-20px"></i>
		</button>
		<p class="selected_photo_text"></p>
		<a href="javascript:void(0)" class="chk_person_done_new done_btn focoutTRV03 action_btn">Done</a>
	</div>
	<nav class="search_for_tag">
		<div class="nav-wrapper">
		  <form>
		    <div class="input-field">
		      <input id="tagged_users_search_box" class="search_box" type="search" required="">
		        <label class="label-icon" for="tagged_users_search_box">
		          <i class="zmdi zmdi-search mdi-22px"></i>
		        </label>
		      </div>
		  </form>
		</div>
	</nav>
	<div class="person_box"></div>
</div>

<script>
var col_id = '<?=$col_id?>';
$(document).ready(function() {
	$(".discard_md_modal .modal_discard").attr('data-id','<?=$col_id?>');
	var collectionName = '<?= ucfirst($collectiondetail['name']);?>';
	$('.page-name.mainpage-name').html(collectionName);
});	
</script>
<?php include('../views/layouts/custom_modal.php'); ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>

<script src="<?=$baseUrl?>/js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/collection.js"></script>
<script src="<?=$baseUrl?>/js/post.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		justifiedGalleryinitialize();
		lightGalleryinitialize();
	}); 
</script> 