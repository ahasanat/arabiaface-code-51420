<?php 
use frontend\assets\AppAsset;
use yii\widgets\ActiveForm;
use frontend\models\Collection;
use frontend\models\CollectionFollow;
use frontend\models\CollectionReport;

$baseUrl = AppAsset::register($this)->baseUrl;  
$collectiondetail = Collection::collectiondetail($collection_id);
$col_image = $this->context->getcollectionimage($collectiondetail['_id']);
?>
<div class="modal_content_container">
	<div class="modal_content_child modal-content">
		<div class="popup-title">
			<button class="hidden_close_span close_span waves-effect">
			  <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
			</button>
			<h3>Edit collection</h3>
			<a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" onclick="update_collecction('<?= $collectiondetail['_id'];?>', this)" href="javascript:void(0)" data-class="cancelbtn">Update</a>				
		</div>
		<div class="main-pcontent">
			<?php $form = ActiveForm::begin(['options' => ['method' => 'post','enctype'=>'multipart/form-data','id'=>'add_collecction','class' =>'add-item-form']]) ?>
				<div class="frow frowfull">	
					<div class="crop-holder" id="image-cropper">
						<div class="cropit-preview"></div>
						<div class="main-img">
							<img src="<?= $col_image;?>">
						</div>
						<div class="main-img1">
							<img id="imageid" draggable="false"/>
						</div>
						<div class="btnupload custom_up_load" id="upload_img_action">
							<div class="fileUpload">
								<i class="zmdi zmdi-hc-lg zmdi-camera"></i>
								<input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
							</div>
						</div>
						<a  href="javascript:void(0)" class="saveimg btn btn-save image_save_btn image_save dis-none">
							<span class="zmdi zmdi-check"></span>
						</a>
						<a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash removeimg">
							<i class="mdi mdi-close"></i>	
						</a>
					</div>
				</div>
				<div class="sidepad">
					<div class="frow">
						<input id="title" name="title" type="text" class="validate additem_title" value="<?= $collectiondetail['name'];?>" placeholder="Collection title" />							
					</div>
					<div class="frow">
						<textarea id="tagline" name="tagline" class="materialize-textarea mb0 md_textarea md_textarea_height additem_tagline" placeholder="Collection tagline"><?= $collectiondetail['tagline'];?></textarea>
						<span class="char-limit"><?= strlen($collectiondetail['tagline']);?>/80</span>
					</div>
					<div class="frow security-area">
						<label>Visible To:</label>					
						<div class="right">
							<a class="dropdown-button collectioncreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="collectioncreateprivacylabel" data-fetch="yes" data-label="collection">
								<span class="privacy-value"><?= $collectiondetail['privacy'];?></span>
								<i class="zmdi zmdi-caret-down"></i>
							</a>
						</div>					
					</div>
					<input type="hidden" id="images" value="<?= $col_image;?>">						
				</div>		
				
			<?php ActiveForm::end() ?>			
			
		</div>
	</div>
</div>
<div class="valign-wrapper additem_modal_footer modal-footer">
	<a href="javascript:void(0)" class="btngen-center-align  close_modal open_discard_modal waves-effect">Cancel</a>
	<a href="javascript:void(0)" class="btngen-center-align waves-effect" onclick="update_collecction('<?= $collectiondetail['_id'];?>', this)" data-class="addbtn">Update</a>
</div>
<?php exit; ?>