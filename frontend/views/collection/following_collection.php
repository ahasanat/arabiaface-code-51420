<?php 
use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
use frontend\models\Friend;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$status = $session->get('status');
$baseUrl = AppAsset::register($this)->baseUrl;
if(isset($followingcollection) && !empty($followingcollection)) { 
	foreach($followingcollection as $followingcollections) {
		$followingcollectionss = $followingcollections['collectiondata'];
		if($followingcollectionss['is_deleted']== '1') {
			$isWriteAllow = 'no';
			$idArray = array_values($followingcollectionss['_id']);
			$collection_id = $idArray[0];
			//$collection_id = (string)$followingcollectionss['_id']['$id'];
			$dp = $this->context->getimage($followingcollectionss['user_id'],'thumb');
			$follow_count = CollectionFollow::getcollectionfollowcount($collection_id);
			$follow_name = CollectionFollow::getFollowerUserNames((string)$collection_id);
			$color = isset($followingcollections['color']) ? $followingcollections['color'] : '';
			$tagline = isset($followingcollections['tagline']) ? $followingcollections['tagline'] : '';
			
			if($follow_count == 0){
				$follow_count = "No";
				$follow_name  = "Become a first to follow this";
			}else{
				$follow_name = $follow_name." followed this";
			}

			$col_created_by =$followingcollectionss['user_id'];
			$col_privacy = $followingcollectionss['privacy'];

			$is_friend = Friend::find()->where(['from_id' => "$user_id",'to_id' => "$col_created_by",'status' => '1'])->one();

			if($user_id == $col_created_by) {
				$isWriteAllow = 'yes';
			} else if(($col_privacy == 'Public' || $status == '10') || ($col_privacy == 'Friends' && ($is_friend || $status == '10')) || ($col_privacy == 'Private')) {
				$isWriteAllow = 'yes';
			} else if($col_privacy == 'Custom') {
				$customids = array();
			    $customids = isset($allcollections['customids']) ? $allcollections['customids'] : '';
			    $customids = explode(',', $customids);

			    if(in_array($user_id, $customids)) {
					$isWriteAllow = 'yes';	    	
			    }
			} else if($col_privacy == 'Friend of friend') {
				$friendoffriendids = Friend::getuserFriendsIds($col_created_by);
				foreach ($friendoffriendids as $levelsingle) {
					$level = Friend::getuserFriendsIds($levelsingle);
					if(!empty($level)) {
						$friendoffriendids = array_merge($friendoffriendids, $level);	
					}
				}
				$friendoffriendids = array_unique($friendoffriendids);

				if(in_array($user_id, $friendoffriendids)) {
					$isWriteAllow = 'yes';	 	
				}
			}

			if($isWriteAllow == 'yes')
			{

			$profile = $this->context->getcollectionimage($collection_id);

			$isDefaultProfile = $imgclass = '';
			if (strpos($profile, 'additem-collections.png') !== false) {
			    $isDefaultProfile ='defaultprofile';
			} else {
				$val = getimagesize($profile);
		        if($val[0] > $val[1]) { 
		            $imgclass = 'himg';
		        } else if($val[1] > $val[0]) { 
		            $imgclass = 'vimg';
		        } else {
		            $imgclass = 'himg';
		        }
		    }
			?>
			<div class="col s6 m4 l3 gridBox127 collectionbox collectionbox_<?=$collection_id?>">
				<div class="card hoverable collectionCard">
					<a href="javascript:void(0);" class="general-box <?=$color;?>" onclick="collectionFollows(event,'<?=$collection_id;?>',this, true)">
						<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
							<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
						</div>
						<div class="content-holder">
							<h4><?= ucfirst($followingcollectionss['name']);?></h4>
							<div class="icon-line">
								<span><?=$tagline;?></span>
							</div>	
							<div class="userinfo">
								<img src="<?= $dp;?>"/>
							</div>	
							<div class="username">
								<span><?= $this->context->getuserdata($followingcollectionss['user_id'],'fullname');?></span>
							</div>																			
						<div class="action-btns">		
							<span class="noClick" onclick="collectionFollows(event,'<?=$collection_id;?>',this, false)"><?= CollectionFollow::getcollectionfollow($collection_id,$user_id);?></span>
						</div>
						</div>
					</a>
				</div>
			</div>
			<?php 
			}
		}
	}
}
exit;?>
	
