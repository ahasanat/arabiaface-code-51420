<?php 

use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
use frontend\models\Collection;
use frontend\models\Friend;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$baseUrl = AppAsset::register($this)->baseUrl;
?>
<div class="collections-list">									
	<div class="row">
		<?php 
		$i=0;
		foreach($popularcollection as $popularcollections)
		{
			$collection_id = (string)$popularcollections['_id'];
			$popularcollections = Collection::collectiondetail($collection_id);
			if($popularcollections['is_deleted']=='1')
			{
				$isWriteAllow = 'no';
				$dp = $this->context->getimage($popularcollections['user_id'],'thumb');
				$follow_count = CollectionFollow::getcollectionfollowcount($collection_id);
				$follow_name = CollectionFollow::getFollowerUserNames((string)$collection_id);
				if($follow_count == 0){
					$follow_count = "No";
					$follow_name  = "Become a first to follow this";
				}else{
					$follow_name = $follow_name." followed this";
				}
				$col_created_by =$popularcollections['user_id'];
				$col_privacy = $popularcollections['privacy'];
				$is_friend = Friend::find()->where(['from_id' => "$user_id",'to_id' => "$col_created_by",'status' => '1'])->one();

				if($user_id == $col_created_by) {
					$isWriteAllow = 'yes';
				} else if(($col_privacy == 'Public' || $status == '10') || ($col_privacy == 'Friends' && ($is_friend || $status == '10')) || ($col_privacy == 'Private')) {
					$isWriteAllow = 'yes';
				} else if($col_privacy == 'Custom') {
					$customids = array();
				    $customids = isset($allcollections['customids']) ? $allcollections['customids'] : '';
				    $customids = explode(',', $customids);

				    if(in_array($user_id, $customids)) {
						$isWriteAllow = 'yes';	    	
				    }
				} else if($col_privacy == 'Friend of friend') {
					$friendoffriendids = Friend::getuserFriendsIds($col_created_by);
					foreach ($friendoffriendids as $levelsingle) {
						$level = Friend::getuserFriendsIds($levelsingle);
						if(!empty($level)) {
							$friendoffriendids = array_merge($friendoffriendids, $level);	
						}
					}
					$friendoffriendids = array_unique($friendoffriendids);

					if(in_array($user_id, $friendoffriendids)) {
						$isWriteAllow = 'yes';	 	
					}
				}
				
				if($isWriteAllow == 'yes')
				{

				$profile = $this->context->getcollectionimage($collection_id);

				$isDefaultProfile = $imgclass = '';
				if (strpos($profile, 'additem-collections.png') !== false) {
				    $isDefaultProfile ='defaultprofile';
				} else {
					$val = getimagesize($profile);
			        if($val[0] > $val[1]) { 
			            $imgclass = 'himg';
			        } else if($val[1] > $val[0]) { 
			            $imgclass = 'vimg';
			        } else {
			            $imgclass = 'himg';
			        }
			    }
				?>
					<div class="col s6 m4 l3 gridBox127 collectionbox_<?=$collection_id?>">
						<div class="card hoverable collectionCard">
							<a href="javascript:void(0);" onclick="collectionFollows(event,'<?=$collection_id;?>',this, true)" class="collection-box <?= $popularcollections['color'];?>">
								<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
									<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
								</div>
								<div class="content-holder">
									<h4><?= ucfirst($popularcollections['name']);?></h4>
									<div class="icon-line">
										<span><?= $popularcollections['tagline'];?></span>
									</div>	
									<div class="userinfo">
										<img src="<?= $dp;?>"/>
									</div>													
									<div class="username">
										<span><?= $this->context->getuserdata($popularcollections['user_id'],'fullname');?></span>
									</div>	
									<div class="icon-line">
										<i class="zmdi zmdi-check"></i>
										<span class="foname_<?= $collection_id;?>"><?= $follow_name;?></span>
									</div>
									<div class="action-btns">														
										<span class="followers fcount_<?= $collection_id;?>"><?= $follow_count;?> Followers</span>
										<span class="noClick" onclick="collectionFollows(event,'<?=$collection_id;?>',this, false)"><?= CollectionFollow::getcollectionfollow($collection_id,$user_id);?></span>
									</div>
								</div>
							</a>
						</div>
					</div>
				<?php   }
			$i++;
			}	
		} ?>
		<?php if((empty($popularcollection)) || $i == 0){ ?>
		<?php $this->context->getnolistfound('nopopularcollection');?>
		<?php }?>
	</div>
</div>	
<?php exit;?>							