<?php 

use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id'); 
$baseUrl = AppAsset::register($this)->baseUrl;
?>

<div class="col s6 m4 l3 add-cbox">
	<div class="card hoverable collectionCard">
		<div class="general-box">
			<?php if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { ?>
			<a href="javascript:void(0)" class="add-collection add-general <?=$checkuserauthclass?> directcheckuserauthclass">
				<span class="icont">+</span>
				Create collection
			</a>
			<?php } else { ?>
			<a href="javascript:void(0)" class="add-collection add-general composeCreateCollectionBox">
				<span class="icont">+</span>
				Create collection
			</a>
			<?php } ?>
		</div>
	</div>
</div>
<?php
if(isset($yourscollection) && !empty($yourscollection)) { 
	foreach($yourscollection as $yourscollections) {
		$idArray = array_values($yourscollections['_id']);
		$collection_id = $idArray[0];
		//$collection_id = (string)$yourscollections['_id']['$id'];
		$dp = $this->context->getimage($yourscollections['user_id'],'thumb');
		$color = isset($yourscollections['color']) ? $yourscollections['color'] : '';
		$tagline = isset($yourscollections['tagline']) ? $yourscollections['tagline'] : '';
		$follow_count = CollectionFollow::getcollectionfollowcount($collection_id);
		$follow_name = CollectionFollow::getFollowerUserNames((string)$collection_id);
		if($follow_count == 0){
			$follow_count = "No";
			$follow_name  = "Become a first to follow this";
		} else {
			$follow_name = $follow_name." followed this";
		}

		$profile = $this->context->getcollectionimage($collection_id);
		$isDefaultProfile = $imgclass = '';
		if (strpos($profile, 'additem-collections.png') !== false) {
		    $isDefaultProfile ='defaultprofile';
		} else {
			$val = getimagesize($profile);
	        if($val[0] > $val[1]) { 
	            $imgclass = 'himg';
	        } else if($val[1] > $val[0]) { 
	            $imgclass = 'vimg';
	        } else {
	            $imgclass = 'himg';
	        }
	    }
	?>
	<div class="col s6 m4 l3 collectionbox gridBox127 collectionbox_<?=$collection_id?>"> 
		<div class="card hoverable collectionCard">
			<a href="javascript:void(0);" onclick="collectionFollows(event,'<?=$collection_id;?>',this, true)" class="general-box <?=$color;?>">
				<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
					<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
				</div>
				<div class="content-holder">
					<h4><?= ucfirst($yourscollections['name']);?></h4>
					<div class="icon-line">
						<span><?=$tagline;?></span>
					</div>	
					<div class="userinfo">
						<img src="<?= $dp;?>"/>
					</div>													
					<div class="username">
						<span><?= $this->context->getuserdata($yourscollections['user_id'],'fullname');?></span>
					</div>	
					<div class="action-btns">
						<span class="noClick" onclick="collectionFollows(event,'<?=$collection_id;?>',this,false)"><?= CollectionFollow::getcollectionfollow($collection_id,$user_id);?></span>
					</div>
				</div>
			</a>
		</div>
	</div>
	<?php 
	}
} 
exit;?>							