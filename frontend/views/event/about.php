<?php 
use yii\helpers\Url;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$eventMonth = '';
$eventDay = '';
$address = '';
$short_desc = '';
if(isset($event_details['event_date']) && $event_details['event_date'] != '') {
    $eventMonth = date('M', strtotime($event_details['event_date'])); 
    $eventDay = date('d', strtotime($event_details['event_date'])); 
}
if(isset($event_details['address']) && $event_details['address'] != '') {
    $address = $event_details['address'];
}
if(isset($event_details['short_desc']) && $event_details['short_desc'] != '') {
    $short_desc = $event_details['short_desc'];
}

$today=date_create(date('m/d/Y', strtotime('+1 day')));;
$eventDate = date_create($event_details['event_date']);
$eventTime = $event_details['event_time'];
$diff = date_diff($eventDate, $today);
$label = date('M d, Y', strtotime($event_details['event_date']));
if($diff->days == 0) {
    $label = 'Today';
} elseif($diff->days == 1 && $diff->invert == 0) {
    $label = 'Yesterday';
} elseif($diff->days == 1 && $diff->invert == 1) {
    $label = 'Tomorrow';
}
?>
<span class="mob-title"><i class="mdi mdi-information-variant"></i>About Event</span>
<div class="content-box bshadow inner-content-box">
    <div class="event-info">
        <div class="cbox-title">
            <h5>
                <i class="mdi mdi-information-variant"></i>
                About Event
            </h5>
        </div>
        <div class="cbox-desc">
            <ul class="section-ul">
                <li>    
                    <div class="eventinfo">
                        <div class="dateinfo">
                            <span class="month"><?=$eventMonth?></span>
                            <span class="date"><?=$eventDay?></span>
                        </div>                                                                          
                        <div class="descinfo">
                            <h4><?=$eventName?></h4>
                            <p><span><?=$privacy?></span> : Organized by <a href="<?php echo Url::to(['userwall/index', 'id' => (string)$event_details['user']['_id']]); ?>"><?=$createdBy?></a></p>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <ul class="eventinfo-ul">
                        <li>
                            <div class="eventinfo-row">
                                <span class="iconholder"><i class="mdi mdi-clock-outline"></i></span>
                                <span class="descholder">
                                    <h5><?=$label?> at <?=$eventTime?></h5>
                                </span>
                            </div>
                        </li>
                        <li>
                            <div class="eventinfo-row">
                                <span class="iconholder"><i class="zmdi zmdi-pin"></i></span>
                                <span class="descholder">
                                    <h5><?=$address?></h5>
                                    <a href="javascript:void(0)" class="map" onclick="showEventMap(this)">Show map</a>
                                </span>
                                <div class="mapholder">
								<?php $this->context->GetMap($address);?>
                                </div>
                            </div>
                        </li>
                        <?php 
                        if(isset($event_details['wu']) && $event_details['wu'] != '') {
                            $wu = $event_details['wu'];
                        ?>
                        <li>
                            <div class="eventinfo-row">
                                <span class="iconholder"><i class="mdi mdi-link"></i></span>
                                <span class="descholder">
                                    <h5>Website</h5>
                                    <p><?=$wu?></p>               
                                </span>
                            </div>
                        </li>
                        <?php } 
                        
                        if(isset($event_details['tu']) && $event_details['tu'] != '') {
                            $tu = $event_details['tu'];
                        ?>
                        <li>
                            <div class="eventinfo-row">
                                <span class="iconholder"><i class="mdi mdi-ticket"></i></span>
                                <span class="descholder">
                                    <h5>Ticket Information</h5>
                                    <p><?=$tu?></p>
                                    <?php            
                                        if (!preg_match("~^(?:f|ht)tps?://~i", $tu)) {
                                            $tu = "http://" . $tu;
                                        }
                                    ?>
                                    <a href="<?=$tu?>" target="_blank">Find Tickets</a>
                                </span>
                            </div>
                        </li>
                        <?php } 

                        if(isset($event_details['tpiu']) && $event_details['tpiu'] != '') {
                            $tpiu = $event_details['tpiu'];
                        ?>
                        <li>
                            <div class="eventinfo-row">
                                <span class="iconholder"><i class="mdi mdi-car"></i></span>
                                <span class="descholder">
                                    <h5>Parking Information</h5>
                                    <p><?=$tpiu?></p>
                                    <?php            
                                        if (!preg_match("~^(?:f|ht)tps?://~i", $tpiu)) {
                                            $tpiu = "http://" . $tpiu;
                                        }
                                    ?>
                                    <a href="<?=$tpiu?>" target="_blank">Parking info</a>
                                </span>
                            </div>
                        </li>
                        <?php } 
                        
                        if(isset($event_details['yu']) && $event_details['yu'] != '') {
                            $yu = $event_details['yu'];
                        ?>
                        <li>
                            <div class="eventinfo-row">
                                <span class="iconholder"><i class="mdi mdi-link"></i></span>
                                <span class="descholder">
                                    <h5>Youtube</h5>
                                    <p><?=$yu?></p>               
                                </span>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <li>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <label>Event's Organizer</label>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <ul class="member-ul">
                                <li>
                                    <div class="member-box">
                                    <a href="<?php echo Url::to(['userwall/index', 'id' => (string)$event_details['user']['_id']]); ?>">
                                            <img title="<?=$event_details['user']['fullname']?>" src="<?= $this->context->getimage($event_details['user']['_id'],'thumb')?>">
                                            <span><?=$createdBy?></span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="content-box bshadow inner-content-box">
    <div class="event-info">
        <div class="cbox-title">
            <h5>Details</h5>
        </div>
        <div class="post-desc white-box">
            <div class="para-section">                                                          
                <div class="para">                                                          
                    <p><?=trim($short_desc)?></p>                            
                </div>
                <?php if(strlen(trim($short_desc)) > 365) { ?>
                <a href="javascript:void(0)" class="readlink" onclick="showAllContent(this)">Read More</a>
                <?php } ?>
            </div>              
        </div>
    </div>
</div>	 