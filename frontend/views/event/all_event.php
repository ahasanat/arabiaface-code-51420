<?php 
use frontend\assets\AppAsset;
use frontend\models\EventVisitors;
use frontend\models\PageEvents;
use frontend\models\Friend;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$status = $session->get('status');
$baseUrl = AppAsset::register($this)->baseUrl;  
foreach($allevents as $allevent) { 
    $event_id = (string)$allevent['_id'];
    $event_count = EventVisitors::getEventCounts($event_id);
    $allevent = PageEvents::getEventdetails($event_id);
    $date = $allevent['event_date'];
    $col_created_by =$allevent['created_by'];
    $col_privacy = $allevent['event_privacy'];
    $is_friend = Friend::find()->where(['from_id' => "$user_id",'to_id' => "$col_created_by",'status' => '1'])->one();
    $isWriteAllow = 'no';

    if($user_id == $col_created_by) {
		$isWriteAllow = 'yes';
	} else if(($col_privacy == 'Public' || $status == '10') || ($col_privacy == 'Friends' && ($is_friend || $status == '10')) || ($col_privacy == 'Private')) {
		$isWriteAllow = 'yes';
	} else if($col_privacy == 'Custom') {
		$customids = array();
	    $customids = isset($allevent['customids']) ? $allevent['customids'] : '';
	    $customids = explode(',', $customids);

	    if(in_array($user_id, $customids)) {
			$isWriteAllow = 'yes';	    	
	    }
	} else if($col_privacy == 'Friend of friend') {
		$friendoffriendids = Friend::getuserFriendsIds($col_created_by);
		foreach ($friendoffriendids as $levelsingle) {
			$level = Friend::getuserFriendsIds($levelsingle);
			if(!empty($level)) {
				$friendoffriendids = array_merge($friendoffriendids, $level);	
			}
		}
		$friendoffriendids = array_unique($friendoffriendids);

		if(in_array($user_id, $friendoffriendids)) {
			$isWriteAllow = 'yes';	 	
		}
	}


	if($isWriteAllow == 'yes') {
	    $profile = $this->context->geteventimage($event_id);
		$isDefaultProfile = $imgclass = '';
		if (strpos($profile, 'additem-commevents.png') !== false) {
		    $isDefaultProfile ='defaultprofile';
		} else {
			$val = getimagesize($profile);
	        if($val[0] > $val[1]) { 
	            $imgclass = 'himg'; 
	        } else if($val[1] > $val[0]) { 
	            $imgclass = 'vimg';
	        } else {
	            $imgclass = 'himg';
	        }
	    }
	    ?>
	    <div class="col s6 m4 l3 gridBox127 uniqidentitybox eventbox_<?=$event_id?>">
			<div class="card hoverable eventCard">
				<a href="javascript:void(0);" onclick="joinEvent(event,'<?=$event_id;?>',this, true)" class="commevents-box general-box">
					<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
						<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
					</div>
					<div data-id="al_event-cloud" class="content-holder">
						<h4><?= $allevent['event_name'];?></h4>
						<div class="username fcount_<?= $event_id;?>">
							<?=$event_count?> Attending
						</div>	
						<div class="userinfo">
							<span class="month"><?=date("M", strtotime($date))?></span>
							<span class="date"><?=date("d", strtotime($date))?></span>
						</div>                    
						<div class="action-btns">                        
							<span class="noClick" onclick="joinEvent(event,'<?=$event_id;?>',this, false)"><?= EventVisitors::getEventGoing($event_id);?></span>
						</div>
					</div>
				</a>
			</div>
	    </div>
	    <?php 
	} 
}
exit;?>							