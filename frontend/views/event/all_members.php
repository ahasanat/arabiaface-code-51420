<?php 
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl; 
$user_admin = $event_detail['created_by'];
$group_id 	= $event_detail['_id'];
?>
<ul class="manage-members">
	<li>
		<div class="member-li">
			<div class="imgholder"><img src="<?= $this->context->getimage($user_admin,'thumb');?>"/></div>
			<div class="descholder">
				<span class="head6"><?= $this->context->getuserdata($user_admin,'fullname');?></span>
				<div class="settings">
					<span class="status">Admin</span>
				</div>
			</div>
		</div>
	</li>
	<?php foreach($event_member as $event_members)
	{
		$event_user_id = $event_members['user_id'];
		$event_member_id = $event_members['_id'];
	?>
	<li class="member_<?= $event_member_id;?>">
		<div class="member-li">
			<div class="imgholder"><img src="<?= $this->context->getimage($event_user_id,'thumb');?>"/></div>
			<div class="descholder">
				<span class="head6"><?= $this->context->getuserdata($event_user_id,'fullname');?></span>
				<div class="settings">
					<span class="status">Attendee</span>
					<div class="right">
						<a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings_<?=$event_member_id?>">
							<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
						</a>
						<ul id="mm_settings_<?=$event_member_id?>" class="dropdown-content custom_dropdown">
							<li><a href="javascript:void(0)" onclick="remove_member('<?= $event_member_id;?>')">Remove attendee</a></li>
						</ul>
					</div>									
				</div>
			</div>
		</div>
	</li>
	<?php }?>
</ul>
<?php exit;?>