<?php 
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl; 
?>
<ul class="manage-members">
	<?php if(empty($member_request)){ ?>
		<div class="post-holder bshadow">      
			<div class="joined-tb">
				<i class="mdi mdi-file-outline"></i>        
				<p>No request found.</p>
			</div>    
		</div>	
	<?php }?>
	<?php foreach($member_request as $member_requests){ ?>
	<li>
		<div class="member-li">
			<div class="imgholder"><img src="<?= $this->context->getimage($member_requests['user_id'],'thumb');?>"/></div>
			<div class="descholder">
				<span class="head6"><?= $this->context->getuserdata($member_requests['user_id'],'fullname');?></span>
				<div class="settings accept_<?= $member_requests['_id'];?>">
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="request_responce('1','<?= $member_requests['_id'];?>')" >Accept</a>
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="request_responce('0','<?= $member_requests['_id'];?>')">Reject</a>
				</div>
			</div>
		</div>
	</li>
	<?php }?>
</ul>
<?php exit;?>