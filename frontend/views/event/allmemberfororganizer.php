<?php 
use frontend\models\PageEvents;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$isEmpty = true;	
if(isset($event_members) && !empty($event_members)) {
	foreach($event_members as $event_member)
	{
		$event_user_id = $event_member['user_id'];
		if($user_id == $event_user_id) {
			continue;
		}
		$event_member_id = (string)$event_member['_id'];
		$isRequestSend = PageEvents::isRequestSend($event_user_id, $event_id);
		$isEmpty = false;
		?>
		<li class="member_<?= $event_member_id;?>">
		<div class="member-li">
			<div class="imgholder"><img src="<?= $this->context->getimage($event_user_id,'thumb');?>"/></div>
			<div class="descholder">
				<span class="head6"><?= $this->context->getuserdata($event_user_id,'fullname');?></span>
				<div class="settings">
				
				<?php if($isRequestSend == true) { ?>
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="setorganizer('<?=$event_user_id?>', '<?=$event_id?>', this)" >Cancel Organizer Request</a>
				<?php } else { ?>
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="setorganizer('<?=$event_user_id?>', '<?=$event_id?>', this)" >Set as Organizer</a>
				<?php } ?>
				</div>
			</div>
		</div>
	</li>
<?php } 
}
if($isEmpty) { ?>
	<div class="no-listcontent">
		No request found.
	</div>
<?php }
exit;?>