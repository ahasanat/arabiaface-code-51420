<?php 
use yii\helpers\Url;
?>
<span class="mob-title"><i class="mdi mdi-account-group"></i>People attending this event</span>
<div class="content-box bshadow inner-content-box">
	<div class="cbox-title">
		<h5>
			<i class="mdi mdi-account-group"></i>
			<?=$eventuserscount?> User<?php if($eventuserscount > 1){?>s<?php } ?> attending this event
		</h5> 
	</div>
	<div class="cbox-desc">
		<?php if(empty($eventusers)){ ?>
		<?php $this->context->getnolistfound('becomefirsttojoinevent');?>
		<?php } ?>
		<div class="likes-summery" data-id="summery-store">															
			<div class="invite-likes">																
				<div class="invite-holder">
					<div style="overflow-y: hidden;" class="list-holder nice-scroll" tabindex="6">
						<ul>
							<?php foreach($eventusers as $eventuser){ ?>
							<li>
								<a class="invitelike-friend" href="<?php echo Url::to(['userwall/index', 'id' => $eventuser['user_id']]);?>">
									<span class="imgholder">
										<img src="<?= $this->context->getimage($eventuser['user']['_id'],'thumb')?>">
									</span>
									<span class="descholder">
										<h6><?=$eventuser['user']['fullname']?></h6>
									</span>														
								</a>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>
<?php exit;?>							