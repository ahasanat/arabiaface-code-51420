<?php   
use yii\helpers\Url;
use frontend\assets\AppAsset;
use yii\widgets\ActiveForm;
use frontend\models\Friend;
use frontend\models\EventVisitors;
use backend\models\Googlekey;
$baseUrl = AppAsset::register($this)->baseUrl; 
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id'); 
$status = $session->get('status');
$request = Yii::$app->request;
$event_id = (string)$request->get('e');
$date = date('m/d/Y');
$eventdate = $event_details['event_date'];
$this->title = 'Event Detail';
$eventimg = $this->context->geteventimage($event_id);
$event_type = isset($event_details['type']) ? $event_details['type'] : '';

$privacy = $event_details['event_privacy'];

if ($event_type == 'page') {
    $entity_id = $event_details['parent_id'];
    $event_title = 'Page Events';
    $back_link = '?r=page/index&id='.$entity_id.'&par=event';
    $back_text = 'Back to Event List';
} else if ($event_type == 'group') {
    $entity_id = $event_details['parent_id'];
    $event_title = 'Events';
    $back_link = '?r=groups/detail&group_id='.$entity_id.'&par=event';
    $back_text = 'Back to Event List';
} else {
    $event_title = 'Community Events';
    $back_link = '?r=event';
    $back_text = 'Back to Community Events';
}

$eventName = '';
if(isset($event_details['event_name'])) {
	$eventName = $event_details['event_name'];
}
$createdBy = '';
if(isset($event_details['user']['fullname'])) {
	$createdBy = $event_details['user']['fullname'];
}
$directauthcall = '';
if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { 
	$directauthcall = $checkuserauthclass . ' directcheckuserauthclass';
}
$members_count = EventVisitors::getEventCounts($event_id);
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>

<div class="page-wrapper menutransheader-wrapper menuhideicons-wrapper">
    <div class="header-section">
        <?php include('../views/layouts/header.php'); ?>
    </div>
    <div class="floating-icon">
        <div class="scrollup-btnbox anim-side btnbox scrollup-float">
            <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>			
        </div>        
    </div>
    <div class="clear"></div>
	<div class="container page_container fulltab">		
		<?php include('../views/layouts/leftmenu.php'); ?>		
		<div class="fixed-layout ipad-mfix">
			<div class="main-content with-lmenu general-page generaldetails-page commevents-page commevents-details-page main-page">
				<div class="combined-column">
					<div class="content-box">
						<div class="cbox-title nborder">
							<i class="mdi mdi-calendar"></i>
							<?=$event_title?>
							<a href="<?=$back_link?>" class="backbtn"><i class="mdi mdi-menu-left"></i> <?=$back_text?></a>
						</div>
						<div class="cbox-desc">
							<div class="view-holder">
								<div class="general-details">
									
										<div class="gdetails-summery">
											<div class="main-info">
												<div class="imgholder">                                        
													<img src="<?=$eventimg?>"/>
													<div class="back-link">
														<a href="<?=$back_link?>" class="waves-effect waves-theme">
															<i class="mdi mdi-arrow-left"></i>
														</a>
													</div>
													<?php if($status == '10') { ?>
													<div class="action-links item_detail_dropdown">                                        
														<div class="settings-icon">
															<a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting">
																<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
															</a>
															<ul id="detail_setting" class="dropdown-content custom_dropdown">
																<?php if($event_details['is_deleted'] != '2'){ ?>	
																<li><a href="javascript:void(0)" class="popup-modal" onclick="reportabuseopenpopup('<?=$event_id?>', 'Event');">Report abuse</a></li>
																
																<?php } ?>
																<?php if($event_details['is_deleted'] == '2'){ ?>	
																
																<li id="publish_event"><a href="javascript:void(0)" onclick ="publish_event('<?=$event_id?>')">Publish event</a>
																</li>
																
																<?php } ?>
																
															</ul>
														</div>														
														</div>
														<?php }  
														else { ?>
														<div class="action-links item_detail_dropdown">
														<?php 
														if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { ?>

														<a href="javascript:void(0)" class="waves-effect waves-theme orglink share-it <?=$checkuserauthclass?> directcheckuserauthclass">
															<i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
														</a>
														<?php } else if(EventVisitors::getEventGoing($event_id) == 'Organizer') { ?>	
														
														<a href="javascript:void(0)" class="waves-effect waves-theme orglink share-it sharepostmodalAction" data-sharepostid="event_<?=$event_id;?>">
															<i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
														</a>
														<div class="settings-icon">
															<a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting">
																<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
															</a>
															<ul id="detail_setting" class="dropdown-content custom_dropdown">
																<li>
																	<a href="javascript:void(0)" onclick="openAddItemModal()">Edit Event</a>
																</li>
																<li>
																	<a href="javascript:void(0)" onclick="allrequests(true)">Manage Attendees</a>
																</li>
																<li>
																	<a href="javascript:void(0)" id="directusepromote" onclick="allmemberfororganizer()">Promote to organizer </a>
																</li>
																<li>
																	<a href="javascript:void(0)" data-id="<?= $event_id;?>"onclick="preferencesopenpopup('<?= $event_id;?>', 'event');">Preferences</a>
																</li>
																<?php if($members_count > 1) {?>
																<li>		
																	<a href="javascript:void(0)" data-id="<?=$event_id?>" onclick="left_event()">Leave Event</a>
																</li>
																<?php } else { ?>
																<li>
																	<a href="javascript:void(0)" data-id="<?=$event_id?>" onclick="deleteEvent()">Delete Event</a>
																</li>
																<?php } ?>
															</ul>
														</div>														
													
														<?php } else if(EventVisitors::getEventGoing($event_id) == 'Attending') { ?>
														
														<a href="javascript:void(0)" class="waves-effect waves-theme orglink share-it sharepostmodalAction" data-sharepostid="event_<?=$event_id;?>">
															<i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
														</a>
														
														<div class="settings-icon">
															<a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting">
																<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
															</a>
															<ul id="detail_setting" class="dropdown-content custom_dropdown">
																<li>
																	<a href="javascript:void(0)" data-id="<?= $event_id;?>"onclick="preferencesopenpopup('<?= $event_id;?>', 'event');">Preferences</a>
																</li>
																<li>
																	<a href="javascript:void(0)" data-id="<?= $event_id;?>" onclick="left_event()">Leave Event</a>
																</li>
																<li>
																	<a href="javascript:void(0)" class="popup-modal" onclick="reportabuseopenpopup('<?=$event_id?>', 'Event');">Report abuse</a>
																</li>
															</ul>
														</div>
														<?php } else { ?>
														<div class="settings-icon item_detail_dropdown">
															<a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting">
																<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
															</a>
															<ul id="detail_setting" class="dropdown-content custom_dropdown">
																<li><a href="javascript:void(0)" class="popup-modal" onclick="reportabuseopenpopup('<?=$event_id?>', 'Event');">Report abuse</a></li>
															</ul>
														</div>
														<?php } ?>
													</div>
													<?php } ?>	
												</div>
												<div class="content-holder dateholder">
													<div class="dateinfo">
														<span class="month"><?=date("M", strtotime($eventdate))?></span>
														<span class="date"><?=date("d", strtotime($eventdate))?></span>
													</div>
												</div>
												<div class="content-holder gdetails-moreinfo expandable-holder">
													<a href="javascript:void(0)" class="expandable-link invertsign" onclick="mng_expandable(this)"><i class="mdi mdi-chevron-down"></i></a>
													<div class="expandable-area">
														<h4><?=$event_details['event_name']?></h4>					
														<div class="member-info">
															<span><?php
																if($members_count>1) {
																	echo $members_count .' Attendees';
																} else {
																	echo $members_count .' Attendee';
																} ?>
															</span>
															<span>+</span>
															<span><?= $event_details['event_privacy'];?></span>
														</div>
														<div style="clear:both;"></div>
														<div class="tagline">												
															<p><?= $event_details['tagline'];?></p>
														</div>
														<div class="action-btns">
															<a class="noClick <?=$checkuserauthclass?>" onclick="joinEvent(event,'<?=$event_id;?>',this, false)"><?= EventVisitors::getEventGoing($event_id);?></a>
														</div>											
													</div>
												</div> 
												<div class="content-holder">
													<div class="hori-menus nice-scroll">
														<ul class="icon-list tabs tabs-vert">
															<li class="tab"><a href="#commevents-about" data-toggle="tab" aria-expanded="true" onclick="resetDetailTabs(this,'commevents-details','commevents-about')">About <span class="mobile_hide">Event</span><i class="zmdi zmdi-check"></i></a></li>
															<li class="tab" id="DiscussionLi"><a href="#commevents-discussion" data-toggle="tab" aria-expanded="false" onclick="resetDetailTabs(this,'commevents-details','commevents-discussion')" class="active <?=$directauthcall?>">Discussion<i class="zmdi zmdi-check"></i></a></li>
															<li class="tab"><a href="#commevents-photos" data-toggle="tab" aria-expanded="true"  onclick="resetDetailTabs(this,'commevents-details','commevents-photos')" class="phototab <?=$directauthcall?>">Photos<i class="zmdi zmdi-check"></i></a></li>						
															<?php if(isset($isAttending) &&$isAttending == 'Y') {?>		
																<li class="tab invite"><a href="#commevents-invite" data-toggle="tab" aria-expanded="true"  onclick="resetDetailTabs(this,'commevents-details','commevents-invite')" class="<?=$directauthcall?>">Invite <span class="mobile_hide">People</span><i class="zmdi zmdi-check"></i></a></li>
															<?php } ?>
															<li class="tab attending member-link"><a href="#commevents-attending" id="attendingTOtal" class="pl-0" data-toggle="tab" aria-expanded="false" onclick="resetDetailTabs(this,'commevents-details','commevents-attending')">
															<span class="mobile_hide">
															<?php if($eventcount>1) {	
																echo 'Attendees';
															} else {
																echo 'Attendee';	
															} ?>
															</span>
															<span class="show-on-small forcefullyhide">
															<?php if($eventcount>1) {	
																echo 'Attendees';
															} else {
																echo 'Attendee';	
															} ?>
															</span>
															</a></li>
														</ul>	
														<div class="people">
															<?php if($eventcount > 0){ ?>
															<div class="plist">
																<?php foreach($eventusers as $eventuser){ ?>
																	<span>
																		<a href="<?php echo Url::to(['userwall/index', 'id' => $eventuser['user_id']]);?>">
																			<img title="<?=$eventuser['user']['fullname']?>" src="<?= $this->context->getimage($eventuser['user']['_id'],'thumb')?>">
																		</a>
																	</span>
																<?php } ?>
															</div>
															<?php } else { ?>
															<div class="no-listcontent">
																Become a first to join this event
															</div>
															<?php } ?>
														</div>																								
													</div>
												</div>
											</div> 
											<div class="sideboxes">
												<?php include('../views/layouts/recently_joined.php'); ?>
												<?php include('../views/layouts/travads.php'); ?>
											</div>
										</div>
										<div class="post-column">
											<div class="tab-content">
												
												<div class="tab-pane fade main-pane active in" id="commevents-discussion">
													<span class="mob-title"><i class="mdi mdi-newspaper"></i>Discussion</span>
													<?php 
													$lp = 1; 
													if(isset($user_id) && !empty($user_id)){

													if(EventVisitors::getEventGoing((string)$event_details['_id']) == 'Organizer' || EventVisitors::getEventGoing((string)$event_details['_id']) == 'Attending') {

													$checkposttime = EventVisitors::find()->select(['created_date'])->where(['event_id' => (string)$event_details['_id'], 'user_id' => $user_id])->asarray()->one();
													$joinedTime = $checkposttime['created_date'];

													 ?>
														<div class="row">
															<div class="col s12 m12"> 
																<div class="new-post base-newpost">
																	<form action="">
																		<div class="npost-content">
																			<div class="post-mcontent">
																				<i class="mdi mdi-pencil-box-outline main-icon"></i>
																				<div class="desc">									
																					<div class="input-field comments_box">
																						<input placeholder="What's new?" type="text" class="validate commentmodalAction_form" />
																					</div>
																				</div>
																			</div>
																		</div>				
																		
																	</form>
																	<div class="overlay" id="composetoolboxAction"></div>
																</div>
															</div>
														</div>
														<div class="post-list margint15">
															<div class="row">
																<!-- main post -->
																
																<?php 
																	$templp = 1;
																	$lp = 1;
																	foreach($eventposts as $post)
																	{ 
																		$postTime = $post['post_created_date'];
																		if($postTime < $joinedTime) {
																			continue;
																		}

																		$existing_posts = '1';
																		$cls = '';
																		if(count($post)==$templp)
																		{
																			$cls = 'lazyloadscroll';
																		}

																		$postid = (string)$post['_id'];
																		$postownerid = (string)$post['post_user_id'];
																		$postprivacy = $post['post_privacy'];

																		$isOk = $this->context->filterDisplayLastPost($postid, $postownerid, $postprivacy);
																		if($isOk == 'ok2389Ko') {
																			if(($lp%8) == 0) {
																				$ads = $this->context->getad(true); 
																				if(isset($ads) && !empty($ads))
																				{
																					$ad_id = (string) $ads['_id'];	
																					$this->context->display_last_post($ad_id, $existing_posts, '', $cls);
																					$lp++;
																				} else {
																					$lp++;
																				}
																			} else {
																				$this->context->display_last_post((string)$postid, $existing_posts, '', $cls);
																				$lp++;	
																			}						
																		}
																		$templp++;
																	}
																?>
															</div>
														</div>
													
													<?php } } else { ?>
													
														<div class="post-list margint15">
															<div class="row">
																<!-- main post -->
																
																<?php 
																	$templp = 1;
																	$lp = 1;
																	foreach($eventposts as $post)
																	{ 
																		$existing_posts = '1';
																		$cls = '';
																		if(count($post)==$templp)
																		{
																			$cls = 'lazyloadscroll';
																		}

																		$postid = (string)$post['_id'];
																		$postownerid = (string)$post['post_user_id'];
																		$postprivacy = $post['post_privacy'];

																		$isOk = $this->context->filterDisplayLastPost($postid, $postownerid, $postprivacy);
																		if($isOk == 'ok2389Ko') {
																			if(($lp%8) == 0) {
																				$ads = $this->context->getad(true); 
																				if(isset($ads) && !empty($ads))
																				{
																					$ad_id = (string) $ads['_id'];	
																					$this->context->display_last_post($ad_id, $existing_posts, '', $cls);
																					$lp++;
																				} else {
																					$lp++;
																				}
																			} else {
																				$this->context->display_last_post((string)$postid, $existing_posts, '', $cls);
																				$lp++;	
																			}						
																		}
																		$templp++;
																	}
																?>
															</div>
														</div>
													<?php } ?>
													<div class="clear"></div>
													<?php if($lp <= 1) {
														$this->context->getwelcomebox("event");
													} ?>
													<center><div class="lds-css ng-scope dis-none"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
												</div>

												<div class="tab-pane fade main-pane dis-none" id="commevents-about">
													<?php include('about.php'); ?>
												</div>
												<div class="tab-pane fade main-pane" id="commevents-attending"></div> 
												<div class="tab-pane fade main-pane" id="commevents-invite"></div>
												<div class="tab-pane fade main-pane" id="commevents-photos"></div>
											</div>
										</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="chatblock">
					<div class="float-chat anim-side">
						<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
						</div>
					</div>
				</div>
				<?php 
					$userStatus = EventVisitors::getEventGoing($event_id);
					if($userStatus == 'Organizer' || $userStatus == 'Attending') { ?>
					<div class="new-post-mobile clear">
						<a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php include('../views/layouts/footer.php'); ?>
</div>

<div id="compose_tool_box" class="modal compose_tool_box post-popup custom_modal main_modal">
</div>

<div id="composeeditpostmodal" class="modal compose_tool_box edit_post_modal post-popup main_modal custom_modal compose_edit_modal">
</div>

<div id="sharepostmodal" class="modal sharepost_modal post-popup main_modal custom_modal">
</div>

<!-- Post detail modal -->
<div id="postopenmodal" class="modal modal_main compose_tool_box custom_modal postopenmodal_main postopenmodal_new">	
</div>

<!--post comment modal for xs view-->
<div id="comment_modal_xs" class="modal comment_modal_xs">
</div> 
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal map_modalUniq modalxii_level1">
	<?php include('../views/layouts/mapmodal.php'); ?>
</div>
<?php include('../views/layouts/addpersonmodal.php'); ?>
<?php include('../views/layouts/editphotomadol.php'); ?>


<?php if($user_id == $event_details['created_by']){ ?>	
<!--add event modal-->
<div id="add_event_modal" class="modal add-item-popup custom_md_modal">
	<div class="modal_content_container">
		<div class="modal_content_child modal-content">			
			<div class="popup-title">
				<button class="hidden_close_span close_span waves-effect">
					<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
				</button>	
				<h3>Update Event</h3>
				<a type="button" class="item_done crop_done waves-effect hidden_close_span close_modal" onclick="addEvent(this)" href="javascript:void(0)" data-class="cancelbtn">Done</a>
			</div>    
			<div class="main-pcontent">
				<form class="add-item-form" id="eventform">
					<div class="frow frowfull">	
						<div class="crop-holder" id="image-cropper">
							<div class="cropit-preview"></div>
							<div class="main-img">
		                        <img src="<?= $eventimg;?>">
		                    </div>
							<div class="main-img1">
							  <img id="imageid" draggable="false"/>
							</div>
							<div class="btnupload custom_up_load" id="upload_img_action">
							  <div class="fileUpload">
								<i class="zmdi zmdi-hc-lg zmdi-camera"></i>
								<input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
							  </div>
							</div>
							<a  href="javascript:void(0)" class="saveimg btn btn-save image_save_btn image_save dis-none">
							  <span class="zmdi zmdi-check"></span>
							</a>
							<a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash removeimg">
							  <i class="mdi mdi-close"></i>	
							</a>
						</div>
					</div>			   
					<div class="sidepad" data-id="sidepad-dis">						
						<div class="frow">
							<input id="title" name="title" type="text" class="validate additem_title" placeholder="Event title" value="<?=$event_details['event_name']?>"/>
						</div>
						<div class="frow">
							<textarea id="tagline" name="tagline" class="materialize-textarea mb0 md_textarea additem_tagline" placeholder="Event tagline"><?=$event_details['tagline']?></textarea>
						</div>
						<div class="frow">
							<input type="text" placeholder="Location" class="materialize-textarea md_textarea item_address" id="placelocationsearch" value="<?=$event_details['address']?>" data-query="all"  onfocus="filderMapLocationModal(this)" autocomplete='off'/>
							<input type="hidden" id="country">
							<input type="hidden" id="au_country">
							<input type="hidden" id="country_code">
							<input type="hidden" id="isd_code">						
						</div>
						<div class="frow">
							<div class="row">
								<div class="col s6">
									<input type="text" data-toggle="datepicker" data-query="all" class="datepicker datepickerinput"  placeholder="<?=$event_details['event_date']?>" name="event_date" id="eventdatepicker" data-value="<?=$event_details['event_date']?>" value="<?=$event_details['event_date']?>" readonly/>
								</div>
								<div class="col s6">
									<input type="text" class="timepicker" placeholder="<?=$event_details['event_time']?>" id="eventtimepicker" data-value="<?=$event_details['event_time']?>" value="<?=$event_details['event_time']?>"/>
								</div>
							</div>
						</div>
						<div class="frow">
							<textarea type="text" placeholder="Tell people more about the event" class="materialize-textarea md_textarea item_about" id="aboutevent"><?=$event_details['short_desc']?></textarea>							
						</div>
						<div class="frow">
							<div class="row"> 
								<div class="col s9">
									<p>Ask to join</p> 
								</div>
								<div class="col s3">
									<div class="switch">
									  <label>
										<input id="ask_to_join_switch" data-value="<?php echo (isset($event_details['isasktojoin']) && $event_details['isasktojoin'] == '1') ? '1' : '0';?>" class="myCheckbox1" <?php if(isset($event_details['isasktojoin']) && $event_details['isasktojoin'] == '1') { echo 'checked'; } ?>  type="checkbox">
										<span class="lever" for="ask_to_join_switch"></span>
									  </label>										
									</div>									
								</div>
							</div>
						</div>
						<div class="frow security-area">
							<label>Privacy</label>
							<div class="right">
								<a class="dropdown_text dropdown-button eventcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="eventcreateprivacylabel" data-fetch="yes" data-label="event">
									<span class="privacyarea"><?=$privacy?></span>
									<i class="mdi mdi-menu-down"></i>
								</a>
							</div>
						</div>
						<div class="frow">
							<div class="expandable-holder">
								<a href="javascript:void(0)" class="expand-link invertsign" onclick="mng_expandable(this)">Advanced Option <i class="mdi mdi-menu-down"></i></a>
								<div class="expandable-area">
									<div class="frow">
										<input type="text" placeholder="Website URL (optional)" class="materialize-textarea md_textarea item_website" id="wu" value="<?=$event_details['wu']?>"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Ticket seller URL (optional)" class="materialize-textarea md_textarea item_ticket" id="tu" value="<?=$event_details['tu']?>"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Youtube URL (optional)" class="materialize-textarea md_textarea item_youtube" id="yu" value="<?=$event_details['yu']?>"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Transit and parking information URL (optional)" class="materialize-textarea md_textarea item_parking" id="tpiu" value="<?=$event_details['tpiu']?>"/>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" id="images" value="">
					</div>
				</form>
			</div>
	
		</div>
	</div>
	<div class="valign-wrapper additem_modal_footer modal-footer">
		<a href="javascript:void(0)" class="btngen-center-align  close_modal open_discard_modal waves-effect">Cancel</a>
		<a href="javascript:void(0)" class="btngen-center-align waves-effect" data-class="addbtn" onclick="addEvent(this)">Update</a>
	</div>
</div>
<?php } ?>	
<?php include('../views/layouts/preferences.php'); ?>

<div id="managemembers-popup" class="modal manage-modal managemembers-popup">
	<div class="modal_header">
		<button class="close_btn custom_modal_close_btn close_modal waves-effect">
		  <i class="mdi mdi-close mdi-20px	"></i>
		</button>
		<h3>Manage Attendees</h3>
	</div>
	<div class="custom_modal_content modal_content">
		<div class="main-pcontent spadding">
			<ul class="tabs">
				<li class="tab active"><a href="#member-request-new" class="active" onclick="allrequests()">Attendee requested</a></li>
				<li class="tab"><a href="#member-all-new" onclick="allmembers()">All attendees</a></li>
			</ul>
			<div class="tab-content">
				<div id="member-request-new" class="tab-pane active"> </div>
				<div id="member-all-new" class="tab-pane" data-id="froup-stab"></div>
			</div>
		</div>
	</div>
</div>
 
<div id="manageorganizer-popup" class="modal manage-modal manageorganizer-popup">
	<div class="modal_header">
		<button class="close_btn custom_modal_close_btn close_modal waves-effect">
		  <i class="mdi mdi-close mdi-20px	"></i>
		</button>
		<h3>Manage Organizer</h3>
	</div>	
	<div class="custom_modal_content modal_content">
		<div class="main-pcontent spadding">
			<ul class="tabs">
				<li onclick="allmemberfororganizer();" class="tab active"><a href="#member-all" data-toggle="tab" aria-expanded="false">Promote to Organizer</a></li>
				<li onclick="memberrequestorganizer();" class="tab"><a href="#member-request" data-toggle="tab" aria-expanded="true">Request</a></li>
			</ul>
			<div class="tab-content">
				<div id="member-all" class="tab-pane active in" data-id="froup-stab">
					<ul class="manage-members">
						<center>
						<div class="preloader-wrapper small active">
						    <div class="spinner-layer spinner-blue-only">
						      <div class="circle-clipper left">
						        <div class="circle"></div>
						      </div><div class="gap-patch">
						        <div class="circle"></div>
						      </div><div class="circle-clipper right">
						        <div class="circle"></div>
						      </div>
						    </div>
						  </div>
						</center>
					</ul>
				</div>
				<div id="member-request" class="tab-pane">
					<ul class="manage-members">
						<center>
						<div class="preloader-wrapper small active">
						    <div class="spinner-layer spinner-blue-only">
						      <div class="circle-clipper left">
						        <div class="circle"></div>
						      </div><div class="gap-patch">
						        <div class="circle"></div>
						      </div><div class="circle-clipper right">
						        <div class="circle"></div>
						      </div>
						    </div>
						  </div>
						</center>
					</ul>
				</div>
			</div>
		</div>
	</div>

</div>

<div id="add-photo-popup" class="modal addphoto_modal custom_md_modal">
	<div class="modal_header">
		<button class="close_btn custom_modal_close_btn close_modal waves-effect">
		  <i class="mdi mdi-close mdi-20px	"></i>
		</button>
		<h3>Add photos</h3>
	</div>
	<div class="custom_modal_content modal_content">
		<div class="content-holder nsidepad nbpad">
			<?php $form = ActiveForm::begin(['options' => ['method' => 'post','enctype'=>'multipart/form-data','id' =>'albumdata','class' =>'add-album-form']]) ?>						
				<div class="post-photos">
					<div class="img-row nice-scroll">
					</div>
				</div>
				<div class="frow stretch-side">
					<div class="btn-holder">
						<input type="hidden" class="imgfile-count" value="0" />
						<input type="hidden" class="event_id" value="<?= $event_id;?>" />
						<a href="javascript:void(0)" onclick="addEventPhoto()" class="btn btn-primary btn-sm close-modal" data-class="addbtn">Add</a>
					</div>				
				</div>				
			<?php ActiveForm::end() ?>			
		</div>
	</div>
</div>

<?php include('../views/layouts/custom_modal.php'); ?>

<div id="upload-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="edit-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="userwall_tagged_users" class="modal modalxii_level1">
	<div class="content_header">
		<button class="close_span waves-effect">
			<i class="mdi mdi-close mdi-20px"></i>
		</button>
		<p class="selected_photo_text"></p>
		<a href="javascript:void(0)" class="chk_person_done_new done_btn focoutTRV03 action_btn">Done</a>
	</div>
	<nav class="search_for_tag">
		<div class="nav-wrapper">
		  <form>
		    <div class="input-field">
		      <input id="tagged_users_search_box" class="search_box" type="search" required="">
		        <label class="label-icon" for="tagged_users_search_box">
		          <i class="zmdi zmdi-search mdi-22px"></i>
		        </label>
		      </div>
		  </form>
		</div>
	</nav>
	<div class="person_box"></div>
</div>

<input type="hidden" name="eventownerid" id="eventownerid" value="<?=$event_details['created_by']?>" />
<input type="hidden" name="pagename" id="pagename" value="event" />
<input type="hidden" name="tlid" id="tlid" value="<?=$event_id?>" />
<input type="hidden" name="baseurl" id="baseurl" value="<?=$baseUrl?>" />
<script type="text/javascript">
	var event_id ='<?php echo $event_id;?>';
	$(document).ready(function() {
		var eventName = '<?=$eventName?>';
		$('.page-name.mainpage-name').html(eventName);
	});
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>

<script src="<?=$baseUrl?>/js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/event.js"></script>
<script src="<?=$baseUrl?>/js/post.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		justifiedGalleryinitialize();
		lightGalleryinitialize();
	}); 
</script>
