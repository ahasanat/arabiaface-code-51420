<?php
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl;
?>
<span class="mob-title"><i class="mdi mdi-newspaper"></i>Discussion</span>
<div class="new-post compose-box" id="viacall_new_post">
    <?= \Yii::$app->view->renderFile('@app/views/layouts/postblock.php'); ?>
</div>
<div class="post-list">
<?php 
    $lp = 1; 
    foreach($eventposts as $post)
    { 
        $existing_posts = '1';
        $cls = '';
        if(count($post)==$lp) 
        {
            $cls = 'lazyloadscroll';
        }

        $postid = (string)$post['_id'];
        $postownerid = (string)$post['post_user_id'];
        $postprivacy = $post['post_privacy'];

        $isOk = $this->context->filterDisplayLastPost($postid, $postownerid, $postprivacy);
        if($isOk == 'ok2389Ko') {
            $this->context->display_last_post($postid,$existing_posts, '', $cls);
        }
    }
?>
</div>
<div class="clear"></div>
<?php if($lp <= 1){
	$this->context->getwelcomebox("event");
} ?>
<center><div class="lds-css ng-scope dis-none"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
<?php exit;?>							  