<?php   
use frontend\assets\AppAsset;
use backend\models\Googlekey;
$session = Yii::$app->session;
$user_id = $session->get('user_id');
$this->title = 'Events';
$baseUrl = AppAsset::register($this)->baseUrl;
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
<link href="<?=$baseUrl?>/css/animate.css" rel="stylesheet">
<div class="page-wrapper gen-pages">
    <div class="header-section">
        <?php include('../views/layouts/header.php'); ?>
    </div>
    <div class="floating-icon">
        <div class="scrollup-btnbox anim-side btnbox scrollup-float">
            <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>			
        </div>        
    </div>
    <div class="clear"></div>
	<div class="container page_container event_container">
		<?php include('../views/layouts/leftmenu.php'); ?>
		<div class="fixed-layout ipad-mfix">
			<div class="main-content with-lmenu commevents-page main-page grid-view general-page">
				<div class="combined-column comsepcolumn">
					<div class="content-box nbg">
						<div class="cbox-desc md_card_tab">
							<div class="fake-title-area divided-nav mobile-header">
								<ul class="tabs">
									<li class="tab col s3 allevents active"><a href="#commevents-suggested" data-toggle="tab" aria-expanded="true">Suggested</a></li>
									<li class="tab col s3 going"><a href="#commevents-attending" data-toggle="tab" aria-expanded="false">Attending</a></li>
									<li class="tab col s3 yours"><a href="#commevents-yours" data-toggle="tab" aria-expanded="false">Yours</a></li>
								</ul>
							</div>
							 
							<div class="tab-content view-holder grid-view">												
								<div class="tab-pane fade main-pane active in" id="commevents-suggested">
									<div class="commevents-list generalbox-list ">
										<div class="row">
											<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
										</div>
									</div>
								</div>
								<div class="tab-pane fade main-pane" id="commevents-attending">
									<div class="commevents-list generalbox-list animated fadeInUp">
										<div class="row">
										</div>
									</div>
								</div>
								<div class="tab-pane fade main-pane commevents-yours general-yours" id="commevents-yours">
									<div class="commevents-list generalbox-list animated fadeInUp">
										<div class="row">
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="chatblock">
					<div class="float-chat anim-side">
						<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('../views/layouts/footer.php'); ?>
</div>
<div id="upload_img_box" class="modal crop_upload_modal">
  <div class="upload_img_title">
	<h3>Crop and Reposition image</h3>
	<a class="popup-modal-dismiss crossicon" data-dismiss="modal" href="javascript:void(0)">
	  <i class="mdi mdi-close"></i>
	</a>
  </div>
  <div class="jc-demo-box">
	<form id="formCrop">
	  <div id="views"></div>
	  <br />
	  <button id="cropbutton" type="button">Crop</button>
	  <button id="saveimg" type="button">Save</button>
	</form>
  </div>
</div>	

<!--add event modal-->
<div id="add_event_modal" class="modal add-item-popup custom_md_modal">
	<div class="modal_content_container">
		<div class="modal_content_child modal-content">			
			<div class="popup-title">
				<button class="hidden_close_span close_span waves-effect">
					<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
				</button>	
				<h3>Create Event</h3>
				<a type="button" class="item_done crop_done waves-effect hidden_close_span close_modal" onclick="addEvent(this)" href="javascript:void(0)" data-class="cancelbtn">Done</a>
			</div>    
			<div class="main-pcontent">
				<form class="add-item-form" id="eventform">
					<div class="frow frowfull">	
						<div class="crop-holder" id="image-cropper">
							<div class="cropit-preview"></div>
							<div class="main-img">
								<img src="<?=$baseUrl?>/images/additem-commevents.png">
							</div>
							<div class="main-img1">
							  <img id="imageid" draggable="false"/>
							</div>
							<div class="btnupload custom_up_load" id="upload_img_action">
							  <div class="fileUpload">
								<i class="zmdi zmdi-hc-lg zmdi-camera"></i>
								<input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
							  </div>
							</div>
							<a  href="javascript:void(0)" class="saveimg btn btn-save image_save_btn image_save dis-none">
							  <span class="zmdi zmdi-check"></span>
							</a>
							<a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash removeimg">
							  <i class="mdi mdi-close"></i>	
							</a>
						</div>
					</div>			   
					<div class="sidepad" data-id="sidepad-dis">						
						<div class="frow">
							<input id="title" name="title" type="text" class="validate additem_title" placeholder="Event title" />
						</div>
						<div class="frow">
							<textarea id="tagline" name="tagline" class="materialize-textarea mb0 md_textarea additem_tagline" placeholder="Event tagline"></textarea>
						</div>
						<div class="frow">
							<input type="text" placeholder="Location" class="materialize-textarea md_textarea item_address" id="placelocationsearch" data-query="all"  onfocus="filderMapLocationModal(this)" autocomplete='off'/>
							<input type="hidden" id="country">
							<input type="hidden" id="au_country">
							<input type="hidden" id="country_code">
							<input type="hidden" id="isd_code">						
						</div>
						<div class="frow">
							<div class="row">
								<div class="col s6">
									<input type="text" data-toggle="datepicker" data-query="all" class="datepickerinput" placeholder="Event Date" name="event_date" id="eventdatepicker" readonly/>
								</div>
								<div class="col s6">
									<input type="text" class="timepicker timeinput" placeholder="Event Time" id="eventtimepicker"/>
								</div>
							</div>
						</div>
						<div class="frow">
							<textarea type="text" placeholder="Tell people more about the event" class="materialize-textarea md_textarea item_about" id="aboutevent"></textarea>							
						</div>
						<div class="frow">
							<div class="row">
								<div class="col s9">
									<p>Ask to join</p> 
								</div>
								<div class="col s3">
									<div class="switch">
									  <label>
										<input type="checkbox"  name="isasktojoin" id="ask_to_join_switch" data-value="0" class="myCheckbox1"/>
										<span class="lever" for="ask_to_join_switch"></span>
									  </label>
									</div>
								</div>
							</div>
						</div>
						<div class="frow security-area">
							<label>Privacy</label>
							<div class="right">
								<a class="dropdown_text dropdown-button eventcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="eventcreateprivacylabel" data-fetch="no" data-label="event">
									<span class="privacyarea">Public</span>
									<i class="mdi mdi-menu-down"></i>
								</a>
							</div>
						</div>
						<div class="frow">
							<div class="expandable-holder">
								<a href="javascript:void(0)" class="expand-link invertsign" onclick="mng_expandable(this)">Advanced Option <i class="mdi mdi-menu-down"></i></a>
								<div class="expandable-area">
									<div class="frow">
										<input type="text" placeholder="Website URL (optional)" class="materialize-textarea md_textarea item_website" id="wu"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Ticket seller URL (optional)" class="materialize-textarea md_textarea item_ticket" id="tu"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Youtube URL (optional)" class="materialize-textarea md_textarea item_youtube" id="yu"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Transit and parking information URL (optional)" class="materialize-textarea md_textarea item_parking" id="tpiu"/>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" id="images" value="">
					</div>
				</form>
			</div>
	
		</div>
	</div>
	<div class="valign-wrapper additem_modal_footer modal-footer">
		<a href="javascript:void(0)" class="btngen-center-align  close_modal open_discard_modal waves-effect">Cancel</a>
		<a href="javascript:void(0)" class="btngen-center-align waves-effect" data-class="addbtn" onclick="addEvent(this)">Create</a>
	</div>
</div>

<div id="compose_mapmodal" class="modal map_modal compose_inner_modal map_modalUniq modalxii_level1">
	<?php include('../views/layouts/mapmodal.php'); ?>
</div>

<?php include('../views/layouts/custom_modal.php'); ?>

<div id="upload-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="edit-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="userwall_tagged_users" class="modal modalxii_level1">
	<div class="content_header">
		<button class="close_span waves-effect">
			<i class="mdi mdi-close mdi-20px"></i>
		</button>
		<p class="selected_photo_text"></p>
		<a href="javascript:void(0)" class="chk_person_done_new done_btn focoutTRV03 action_btn">Done</a>
	</div>
	<nav class="search_for_tag">
		<div class="nav-wrapper">
		  <form>
		    <div class="input-field">
		      <input id="tagged_users_search_box" class="search_box" type="search" required="">
		        <label class="label-icon" for="tagged_users_search_box">
		          <i class="zmdi zmdi-search mdi-22px"></i>
		        </label>
		      </div>
		  </form>
		</div>
	</nav>
	<div class="person_box"></div>
</div>

<style>
  .ui-datepicker{z-index:9999 !important;} 
  .pac-container{z-index: 1051 !important;}
</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>
<script src="<?=$baseUrl?>/js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/event.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		justifiedGalleryinitialize();
		lightGalleryinitialize();
	}); 
</script>
