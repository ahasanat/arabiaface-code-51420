<?php 
use frontend\assets\AppAsset;
use frontend\models\LoginForm;
use frontend\models\Notification;
use frontend\models\EventVisitors;
use yii\helpers\Url;
$baseUrl = AppAsset::register($this)->baseUrl; 
?>
<span class="mob-title"><i class="mdi mdi-bullhorn"></i>Invite Friends</span>
<div class="content-box bshadow inner-content-box">
	<div class="cbox-title">
		<h5><i class="mdi mdi-bullhorn"></i>Invite Friends</h5>
	</div>
	<div class="cbox-desc">
		<div class="likes-summery">
			<div class="friend-likes">
				<h5><a href="javascript:void(0)"><?=$eventcount?> User<?php if($eventcount > 1){ ?>s are<?php } else { ?>is<?php } ?></a>  attending <?=$event_details['event_name']?> event</h5>
				<?php if($eventcount > 0){ ?>
				<ul>
					<?php foreach($eventusers as $eventuser){ ?>
					<li>
						<a href="<?php echo Url::to(['userwall/index', 'id' => $eventuser['user_id']]);?>">
							<img title="<?=$eventuser['user']['fullname']?>" src="<?= $this->context->getimage($eventuser['user']['_id'],'thumb')?>">
						</a>
					</li>
					<?php } ?>
				</ul>										
				<?php } else { ?>
				<?php $this->context->getnolistfound('becomefirsttojoinevent');?>
				<?php } ?>
			</div>
			<div class="invite-likes">  
				<?php if(count($invitedfriend) > 0) { ?>
				<p>Invite your friends to attend this event</p>
				<?php } ?>
				<div class="invite-holder">
					<form>
						<div class="tholder">
							<div class="sliding-middle-out anim-area underlined">
								<input placeholder="Type a friend's name" type="text" class="invite_friend_event" data-id="invite_friend_event">
								<a href="javascript:void(0)" onclick="removeinvitesearchinput(this);"><img src="<?=$baseUrl?>/images/cross-icon.png"/></a>
							</div>
						</div>
					</form>
					<div class="list-holder blockinvite_friend_event">
						<ul>
							<?php 
							if(count($invitedfriend) > 0){
								foreach($invitedfriend as $invitedfriends){
								$friendid = (string)$invitedfriends['to_id'];
								$result = LoginForm::find()->where(['_id' => $friendid])->one();
								$frndimg = $this->context->getimage($friendid,'thumb');
								$attending = EventVisitors::find()->where(['event_id' => "$event_id", 'user_id' => "$friendid", 'is_deleted' => '1'])->one();
								$invitaionsent = Notification::find()->where(['event_id' => $event_id, 'status' => '1', 'event_invited_to_id' => $friendid, 'user_id' => $user_id, 'notification_type' => 'eventinvited'])->one(); 
							?>
							<li class="invite_<?=$friendid?>">
								<div class="invitelike-friend">
									<div class="imgholder"><img src="<?=$frndimg?>"></div>
									<div class="descholder">
										<h6><?=$result['fullname']?></h6>
										<div class="btn-holder events_<?=$friendid?>">
										<?php if($attending) {
											echo '<label class="infolabel"><i class="zmdi zmdi-check"></i> Attending</label>';
										} else if($invitaionsent) {
											echo '<label class="infolabel"><i class="zmdi zmdi-check"></i> Invited</label>';
										} else { ?>
											<a href="javascript:void(0)" class="btn-invite" onclick="sendinviteevent('<?=$friendid?>','<?=$event_id?>')">Invite</a>
											<a href="javascript:void(0)" class="btn-invite-close" onclick="cancelinviteevent('<?=$friendid?>')"><i class="mdi mdi-close"></i></a>
										<?php } ?>
										</div>
										<div class="dis-none btn-holder sendinvitation_<?=$friendid?>">
											<label class="infolabel"><i class="zmdi zmdi-check"></i> Invitation sent</label>
										</div>
									</div>														
								</div>
							</li>
							<?php } } else { ?>
							<?php $this->context->getnolistfound('allfriendsattendingevent');?>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php exit;?>							