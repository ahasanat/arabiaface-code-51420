<?php 
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl; 
?>
<div class="modal_header">
	<button class="close_btn custom_modal_close_btn close_modal">
	  <i class="mdi mdi-close mdi-20px	"></i>
	</button>
	<h3>Manage Attendees</h3>
</div>
<div class="custom_modal_content modal_content">
	
	<div class="main-pcontent spadding">
		<ul class="tabs">
			<li class="tab active"><a href="#member-request" data-toggle="tab" aria-expanded="false">Attendee requested</a></li>
			<li class="tab"><a href="#member-all" data-toggle="tab" aria-expanded="true">All attendees</a></li>
		</ul>
		<div class="tab-content">
			<div id="member-request" class="tab-pane active in">
				<h5>Attendee requests</h5>
				<ul class="manage-members">
					<?php if(empty($member_request)){ ?>
						<div class="no-listcontent">
							No attendee request
						</div>	
					<?php }?>
					<?php foreach($member_request as $member_requests){ ?>
					<li>
						<div class="member-li">
							<div class="imgholder"><img src="<?= $this->context->getimage($member_requests['user_id'],'thumb');?>"/></div>
							<div class="descholder">
								<span class="head6"><?= $this->context->getuserdata($member_requests['user_id'],'fullname');?></span>
								<div class="settings accept_<?= $member_requests['_id'];?>">
									<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="request_responce('1','<?= $member_requests['_id'];?>')" >Accept</a>
									<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="request_responce('0','<?= $member_requests['_id'];?>')">Reject</a>
								</div>
							</div>
						</div>
					</li>
					<?php }?>
				</ul>
			</div>
			<div id="member-all" class="tab-pane" data-id="froup-stab">
				<h5>All attendees</h5>
				<?php 	$user_admin = $event_detail['created_by'];
						$group_id 	= $event_detail['_id'];
				?>
				<ul class="manage-members">
					<li>
						<div class="member-li">
							<div class="imgholder"><img src="<?= $this->context->getimage($user_admin,'thumb');?>"/></div>
							<div class="descholder">
								<span class="head6"><?= $this->context->getuserdata($user_admin,'fullname');?></span>
								<div class="settings">
									<span class="status">Admin</span>
								</div>
							</div>
						</div>
					</li>
					<?php foreach($event_member as $event_members)
					{
						$event_user_id = $event_members['user_id'];
						$event_member_id = $event_members['_id'];
					?>
					<li class="member_<?= $event_member_id;?>">
						<div class="member-li">
							<div class="imgholder"><img src="<?= $this->context->getimage($event_user_id,'thumb');?>"/></div>
							<div class="descholder">
								<span class="head6"><?= $this->context->getuserdata($event_user_id,'fullname');?></span>
								<div class="settings">
									<span class="status">Attendee</span>
									<div class="right">
										<a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings_<?=$event_member_id?>">
											<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
										</a>
										<ul id="mm_settings_<?=$event_member_id?>" class="dropdown-content custom_dropdown">
											<li><a href="javascript:void(0)" onclick="remove_member('<?= $event_member_id;?>')">Remove attendee</a></li>
										</ul>
									</div>									
								</div>
							</div>
						</div>
					</li>
					<?php }?>
				</ul>
			</div>
		</div>
	</div>

</div>
<?php exit;?>