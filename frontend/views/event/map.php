<?php 
if(!$event_details)
{
	$this->context->getnolistfound('noaccesstoviewevent');
}
else
{ ?>
	<span class="mob-title"><i class="zmdi zmdi-pin"></i>Location</span>
	<div class="content-box bshadow inner-content-box">
		<div class="cbox-title">
			<h5>
				<i class="zmdi zmdi-pin"></i>
				Location
			</h5>
		</div>
		<div class="cbox-desc">
			<div class="map-holder">
				<iframe width="640" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=<?= $event_details['address']; ?>&output=embed"></iframe>
			</div>
		</div>
	</div>
<?php } ?>
<?php exit;?>							