<?php 
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
if(isset($data) && !empty($data)) {
	$event_user_id = $data['user_id'];
	if($user_id == $event_user_id) {
		continue;
	}
	$event_member_id = (string)$data['_id'];
	?>
	<li class="member_<?= $event_member_id;?>">
		<div class="member-li">
			<div class="imgholder"><img src="<?= $this->context->getimage($event_user_id,'thumb');?>"/></div>
			<div class="descholder">
				<span class="head6"><?= $this->context->getuserdata($event_user_id, 'fullname');?></span>
				<div class="settings">
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="setorganizer('<?=$event_user_id?>', '<?=$event_id?>', this, true)" >Cancel Organizer Request</a>
				</div>
			</div>
		</div>
	</li>
<?php } else { ?>
	<?php $this->context->getnolistfound('noorganizerrequest'); ?>
<?php }
exit;?>