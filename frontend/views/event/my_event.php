<?php 

use frontend\assets\AppAsset;
use frontend\models\EventVisitors;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id'); 
$baseUrl = AppAsset::register($this)->baseUrl;
?>

<div class="col s6 m4 l3 add-cbox">
	<div class="card hoverable eventCard">
		<div class="commevents-box general-box">
		<?php if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { ?>
			<a href="javascript:void(0)" class="add-commevents add-general <?=$checkuserauthclass?> directcheckuserauthclass">
				<span class="icont">+</span>
				Create Community Event
			</a>
			<?php  } else { ?>
			<a href="javascript:void(0)" class="add-commevents add-general" onclick="openAddItemModal()">
				<span class="icont">+</span>
				Create Community Event
			</a>
			<?php } ?>
		</div>
	</div>
</div>
<?php
foreach($myevents as $myevent)
{
	$event_id = (string)$myevent['_id'];
	$event_count = EventVisitors::getEventCounts($event_id);
	$date = $myevent['event_date'];
	$profile = $this->context->geteventimage($event_id);
    $isDefaultProfile = $imgclass = '';
	if (strpos($profile, 'additem-commevents.png') !== false) {
	    $isDefaultProfile ='defaultprofile';
	} else {
		$val = getimagesize($profile);
        if($val[0] > $val[1]) { 
            $imgclass = 'himg';
        } else if($val[1] > $val[0]) { 
            $imgclass = 'vimg';
        } else {
            $imgclass = 'himg';
        }
    }
	?>
	<div class="col s6 m4 l3 gridBox127 uniqidentitybox eventbox_<?=$event_id?>">
		<div class="card hoverable eventCard">
			<a href="javascript:void(0);" onclick="joinEvent(event,'<?=$event_id;?>',this, true)" class="commevents-box general-box">
				<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
					<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
				</div>
				<div class="content-holder">
					<h4><?= $myevent['event_name'];?></h4>
					<div class="username fcount_<?= $event_id;?>">
						<?=$event_count?> Attending
					</div>	
					<div class="userinfo">
						<span class="month"><?=date("M", strtotime($date))?></span>
						<span class="date"><?=date("d", strtotime($date))?></span>
					</div>                    
					<div class="action-btns">                        
						<span class="noClick" onclick="joinEvent(event,'<?=$event_id;?>',this, false)"><?= EventVisitors::getEventGoing($event_id);?></span>
					</div>
				</div>
			</a>
		</div>
	</div>
<?php } 
exit;?>