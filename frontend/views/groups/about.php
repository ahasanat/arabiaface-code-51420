<?php 
use yii\helpers\Url;
use backend\models\Googlekey;
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
if(isset($detail) && !empty($detail)) {
	$groupName = $detail['name'];
	$privacy = $detail['privacy'];
	$adminid = $detail['user_id'];
	$created_date = $detail['created_date'];
	$location = $detail['location'];
	$tagline = isset($detail['tagline']) ? $detail['tagline'] : '';
	$created_at = date("M d, Y", $created_date);
	$created_by = $detail['created_by'];	
	?>
	<span class="mob-title"><i class="mdi mdi-information-variant"></i>About Group</span>
	<div class="content-box bshadow inner-content-box">
		<div class="event-info group-info">		
			<div class="cbox-title">
				<h5>
					<i class="mdi mdi-information-variant"></i>
					About Group
				</h5>
			</div>	
			<div class="cbox-desc">
				<ul class="section-ul">
					<li>	
						<div class="eventinfo">												
							<div class="descinfo">
								<h4><?=$groupName?></h4>
								<p><span><?=$privacy?></span> : Created by 
									<a href="<?php echo Url::to(['userwall/index', 'id' => "$created_by"]); ?>"><?= $this->context->getuserdata($created_by, 'fullname');?></a>
								</p>
							</div>																			
						</div>
						<div class="clear"></div>
						<ul class="eventinfo-ul">
							<li>
								<div class="eventinfo-row">
									<span class="iconholder"><i class="mdi mdi-clock-outline"></i></span>
									<span class="descholder">
										<h5>Created on <?=$created_at?></h5>
										<div style="clear: both;"></div>
										<p><?=$tagline?></p>
									</span>
								</div>
							</li>
							<li> 
								<div class="eventinfo-row">
									<span class="iconholder"><i class="zmdi zmdi-pin"></i></span>
									<span class="descholder">
										<h5><?=$location?></h5>
										<a href="javascript:void(0)" onclick="showEventMap(this)">Show map</a>
									</span>
									<div class="mapholder">
										<iframe src="https://www.google.com/maps/embed/v1/search?key=<?=$GApiKeyL?>&q=<?=urlencode($location)?>" width="600" height="450" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>
							</li>																			
						</ul>
					</li>
					<li>
						<div class="row">
							<div class="col m12 l12 s12">
								<label>Group Admin</label>
							</div>
							<div class="col m12 l12 s12">
								<ul class="member-ul">
									<li>
										<div class="member-box">
											<a href="<?php echo Url::to(['userwall/index', 'id' => $adminid]); ?>">
												<img src="<?= $this->context->getimage($adminid, 'thumb');?>"/>
												<span><?= $this->context->getuserdata($adminid, 'fullname');?></span>
											</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="content-box bshadow inner-content-box">
		<div class="event-info">
			<div class="cbox-title">
				<h5>																	
					Details
				</h5>
			</div>
			<div class="post-desc white-box">
				<div class="para-section">															
					<div class="para">								
						<p><?= trim($detail['description']);?></p>							
					</div>
					<?php if(strlen($detail['description']) > 365) { ?>
					<a href="javascript:void(0)" class="readlink" onclick="showAllContent(this)">Read More</a>
					<?php } ?>
					<a href="javascript:void(0)" class="readlink">Read More</a>
				</div>
			</div>
		</div>
	</div>
<?php 
} exit;