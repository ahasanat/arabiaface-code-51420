<?php 
use frontend\assets\AppAsset;
use frontend\models\GroupMember;
use frontend\models\Friend;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$status = $session->get('status');
$baseUrl = AppAsset::register($this)->baseUrl; 

foreach($allgroup as $allgroups)
{
	$group_id = (string)$allgroups['_id'];
	$group_user_id = $allgroups['user_id'];
	$col_created_by =$allgroups['user_id'];
	$col_privacy = $allgroups['privacy'];
	$isWriteAllow = 'no';
	$is_friend = Friend::find()->where(['from_id' => "$user_id",'to_id' => "$col_created_by",'status' => '1'])->one();

	if($user_id == $group_user_id) {
		$isWriteAllow = 'yes';
	} else if(($col_privacy == 'Public' || $status == '10') || ($col_privacy == 'Friends' && ($is_friend || $status == '10')) || ($col_privacy == 'Private')) {
		$isWriteAllow = 'yes';
	} else if($col_privacy == 'Custom') {
		$customids = array();
	    $customids = isset($allgroups['customids']) ? $allgroups['customids'] : '';
	    $customids = explode(',', $customids);

	    if(in_array($user_id, $customids)) {
			$isWriteAllow = 'yes';	    	
	    }
	} else if($col_privacy == 'Friend of friend') {
		$friendoffriendids = Friend::getuserFriendsIds($group_user_id);
		foreach ($friendoffriendids as $levelsingle) {
			$level = Friend::getuserFriendsIds($levelsingle);
			if(!empty($level)) {
				$friendoffriendids = array_merge($friendoffriendids, $level);	
			}
		}
		$friendoffriendids = array_unique($friendoffriendids);

		if(in_array($user_id, $friendoffriendids)) {
			$isWriteAllow = 'yes';	 	
		}
	}

	$members_count = GroupMember::getgroupmembercount($group_id);
	if($user_id != $group_user_id) {
		$member_status = GroupMember::getgroupmemerstatus($group_id);
	}
	
	if($isWriteAllow == 'yes')
	{

	$profile = $this->context->getgroupimage($group_id);

	$isDefaultProfile = $imgclass = '';
	if (strpos($profile, 'additem-groups.png') !== false) {
	    $isDefaultProfile ='defaultprofile';
	} else {
		$val = getimagesize($profile);
        if($val[0] > $val[1]) { 
            $imgclass = 'himg';
        } else if($val[1] > $val[0]) { 
            $imgclass = 'vimg';
        } else {
            $imgclass = 'himg';
        }
    }
	?>
	<div class="col s6 m4 l3 gridBox127 uniqidentitybox groupbox_<?=$group_id?>">
		<div class="card hoverable groupCard">
			<a href="javascript:void(0);" onclick="groupsJoins(event,'<?=$group_id;?>',this, true)" class="groups-box general-box">
				<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
					<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
				</div>
				<div class="content-holder">
					<h4><?= $allgroups['name'];?></h4>													
					<div class="members member_count_<?=$group_id;?>">
						<?=$members_count;?> Members
					</div>
					<div class="action-btns">								
						<?php if($group_user_id != $user_id) { ?>
							<span class="noClick <?=$checkuserauthclass?>" onclick="groupsJoins(event,'<?=$group_id;?>',this, false)"><?= $member_status;?></span>
						<?php } else { ?> 
							<span>Admin</span>
						<?php } ?>	
					</div>
				</div>
			</a>
		</div>
	</div>
	<?php 
	} 
}
exit;