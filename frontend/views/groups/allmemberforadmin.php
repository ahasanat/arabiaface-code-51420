<?php 
use frontend\models\Group;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$isEmpty = true;
if(!empty($group_member) ) {
	foreach($group_member as $group_members)
	{
		$group_user_id = $group_members['user_id'];
		if($user_id == $group_user_id) {
			continue;
		}
		$group_member_id = (string)$group_members['_id'];
		$isRequestSend = Group::isRequestSend($group_user_id, $group_id);
		$isEmpty = false;
		?>
		<li class="member_<?= $group_member_id;?>">
		<div class="member-li">
			<div class="imgholder"><img src="<?= $this->context->getimage($group_user_id,'thumb');?>"/></div>
			<div class="descholder">
				<span class="head6"><?= $this->context->getuserdata($group_user_id,'fullname');?></span>
				<div class="settings">
				<?php if($isRequestSend == true) { ?>
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="setadmin('<?=$group_user_id?>', '<?=$group_id?>', this)" >Cancel Admin Request</a>
				<?php } else { ?>
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="setadmin('<?=$group_user_id?>', '<?=$group_id?>', this)" >Set as Admin</a>
				<?php } ?>
				</div>
			</div>
		</div>
	</li>
<?php }
} 
 
if($isEmpty) { ?>
	<?php $this->context->getnolistfound('norequestformembergroup'); ?>
<?php }
exit;?>