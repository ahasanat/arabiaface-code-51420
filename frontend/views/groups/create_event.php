<?php 
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl;
?>	
	<div class="modal_content_container">
		<div class="modal_content_child modal-content">			
			<div class="popup-title">
				<button class="hidden_close_span close_span waves-effect">
					<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
				</button>	
				<h3>Create Event</h3>
				<a type="button" class="item_done crop_done waves-effect hidden_close_span close_modal" onclick="addEvent(this)" href="javascript:void(0)" data-class="cancelbtn">Done</a>
			</div>    
			<div class="main-pcontent">
				<form class="add-item-form" id="eventform">
					<div class="frow frowfull">	
						<div class="crop-holder" id="image-cropper">
							<div class="cropit-preview"></div>
							<div class="main-img">
								<img src="<?=$baseUrl?>/images/additem-commevents.png">
							</div>
							<div class="main-img1">
							  <img id="imageid" draggable="false"/>
							</div>
							<div class="btnupload custom_up_load" id="upload_img_action">
							  <div class="fileUpload">
								<i class="zmdi zmdi-hc-lg zmdi-camera"></i>
								<input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
							  </div>
							</div>
							<a  href="javascript:void(0)" class="saveimg btn btn-save image_save_btn image_save dis-none">
							  <span class="zmdi zmdi-check"></span>
							</a>
							<a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash removeimg">
							  <i class="mdi mdi-close"></i>	
							</a>
						</div>
					</div>			   
					<div class="sidepad" data-id="sidepad-dis">						
						<div class="frow">
							<input id="title" name="title" type="text" class="validate additem_title" placeholder="Event title" />
						</div>
						<div class="frow">
							<textarea id="tagline" name="tagline" class="materialize-textarea mb0 md_textarea additem_tagline" placeholder="Event tagline"></textarea>
						</div>
						<div class="frow">
							<input type="text" placeholder="Location" class="materialize-textarea md_textarea item_address getplacelocation" id="address"/>
							<input type="hidden" id="country">
							<input type="hidden" id="au_country">
							<input type="hidden" id="country_code">
							<input type="hidden" id="isd_code">						
						</div>
						<div class="frow">
							<div class="row">
								<div class="col s6">
									<input type="text" data-toggle="datepicker" data-query="M" class="datepickerinput" placeholder="Event Date" name="event_date" id="eventdatepicker" readonly/>
								</div>
								<div class="col s6">
									<input type="text" class="timepicker" placeholder="Event Time" id="eventtimepicker"/>
								</div>
							</div>
						</div>
						<div class="frow">
							<textarea type="text" placeholder="Tell people more about the event" class="materialize-textarea md_textarea item_about" id="aboutevent"></textarea>							
						</div>
						<div class="frow">
							<div class="row">
								<div class="col-sm-9">
									<p>Ask to join</p> 
								</div>
								<div class="col-sm-3">
									<div class="switch">
									  <label>
										<input type="checkbox"  name="isasktojoin" id="ask_to_join_switch" data-value="0" class="myCheckbox1"/>
										<span class="lever" for="ask_to_join_switch"></span>
									  </label>
									</div>
								</div>
							</div>
						</div>
						<div class="frow security-area">
							<label>Privacy</label>
							<div class="dropdown dropdown-custom dropdown-xsmall setDropVal">
								<a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="event_privacy">
									<span>Public</span>
									<i class="zmdi zmdi-caret-down"></i>
								</a>
								<ul id="event_privacy" class="dropdown-content custom_dropdown">
									<li class="post-friends">
										<a href="javascript:void(0)">Friends</a>
									</li>
									<li class="post-friend-of-friend">
										<a href="javascript:void(0)">Friend of friend</a>
									</li>
									<li class="post-settings">
										<a href="javascript:void(0)">Custom</a>
									</li>
									<li class="post-public">
										<a href="javascript:void(0)">Public</a>
									</li>
								</ul>
								<input type="hidden" name="post_privacy" id="post_privacy" value="Public"/>
							</div>
						</div>
						<div class="frow">
							<div class="expandable-holder">
								<a href="javascript:void(0)" class="expand-link invertsign" onclick="mng_expandable(this)">Advanced Option <i class="mdi mdi-menu-down"></i></a>
								<div class="expandable-area">
									<div class="frow">
										<input type="text" placeholder="Website URL (optional)" class="materialize-textarea md_textarea item_website" id="wu"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Ticket seller URL (optional)" class="materialize-textarea md_textarea item_ticket" id="tu"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Youtube URL (optional)" class="materialize-textarea md_textarea item_youtube" id="yu"/>
									</div>
									<div class="frow">
										<input type="text" placeholder="Transit and parking information (optional)" class="materialize-textarea md_textarea item_parking" id="tpiu"/>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" id="images" value="">
					</div>
				</form>
			</div>
	
		</div>
	</div>
	<div class="valign-wrapper additem_modal_footer modal-footer">
		<a href="javascript:void(0)" class="btngen-center-align  close_modal open_discard_modal waves-effect">Cancel</a>
		<a href="javascript:void(0)" class="btngen-center-align waves-effect" data-class="addbtn" onclick="addEvent(this)">Create</a>
	</div>
<?php exit; ?>