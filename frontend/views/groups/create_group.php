<?php 
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl;
?>
<div class="modal_content_container">
	<div class="modal_content_child modal-content">
		<div class="popup-title">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>	
            <h3>Create Group</h3>
            <a type="button" class="item_done crop_done waves-effect hidden_close_span close_modal" href="javascript:void(0)" >Done</a>
         </div>
		<div class="main-pcontent">
		<form class="add-item-form">
			<div class="frow frowfull">	
				<div class="crop-holder" id="image-cropper">
					<div class="cropit-preview"></div>
					<div class="main-img">
						<img src="<?=$baseUrl?>/images/additem-groups.png">
					</div>
					<div class="main-img1">
					  <img id="imageid" draggable="false"/>
					</div>
					<div class="btnupload custom_up_load" id="upload_img_action">
					  <div class="fileUpload">
					  	<i class="zmdi zmdi-camera zmdi-hc-lg"></i>
						<input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
					  </div>
					</div>
					<a  href="javascript:void(0)" class="saveimg btn btn-save image_save_btn image_save dis-none">
					  <span class="zmdi zmdi-check"></span>
					</a>
					<a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash removeimg">
					  <i class="mdi mdi-close"></i>	
					</a>
				</div>
			</div>
			<div class="sidepad">
				<div class="frow">
					<input type="text" class="validate item_title title" placeholder="Group name" />
				</div>
				<div class="frow">
					<textarea class="materialize-textarea mb0 md_textarea item_tagline tagline" placeholder="Group tagline"></textarea>
                    <span class="char-limit">0/80</span>								
				</div>
				<div class="frow">
					<input placeholder="Location" class="placelocationsearch item_address" id="placelocationsearch" type="text" data-query="all"  onfocus="filderMapLocationModal(this)" autocomplete='off'>
				</div>	
				<div class="frow">
				<div class="row ">
					<div class="col s9">
						<p>Review posts before feed</p>
					</div>
					<div class="col s3">
						  <div class="switch">
							<label>
							  <input type="checkbox" id="friend_activity_switch">
							  <span class="lever"></span>
							</label>
						  </div>
					</div>
				</div>	
				</div>	
				
				<div class="frow">
				<div class="row ">
					<div class="col s9">
						<p>Ask to join</p>
					</div>
					<div class="col s3">
						  <div class="switch">
							<label>
							  <input type="checkbox" id="ask_to_join_switch">
							  <span class="lever"></span>
							</label>
						  </div>
					</div>
				</div>	
				</div>							
				
				<div class="frow security-area">
					<label>Privacy</label>
					
						<div class="right">
						<a class="dropdown_text dropdown-button groupcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="groupcreateprivacylabel" data-fetch="no" data-label="group">
							<span class="privacyLabel">Public</span>
							<i class="zmdi zmdi-caret-down"></i>
						</a>
					</div>
					
				</div>	
				<div class="frow">
					<textarea placeholder="Descriptions of your group" class="materialize-textarea md_textarea item_about description"></textarea>
				</div>		
			</div>
		</form>
		</div>

	</div>
</div>
 <div class="valign-wrapper additem_modal_footer modal-footer">
  <a  class="btngen-center-align  close_modal open_discard_modal waves-effect" href="javascript:void(0)">Cancel</a>
  <a class="btngen-center-align waves-effect" onclick="add_group(this)">Create</a>
</div>
<?php exit; ?>   