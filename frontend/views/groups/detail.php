<?php 
use yii\helpers\Url;
use yii\widgets\ActiveForm;  
use frontend\assets\AppAsset;
use frontend\models\GroupMember;
use backend\models\Googlekey;
$this->title = 'Group Detail';
$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$status = $session->get('status');
$request = Yii::$app->request;   
$group_id = (string)$request->get('group_id');
$members_count = GroupMember::getgroupmembercount($group_id);
$groupimage = $this->context->getgroupimage($group_id);
$is_group_member= GroupMember::isgroupmember($user_id, $group_id);

$directauthcall = '';
if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { 
	$directauthcall = $checkuserauthclass . ' directcheckuserauthclass';
}
$shownPermit = GroupMember::getgroupmemerstatus($group_id);
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>

<div class="page-wrapper menutransheader-wrapper menuhideicons-wrapper">
<div class="header-section">
	<?php include('../views/layouts/header.php'); ?>
</div>
<div class="floating-icon">
	<div class="scrollup-btnbox anim-side btnbox scrollup-float">
		<div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>		 	
	</div>	
</div>
<div class="clear"></div>
<div class="container page_container fulltab">		
<?php include('../views/layouts/leftmenu.php'); ?>
<div class="fixed-layout ipad-mfix">
	<div class="main-content with-lmenu general-page generaldetails-page commevents-page commevents-details-page main-page">
		<div class="combined-column">
			<div class="content-box">
				<div class="cbox-title nborder">	
					<i class="mdi mdi-account-group"></i>
					Groups
					<a href="?r=groups" class="backbtn"><i class="mdi mdi-menu-left"></i> Back to Groups</a>
				</div>
				<div class="cbox-desc">						
					<div class="view-holder grid-view">
						<div class="general-details">
								<div class="gdetails-summery" id="group-stab">
									<div class="main-info">
										<div class="imgholder">
											<img src="<?=$groupimage;?>"/>
											<div class="back-link"> <a href="?r=groups" class="waves-effect waves-theme"><i class="mdi mdi-arrow-left"></i></a> </div>
											<?php if($status == '10') { ?>
											    <div class="action-links item_detail_dropdown"> 
													<a href="javascript:void(0)" class="waves-effect waves-theme orglink share-it"> <i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i> </a>
	                      							<div class="settings-icon"> 
								                        <a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting"> <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i> </a>
								                        <ul id="detail_setting" class="dropdown-content custom_dropdown">
															<?php if($detail['is_deleted'] != '2'){ ?>	
																<li><a href="#reportpost-popup-<?php echo $detail['_id'];?>" data-reportpostid="<?php echo $detail['_id'];?>" class="customreportpopup-modal reportpost-link">Flag Group</a></li>
																<?php } ?>
																<?php if($detail['is_deleted'] == '2'){ ?>	
																<li id="publish_group"><a href="javascript:void(0)" onclick ="publish_group('<?=$detail['_id']?>')">Publish Group</a></li>
																<?php } ?>
														</ul>
													</div>
												</div>
											<?php } else { ?>
												<div class="action-links item_detail_dropdown">
													<?php if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { ?>
														<a href="javascript:void(0)" class="waves-effect waves-theme orglink share-it"> <i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i> </a>
														<div class="settings-icon"> 
								                        <a class="dropdown-button waves-effect waves-theme <?=$checkuserauthclass?> directcheckuserauthclass" href="javascript:void(0)" data-activates="detail_setting"> <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i> </a>
								                        </div>
													<?php } else { ?>	
														<?php if(($detail['user_id'] == $user_id) || $is_group_member) { ?>
								                        	<a href="javascript:void(0)" class="waves-effect waves-theme orglink share-it sharepostmodalAction" data-sharepostid="group_<?=$group_id;?>">
																<i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
															</a>
														<?php }?>

															<div class="settings-icon"> 
							                        		<a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting"> <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i> </a>

							                       			<ul id="detail_setting" class="dropdown-content custom_dropdown">
																<?php if($user_id == $detail['user_id']) { ?>
																	<li> <a href="javascript:void(0)" onClick="openEditGroupModal('<?=(string)$detail['_id']?>')">Edit group</a> </li>
																	<li> <a href="javascript:void(0)" onClick="manage_members()">Manage Members</a> </li> 
																	<li><a href="javascript:void(0)" id="directusepromote" onclick="promoteAdminModalOpen()">Promote to admin</a></li>
																	<li> <a href="javascript:void(0)" onClick="preferencesopenpopup('<?=(string)$detail['_id'];?>', 'Group');">Preferences</a> </li>
																	<?php if($members_count >1) {?>
																	<li><a href="javascript:void(0)" onclick="left_group('<?= $detail['_id'];?>')">Leave Group</a></li>
																	<?php } else { ?>
																	<li> <a href="javascript:void(0)" onclick="delete_group('<?= $detail['_id'];?>')">Delete group</a> </li>
																	<?php } ?>
																<?php } elseif($is_group_member) { ?>
																	<li><a href="#preferences-popup" class="popup-modal" onclick="preferencesopenpopup('<?=(string)$detail['_id'];?>', 'Group');">Preferences</a></li>
																	<li><a href="javascript:void(0)" onclick="left_group('<?= $detail['_id'];?>')">Leave group</a></li>	
																	<li><a href="#reportpost-popup" class="popup-modal" onclick="reportabuseopenpopup('<?=(string)$detail['_id'];?>', 'Group');">Report abuse</a></li>
																<?php } else { ?>
																	<li><a href="#reportpost-popup" class="popup-modal" onclick="reportabuseopenpopup('<?=(string)$detail['_id'];?>', 'Group');">Report abuse</a></li>
																<?php } ?>
															</ul>
														</div>
													<?php } ?>	 
												</div>
											<?php } ?>
										</div>
										<div class="content-holder gdetails-moreinfo expandable-holder expanded"> <a href="javascript:void(0)" class="expandable-link invertsign active" onClick="mng_expandable(this)"><i class="mdi mdi-chevron-down"></i></a>
											<div class="expandable-area">
												<h4><?= $detail['name'];?></h4>												
												<div class="member-info">
													<span><?= $members_count;?> Members</span>
													<span>+</span>
													<span><?= $detail['privacy'];?></span>
												</div>
												<div style="clear:both;"></div>
												<div class="tagline">												
													<p><?= $detail['tagline'];?></p>
												</div>
												<div class="action-btns">
													<a href="javascript:void(0)" class="noClick <?=$checkuserauthclass?>"  onclick="groupsJoins(event,'<?=$group_id;?>',this, false)"><?= GroupMember::getgroupmemerstatus($group_id);?></a>
												</div>
											</div>
										</div>
										<div class="content-holder">				
											<div class="hori-menus">
												<ul class="icon-list tabs tabs-vert">
													<li class="tab abouttab"><a href="#groups-about" class="" data-toggle="tab" aria-expanded="true" onClick="resetDetailTabs(this,'groups-details','groups-about')">About<i class="zmdi zmdi-check"></i></a></li>
													<li class="tab"><a href="#groups-discussion" data-toggle="tab" aria-expanded="true" onClick="resetDetailTabs(this,'groups-details','groups-discussion')" class="<?=$directauthcall?> active">Discussion<i class="zmdi zmdi-check"></i></a></li>

													<li class="tab eventtabli"><a href="#groups-events" data-toggle="tab" aria-expanded="false" onClick="resetDetailTabs(this,'groups-details','groups-events')" class="eventtab <?=$directauthcall?>">Events<i class="zmdi zmdi-check"></i></a></li>
													<li class="tab"><a href="#groups-photos" data-toggle="tab" aria-expanded="true" onClick="resetDetailTabs(this,'groups-details','groups-events')" class="phototab <?=$directauthcall?>">Photos<i class="zmdi zmdi-check"></i></a></li>
													
													<?php 
													if(GroupMember::getgroupmemerstatus($group_id) == 'Member' || GroupMember::getgroupmemerstatus($group_id) == 'Admin') { ?>
													<li class="tab invite"><a href="#groups-invite" data-toggle="tab" aria-expanded="true" onClick="resetDetailTabs(this,'groups-details','groups-invite')" class="<?=$directauthcall?>">Invite <span class="mobile_hide">People</span><i class="zmdi zmdi-check"></i></a></li>
													<?php } ?>
													<li class="tab member-link"><a class="membertab pl-0" href="#groups-members" data-toggle="tab" aria-expanded="false" onclick="resetDetailTabs(this,'groups-details','groups-members')"><?= $members_count;?> Member
													<?php if($members_count>1) { ?>	
													's
													<?php } ?></a></li>
													<li class="indicator" style="right: 0px; left: 0px;"></li>
												</ul>
												<div class="people">													
													<div class="plist">
														<?php foreach($group_member as $group_members){
														$group_user_id = $group_members['user_id'];	
														?>
														<span><a href="<?php echo Url::to(['userwall/index', 'id' => "$group_user_id"]); ?>"><img title="<?= $this->context->getuserdata($group_members['user_id'],'fullname');?>" src="<?= $this->context->getimage($group_members['user_id'],'thumb');?>"></a></span>
														<?php }?>
													</div>													
												</div>											
											</div>
										</div>
									</div>
									<div class="sideboxes">
										<?php include('../views/layouts/recently_joined.php'); ?>
										<div class="content-box bshadow">
											<div class="cbox-desc">
												<div class="side-travad brand-ad">
												  <div class="travad-maintitle">Best coffee in the world!</div>
												  <div class="imgholder"> <img src="<?=$baseUrl?>/images/brand-p.jpg"> </div>
												  <div class="descholder">
												    <div class="ad-subtitle">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
												    <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn">Explore</a> </div>
												</div>
											</div>
										</div>
										<div class="content-box bshadow">
											<div class="side-travad action-travad">
												<div class="travad-maintitle"><span class="iholder"><i class="mdi mdi-account-group"></i></span>
												  <h6>Heal Well</h6>
												  <span class="adtext">Sponsored</span></div>
												<div class="imgholder"> <img src="<?=$baseUrl?>/images/groupad-actionvideo.jpg"> </div>
												<div class="descholder">
												  <div class="ad-title">Medical Research Methodolgy</div>
												  <div class="ad-subtitle">Checkout the new video on our website exploring the latest techniques of medicine research</div>
												  <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn">Learn More</a> 
												</div>
											</div>
										</div>	
										<?php include('../views/layouts/travads.php'); ?>
									</div>
								</div>

								<div class="post-column">
									<div class="tab-content">
<div class="tab-pane fade main-pane active in" id="groups-discussion">
	<span class="mob-title"><i class="mdi mdi-newspaper"></i>Discussion</span>
	
	<?php 
	$lp = 1; 
	if(isset($user_id) && !empty($user_id)){
	if($shownPermit == 'Admin' || $shownPermit == 'Member') { 
	$checkposttime = GroupMember::find()->select(['created_date'])->where(['group_id' => (string)$detail['_id'], 'user_id' => $user_id])->asarray()->one();
	$joinedTime = $checkposttime['created_date'];
	?> 	<div class="row">
			<div class="col s12">
	            <div class="new-post base-newpost">
	              <form action="">
	                <div class="npost-content">
	                  <div class="post-mcontent"> <i class="mdi mdi-pencil-box-outline main-icon"></i>
	                    <div class="desc">
	                      <div class="input-field comments_box">
	                        <input placeholder="What's new?" class="validate commentmodalAction_form" type="text">
	                      </div>
	                    </div>
	                  </div>
	                </div>
	              </form>
	              <div class="overlay" id="composetoolboxAction"></div>
	            </div>
	        </div>
	    </div>
        <div class="post-list margint15">
        	<div class="row">	
				<?php 
				$templp = 1;
				$lp = 1;
				foreach($posts as $post)
				{ 
					$postTime = $post['post_created_date'];
					if($postTime < $joinedTime) {
						continue;
					}
					 
					$existing_posts = '1';
					$cls = '';
					if(count($posts)==$templp) {
					  $cls = 'lazyloadscroll'; 
					}

					$postid = (string)$post['_id'];
					$postownerid = (string)$post['post_user_id'];
					$postprivacy = $post['post_privacy'];

					$isOk = $this->context->filterDisplayLastPost($postid, $postownerid, $postprivacy);
					if($isOk == 'ok2389Ko') {
						if(($lp%8) == 0) {
							$ads = $this->context->getad(true); 
							if(isset($ads) && !empty($ads))
							{
								$ad_id = (string) $ads['_id'];	
								$this->context->display_last_post($ad_id, $existing_posts, '', $cls);
								$lp++;
							} else {
								$lp++;
							}
						} else {
							$this->context->display_last_post((string)$postid, $existing_posts, '', $cls, '', '', 'wid_all');
							$lp++;	
						}						
					}
					$templp++;
				}?>
				<div class="clear"></div>
			</div>
		</div>
	<?php 
	} } else { ?>
		<div class="post-list margint15">
        	<div class="row">	
				<?php 
				$templp = 1;
				$lp = 1;
				foreach($posts as $post)
				{ 
					
					$existing_posts = '1';
					$cls = '';
					if(count($posts)==$templp) {
					  $cls = 'lazyloadscroll'; 
					}

					$postid = (string)$post['_id'];
					$postownerid = (string)$post['post_user_id'];
					$postprivacy = $post['post_privacy'];

					$isOk = $this->context->filterDisplayLastPost($postid, $postownerid, $postprivacy);
					if($isOk == 'ok2389Ko') {
						if(($lp%8) == 0) {
							$ads = $this->context->getad(true); 
							if(isset($ads) && !empty($ads))
							{
								$ad_id = (string) $ads['_id'];	
								$this->context->display_last_post($ad_id, $existing_posts, '', $cls);
								$lp++;
							} else {
								$lp++;
							}
						} else {
							$this->context->display_last_post((string)$postid, $existing_posts, '', $cls);
							$lp++;	
						}						
					}
					$templp++;
				}?>
				<div class="clear"></div>
			</div>
		</div>
	<?php } 

	if($lp <= 1){
		$this->context->getwelcomebox("group");
	} ?>
	<div class="col s12">
		<center><div class="lds-css ng-scope dis-none"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
	</div>
</div>
										<div class="tab-pane fade main-pane" id="groups-about">
										</div>
										<div class="tab-pane fade main-pane" id="groups-members">
										</div>
										<div class="tab-pane fade main-pane" id="groups-events">
										</div>
										<div class="tab-pane fade main-pane" id="groups-photos">
										</div>
										<div class="tab-pane fade main-pane" id="groups-invite">
										</div>
									</div>										
								</div>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="chatblock">
		<div class="float-chat anim-side">
			<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
			</div>
		</div>
	</div>
	<?php 
	$userStatus = GroupMember::getgroupmemerstatus($group_id);
	if($is_group_member || $user_id == $detail['user_id']) { ?>
	<div class="new-post-mobile clear">
		<a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
	</div>
	<?php } ?>
</div>
</div>
<?php include('../views/layouts/footer.php'); ?>
</div>

<input type="hidden" name="groupownerid" id="groupownerid" value="<?=$detail['user_id']?>" />
<input type="hidden" name="pagename" id="pagename" value="group" />
<input type="hidden" name="tlid" id="tlid" value="<?=$group_id?>" />
<input type="hidden" name="baseurl" id="baseurl" value="<?=$baseUrl?>" />

<div id="add_gruop_modal" class="modal add-item-popup add-item-popup-group custom_md_modal cus_popu">
</div>
  
<div id="managemembers-popup" class="modal manage-modal managemembers-popup"> </div> 

<div id="compose_tool_box" class="modal compose_tool_box post-popup custom_modal main_modal">
</div>

<div id="composeeditpostmodal" class="modal compose_tool_box edit_post_modal post-popup main_modal custom_modal compose_edit_modal">
</div>

<div id="sharepostmodal" class="modal sharepost_modal post-popup main_modal custom_modal">
</div>

<div id="postopenmodal" class="modal modal_main compose_tool_box custom_modal postopenmodal_main postopenmodal_new">	
</div>

<div id="comment_modal_xs" class="modal comment_modal_xs">
</div> 
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal map_modalUniq modalxii_level1">
	<?php include('../views/layouts/mapmodal.php'); ?>
</div>
<?php include('../views/layouts/addpersonmodal.php'); ?>
<?php include('../views/layouts/editphotomadol.php'); ?>


<div id="add_event_modal" class="modal add-item-popup-event add-item-popup custom_md_modal"></div>

<div id="manageadmin-popup" class="modal manage-modal manageadmin-popup">
	<div class="modal_header">
		<button class="close_btn custom_modal_close_btn close_modal waves-effect">
		  <i class="mdi mdi-close mdi-20px	"></i>
		</button>
		<h3>Manage Admin</h3>
	</div>
	<div class="custom_modal_content modal_content">
		<div class="main-pcontent spadding">
			<ul class="tabs">
				<li onclick="allmemberforadmin();" class="tab active"><a href="#member-all" data-toggle="tab" aria-expanded="false">Promote to Admin</a></li>
				<li onclick="memberrequest();" class="tab"><a href="#member-request" data-toggle="tab" aria-expanded="true">Request</a></li>
			</ul>
			<div class="tab-content">
				<div id="member-all" class="tab-pane active in" data-id="froup-stab">
					<ul class="manage-members">
						<center>
						<div class="preloader-wrapper small active">
						    <div class="spinner-layer spinner-blue-only">
						      <div class="circle-clipper left">
						        <div class="circle"></div>
						      </div><div class="gap-patch">
						        <div class="circle"></div>
						      </div><div class="circle-clipper right">
						        <div class="circle"></div>
						      </div>
						    </div>
						  </div>
						</center>
					</ul>
				</div>
				<div id="member-request" class="tab-pane">
					<ul class="manage-members">
						<center>
							<div class="preloader-wrapper small active">
							    <div class="spinner-layer spinner-blue-only">
							      <div class="circle-clipper left">
							        <div class="circle"></div>
							      </div><div class="gap-patch">
							        <div class="circle"></div>
							      </div><div class="circle-clipper right">
							        <div class="circle"></div>
							      </div>
							    </div>
							  </div>
						</center>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('../views/layouts/preferences.php'); ?>

<div id="add-photo-popup" class="modal addphoto_modal custom_md_modal">
	<div class="modal_header">
		<button class="close_btn custom_modal_close_btn close_modal">
		  <i class="mdi mdi-close mdi-20px	"></i>
		</button>
		<h3>Add photos</h3>
	</div>
	<div class="custom_modal_content modal_content">
		<div class="content-holder nsidepad nbpad">
			<?php $form = ActiveForm::begin(['options' => ['method' => 'post','enctype'=>'multipart/form-data','id' =>'albumdata','class' =>'add-album-form']]) ?>						
				<div class="post-photos">
					<div class="img-row nice-scroll">
					</div>
				</div>
				<div class="frow stretch-side">
					<div class="btn-holder">
						<input type="hidden" class="imgfile-count" value="0" />
						<input type="hidden" class="group_id" value="<?= $group_id;?>" />
						<a href="javascript:void(0)" onclick="addGroupPhoto()" class="btn btn-primary btn-sm close-popup" data-class="addbtn">Add</a>
					</div>				
				</div>				
			<?php ActiveForm::end() ?>
		</div>
	</div>
</div>

<?php include('../views/layouts/custom_modal.php'); ?>

<div id="upload-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="edit-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="userwall_tagged_users" class="modal modalxii_level1">
	<div class="content_header">
		<button class="close_span waves-effect">
			<i class="mdi mdi-close mdi-20px"></i>
		</button>
		<p class="selected_photo_text"></p>
		<a href="javascript:void(0)" class="chk_person_done_new done_btn focoutTRV03 action_btn">Done</a>
	</div>
	<nav class="search_for_tag">
		<div class="nav-wrapper">
		  <form>
		    <div class="input-field">
		      <input id="tagged_users_search_box" class="search_box" type="search" required="">
		        <label class="label-icon" for="tagged_users_search_box">
		          <i class="zmdi zmdi-search mdi-22px"></i>
		        </label>
		      </div>
		  </form>
		</div>
	</nav>
	<div class="person_box"></div>
</div>

<?php
$par = ''; 
if(isset($_GET['par']) && !empty($_GET['par']) && $_GET['par']) {
	$par = $_GET['par'];
}
?>
<!-- Style for datepicker and address-->
<style>
.ui-datepicker{z-index:9999 !important;}
.pac-container{z-index: 1051 !important;}
</style>	

<script>
var baseUrl ='<?php echo (string) $baseUrl; ?>';
var group_id ='<?php echo $group_id;?>';
$(document).ready(function(){
	var groupName = '<?= ucfirst($detail['name']);?>';
	$('.page-name.mainpage-name').html(groupName);
});
</script>	
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>

<script src="<?=$baseUrl?>/js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/group.js"></script>
<script src="<?=$baseUrl?>/js/post.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		justifiedGalleryinitialize();
		lightGalleryinitialize();
	}); 
</script>