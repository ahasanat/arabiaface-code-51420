<?php 
use frontend\models\EventVisitors;
use frontend\models\GroupMember;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$shownPermit = GroupMember::getgroupmemerstatus((string)$groupdetail['_id']);
$isEmpty = true;
?>
<span class="mob-title"><i class="mdi mdi-calendar"></i>Events</span>
<div class="content-box bshadow inner-content-box">

	<div class="cbox-title">
		<h5>
			<i class="mdi mdi-calendar"></i>
			Events
		</h5>
	</div>
	<div id="group-events" class="cbox-desc" data-id="event-steps">
		<div class="commevents-list generalbox-list">
			<div class="row">
				<?php if($shownPermit == 'Admin' || $shownPermit == 'Member') { ?>
				<div class="col s4 add-cbox">
				 	<div class="commevents-box general-box" style="padding:0px;"> 
						<a href="javascript:void(0)" class="add-commevents add-general" onclick="openAddItemModalevent()">
							<span class="icont">+</span>
							Create Community Event
						</a>
					</div>
				</div>
				<?php } ?>
				<?php foreach($groupevent as $allevent){
					$event_id = (string)$allevent['_id'];
					$event_count = EventVisitors::getEventCounts($event_id);
					$date = $allevent['event_date'];
					$event_privacy = $allevent['event_privacy'];
					$group_id = $groupdetail['_id'];
					$event_type = $allevent['type'];
					$dp = $this->context->geteventimage($event_id);
					$is_member = GroupMember::find()->where(['user_id' => "$user_id",'group_id' => "$group_id",'status' => '1'])->one();
					if(($event_privacy == 'Public') || ($event_privacy == 'Friends' && ($is_member || $user_id == $groupdetail['user_id'])))
					{

					$isEmpty = false;	
				?>
				<div class="col s4 eventbox_<?=$event_id?>">
					<a href="javascript:void(0);" onclick="joinEvent(event,'<?=$event_id;?>',this, true)" class="commevents-box general-box">
						<div class="photo-holder waves-effect waves-block waves-light">
							<img src="<?=$dp?>">
						</div>
						<div class="content-holder">
							<h4><?= $allevent['event_name'];?></h4>
							<div class="username fcount_<?= $event_id;?>">
								<?=$event_count?> Attending
							</div>	
							<div class="userinfo">
								<span class="month"><?=date("M", strtotime($date))?></span>
								<span class="date"><?=date("d", strtotime($date))?></span>
							</div> 
							<div class="action-btns">                        
								<span class="noClick" onclick="joinEvent(event,'<?=$event_id;?>',this, false)"><?= EventVisitors::getEventGoing($event_id);?></span>
							</div>
						</div>
					</a>									
				</div>
				<?php } 
				}
				if($isEmpty == true && ($shownPermit != 'Admin' && $shownPermit != 'Member')) { ?>
				<?php $this->context->getnolistfound('nophotofound'); ?>
				<?php } ?>	
			</div>
		</div>
	
		
	</div>
</div>
<?php exit;?>