<?php   
use frontend\assets\AppAsset;
use backend\models\Googlekey; 
$baseUrl = AppAsset::register($this)->baseUrl;
$this->title = 'Group';
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
<link href="<?=$baseUrl?>/css/animate.css" rel="stylesheet">
<div class="page-wrapper gen-pages">
	<div class="header-section">
		<?php include('../views/layouts/header.php'); ?>
	</div>
	<div class="floating-icon">
		<div class="scrollup-btnbox anim-side btnbox scrollup-float">
			<div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>			
		</div>
	</div>
	<div class="clear"></div> 
	<div class="container page_container group_container">
	<?php include('../views/layouts/leftmenu.php'); ?>
	<div class="fixed-layout ipad-mfix">
		<div class="main-content with-lmenu groups-page main-page grid-view general-page">
			<div class="combined-column">
				<div class="content-box nbg">
					<div class="cbox-desc md_card_tab">
						<div class="fake-title-area divided-nav mobile-header">
							<ul class="tabs">								
								<li class="tab allgroup"><a class="active" href="#groups-suggested">Suggested</a></li>
								<li class="tab joingroup"><a href="#groups-member">Member</a></li>			
								<li class="tab yoursgroup"><a href="#groups-yours">Yours</a></li>			
							</ul>
						</div>
						<div class="tab-content view-holder grid-view">				
							<div class="tab-pane main-pane fade active" id="groups-suggested">
								<div class="groups-list generalbox-list">
									<div class="row"> 
										<div class="clear loaderblock"></div>
										<center>
											<div class="lds-css ng-scope"> 
												<div class="lds-rolling lds-rolling100"><div></div></div>
											</div>
										</center>
									</div>
		    					</div>
							</div>
							<div class="tab-pane fade main-pane" id="groups-member">							
								<div class="groups-list generalbox-list animated fadeInUp" id="group_lists">
									<div class="row"> </div>
		    					</div>
							</div>
							<div class="tab-pane fade main-pane groups-yours" id="groups-yours">	
								<div class="groups-list generalbox-list animated fadeInUp">
									<div class="row"> </div>
								</div>
							</div>														
						</div>
					</div>
				</div>
			</div>
			<div id="chatblock">
				<div class="float-chat anim-side">
					<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
					</div>
				</div>
			</div>
		</div>
	</div>				
	</div>
	<?php include('../views/layouts/footer.php'); ?>
</div>

<div id="compose_mapmodal" class="modal map_modal compose_inner_modal map_modalUniq modalxii_level1">
	<?php include('../views/layouts/mapmodal.php'); ?>
</div>


<div id="add_event_modal" class="modal add-item-popup custom_md_modal cus_popu">
</div>

<?php include('../views/layouts/custom_modal.php'); ?>

<div id="upload-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="edit-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="userwall_tagged_users" class="modal modalxii_level1">
	<div class="content_header">
		<button class="close_span waves-effect">
			<i class="mdi mdi-close mdi-20px"></i>
		</button>
		<p class="selected_photo_text"></p>
		<a href="javascript:void(0)" class="chk_person_done_new done_btn focoutTRV03 action_btn">Done</a>
	</div>
	<nav class="search_for_tag">
		<div class="nav-wrapper">
		  <form>
		    <div class="input-field">
		      <input id="tagged_users_search_box" class="search_box" type="search" required="">
		        <label class="label-icon" for="tagged_users_search_box">
		          <i class="zmdi zmdi-search mdi-22px"></i>
		        </label>
		      </div>
		  </form>
		</div>
	</nav>
	<div class="person_box"></div>
</div>

<style>
  .pac-container{z-index: 1051 !important;}
</style>	
<script> 
var baseUrl ='<?php echo (string) $baseUrl; ?>';
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>
<script src="<?=$baseUrl?>/js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/group.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		justifiedGalleryinitialize();
		lightGalleryinitialize();
	}); 
</script>
