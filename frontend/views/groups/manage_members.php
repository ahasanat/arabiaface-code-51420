<?php 
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl; 

?>
<div class="modal_header">
	<button class="close_btn custom_modal_close_btn close_modal waves-effect">
	  <i class="mdi mdi-close mdi-20px	"></i>
	</button>
	<h3>Manage Members</h3>
</div> 
<div class="custom_modal_content modal_content">			
	<div class="main-pcontent spadding">
		<ul class="tabs">
			<li class="tab"><a href="#member-request" data-toggle="tab" aria-expanded="false" class="">Member Requested</a></li>
			<li class="tab"><a href="#member-all" class="active" data-toggle="tab" aria-expanded="true">All Members</a></li>
			<li class="indicator" style="right: 1px; left: 346px;"></li>
		</ul>
		<div class="tab-content">
			<div id="member-request" class="tab-pane fade dis-none">
				<ul class="manage-members">
					<?php if(empty($member_request)){ ?>
						<?php $this->context->getnolistfound('noorganizerrequest'); ?>	
					<?php }?>
					<?php foreach($member_request as $member_requests){ ?>
					<li>
						<div class="member-li">
							<div class="imgholder"><img src="<?= $this->context->getimage($member_requests['user_id'],'thumb');?>"/></div>
							<div class="descholder">
								<span class="head6"><?= $this->context->getuserdata($member_requests['user_id'],'fullname');?></span>
								<div class="settings accept_<?= $member_requests['_id'];?>">
									<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="request_responce('1','<?= $member_requests['_id'];?>')" >Accept</a>
									<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="request_responce('0','<?= $member_requests['_id'];?>')">Reject</a>
								</div>
							</div>
						</div>
					</li>
					<?php }?>
				</ul>
			</div>
			<div id="member-all" class="tab-pane fade active dis-none">
				<ul class="manage-members">
				<li>
					<div class="member-li">
						<div class="imgholder"><img src="<?= $this->context->getimage($group_admin,'thumb');?>"/></div>
						<div class="descholder">
							<span class="head6"><?= $this->context->getuserdata($group_admin,'fullname');?></span>
							<div class="settings">
								<span class="status">Admin</span>
								<div class="right">
										<a class="dropdown-button " href="javascript:void(0)" data-activates="mm_settings1">
										<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</li>
				<?php foreach($group_member as $group_members)
				{
					$group_user_id = $group_members['user_id'];
					$group_member_id = $group_members['_id'];
					$time = time().(string)$group_member_id;
				?>
				<li class="member_<?= $group_member_id;?>">
					<div class="member-li">
						<div class="imgholder"><img src="<?= $this->context->getimage($group_user_id,'thumb');?>"/></div>
						<div class="descholder">
							<span class="head6"><?= $this->context->getuserdata($group_user_id,'fullname');?></span>
							<div class="settings">
								<span class="status">Member</span>
								<a class="dropdown-button " href="javascript:void(0)" data-activates="<?=$time?>">
									<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
								</a>
							  	<ul id="<?=$time?>">
									<li><a href="javascript:void(0)" onclick="remove_member('<?= $group_member_id;?>')">Remove member</a></li>									
							 	 </ul>
							</div>
						</div>
					</div>
				</li>
				<?php }?>
			</ul>
		</div>
	</div>
</div>
</div>
<?php exit;?>