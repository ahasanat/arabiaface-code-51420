<?php 
use yii\helpers\Url;
?>
<span class="mob-title"><i class="mdi mdi-account-group"></i>Members of this group</span>
<div class="content-box bshadow inner-content-box">
	<div class="cbox-title">
		<h5>
			<i class="mdi mdi-account-group"></i>
			Members of this group
		</h5>
	</div>
	<div class="cbox-desc">
		<div class="likes-summery" data-id="stable">															
			<div class="invite-likes">																
				<div class="invite-holder">
					<div class="list-holder nice-scroll" style="overflow-y: hidden;" tabindex="4">
						<?php if(empty($group_member)){
								echo "Be the first member";	
							}?>
						<ul>
						<?php foreach($group_member as $group_members)
						{
							$group_user_id = $group_members['user_id'];
						?>
							<li>
								<a class="invitelike-friend" href="<?php echo Url::to(['userwall/index', 'id' => "$group_user_id"]); ?>">
									<span class="imgholder"><img src="<?= $this->context->getimage($group_user_id,'thumb');?>"/></span>
									<span class="descholder">
										<h6><?= $this->context->getuserdata($group_user_id,'fullname');?></h6>
									</span>
								</a>
							</li>
						<?php } ?>	
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php exit;?>