<?php 
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$isEmpty = true;	
if(!empty($data)) {
	$group_user_id = $data['user_id'];
	if($user_id == $group_user_id) {
		continue;
	}
	$group_member_id = (string)$data['_id'];
	$isEmpty = false;
	?>
	<li class="member_<?= $group_member_id;?>">
		<div class="member-li">
			<div class="imgholder"><img src="<?= $this->context->getimage($group_user_id,'thumb');?>"/></div>
			<div class="descholder">
				<span class="head6"><?= $this->context->getuserdata($group_user_id, 'fullname');?></span>
				<div class="settings">
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="setadmin('<?=$group_user_id?>', '<?=$group_id?>', this, true)" >Cancel Admin Request</a>
				</div>
			</div>
		</div>
	</li>
<?php }
 
if($isEmpty) { 
	$this->context->getnolistfound('noorganizerrequestforgroup');
} exit; ?>