<?php
use frontend\models\PinImage;
use frontend\models\GroupMember;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$shownPermit = GroupMember::getgroupmemerstatus((string)$groupdetail['_id']);
$isEmpty = true;
?>
<span class="mob-title"><i class="mdi mdi-file-image"></i>Photos</span>
<div class="content-box bshadow inner-content-box">
	<div class="cbox-title">
		<h5><i class="mdi mdi-file-image"></i>Photos</h5>
	</div>
	<div class="cbox-desc">
		<div class="albums-grid images-container">
			<div class="row">
				<?php if($shownPermit == 'Admin' || $shownPermit == 'Member') { ?>
				<div class="grid-box">
					<div class="divrel">
						<a href="javascript:void(0)" id="add-photo-photos" class="add-photo">
							<span class="icont">+</span>
							Add New Photo
						</a>
						<input type="file" name="upload" class="hidden_uploader custom-upload-new" title="Choose a file to upload" required data-class="#add-photo-popup .post-photos .img-row" multiple/>
					</div>
				</div>
				<?php } ?>
				<?php foreach($photo as $photos)
				{
					$time = time();
					if(file_exists('../web'.$photos['image'])){
						$imgclass = '';
						$val = getimagesize('../web'.$photos['image']);
						$iname = $this->context->getimagename($photos['image']);
						$inameclass = $this->context->getimagefilename($photos['image']);
						if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}
						$pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
						if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
						$isEmpty = false;
					?>
						<div class="grid-box">
							<div class="photo-box">
								<div class="imgholder">
									<figure>
										<a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $photos['image'];?>" data-imgid="<?=$inameclass?>" data-size="1600x1600" data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $photos['image'];?>" data-med-size="1024x1024" data-author="Folkert Gorter" data-pinit="<?=$pinval?>" class="<?= $imgclass;?>-box imgpin">
										  <img class="<?= $imgclass;?>" src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $photos['image'];?>"/>
										</a>																		
									</figure>
								</div>
								<?php if($shownPermit == 'Admin' || ($shownPermit == 'Member' && $photos['user_id'] == $user_id)) { ?>
									<div class="edit-link">
										<div class="dropdown dropdown-custom ">
											<a class="dropdown-button " href="javascript:void(0)" data-activates="<?=$time?>album_setting1">
												<i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
											</a>
											<ul id="<?=$time?>album_setting1" class="dropdown-small dropdown-content custom_dropdown">
												<li><a href="javascript:void(0)" onclick="delete_photo('<?= $photos['_id'];?>')">Delete this photo</a></li>
											</ul>
										</div>
									</div>
								<?php }?>	
								<div class="descholder">
									<a href="javascript:void(0)" class="namelink"><span><?= $photos['name'];?></span></a>
								</div>
							</div>
						</div>
					<?php } 
				}
				if($isEmpty == true && ($shownPermit != 'Admin' && $shownPermit != 'Member')) { ?>
				<?php $this->context->getnolistfound('nophotofound'); ?>	
				<?php } ?>	
			</div>
		</div>
	</div>
</div>
<?php exit;?>
