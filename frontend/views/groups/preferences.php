<?php
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl; 
$sort = 'Recent';
$feedresult = 'checked';
$notificationresult = 'checked';

if(isset($data) && !empty($data)) { 
	$sort = $data['sort'];
	if($sort == 'popular') {
		$sort = 'Popular';
	}

	$feed = $data['feed'];
	if($feed == 'off') {
		$feedresult = '';
	}

	$notification = $data['notification'];
	if($notification == 'off') {
		$notificationresult = '';
	}
}
?>
	<div class="modal_header">
		<button class="close_btn custom_modal_close_btn close_modal waves-effect">
		  <i class="mdi mdi-close mdi-20px"></i>
		</button>
		<h3>Preferences</h3>
	</div>
	<div class="custom_modal_content modal_content">
		<div class="preference_label_container">
			<h5>Post in main stream</h5>
			<p class="preference_switch_label">Allow posts in the main status feed</p>
			<div class="right right_object">
				<div class="switch">
					<label>
						<input id="pref_feed_switch" onclick="savepreference('<?=$postid?>', '<?=$action?>');" class="cmn-toggle cmn-toggle-round" <?=$feedresult?> type="checkbox">						
						<span class="lever" for="pref_feed_switch"></span>
					</label>
				</div>
			</div>
		</div>
		<div class="preference_label_container">
			<h5>Order of posts</h5>
			<p class="preference_switch_label">Sort by recent or popular posts</p>
			<div class="right right_object">
				<div class="preference_dropdown dropdown_right">
				  <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="preferencesdropdown">
						<span><?=$sort?></span>
						<i class="zmdi zmdi-caret-down"></i>
					</a>
					<ul id="preferencesdropdown" class="dropdown-content custom_dropdown">
						<li onclick="savepreference('<?=$postid?>', '<?=$action?>', 'Popular');"><a href="javascript:void(0)">Popular</a></li>
						<li onclick="savepreference('<?=$postid?>', '<?=$action?>', 'Recent');"><a href="javascript:void(0)">Recent</a></li>
					</ul>
				</div>
			  </div>
		</div>
		<div class="preference_label_container">
			<h5>Notifications</h5>
			<p class="preference_switch_label">Get notified about new posts</p>
			<div class="right right_object">
				<div class="switch">
					<label>
						<input id="pref_newpost_switch" onclick="savepreference('<?=$postid?>', '<?=$action?>');" class="cmn-toggle cmn-toggle-round" <?=$notificationresult?> type="checkbox">
						<span class="lever" for="pref_newpost_switch"></label>
					</label>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
/* preferences save */
	function savepreference($id, $action, $label='') {
		var $feed = 'off';
		if($("#pref_feed_switch").prop('checked') == true){
			$feed = 'on';
		}

		$sort = 'Recent';
		if($label != '') {
			if($label == 'Popular' || $label == 'Recent') {
				$sort = $label;
			}
		} else {
			$sort = $('.getvalue').find('span').text();
		}
		
		var $notification = 'off';
		if($("#pref_newpost_switch").prop('checked') == true){
			$notification = 'on';
		}

		$.ajax({ 
			type: 'POST',
			url: '?r=groups/preferences-save', 
			data: {
				'$feed' : $feed,
				'$sort' : $sort,
				'$notification' : $notification,
				'$id' : $id,
				'$action' : $action
			},
			success: function (data)
			{
				if(data == true) {
					Materialize.toast('Successfully updated.', 2000, 'green');
				} else {
					Materialize.toast('Oops, something is wrong please try after sometimes.', 2000, 'red');
				}
			}
		});

	}
/* end preferences save */
</script>
<?php exit;?>
