<?php 
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl;
?>
<div class="hidden_header">
	<div class="content_header">
		<button class="close_span cancel_poup waves-effect">
			<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
		</button>
		<p class="modal_header_xs">Report abuse</p>
		<a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect" onclick="verify()">Report</a>
	</div> 
</div>
<div class="modal-content">	
	<div class="new-post active">
		<?php if(isset($data) && !empty($data)) { 
			$fullname = $data['fullname'];
			$thumb = $data['thumb'];
			?>
			<form id="report_abuse_form" enctype="multipart/form-data">
				<div class="top-stuff">
					<div class="postuser-info">
						<div class="img-holder">
							<img src="<?=$thumb?>">
						</div>
						<div class="desc-holder">
							<span class="profile_name"><a href="javascript:void(0)"><?=$fullname?></a></span>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="scroll_div">
					<div class="npost-content">
						<div class="post-mcontent">                                                     
							<div class="desc">
								<textarea id="desc"  placeholder="Reason for reporting" class="materialize-textarea comment_textarea new_post_comment"></textarea>
							</div>
						</div>
					</div>
				</div>
			</form>
		<?php } else { ?>
		<?php $this->context->getnolistfound('somethingwentwrong'); ?>	
		<?php } ?>
	</div>
</div>
<div class="modal-footer">
	<div class="new-post active">
		<div class="post-bcontent">
			<div class="post-bholder">
				<div class="post-loader dis-none"><img src="<?=$baseUrl?>/images/home-loader.gif"/></div>
				<div class="hidden_xs">
					<a href="javascript:void(0)" class="btngen-center-align  close_modal open_discard_modal waves-effect">Cancel</a>					
					<a href="javascript:void(0)" class="btngen-center-align  waves-effect close_modal" onclick="reportabusestored('<?=$id?>', '<?=$action?>');" type="button" name="post">Report <?=$action?></a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php exit;?>
