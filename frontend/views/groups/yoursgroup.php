<?php 
use frontend\models\GroupMember;

if(isset($lazyhelpcount) && $lazyhelpcount == 0) {
?>
<div class="col s6 m4 l3 add-cbox">
	<div class="card hoverable groupCard">
		<div class="general-box">
			<?php if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') { ?>
				<a href="javascript:void(0)" class="add-group add-general <?=$checkuserauthclass?> directcheckuserauthclass">
					<span class="icont">+</span>
					Add new group
				</a>
			<?php } else { ?>
				<a href="javascript:void(0)" class="add-commevents add-general" onclick="openCreateGroupModal()">
					<span class="icont">+</span>
					Add New Group
				</a>
			<?php } ?>
		</div> 
	</div>	
</div>	
<?php
}

foreach($yoursgroup as $yoursgroups) {
	$group_id = (string)$yoursgroups['_id'];
	$members_count = GroupMember::getgroupmembercount($group_id);
	$profile = $this->context->getgroupimage($group_id);

	$isDefaultProfile = $imgclass = '';
	if (strpos($profile, 'additem-groups.png') !== false) {
	    $isDefaultProfile ='defaultprofile';
	} else {
		$val = getimagesize($profile);
        if($val[0] > $val[1]) { 
            $imgclass = 'himg';
        } else if($val[1] > $val[0]) { 
            $imgclass = 'vimg';
        } else {
            $imgclass = 'himg';
        }
    }
	?>
	<div class="col s6 m4 l3 gridBox127 uniqidentitybox uniqidentitybox groupbox_<?=$group_id?>">
		<div class="card hoverable groupCard animated fadeInUp">
			<a href="javascript:void(0);" onclick="groupsJoins(event,'<?=$group_id;?>',this, true)" class="groups-box general-box">
				<div class="photo-holder waves-effect waves-block waves-light <?=$imgclass?>-box">  
					<img class="<?=$imgclass?> <?=$isDefaultProfile?>" src="<?=$profile?>">
				</div>
				<div class="content-holder">
					<h4><?= $yoursgroups['name'];?></h4>													
					<div class="members member_count_<?=$group_id;?>">
						<?=$members_count;?> Members
					</div>								
					<div class="action-btns">						
						<span>Admin</span>
					</div>
				</div>
			</a>						
		</div>
	</div>
<?php }
exit;