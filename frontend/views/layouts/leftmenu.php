<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\UserForm;

$baseUrl = AppAsset::register($this)->baseUrl;

$session = Yii::$app->session;
$email = $session->get('email'); 
$uid = (string)$session->get('user_id');

$Auth = '';
if(isset($uid) && $uid != '') 
{
	$authstatus = UserForm::isUserExistByUid($uid);
	if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
	{
		$Auth = $authstatus;
	}
}	
else	
{
	$Auth = 'checkuserauthclassg';
}
	

$result = UserForm::find()->where(['email' => $email])->one();
?>
 <div class="sidemenu-holder <?php if(isset($_GET['r']) && !empty($_GET['r']) && ($_GET['r']=='trip' || $_GET['r']=='site/credits' || $_GET['r']=='site/transfercredits' || $_GET['r']=='site/creditshistory' || $_GET['r']=='site/verifyme' || $_GET['r']=='site/addvip' || $_GET['r']=='site/billing' || $_GET['r']=='site/accountsettings' || $_GET['r']=='ads' || $_GET['r']=='ads/manage' || $_GET['r']=='ads/create' || $_GET['r']=='tours' || $_GET['r']=='userwall/index' || $_GET['r']=='page/index' || $_GET['r']=='site/messages')){ ?>m-hide<?php } ?>">
		<div class="sidemenu nice-scroll">
			<a href="javascript:void(0)" class="closemenu">
				<i class="mdi mdi-close"></i>
			</a>  
			<div class="side-user-cover">
				<img src="<?=$baseUrl?>/images/wgallery3.jpg">
			</div>
			<div class="side-user">
				<?php
				if($Auth == 'checkuserauthclassg') {
				$user_img = $baseUrl.'/images/guest_thumb.png';
				?>
				<span class="img-holder">
					<img class="circle" src="<?= $user_img?>">
				</span>
				<a href="javascript:void(0)"><span class="desc-holder">Hello Guest</span></a>
				<?php } else { 
				$user_img = $this->context->getimage($result['_id'],'thumb');
				?>
				<span class="img-holder">
					<img src="<?= $user_img?>">
				</span>
				<a href="<?php $uid = $result['_id']; echo Url::to(['userwall/index', 'id' => "$uid"]); ?>"><span class="desc-holder"><?= ucfirst($result['fullname']);?></span></a>

				<?php } ?>
			</div>
			<div class="sidemenu-ul">
				<ul>
					<li class="lm-home <?php if(Yii::$app->controller->action->id =='mainfeed'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['site/mainfeed']); ?>">Home</a></li>
					<li class="lm-home <?php if(Yii::$app->controller->action->id =='messages'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['site/messages']); ?>">Messenger</a></li>
					<li class="lm-waround <?php if(Yii::$app->controller->id =='whoisaround'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['whoisaround/index']); ?>">People</a></li>
					<li class="lm-channels <?php if(Yii::$app->controller->id =='channel'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['channel']); ?>">Channels</a></li>
					<li class="lm-collections <?php if(Yii::$app->controller->id =='collection'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['collection']); ?>">Collections</a></li>
					<li class="lm-snapit <?php if(Yii::$app->controller->id =='snapit'){echo'active';}?> "><a href="<?php echo Yii::$app->urlManager->createUrl(['snapit']); ?>">Snapit</a></li>
					<li class="lm-texp <?php if(Yii::$app->controller->id =='moments'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['moments']); ?>">Moments</a></li>
					<li class="lm-commeve <?php if(Yii::$app->controller->id =='event'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['event']); ?>">Events</a></li>
					<li class="lm-groups <?php if(Yii::$app->controller->id =='groups'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['groups']); ?>">Groups</a></li>
					<li class="lm-pages <?php if(Yii::$app->controller->action->id =='travpage'){echo'active';}?>"><a href="<?php echo Yii::$app->urlManager->createUrl(['site/travpage']); ?>">Pages</a></li>
				</ul>
			</div>
		</div>
	   <div class="mobile-menu">
			<a href="javascript:void(0)"><i class="mdi mdi-menu"></i></a>				
		</div>
</div>