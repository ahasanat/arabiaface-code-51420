<?php

use frontend\models\Friend;
use frontend\models\SecuritySetting;
use frontend\assets\AppAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

if(isset($_GET['r']) && $_GET['r'] == 'userwall/saved-content')
{
    $baseUrl = $_POST['baseUrl'];
}
else
{
    $baseUrl = AppAsset::register($this)->baseUrl;
}

$session = Yii::$app->session;
$uid = $session->get('user_id');
$model_friend = new Friend();
$init_friends = $model_friend->userlistFirstfive($uid);
?>

<div class="content-box bshadow peopleyoumayknow">
    <div class="cbox-title">
        People you may know
    </div>
    <div class="cbox-desc">
        <ul class="people-list">
            <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
            <?php  
                $counter = 0;
                foreach($init_friends as $friend){ 
				$friend_id = (string) $friend['_id'];
                $requestexists = $model_friend->requestexists($friend['id']);
                $alreadysend = $model_friend->requestalreadysend($friend['id']);
                {
                    $counter++;
                    $ctr = $model_friend->mutualfriendcount($friend['id']);
                    if(isset($_GET['r']) && $_GET['r'] == 'userwall/saved-content')
                    {
                        $dpimg = $this->getimage($friend['_id'],'photo');
                    }
                    else
                    {
                        $dpimg = $this->context->getimage($friend['_id'],'photo');
                    }
                    $form = ActiveForm::begin(
                    [
                        'id' => 'add_friend',
                        'options'=>[
                        'onsubmit'=>'return false;',
                        ],
                    ]
                );
				$result_security = SecuritySetting::find()->where(['user_id' => $friend_id])->one();
												
				if($result_security)
				{
					$lookup_settings = $result_security['my_view_status'];
				}
				else
				{
					$lookup_settings = 'Public';
				}
				$is_friend = Friend::find()->where(['from_id' => $friend_id,'to_id' => (string) $user_id,'status' => '1'])->one();
				if(($lookup_settings == 'Public') || ($lookup_settings == 'Friends' && $is_friend)) 
				{
            ?>
            <li id='remove_<?php echo $friend['id'];?>'>
                <div class="people-box">
                    <div class="img-holder"><img src="<?= $dpimg?>"/></div>
                    <input type="hidden" name="to_id" id="to_id" value="<?php echo $friend['id'];?>">
                    <div class="desc-holder">
                        <a href="<?php $id = $friend['id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>" class="userlink"><?php echo $friend['fname']." ".$friend['lname'];?></a>
                        <span class="info"><?php if($ctr > 0){?><?php echo $ctr;?> Mutual Friends<?php }else{echo "No Mutual Friend";} ?></span>								
                        <?php 
                        $result_security = SecuritySetting::find()->where(['user_id' => "$id"])->one();
                        if ($result_security)
                        {
                            $request_setting = $result_security['friend_request'];
                        }
                        else
                        {
                            $request_setting = 'Public';
                        }
                        if(($request_setting == 'Public') || ($request_setting == 'Friends of Friends' && $ctr > 0)){ ?>
                        <a href="javascript:void(0)" class="btn btn-default title_<?php echo $friend['id'];?>" title="Add friend">
                            <i class="mdi mdi-account-plus people_<?php echo $friend['id'];?>" onclick="addFriend('<?=$friend['id']?>')"></i>
                            <i class="mdi mdi-account-minus dis-none sendmsg_<?php echo $friend['id'];?>" onclick="removeFriend('<?=(string)$friend['_id']?>','<?=$uid?>','<?=(string)$friend['_id']?>', 'cancle_friend_request')"></i>  
                        </a>
                        <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                        <?php } else { ?>
                            <a href="javascript:void(0)" class="btn btn-default">
                            <img onclick="privateMessage()" src="<?=$baseUrl?>/images/private-friend.png"/>
                            </a>
                        <?php } ?>
                        <a href="javascript:void(0)" onclick="removeFriendFromListing('<?php echo $friend['id'];?>')" class="close-btn"><i class="mdi mdi-close	"></i></a>
                    </div>
                </div>
            </li>
            <?php ActiveForm::end() ?>
				<?php } } } ?>
            <li class="viewall">
                <div class="right"><a href="<?php echo Url::to(['site/travpeople']); ?>">View All</a></div>
            </li>                       
        </ul>
    </div>
</div>
<script type="text/javascript" src="<?=$baseUrl?>/js/friend.js"></script>
