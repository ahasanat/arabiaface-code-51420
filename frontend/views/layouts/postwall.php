<?php 
use frontend\models\Like;
use frontend\models\LoginForm;
use frontend\models\UserForm;
use frontend\models\UnfollowFriend;
use frontend\models\HidePost;
use frontend\models\Comment;
use frontend\models\PostForm; 
use frontend\models\SavePost;
use frontend\models\Personalinfo;
use frontend\models\HideComment;
use frontend\models\Friend; 
use frontend\models\TravAds;
use frontend\assets\AppAsset;
use frontend\models\CollectionFollow;
use frontend\models\Collection;
use frontend\models\ChannelSubscriber;
use frontend\models\Channel;
use frontend\models\EventVisitors;
use frontend\models\GroupMember;
use frontend\models\SecuritySetting;
use frontend\models\ReportPost; 
use frontend\models\BlockFriend; 

$session = Yii::$app->session;
$email = $session->get('email'); 
$result = UserForm::find()->where(['email' => $email])->one();
$userid = $user_id =  (string)$session->get('user_id');

if(isset($userid) && $userid != '') {
	$authstatus = UserForm::isUserExistByUid($user_id);
} else {
	$authstatus = 'checkuserauthclassg';
} 

$baseUrl = AppAsset::register($this)->baseUrl;  
$unfollow = new UnfollowFriend();
$unfollow = UnfollowFriend::find()->where(['user_id' => (string)$user_id])->one();
$profile_tip_counter = 1;
$_SESSION['loadedAds'] = array();
$isEmpty = true;

?>
<div class="col s12 m12">  
	<div class="new-post base-newpost">
		<form action="">
			<div class="npost-content">
				<div class="post-mcontent">
					<i class="mdi mdi-pencil-box-outline main-icon"></i>
					<div class="desc">									
						<div class="input-field comments_box">
							<input placeholder="What's new?" type="text" class="validate commentmodalAction_form" disabled="disabled" readonly />
						</div>
					</div>
				</div>
			</div>				
			
		</form>
		<div class="overlay <?=$authstatus?>" id="composetoolboxAction"></div>
	</div>
</div>
 
<div class="post-list margint15"> 
	<div class="row">
		<?php if(count($posts) > 0 ) { ?>
			<?php
			$lpDHSU = 1; 
			
			foreach($posts as $post)
			{  
				// you are exist in post owner restartiction list....
				$post_user_id = $post['post_user_id'];         
				$SecuritySetting = SecuritySetting::find()->where(['user_id' => $post_user_id])->asarray()->one();
				if(isset($userid) && !empty($userid)){
					if(!empty($SecuritySetting)) {
						$filterrestrict = isset($SecuritySetting['restricted_list']) ? $SecuritySetting['restricted_list'] : '';
						$filterrestrict = explode(",", trim($filterrestrict));
						if(in_array($userid, $filterrestrict)) {
							continue;
						}
					}
				}
				// check it is collection post and (also my post or i am follwer of this post)
				$collectionId = isset($post['collection_id']) ? $post['collection_id'] : '';
				if($collectionId != '' && $collectionId != null && $collectionId != 'undefined') {
					$isFollwer = CollectionFollow::isFollwer((string)$userid, $collectionId);
					if(!$isFollwer) {
						continue;
					} 
				}

				// check it is collection post and (also my post or i am follwer of this post)
				$channelId = isset($post['channel_id']) ? $post['channel_id'] : '';
				if($channelId != '' && $channelId != null && $channelId != 'undefined') {
					$isSubscriber = ChannelSubscriber::isSubscriber((string)$userid, $channelId);
					if(!$isSubscriber) {
						continue;
					}
				}

				$eventId = isset($post['event_id']) ? $post['event_id'] : '';
				if($eventId != '' && $eventId != null && $eventId != 'undefined') {
					$isAttendee = EventVisitors::getEventGoing($eventId);
					if($isAttendee != 'Organizer' && $isAttendee != 'Attending') {
						continue;
					}
				} 

				$group_id = isset($post['group_id']) ? $post['group_id'] : '';
				if($group_id != '' && $group_id != null && $group_id != 'undefined') {
					$isMember = GroupMember::getgroupmemerstatus($group_id);
					if($isMember != 'Member' && $isMember != 'Admin') {
						continue;
					}
				}

				// preferences checking item........
				if(!empty($preferences)) {
					if(isset($post['channel_id']) || isset($post['collection_id']) || isset($post['event_id']) || isset($post['group_id'])) {
						if(isset($post['channel_id'])) {
							$demoid = $post['channel_id'];
						} else if(isset($post['collection_id'])) {
							$demoid = $post['collection_id'];
						} else if(isset($post['event_id'])) {
							$demoid = $post['event_id'];
						} else if(isset($post['group_id'])) {
							$demoid = $post['group_id'];
						}
						if(array_key_exists($demoid, $preferences)) {
							$postCreatedDate = $post['post_created_date'];
							$preferencesSetDate = $preferences[$demoid]['modified_at'];
							$feedStatus = $preferences[$demoid]['feed'];
							if($feedStatus == 'on') {
								if($postCreatedDate <= $preferencesSetDate || $post_user_id != $user_id) {
									continue;
								}
							} else {
								continue;
							}
						}
					}
				}

				$existing_posts = '1';
				$cls = '';

				$postid = (string)$post['_id'];
				$postownerid = (string)$post['post_user_id'];
				$postprivacy = $post['post_privacy'];
 
				$isOk = $this->context->filterDisplayLastPost($postid, $postownerid, $postprivacy);
				if($isOk == 'ok2389Ko') {
					if(($lpDHSU%8) == 0) {
						$ads = $this->context->getad(true);
						if(isset($ads) && !empty($ads))
						{
							$ad_id = (string) $ads['_id'];  
							$this->context->display_last_post($ad_id, $existing_posts, '', $cls,'','restingimagefixes','',$lpDHSU);
						}
					}
					
					$this->context->display_last_post((string)$post['_id'], $existing_posts, '', $cls,'','restingimagefixes','',$lpDHSU);
					$lpDHSU++;	
				} 

				$isEmpty = false;
			} 
			?>
		<?php } ?> 
	</div>
</div>

<div class="clear"></div>
<center><div class="lds-css ng-scope dis-none"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
<?php
    if($isEmpty == true) 
    { 	
		$this->context->getwelcomebox("post");
	} 
?>
