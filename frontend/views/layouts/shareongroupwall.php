
<div id="shareongroupwall" class="modal" style="z-index: 1100;">
	<div class="content_header">
		<button class="close_span waves-effect">
		  <i class="mdi mdi-close mdi-20px"></i>
		</button>
		<p class="selected_photo_text"></p>
		<a href="javascript:void(0)" class="shareongroupwalldone done_btn action_btn btn-flat disabled">Done</a>
	</div>
	<nav class="search_for_tag">
		<div class="nav-wrapper">
		  <form>
		    <div class="input-field">

		      <input type="search" required="" class="search_box">
		        <label class="label-icon" for="search_box">
		          <i class="zmdi zmdi-search"></i>
		        </label>
		      </div>
		  </form>
		</div>
	</nav>
	<div class="person_box"></div>
</div>