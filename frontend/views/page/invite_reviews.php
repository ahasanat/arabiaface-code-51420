<?php 
use frontend\models\LoginForm;
use frontend\models\Friend;
use frontend\models\Notification;
use frontend\models\Like;

$i = 0;
if(count($eml_id) > 0){ ?>
	<div class="sresult-list nice-scroll">
	<ul>
		<?php $start = 0;
		foreach($eml_id as $invitedfriends){
			if (!(isset($_GET['key']) && $_GET['key'] != '')) {
				$friendid = (string)$invitedfriends['to_id'];
			} else {
				$friendid = (string)$invitedfriends['_id'];
			}

			$result = LoginForm::find()->where(['_id' => $friendid])->one();
			$frndimg = $this->context->getimage($friendid,'thumb');
			$pagelikeexist = Like::find()->where(['post_id' => "$page_id", 'user_id' => "$friendid", 'status' => '1', 'like_type' => 'page'])->all();
			$invitaionsent = Notification::find()->where(['post_id' => "$page_id", 'status' => '1', 'from_friend_id' => "$friendid", 'user_id' => "$user_id"])->one();
			$is_friend = Friend::find()->where(['from_id' => "$user_id",'to_id' => "$friendid",'status' => '1'])->one();
			if($is_friend) {
				if($_GET['key'] != '') {
                    $fname = isset($result['fname']) ? $result['fname'] : '';
                    $lname = isset($result['lname']) ? $result['lname'] : '';
                    $name = isset($result['fullname']) ? $result['fullname'] : '';
                    if (stripos($fname, $_GET['key']) === 0 || stripos($lname, $_GET['key']) === 0 || stripos($name, $_GET['key']) === 0) {
                    } else {
                        continue;
                    }
                }
		?>
		<li class="invite_<?=$friendid?>">
			<div class="invitelike-friend">
				<div class="imgholder"><img src="<?=$frndimg?>"/></div>
				<div class="descholder">
					<h6><?=$result['fullname']?></h6>
					<div class="btn-holder events_<?=$friendid?>">
						<?php if($pagelikeexist)
						{
							echo '<label class="infolabel"><i class="zmdi zmdi-check"></i> Liked</label>';
						}
						else if($invitaionsent)
						{
							echo '<label class="infolabel"><i class="zmdi zmdi-check"></i> Invited</label>';
						}
						else
						{ ?>
							<a href="javascript:void(0)" onclick="sendinvite('<?=$friendid?>','<?=$page_id?>')" class="btn-invite">Invite</a>
							<a href="javascript:void(0)" onclick="cancelinvite('<?=$friendid?>')" class="btn-invite-close"><i class="mdi mdi-close"></i></a>
						<?php } ?>
					</div>
					<div class="dis-none btn-holder sendinvitation_<?=$friendid?>">
						<label class="infolabel"><i class="zmdi zmdi-check"></i> Invitation sent</label>
					</div>
				</div>														
			</div>
		</li>
		<?php $start++; } } ?>
		<?php if($start == 0){ ?>
		<?php $this->context->getnolistfound('nomorefriendfound'); ?>
		<?php } ?>
		</ul>
	</div>
	<?php } else { ?>
	<?php $this->context->getnolistfound('nofriendfound'); ?>
	<?php } ?>
<?php exit;?>