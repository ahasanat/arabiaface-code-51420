<?php

use frontend\assets\AppAsset;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id'); 
$baseUrl = AppAsset::register($this)->baseUrl;
?>
<div class="formtitle"><h4>Close Account</h4></div>
<div class="close-account-change">
	<div class="top-close-head">
		<h3>We can't close your account yet</h3>
	</div>
	<div class="new-closeaccount">
		<p>You need to be resolved some issues before we can close your account. Once you've resolved the issue, please retry closing your account from your setting page.</p>
	</div>
	<ul class="close-ul">
		<?php if(isset($data['myads']) && $data['myads'] == 'yes') { ?>
		<li>You have an active ad campaign. You'll need to <a href="?r=ads/manage">deactivate it through advert manager</a></li>
		<?php } ?>
		<?php if(isset($data['collection']) && !empty($data['collection'])) { ?>
		<li>
			<span>You own the following collections. You will need to delete it throight collection console</span>
			<ul>
				<?php
				$collections = $data['collection'];
				foreach ($collections as $key => $collection) {
					echo '<li><a href="?r=collection/detail&col_id='.$key.'"><i class="mdi mdi-chevron-right"></i> '.$collection.'</a></li>';
				}
				?>
			</ul>
		</li>
		<?php } ?>
		<?php if(isset($data['groups']) && !empty($data['groups'])) { ?>
		<li>
			<span>You admin the following groups. You will to need delete it or promote another admin throught group console</span>
			<ul>
				<?php
				$groups = $data['groups'];
				foreach ($groups as $key => $group) {
					echo '<li><a href="?r=groups/detail&group_id='.$key.'"><i class="mdi mdi-chevron-right"></i> '.$group.'</a></li>';
				}
				?>
			</ul>
		</li>
		<?php } ?>
		<?php if(isset($data['events']) && !empty($data['events'])) { ?>
		<li>
			<span>You are orginzed the following events. You will need to delete it or promote another orginizer throught event console</span>
			<ul>
				<?php
				$events = $data['events'];
				foreach ($events as $key => $event) {
					echo '<li><a href="?r=event/detail&e='.$key.'"><i class="mdi mdi-chevron-right"></i> '.$event.'</a></li>';
				}
				?>
			</ul>
		</li>
		<?php } ?>
		<?php if(isset($data['page']) && !empty($data['page'])) { ?>
		<li>
			<span>You admin the following pages. You will to need delete it or promote another admin throught page setting</span>
			<ul>
				<?php
				$pages = $data['page'];
				foreach ($pages as $key => $page) {
					echo '<li><a href="?r=page/index&id='.$key.'"><i class="mdi mdi-chevron-right"></i> '.$page.'</a></li>';
				}
				?>
			</ul>
		</li>
		<?php } ?>
	</ul>
	<div class="clear"></div>
</div>
<?php
exit;?>							