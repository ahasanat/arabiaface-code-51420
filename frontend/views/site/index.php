<?php
use frontend\assets\AppAsset;
use yii\widgets\ActiveForm;
use yii\mongodb\ActiveRecord;
use yii\validators\Validator;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\models\Googlekey;
use frontend\models\LoginForm;
use frontend\models\Slider;
use frontend\models\NotificationSetting;
use frontend\models\SecuritySetting;
 
$baseUrl = AppAsset::register($this)->baseUrl;

$session = Yii::$app->session;
$error = $session->get('loginerror');
$email = '';

if(isset($_GET['email']) && !empty($_GET['email']) )
{
	
$notify = new NotificationSetting();
$notify2 = $notify->notification2();
$security_settings = new SecuritySetting();
$security_settings2 = $security_settings->security2();
   
$email = $_GET['email'];
$email =  base64_decode(strrev($email));
$session->set('email',$email);

$update = LoginForm::find()->where(['email' => $email])->one();

$session->set('user_id',$update['_id']);

$update->status = '1';
$update->update();
 
 $admin_email = 'adelhasanat@yahoo.com';
	   
 $user = LoginForm::find()->where(['email' => $email])->one();

$username = $user->username;
$fname = $user->fname;
$lname = $user->lname;
$phone = $user->phone;
$gender = $user->gender;
$city = $user->city;
$country = $user->country;
	
 /* Mail To Admin*/
	try {
	$mailCompose2 = Yii::$app->mailer->compose()
	->setFrom(array('csupport@arabiaface.com' => 'Arabiaface Team'))
	->setTo($email)
	->setSubject('Arabiaface- Registerd User Information')
	->setHtmlBody('<html> <head> <meta charset="utf-8" /> <title>Arabiaface</title> </head> <body style="margin:0;padding:0;"> <div style="text-align: left;font-size: 12px;margin:10px 0 0;color:#666;"> Dear '.$fname.',<br/><br/> We are very happy to have you as a valued member of Arabiaface community. Our aim is to connect each other socially and have Travel benefits.<br/>  Start enjoying being part of our community, share your experience with community and explore the world at finger tip.<br/>  Note: Privacy is important to us; Therefore, we will not sell, rent or give any information from our community to anyone. <br/> To keep Arabiaface safe, fun and very respectable. Here are some social safety tips: <br/>	 <ul style="padding:0 0 0 12px;margin:10px 0 5px;"> <li style="margin:0 0 5px;"> <b>Report abuse:</b>  Please report anything inappropriate, immediate action will be taken. </li><li style="margin:0 0 5px;"> <b>Block people:</b> If someone is bothering you, block them! They wont be able to contact you again </li><li style="margin:0 0 5px;"> <b>Public posts:</b> Dont post anything youll regret. Dont give out personal information.</li><li style="margin:0 0 5px;"><b>Post Decent Content:</b> Absolutely no adult or semi adult photos or videos are permitted on this site. Therefore, please do not try to upload, link and share any adult or semi-adult images. No gambling, drugs, adult or alcoholic ads are allowed. Harassment, inappropriate behaviors or solicitation are not permitted on this site.</br> Software and the management monitor the site, any inappropriate activities </br> will not be tolerated and will result in banning your name, email address </br> and  IP address permanently. So please be social and thank you for your cooperation. </li></ul></br> <b>Have fun and stay safe!</b><br/> Thank you for registering.</br></br><span style="font-size:10px;">If you have any question/suggestion, feel free to write us at <a href="javascript:void(0)">helpdesk@arabiaface.com</a></span></div></body></html>')
	->send();                    

	$url = Yii::$app->urlManager->createUrl(['site/index']);
	Yii::$app->getResponse()->redirect($url); 
	} 
	catch (ErrorException $e) 
	{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
}

elseif(isset($_GET['id']) && !empty($_GET['id']))
{
    $userid = $_GET['id'];
    $update = LoginForm::find()->where(['_id' => $userid])->one();
    
    if(!empty($update))
    {
      $email = $update->email;
      $session = Yii::$app->session;
      $session->set('user_id',$userid);
    }
    echo $email = $update->email;
}

$asset = frontend\assets\AppAsset::register($this);
$baseUrl = AppAsset::register($this)->baseUrl;
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
    <script>
    $(document).ready(function(){
      var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
           if (sParameterName[0] === 'enc') {
               $(".login-part").css("display", "none");
               $(".forgot-part").css("display", "block");
               $(".forgot-part .forgot-box").css("display", "none");
               $(".forgot-part #fp-step-3").css("display", "block");
            }
        }
    });
    </script>
	<link href="<?=$baseUrl?>/css/custom-croppie.css" rel="stylesheet">
	<link href="<?=$baseUrl?>/css/animate.css" rel="stylesheet">
	<!-- 
	<link href="<?=$baseUrl?>/css/emoticons.css" rel="stylesheet">
	<link href="<?=$baseUrl?>/css/emostickers.css" rel="stylesheet"> -->
	<div class="home-page bodydup pageloader">
		<div id="loader-wrapper" class="home_loader">
			<div class="loader-logo">
	            <div class="lds-css ng-scope">
	               <div class="lds-rolling lds-rolling100">
	                  <div></div>
	               </div>
	            </div>
	         </div> 
			<div class="loader-text">Please wait...</div>
			<div class="loader-section section-left"></div>
			<div class="loader-section section-right"></div>
		</div>
			
		<div class="home-wrapper">
			<header>
				<div class="home-container">
					<div class="mobile-menu topicon">
						<a href="javascript:void(0)" class="mbl-menuicon"><i class="mdi mdi-menu"></i></a>
					</div>
					<div class="hlogo-holder">
						<a class="home-logo" href="<?php echo Yii::$app->urlManager->createUrl(['site/index']); ?>"><img src="<?=$baseUrl?>/images/arabiaface-logo-white.png"></a>            						
					</div>
					<ul class="home-menu"> 
						<li><a href="<?php echo Yii::$app->urlManager->createUrl(['site/mainfeed']); ?>">Home</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl(['collection']); ?>">Collection</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl(['channel']); ?>">Channels</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl(['whoisaround/index']); ?>">People</a></li>
						<li><a href="<?php echo Yii::$app->urlManager->createUrl(['ads']); ?>"><i class="zmdi zmdi-plus-square pr-5"></i>Create Advert</a></li>
					</ul>
					<div class="head-right fblabelcolor">
						<div class="search-part">
							<a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
								<i class="mdi mdi-lock"></i>
								<span>Login</span>
							</a>
						</div>
						<div class="signup-part">
							<a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
								<i class="mdi mdi-lock"></i>
								<span>Login</span>
							</a>
						</div>
						<div class="login-part">
							<a class="homebtn" onclick="flipSectionTo('search');" href="javascript:void(0)">
								<i class="mdi mdi-close"></i>
								<span>Close</span>
							</a>
						</div>
						<div class="forgot-part">
							<a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
								<i class="mdi mdi-lock"></i>
								<span>Login</span>
							</a>
						</div>				
					</div>
				</div>	
			</header>
			
			<div class="sidemenu-holder m-hide">
				<div class="sidemenu">
					<a href="javascript:void(0)" class="closemenu"><i class="mdi mdi-close"></i></a>				
					<div class="side-user">							
						Check this out
					</div>
					<div class="sidemenu-ul">
						<ul class="large-menuicons">
							<li><a href="<?php echo Yii::$app->urlManager->createUrl(['collection']); ?>">Collection</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl(['channel']); ?>">Channels</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl(['event']); ?>">Events</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl(['whoisaround/index']); ?>">People</a></li>
							<li><a href="<?php echo Yii::$app->urlManager->createUrl(['ads']); ?>"><i class="zmdi zmdi-plus-square pr-5"></i>Create Advert</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="hcontent-holder banner-section">
				<span class="overlay"></span>
				<div class="home-content">
					<div class="search-part homel-part">						
						<div class="container">
							<div class="search-box">								
								
								<div class="box-content">
									<div class="login-notice">
										<span class="success-note">Successfully Logged in!</span>
										<span class="info-note">Fill in the mandatory fields.</span>
										<span class="error-note">Please Enter Email address and Password</span>
									</div>
									<div class="bc-row home-search">
										<div class="row">			
											<!-- <h3 class="data-privacy white-text">Social Network for Friends and Family</h3> -->
					             		</div>
									</div>
								</div>
							</div>
						</div>
					</div>				
					<div class="login-part homel-part login-sec">	
						<div class="hidden_header">
							<div class="content_header">
								<button class="close_span cancel_poup waves-effect" onclick="flipSectionTo('search');">
									<i class="mdi mdi-close mdi-20px	"></i>
								</button>
								<p class="modal_header_xs">Login</p>
							</div>
						</div>					
						<div class="container">
							<div class="homebox login-box animated wow zoomIn" data-wow-duration="1200ms" data-wow-delay="500ms">					
								<div class="sociallink-area fblabelcolor">
									<a  id="FacebookBtn" href="javascript:void(0)" class="fb-btn white-text"><span><i class="mdi mdi-facebook"></i></span>Connect with Facebook</a>
								</div>
								<div class="home-divider">
									<span class="div-or">or</span>
								</div>
									<input type="hidden" name="_csrf">
									<input type="hidden" value="" id="lat" name="lat">
									<input type="hidden" value="" id="long" name="long">
									<input type="hidden" value="1" name="login">
								<div class="box-content">
									<div class="login-notice">
										<span class="success-note"></span>
										<span class="error-note"></span>
										<span class="info-note"></span>
									</div>
									<div class="bc-row">
											<div class="sliding-middle-out anim-area underlined">
												<input type="text" placeholder="Email Address" onchange="validate_lemail()" onkeyup="validate_lemail()" id="lemail" name="LoginForm[email]" max="75">
											</div>
											<div class="dis-none frm-validicon" id="leml-success"><img src="<?=$baseUrl?>/images/frm-check.png"></div>
											<div class="dis-none frm-validicon" id="leml-fail"><img src="<?=$baseUrl?>/images/frm-cross.png"></div>
									</div>
									<div class="bc-row">
											<div class="sliding-middle-out anim-area underlined">
												<input type="password" placeholder="Password" onchange="validate_lpassword()" onkeyup="validate_lpassword()" id="lpassword" name="LoginForm[password]" max="30">
											</div>
											<div class="dis-none frm-validicon" id="lpwd-success"><img src="<?=$baseUrl?>/images/frm-check.png"></div>
											<div class="dis-none frm-validicon" id="lpwd-fail"><img src="<?=$baseUrl?>/images/frm-cross.png"></div>
									</div>
									<div class="clear"></div>
								</div>
								<div class="nextholder login-popup fblabelcolor">
									<a class="fp-link" href="javascript:void(0)" onclick="setForgotPassStep();flipSectionTo('forgot');">Forgot Password?</a>
									<a href="javascript:void(0)" class="homebtn" onclick="tbLogin()">Login</a>
								</div>
								<div class="btn-holder fblabelcolor">
									<p>Do not have an account?
										<a onclick="flipSectionTo('signup');" href="javascript:void(0)" class="white-text">Sign Up</a>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="signup-part homes-part">
						<div class="hidden_header">
							<div class="content_header">
								<button class="close_span cancel_poup waves-effect" onclick="flipSectionTo('search');">
									<i class="mdi mdi-close mdi-20px	"></i>
								</button>
								<p class="modal_header_xs">Login</p>
							</div>
						</div>		
						<input type="hidden" name="user_email" id="user_email" value="" />
						<div class="">							
						<?php $form = ActiveForm::begin(['id' => 'frm','options'=>['onsubmit'=>'return false;','enableAjaxValidation' => true,],]); ?>
							<div class="homebox signup-box" id="create-account">								
								<div class="box-content">
									<h5>Create Account</h5>
									<div class="bc-row">
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="text" id="fname" name="LoginForm[fname]" placeholder="First Name" onkeyup="validate_fname()" max="50" class="capitalize">
											</div>
											<div id='fnm-success' class="frm-validicon dis-none"><img src="<?=$baseUrl?>/images//frm-check.png"/></div>
											 <div id='fnm-fail' class="frm-validicon dis-none"><img src="<?=$baseUrl?>/images//frm-cross.png"/></div>
										</div>
									</div>
									<div class="bc-row">
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="text" id="lname" name="LoginForm[lname]" placeholder="Last Name" onkeyup="validate_lname()" max="50" class="capitalize">
											</div>
											<div id='lnm-success' class="frm-validicon dis-none"><img src="<?=$baseUrl?>/images//frm-check.png"/></div>
											 <div id='lnm-fail' class="frm-validicon dis-none"><img src="<?=$baseUrl?>/images//frm-cross.png"/></div>
										</div>
									</div>
									<div class="bc-row">
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="text" id="email" name="LoginForm[email]" value="" placeholder="Email" onkeyup="validate_email()" onchange="validate_email()" max="75">
											</div>
											<div id='eml-success' class="frm-validicon dis-none"><img src="<?=$baseUrl?>/images//frm-check.png"/></div>
											 <div id='eml-fail' class="frm-validicon dis-none"><img src="<?=$baseUrl?>/images//frm-cross.png"/></div>
										</div>
									</div>
									<div class="bc-row">
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="password" id="signup_password" name="LoginForm[password]" placeholder="Password" onkeyup="validate_spassword()" max="30">
											</div>
											<div id='spwd-success' class="frm-validicon dis-none"><img src="<?=$baseUrl?>/images//frm-check.png"/></div>
											 <div id='spwd-fail' class="frm-validicon dis-none"><img src="<?=$baseUrl?>/images//frm-cross.png"/></div>
										</div>
									</div>
								</div>
								<div class="nextholder fblabelcolor">
									<a href="javascript:void(0)" id="step1" class="homebtn su-nextbtn" data-class="profile-setting" onclick="tbSignupNavigation(this)">Next</a>
								</div>
								<div class="btn-holder fblabelcolor">
									<p>Have an account?
										<a onclick="flipSectionTo('login');" href="javascript:void(0)" class="white-text">Login</a>
									</p>
								</div>
							</div>
						<?php ActiveForm::end() ?>
						
						<?php $form = ActiveForm::begin(['id' => 'frm2','options'=>['onsubmit'=>'return false;','enableAjaxValidation' => true,],]); ?>
							<div class="homebox signup-box" id="profile-setting">
								<div class="profile-settingblock1">
								<div class="box-content">
									<h5>Profile Setting</h5>    
									<div class="bc-row">
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="text" id="autocomplete" data-query="all" onfocus="filderMapLocationModalSignup(this)" class="validate" autocomplete="off" name="LoginForm[city]" placeholder="City"/>
											</div>
										</div>
									</div>
									<div class="bc-row">
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="text" id="country" name="LoginForm[country]" placeholder="Country">
											</div>
										</div>
									</div>
										<input type="hidden" readonly="true" name="isd_code" id="isd_code"/>
										<input type="hidden" id="country_code" name="country_code" />
									<div class="bc-row">
										<div class="bc-componentnew">
											<div class="sliding-middle-out anim-area underlined">
												<select name="gender" id="gender" class="genderDrop">
													<option value="Male">Male</option>
													<option value="Female">Female</option>
												</select>
											</div>
										</div>
									</div>		
									<div class="bc-row">
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="text" onkeydown="return false;" placeholder="Birthdate" name="LoginForm[birth_date]" data-toggle="datepicker" data-query="all" class="datepickerinputtemp" id="datepicker" readonly>

												<input type="hidden" name="birth_access" value="Private" />
											</div>
										</div>
									</div>										
								</div>
								<div class="nextholder fblabelcolor">
									<a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="upload-photo" onclick="tbSignupNavigation(this)">Next</a>
								</div>
								<div class="btn-holder fblabelcolor">
									<p>Have an account?
										<a onclick="flipSectionTo('login');" href="javascript:void(0)" class="white-text">Login</a>
									</p>
								</div>
								</div>
								<div class="profile-settingblock2">
		                           <div class="content_header">
		                              <button class="close_span waves-effect">
		                                 <i class="mdi mdi-close mdi-20px material_close resetdatepicker"></i>
		                              </button>
		                              <p class="selected_photo_text">Select Date</p>
		                              <a href="javascript:void(0)" class="done_btn action_btn closedatepicker">Done</a>
		                           </div> 
		                           <div class="modal-content">
		                              <div id="datepickerBlocktemp"></div>
		                           </div>
		                        </div>

		                        <div class="profile-settingblock3 inlinemapmodalbox">
		                           <div class="map-model-content">  
		                             <div style="display:none">
		                               <div class="map_header_container">
		                                 <div class="search_container">
		                                   <button class="close_span waves-effect"> 
		                                     <i class="mdi mdi-close mdi-20px material_close"></i>
		                                   </button>
		                                   <div class="near_by_place">
		                                     <input placeholder="Search nearby places.." id="nearby_place_new" type="text" class="validate nearby_place_new specialinput">
		                                   </div>
		                                   <span class="search_span search_map_new" id="map_search_new">
		                                     <i class="zmdi zmdi-search material_close mdi-18px"></i>
		                                   </span>
		                                 </div>
		                               </div>
		                               <div class="map_frame">
		                                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d238133.1523816246!2d72.68221020433099!3d21.15914250210564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04e59411d1563%3A0xfe4558290938b042!2sSurat%2C+Gujarat!5e0!3m2!1sen!2sin!4v1507141795136" style="width:100%;max-width:100%;height:200px" frameborder="0" style="border:0" allowfullscreen=""></iframe>
		                               </div>
		                               <div class="map_container"></div>
		                             </div>
		                             <div id="pac_container_new"  class="map_header_container">
		                               <div class="search_container">
		                                 <div class="map_remove">
		                                   <button class="close_span waves-effect">
		                                     <i class="mdi mdi-close mdi-20px close_search_box"></i>
		                                   </button>
		                                 </div>
		                                 <button class="back_arrow"> 
		                                   <i class="zmdi zmdi-arrow-left mdi-20px"></i>
		                                 </button>
		                                 <input id="pac-input_new" class="controls specialinput" type="text" placeholder="Search Your Location">
		                                 <span class="search_span search_map_new" id="map_search_new">
		                                   <i class="zmdi zmdi-search material_close mdi-18px"></i>
		                                 </span>
		                                 <span class="empty_input empty_close">
		                                   <i class="mdi mdi-close mdi-20px"></i>
		                                 </span>
		                               </div>
		                             </div>
		                             <div id="map_new" class="map_div"></div>
		                             <div class="map_container"> </div>
		                           </div>
		                        </div>
							</div>
						<?php ActiveForm::end() ?>
							<div class="homebox signup-box upload-photo" id="upload-photo">
								<div class="box-content">
									<div class="upload-header"></div>
									<div class="home-cropper">
										<div class="cropper cropper-wrapper">
											<div class="image-upload">
												<label for="file-input">
													<i class="zmdi zmdi-camera-bw"></i>
												</label>
												
												<input id="file-input" type="file" class="js-cropper-upload" value="Select" onclick="$('.js-cropper-result').hide();$('.crop').show();$('.image-upload').hide();$('.cropper-nae').hide();$('.text-hide-show').hide();"/>
											</div>
											<div class="js-cropper-result">
												<img src="<?=$baseUrl?>/images/demo-profile.jpg">
											</div> 
											<div class="crop dis-none"> <div class="grag-title">Drag to crop</div>
												<div class="js-cropping"></div> 
												<i class="js-cropper-result--btn zmdi zmdi-check upload-btn" onclick="$('.cropper-nae').show();"></i>
												<i class="js-cropper-result--btn zmdi zmdi-check upload-btn" onclick="$('.cropper-nae').show();$('.text-hide-show').show();"></i>
												<i class="mdi mdi-close	 img-cancel-btn" onclick="imageCancelUpload();"></i>

											</div> 
											<h1 class="cropper-nae" id="uname"></h1>
										</div> 
										<p class="note red-danger text-hide-show">
											Your photo needs to be 200x200 with gif or jpeg format
										</p>
									</div>
								</div>
								<div class="nextholder fblabelcolor">
									<a href="javascript:void(0)" class="homebtn su-skipbtn left" data-class="security-check" onclick="tbSignupNavigation(this)">Skip</a>
									<a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="security-check" onclick="cropTriggerClk()">Next</a>
								</div>				
								<div class="btn-holder fblabelcolor">
									<p>Have an account?
										<a onclick="flipSectionTo('login');" href="javascript:void(0)" class="white-text">Login</a>
									</p>
								</div>
							</div>
													
							<div class="homebox signup-box" id="security-check">
								<div class="box-content">
									<h5>Security Check</h5>
									<div class="security-box">
										<div class="row">
											<p>To guard against automated systems and robots, please checked the box below</p>
										</div>
										<div class="row center">
											<center>
				                                <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LdZs1sUAAAAAKtNHR72Wb__55sQXghN-AKs_Qct"></div>
				                             </center>
								        	<!-- <input type="checkbox" class="filled-in robot-chk new-robot-chec" id="filled-in-box" />
								        	<label for="filled-in-box">I am not robot</label> -->
							        	</div>

							        	<div class="center pt-20">
							        		<input type="checkbox" class="robot-chk agree-chk" id="filled-in-box2"/>
								        	<label for="filled-in-box2" class="home-f-n">Agreed to the terms and conditions</label>
							        	</div>
									</div>
								</div>
								<div class="nextholder fblabelcolor">
									<a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="confirm-email" onclick="tbSignupNavigation(this)">Next</a>
								</div>
								<div class="btn-holder fblabelcolor">
									<p>Have an account?
										<a onclick="flipSectionTo('login');" href="javascript:void(0)" class="white-text">Login</a>
									</p>
								</div>
							</div>

							<div class="homebox signup-box" id="confirm-email">
								<div class="box-content">
									<h5>You’re almost done</h5>
									<div class="text-center fullwidth">
										<img src="<?=$baseUrl?>/images/confirm-msg.png" class="confirm-img"/>		
									</div> 
									<h6 class="white-text">Please confirm your email to have full access to your account</h6>
								</div>
								<div class="nextholder fblabelcolor" id="confirmlink">
									<a class="homebtn autow" href="javascript:void(0)">Confirm Email</a>
								</div>						
							</div>		
						</div>
					</div>
					<div class="forgot-part homes-part">
						<div class="container">
							<div class="homebox forgot-box" id="fp-step-1">								
								<div class="box-content">
																		<div class="fphome-notice">
										<span class="success-note"></span>
										<span class="error-note"></span>
										<span class="info-note"></span>
									</div>
									<h5>Change Your Password</h5>
									<div class="fp-box">
										<p class="text-center">	Let's find your account</p>
										<div class="bc-row mt25">
											<div class="sliding-middle-out anim-area underlined">
												<input placeholder="Email address or alternate email" type='text' id="forgotemail" name="forgotemail" onkeyup="validateforgotemail()">
												<div class="dis-none frm-validicon" id="fpeml-success">
													<img src="<?=$baseUrl?>/images//frm-check.png">
												</div>
												<div class="dis-none frm-validicon" id="fpeml-fail">
													<img src="<?=$baseUrl?>/images//frm-cross.png">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="nextholder">
									<a href="javascript:void(0)" class="homebtn" data-class="fp-step-2" onclick="forgotPassNavigation(this)">Next</a>
								</div>
							</div>
							<div class="homebox forgot-box" id="fp-step-2">								
								<div class="box-content">
									<h5>We've sent a link to change your password</h5>
									<div class="fp-box">
										<p>check your email and follow the link to quickly reset your password</p>
									</div>
									<div class="nextholder" id="displayresetlink">
									</div>
								</div>
							</div>
							<div class="homebox forgot-box" id="fp-step-3">								
								<?php
								if(isset($_GET['enc']) && !empty($_GET['enc']))
								{
									$enc = $_GET['enc'];
									$trav_id = $enc;
									$travid =  base64_decode(strrev($trav_id));
									$postresult = LoginForm::find()->where(['_id' => $travid])->one();
									if($postresult)
									{
								?>
								<div class="box-content">
									<div class="fphome-notice">
										<span class="success-note"></span>
										<span class="error-note"></span>
										<span class="info-note"></span>
									</div>
									<input type="hidden" name="travid" id="travid" value="<?= $travid?>">
									<h5>Choose New Password</h5>
									<div class="bc-row">										
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="password" id="fppassword" placeholder="Type your new password" onkeyup="tbFp()">
												<div class="dis-none frm-validicon" id="pwd-success"><img src="<?=$baseUrl?>/images/frm-check.png"></div>
												<div class="dis-none frm-validicon" id="pwd-fail"><img src="<?=$baseUrl?>/images/frm-cross.png"></div>
											</div>
										</div>
									</div>
									<div class="bc-row">										
										<div class="bc-component">
											<div class="sliding-middle-out anim-area underlined">
												<input type="password" id="fppasswordcon" placeholder="Confirm your new password" onkeyup="tbConFp()">
												<div class="dis-none frm-validicon" id="conpwd-success"><img src="<?=$baseUrl?>/images/frm-check.png"></div>
												<div class="dis-none frm-validicon" id="conpwd-fail"><img src="<?=$baseUrl?>/images/frm-cross.png"></div>
											</div>
										</div>
									</div>
									<p class="note">Passwords are case sensitive, must be at least 6 characters</p>
									<a href="javascript:void(0)" class="homebtn" data-class="fp-step-4" onclick="forgotPassNavigation(this)">Continue</a>
								</div>
								<?php } else { ?>
									<div class="box-content">
										<h5>No user found !!!</h5>
										<h5>Please paste correct url from your email !!!</h5>
									</div>
								<?php } } else { ?>
								<div class="box-content">
									<h5>Please paste correct url from your email !!!</h5>
								</div>
								<?php }?>
								<div class="nextholder">
									<a href="javascript:void(0)" class="homebtn" data-class="fp-step-4" onclick="forgotPassNavigation(this)">Continue</a>
								</div>
							</div>
							<div class="homebox forgot-box" id="fp-step-4">								
								<div class="box-content">
									<h5>Your password has been reset</h5>
									<div class="fp-box">
										<p class="text-center">Now you can login with your new password!</p>
									</div>
									<div class="nextholder">
									<a href="javascript:void(0)" onclick="flipSectionTo('login');" class="homebtn">Log in</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="banner-footer">
					<div class="home-container">
						<div class="row">
							<div class="col-lg-12 f-left">
								<div class="user-count">
									<p>
										Social Networking for the Arab Community !!
									 </p>
								</div>						
							</div>
							
						</div>            
					</div>
				</div>
				<!-- <div class="pattern"></div> -->
			</div>
		
		<div class="home-section socially-connected">
			<div class="container">
			    <div class="section home-row2">
			        <div class="home-title">
						<h4>Socially Connected!</h4>
						<p>Social Media and network for friends and family</p>
					</div>
					<div class="socials-section socially-connected">
						<div class="row">
							<div class="col s12 m4 wow animated zoomIn" data-wow-delay="500ms" data-wow-duration="1200ms">
					          <div class="icon-block">
					            <img src="<?=$baseUrl?>/images/social-discover.png" class="center-block" alt="img 1">
								<div class="descholder green-box">
									<h3 class="font-25">MESSAGE</h3>
									<h6>WHERE TO GO</h6>
									<p>Stay in tounch and chat or call you friends and family</p>
								</div>
					          </div>
					        </div>
							<div class="col s12 m4 hidden-sm wow animated zoomIn" data-wow-delay="500ms" data-wow-duration="1200ms">
					          <div class="icon-block">
					            <img src="<?=$baseUrl?>/images/social-meet.png" class="center-block" alt="img 1">
								<div class="descholder green-box">
									<h3 class="font-25">SHARE</h3>
									<h6>YOUR EXPERIENCES</h6>
									<p>Share your moments and photos with your friends and acquaintances</p>
								</div>
					          </div>
					        </div>
					        <div class="col s12 m4">
					          <div class="icon-block">
					            <img src="<?=$baseUrl?>/images/social-share.png" class="center-block" alt="img 1">
								<div class="descholder green-box">
									<h3 class="font-25">MEET</h3>
									<h6>LIKE-MINDED PEOPLE</h6>
									<p>Make friends with people that share values and interests</p>
								</div>
					          </div>
					        </div>
						</div>
					</div>
				</div>
			</div>
		</div> 
		
		<div class="hcontent-holder home-section state-section row-icon">
			<div class="container">
				<div class="state-area">
					<div class="row center">
						<div class="col m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="0ms">
							<div class="iconholder">
								<i class="zmdi zmdi-accounts"></i>
							</div>
							<div class="descholder">
								<h3><?=$total_users?></h3>
								<p>People</p>
							</div>
						</div>
						<div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="3ms">
							<div class="iconholder">
								<i class="mdi mdi-clipboard"></i>
							</div>
							<div class="descholder">
								<h3><?=$total_posts?></h3>
								<p>Posts</p>
							</div>
						</div>
						<div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="6ms">
							<div class="iconholder">
								<i class="mdi mdi-file-image"></i>
							</div>
							<div class="descholder">
								<h3><?=$total_photos?></h3>
								<p>Photos</p>
							</div>
						</div>
						<div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="9ms">
							<div class="iconholder">
								<i class="zmdi zmdi-check"></i>
							</div>
							<div class="descholder">
								<h3><?=$total_events?></h3>
								<p>Events</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="hcontent-holder home-section gray-section popular-places">
			<div class="container mt-20"> 
                 <div class="home-title general-page ">
                  <h4>POPULAR COLLECTIONS</h4>
                  <p class="mt-15">With a world full of fascinating ideas, suggesting  the most popular collections to follow</p>
                  <div class="row mt-20">
                     <div class="tab-content view-holder grid-view">                               
                        <div class="tab-pane fade main-pane active in" id="collections-suggested">
                           <div class="generalbox-list">                         
                              <div class="row">       
                                 <div class="col s6 m4 l3 collectionbox collectionbox_5bc1025790d5b61963b3bf9f"> 
                                    <div class="card hoverable collectionCard animated fadeInUp collection-title">
                                       <a href="javascript:void(0);" class="general-box collection-red" onclick="collectionFollows(event,'5bc1025790d5b61963b3bf9f',this, true)">
                                          <div class="photo-holder">
                                             <img src="<?=$baseUrl?>/images/additem-collections.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Testing</h4>
                                             <div class="icon-line">
                                                <span>yesno</span>
                                             </div>                  
                                             <div class="userinfo">
                                                <img src="<?=$baseUrl?>/images/Male.jpg"/>
                                             </div>
                                             <div class="username">
                                                <span>Adel Hasanat</span>
                                             </div>                                       
                                             <div class="action-btns">                 
                                                <span class="noClick" onclick="collectionFollows(event,'5bc1025790d5b61963b3bf9f',this, false)">Follow</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                                 <div class="col s6 m4 l3 collectionbox collectionbox_5ae5a972ea01e359171a7c23"> 
                                    <div class="card hoverable collectionCard animated fadeInUp collection-title">
                                       <a href="javascript:void(0);" class="general-box collection-red" onclick="collectionFollows(event,'5ae5a972ea01e359171a7c23',this, true)">
                                          <div class="photo-holder">
                                             <img src="<?=$baseUrl?>/images/additem-collections.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Jordan Food</h4>
                                             <div class="icon-line">
                                                <span>uuu</span>
                                             </div>                  
                                             <div class="userinfo">
                                                <img src="<?=$baseUrl?>/images/Male.jpg"/>
                                             </div>
                                             <div class="username">
                                                <span>Adel Hasanat</span>
                                             </div>                                       
                                             <div class="action-btns">                 
                                                <span class="noClick" onclick="collectionFollows(event,'5ae5a972ea01e359171a7c23',this, false)">Follow</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                                 <div class="col s6 m4 l3 collectionbox collectionbox_5a9feb52ea01e3142e237953"> 
                                    <div class="card hoverable collectionCard animated fadeInUp collection-title">
                                       <a href="javascript:void(0);" class="general-box collection-red" onclick="collectionFollows(event,'5a9feb52ea01e3142e237953',this, true)">
                                          <div class="photo-holder">
                                             <img src="<?=$baseUrl?>/images/additem-collections.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>London Travel</h4>
                                             <div class="icon-line">
                                                <span>ade</span>
                                             </div>                  
                                             <div class="userinfo">
                                                <img src="<?=$baseUrl?>/images/Male.jpg"/>
                                             </div>
                                             <div class="username">
                                                <span>Adel Hasanat</span>
                                             </div>                                       
                                             <div class="action-btns">                 
                                                <span class="noClick" onclick="collectionFollows(event,'5a9feb52ea01e3142e237953',this, false)">Follow</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                                 <div class="col s6 m4 l3 collectionbox collectionbox_5a9d3df7ea01e3987d23769e"> 
                                    <div class="card hoverable collectionCard animated fadeInUp collection-title">
                                       <a href="javascript:void(0);" class="general-box " onclick="collectionFollows(event,'5a9d3df7ea01e3987d23769e',this, true)">
                                          <div class="photo-holder">
                                             <img src="<?=$baseUrl?>/images/additem-collections.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Test</h4>
                                             <div class="icon-line">
                                                <span>test</span>
                                             </div>                  
                                             <div class="userinfo">
                                                <img src="<?=$baseUrl?>/images/Male.jpg"/>
                                             </div>
                                             <div class="username">
                                                <span>Shweta Agrawal</span>
                                             </div>                                       
                                             <div class="action-btns">                 
                                                <span class="noClick" onclick="collectionFollows(event,'5a9d3df7ea01e3987d23769e',this, false)">Follow</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
		</div>

		<div class="hcontent-holder home-section promo-section promo1-section dark-section">
            <div class="container">
               <div class="promo-area">
                  <div class="row">
                     <div class="col m4 s12 hidden-sm wow bounceInLeft"></div>
                  </div>
               </div>
            </div>
        </div>

        <div class="hcontent-holder home-section">
			<div class="container">				
				<div class="collection-area">
					<div class="row">
						<div class="col m6 s12 wow slideInLeft event-img">
							<div class="imgholder">
								<img src="<?=$baseUrl?>/images/travel-story.jpg" alt="travel event"/>						
							</div>
						</div>
						<div class="col m6 s12">
							<div class="descholder travel-story">
								<h3 class="animated wow zoomIn" data-wow-duration="1200ms" data-wow-delay="500ms">COLLECTIONS</h3>
                           		<p class="animated wow fadeInLeft" data-wow-duration="1200ms" data-wow-delay="500ms">A collection can focus on a set of posts on a particular topic, providing an easy way for you to organize all your experiences in one place. Each collection can be shared publicly, privately, or with your friend. People follow your stories to receive updates on your added posts.</p>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>
		
		<div class="hcontent-holder home-section meetpeople-section dark-section">
			<div class="container">
				<div class="meetpeople-area expert">
					<div class="row">
						<div class="col m6 s12">
							<div class="home-title">								
								<h3>Top Rankers</h3>
							</div>
						</div>
						<div class="col m6 s12 wow bounceInUp">
							<div class="meetpoeple-box">								
								<div class="people">	
								<?php
									foreach($recently_joined as $joined_user){
									$idArray = array_values($joined_user['_id']);
									$uid = $idArray[0];
									//$uid = (string)$joined_user['_id']['$id'];
									$user_img = $this->context->getimage($uid,'photo'); 
									if(isset($joined_user['country']) && !empty($joined_user['country']))
									{
										$cntry = $joined_user['country'];
									}
									else{
										$cntry = '';
									}
									
									?>
									<div class="boxleft">
										<div class="imgholder">
											<img src="<?=$user_img?>"/>
										</div>
										<h4><?=$joined_user['fname']?></h4>
										<p><?=$cntry?></p>
										<a href="<?php echo Url::to(['userwall/index', 'id' => "$uid"]); ?>"><i class="mdi mdi-arrow-right"></i> View Wall</a>
									</div>
									
								<?php } ?>
									
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="hcontent-holder home-section gray-section tours-page tours">
             <div class="container mt-10">
                 <div class="home-title general-page ">
                     <h4>Channels</h4>
                     <p class="mt-15">Channels are topics open for discussion assigned by system Admin.</p>
                     <div class="tab-content view-holder grid-view">
                         <div class="tab-pane fade main-pane active in" id="channels-suggested">
                             <div class="generalbox-list channels-list">
                                 <div class="row">
                                     <div class="col s6 m4 l3 channelbox channelbox_5adc68c9ea01e37c791a7c1a">
                                         <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="javascript:void(0);" onclick="channelSubscribe(event, '5adc68c9ea01e37c791a7c1a', this, true, 1)" class="general-box">
                                                 <div class="photo-holder">
                                                     <img src="<?=$baseUrl?>/images/additem-channels.png">
                                                 </div>
                                                 <div class="content-holder collection-title">
                                                     <h4>Jordan Food</h4>
                                                     <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                     </div>
                                                     <div class="countinfo">
                                                         1 </div>
                                                     <div class="posts-line">
                                                         1 posts </div>
                                                     <div class="action-btns">
                                                         <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                     </div>
                                     <div class="col s6 m4 l3 channelbox channelbox_5adc6823ea01e3b2791a7bd2">
                                         <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="javascript:void(0);" onclick="channelSubscribe(event, '5adc6823ea01e3b2791a7bd2', this, true, 2)" class="general-box">
                                                 <div class="photo-holder">
                                                     <img src="<?=$baseUrl?>/images/additem-channels.png">
                                                 </div>
                                                 <div class="content-holder collection-title">
                                                     <h4>Jordan Travel</h4>
                                                     <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                     </div>
                                                     <div class="countinfo">
                                                         2 </div>
                                                     <div class="posts-line">
                                                         1 posts </div>
                                                     <div class="action-btns">
                                                         <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                     </div>
                                     <div class="col s6 m4 l3 channelbox channelbox_59d4baffea01e3264cec0213">
                                         <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="javascript:void(0);" onclick="channelSubscribe(event, '59d4baffea01e3264cec0213', this, true, 3)" class="general-box">
                                                 <div class="photo-holder">
                                                     <img src="<?=$baseUrl?>/images/additem-channels.png">
                                                 </div>
                                                 <div class="content-holder collection-title">
                                                     <h4>Arabiaface channel</h4>
                                                     <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                     </div>
                                                     <div class="countinfo">
                                                         3 </div>
                                                     <div class="posts-line">
                                                         3 posts </div>
                                                     <div class="action-btns">
                                                         <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                     </div>
                                     <div class="col s6 m4 l3 channelbox channelbox_59d4bae1ea01e3293cec026b">
                                         <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="javascript:void(0);" onclick="channelSubscribe(event, '59d4bae1ea01e3293cec026b', this, true, 4)" class="general-box">
                                                 <div class="photo-holder">
                                                     <img src="<?=$baseUrl?>/images/additem-channels.png">
                                                 </div>
                                                 <div class="content-holder collection-title">
                                                     <h4>Travel a way</h4>
                                                     <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                     </div>
                                                     <div class="countinfo">
                                                         4 </div>
                                                     <div class="posts-line">
                                                         1 posts </div>
                                                     <div class="action-btns">
                                                         <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                     </div>

                                 </div>
                             </div>
                         </div>
                     </div>

                 </div>
             </div>
         </div>


        <div class="petra-siq">
            <div class="hcontent-holder home-section info-section info2-section china-petra">
               <div class="container">
                  <div class="info-area">
                     <div class="row">
                        <div class="col m6 s12 right petra">
                           <div class="home-title">
                              <h4>ENCRYPTION</h4>
                           </div>
                           <p class="para-petra">
                              Your privacy and protecting you data is consider by our team is the most important issue during the design. Before we launch the finial copy of the website, member names, email, phone number, contact, message will be encrypted.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="hcontent-holder home-section gray-section news-section">
				<div class="container">			
					<div class="feedback-area">
						<div class="row">
							<div class="col m4 col s12 wow slideInUp">
								<div class="feedbackbox">
									<div class="imgholder">
										<img src="<?=$baseUrl?>/images/feedback-1.png"/>
									</div>
									<div class="descholder">									
										<p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
									</div>
								</div>
							</div>
							<div class="col m4 col s12 wow slideInUp">
								<div class="feedbackbox">
									<div class="imgholder">
										<img src="<?=$baseUrl?>/images/feedback-2.PNG"/>
									</div>
									<div class="descholder">									
										<p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
									</div>
								</div>
							</div>
							<div class="col m4 col s12 wow slideInU hidden-xs">
								<div class="feedbackbox">
									<div class="imgholder">
										<img src="<?=$baseUrl?>/images/feedback-3.PNG"/>
									</div>
									<div class="descholder">									
										<p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footertag">
               <div class="footer-cols">
                  <div class="container">
                     <div class="row">
                        <div class="col m6 s12">
                           <h5>Email</h5>
                           <i class="mdi mdi-email white-text"></i>
                           <p>
                              General: <a href="mailto:office@yoursite.com" title="">office@Arabiaface.com</a>
                              <br />
                              Support: <a href="mailto:support@example.com" title="">support@Arabiaface.com</a>
                           </p>
                        </div>
                        <div class="col m6 s12">
                           <h5 class="">Follow</h5>
                           <i class="mdi mdi-facebook"></i> &nbsp; &nbsp;
                           <i class="mdi mdi-twitter"></i>
                           <p>
                              <a href="www.facebook.com" target="_blank" title="">Find us on Facebook</a>
                              <br />
                              <a href="www.twitter.com" target="_blank" title="">Get us on Twitter</a>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="copyright">
                  <div class="container">
                     <p>&copy; Arabiaface 2019. All rights reserved.</p>
                  </div>
               </div>
            </footer>
        </div>
	</div>
	
	<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq">
		<?php include('../views/layouts/mapmodal.php'); ?>
	</div>
	 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

	<?php include('../views/layouts/commonjs.php'); ?>
	<script src="<?=$baseUrl?>/js/loginsignup.js" type="text/javascript"></script>
	<script src="<?=$baseUrl?>/js/wow.min.js"></script>
	<script type="text/javascript" src="<?=$baseUrl?>/js/homepage.js"></script>
    