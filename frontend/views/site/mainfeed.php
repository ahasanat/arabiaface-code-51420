<?php  
use frontend\assets\AppAsset;
use backend\models\Googlekey; 
$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session; 
$email = $session->get('email'); 
$status = $session->get('status');
$fullname = $session->get('fullname'); 
$user_id = $session->get('user_id');  
$this->title = 'Discussion';
$data = array('id' => (string)$user_id, 'email'=> $email, 'fullname' => $fullname);
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>

<script src="<?=$baseUrl?>/js/chart.js"></script>
    <div class="page-wrapper">
        <div class="header-section">
            <?php include('../views/layouts/header.php'); ?>
        </div> 
        <div class="floating-icon">
			<div class="scrollup-btnbox anim-side btnbox scrollup-float">
                <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>          
            </div>            
        </div>
        <div class="clear"></div>
		<div class="container page_container">
			<?php include('../views/layouts/leftmenu.php'); ?>
			<div class="fixed-layout">
				<?php 
				if(isset($status) && $status == '0'){ ?>
				<div class="unreg-modal unreg-notice notice">
					<div class="status-note"><div class="success-note">Confirmation link sent successfully!</div><div class="error-note">Error occured. Please try again!</div></div>

					<div class="icon-holder"><i class="mdi mdi-information-variant"></i></div>
					<div class="desc-holder">       
						You are almost there! Please confirm your email address.Check your <?=$email?> account or <a onclick="accountVerify(1)" href="javascript:void(0)">request a new confirmation link</a>
					</div>
					<div class="loading"></div>
				</div>
				<?php } ?>
				<!-- Unverified User -->
				<div class="main-content with-lmenu fullmobile feedpage">
					<div class="post-column mr-top">
						  <?php include('../views/layouts/postwall.php'); ?>
					</div> 
					<div class="scontent-column">
						<?php include('../views/layouts/people_you_may_know.php'); ?>
						<?php include('../views/layouts/people_view_you.php'); ?>
						<?php include('../views/layouts/recently_joined.php'); ?>   
						<?php include('../views/layouts/travads.php'); ?> 
					</div>
					<div id="chatblock">
						<div class="float-chat anim-side">
							<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
							</div>
						</div>
					</div>
				</div>
				<div class="new-post-mobile clear">
					<a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
				</div> 
			</div>
		</div>
		<?php include('../views/layouts/footer.php'); ?>
    </div>  
 
	<input type="hidden" name="pagename" id="pagename" value="feed" />
	<input type="hidden" name="tlid" id="tlid" value="<?=(string)$user_id?>" />
	
    <div id="compose_tool_box" class="modal compose_tool_box post-popup custom_modal main_modal">
    </div> 
	 
	<div id="composeeditpostmodal" class="modal compose_tool_box edit_post_modal post-popup main_modal custom_modal compose_edit_modal">
    </div>
	
	<div id="sharepostmodal" class="modal sharepost_modal post-popup main_modal custom_modal">
	</div>
	
	<!-- Post detail modal -->
	<div id="postopenmodal" class="modal modal_main compose_tool_box custom_modal postopenmodal_main postopenmodal_new">	
	</div>
	
	<!--post comment modal for xs view-->
	<div id="comment_modal_xs" class="modal comment_modal_xs">
	</div>  
    <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
		<?php include('../views/layouts/mapmodal.php'); ?>
	</div>
    <?php include('../views/layouts/addpersonmodal.php'); ?>

    <?php include('../views/layouts/custom_modal.php'); ?>
    <?php include('../views/layouts/shareongroupwall.php'); ?>

    <?php include('../views/layouts/editphotomadol.php'); ?> 
    
   
   	<div id="upload-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

	<div id="edit-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

	<div id="userwall_tagged_users" class="modal modalxii_level1">
		<div class="content_header">
			<button class="close_span waves-effect">
				<i class="mdi mdi-close mdi-20px"></i>
			</button>
			<p class="selected_photo_text"></p>
			<a href="javascript:void(0)" class="chk_person_done_new done_btn focoutTRV03 action_btn">Done</a>
		</div>
		<nav class="search_for_tag">
			<div class="nav-wrapper">
			  <form>
			    <div class="input-field">
			      <input id="tagged_users_search_box" class="search_box" type="search" required="">
			        <label class="label-icon" for="tagged_users_search_box">
			          <i class="zmdi zmdi-search mdi-22px"></i>
			        </label>
			      </div>
			  </form>
			</div>
		</nav>
		<div class="person_box"></div>
	</div>
	
    <script type="text/javascript">
    var data2 = <?php echo json_encode($data);?>;
    var data1 = <?php echo json_encode($usrfrdlist); ?>;
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

    <?php include('../views/layouts/commonjs.php'); ?>
    
    <script src="<?=$baseUrl?>/js/post.js"></script>
    
	<script type="text/javascript">
		$(document).ready(function() {
			justifiedGalleryinitialize();
    		lightGalleryinitialize();
		});
	</script>
<?php $this->endBody() ?> 