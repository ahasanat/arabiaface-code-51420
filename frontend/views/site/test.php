<?php  
use frontend\assets\AppAsset;
use backend\models\Googlekey;
$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session; 
$email = $session->get('email'); 
$status = $session->get('status');
$fullname = $session->get('fullname'); 
$user_id = $session->get('user_id');  
$this->title = 'Discussion';
$data = array('id' => (string)$user_id, 'email'=> $email, 'fullname' => $fullname);
$GApiKeyL = $GApiKeyP = Googlekey::getkey(); 
?>

<script src="<?=$baseUrl?>/js/chart.js"></script>
<script type="text/javascript">
var data2 = <?php echo json_encode($data);?>;
var data1 = <?php echo json_encode($usrfrdlist); ?>;
</script>

<script type="text/javascript" src="<?=$baseUrl?>/js/modal.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/croppie.min.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/custom-functions.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/custom-handler.js"></script>     
<script type="text/javascript" src="<?=$baseUrl?>/js/tooltipster.bundle.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/jquery.nicescroll.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/datepicker.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/custom_user_modal.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/shareongroupwall.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/jquery.justifiedGallery.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/picturefill.min.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/lightgallery-all.min.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/custom_light_justify.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/chat.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/emoticons.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/custom-emotions.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/emostickers.js"></script> 
<script type="text/javascript" src="<?=$baseUrl?>/js/custom-emostickers.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/messages-function.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/messages-handler.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/socket.io.js"></script>
<?php $this->endBody() ?> 