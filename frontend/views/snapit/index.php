<?php   
use frontend\assets\AppAsset;
use backend\models\Googlekey;
$session = Yii::$app->session;
$user_id = $session->get('user_id');
$this->title = 'Snapit';
$baseUrl = AppAsset::register($this)->baseUrl;
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
<link href="<?=$baseUrl?>/css/animate.css" rel="stylesheet">
<link href="<?=$baseUrl?>/css/custom-croppie.css" rel="stylesheet">
<link href="<?=$baseUrl?>/css/owl.carousel.css" rel="stylesheet" type="text/css"> 
<link href="<?=$baseUrl?>/css/jquery-gauge.css" type="text/css" rel="stylesheet">   
<link href="<?=$baseUrl?>/css/animate.css" rel="stylesheet">
<link href="<?=$baseUrl?>/css/nouislider.css" rel="stylesheet">
<link href="<?=$baseUrl?>/css/demo-cover.css" type="text/css" media="screen" rel="stylesheet" />


<div class="page-wrapper snapitpage">
    <div class="header-section">
        <?php include('../views/layouts/header.php'); ?>
    </div>
    <div class="floating-icon">
        <div class="scrollup-btnbox anim-side btnbox scrollup-float">
            <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>			
        </div>        
    </div>
    <div class="clear"></div>
	<div class="container page_container event_container">
		<?php include('../views/layouts/leftmenu.php'); ?>
	   <div class="fixed-layout ipad-mfix">
      <div class="main-content with-lmenu pages-page main-page grid-view general-page">
         <div class="combined-column combined_md_column">
            <div class="content-box nbg">	
               	  <?php include('searcharea.php'); ?>
                  <div class="tab-content view-holder grid-view" style="margin: 0;">
                     <div class="tab-pane fade active in main-pane" id="pages-suggested" style="padding-top: 0;">
                        <div class="pages-list generalbox-list all-list">
                           <div class="clear"></div>
                           <div class="row">
                              <div class="placesphotos-content subtab places-discussion-main bottom_tabs">
                                 <div class="cbox-desc gallery-content">
                                    <div class="gloader" style="display: none;"><img src="<?=$baseUrl?>/images/loading.gif" class="g-loading"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none" id="pages-liked" style="padding-top: 0;">
                        <div class="pages-list generalbox-list liked-list">
                           <div class="clear"></div>
                           <div class="row">
                              <div class="placesphotos-content subtab places-discussion-main bottom_tabs">
                                 <div class="cbox-desc gallery-content">
                                    <div class="gloader" style="display: none;"><img src="<?=$baseUrl?>/images/loading.gif" class="g-loading"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane pages-yours dis-none" id="pages-yours" style="padding-top: 0;">
                        <div class="pages-list generalbox-list admin-list">
                           <div class="row">
                              <div class="placesphotos-content subtab places-discussion-main bottom_tabs">
                                 <div class="cbox-desc gallery-content">
                                    <div class="gloader" style="display: none;"><img src="<?=$baseUrl?>/images/loading.gif" class="g-loading"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none" id="pages-other" style="padding-top: 0;">
                        <div class="pages-list generalbox-list liked-list">
                           <div class="clear"></div>
                           <div class="row">
                              <div class="placesphotos-content subtab places-discussion-main bottom_tabs">
                                 <div class="cbox-desc gallery-content">
                                    <div class="gloader" style="display: none;"><img src="<?=$baseUrl?>/images/loading.gif" class="g-loading"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="chatblock">
			<div class="float-chat anim-side">
				<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
				</div>
			</div>
		</div>
      </div>
   </div>
   <?php include('../views/layouts/footer.php'); ?>
	</div>
</div>

<div id="upload-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<div id="userwall_tagged_users" class="modal modalxii_level1">
  <div class="content_header">
    <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px"></i>
    </button>
    <p class="selected_photo_text"></p>
    <a href="javascript:void(0)" class="chk_person_done_new done_btn focoutTRV03 action_btn">Done</a>
  </div>
  <nav class="search_for_tag">
    <div class="nav-wrapper">
      <form>
        <div class="input-field">
          <input id="tagged_users_search_box" class="search_box" type="search" required="">
            <label class="label-icon" for="tagged_users_search_box">
              <i class="zmdi zmdi-search mdi-22px"></i>
            </label>
          </div>
      </form>
    </div>
  </nav>
  <div class="person_box"></div>
</div>


<div id="edit-gallery-popup" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose upload-gallery-popup"></div>

<?php include('../views/layouts/commonjs.php'); ?>

<!-- <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq">
  <?php include('../views/layouts/mapmodal.php'); ?>
</div>
 -->
<script type="text/javascript" src="<?=$baseUrl?>/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/waterfall-light.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/jquery-gauge.min.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/loader.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/chart.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/post.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/nouislider.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
    snapitgallleryRecent();
		justifiedGalleryinitialize();
    lightGalleryinitializeforgallery();
	});

	$(document).on('input', '#search', function() {
		$value = $(this).val().trim();
		$.ajax({
            type: 'POST',
            url: "?r=snapit/getsnapitsbysearch", 
            data: {value: $value},
            success: function (data) {
            	console.log(data);
            	$('.lazyloadgallerybox').removeClass('lazyloadgallerybox');

            	$('.fake-title-area').find('.tab').find('a').removeClass('active');

            	$('.tab-content.view-holder.grid-view').find('.tab-pane.main-pane').removeClass('active in');
            	$('.tab-content.view-holder.grid-view').find('.tab-pane.main-pane').addClass('dis-none');
            	$('#pages-other').removeClass('dis-none').addClass('active in');

			    
		    	$('#pages-other').find('.gallery-content').html(data);
	 			setTimeout(function(){ 
		 			justifiedGalleryinitialize();
		 			lightGalleryinitializeforgallery();
	    		},500);
	 		}
        });
	});

	$(document).on('click', 'a[href="#pages-suggested"]', function(){
		snapitgallleryRecent();
	});

	$(document).on('click', 'a[href="#pages-liked"]', function(){
		snapitgallleryPopular();
	});

	$(document).on('click', 'a[href="#pages-yours"]', function(){
		snapitgallleryYour();
	}); 

	$(document).on('click', '#categoriesarea_drp li', function(e){
		var category = $(this).find('a').html().trim();
		if(category != '') {
			$.ajax({   
			    url: '?r=snapit/getcategorysnapits',  
			    type: 'POST',
                data: {category: category},
                success: function(data) {
                	$('#search').val('');
                	if($('#pages-suggested').hasClass('active')) {
                		$('#pages-suggested').find('.gallery-content').html(data);
                	} else if ($('#pages-liked').hasClass('active')) {
                		$('#pages-suggested').find('.gallery-content').html(data);
                	} else if ($('#pages-yours').hasClass('active')) {
                		$('#pages-yours').find('.gallery-content').html(data);
                	} else {
                		$('.fake-title-area').find('.tabs').find('li:first').find('a').click();
                		setTimeout(function(){ 
                			$('#pages-suggested').find('.gallery-content').html(data);
			    		},200);
                	}
		 			justifiedGalleryinitialize();
		 			lightGalleryinitializeforgallery();
			    }
			});	
		}
	}); 

  function removesnapit(obj)
  { 
    $this = $(obj);
    var $id = $this.attr('data-id');
    if($id) {   
      $()
      $.ajax({
        type: 'POST', 
          url: '?r=snapit/removesnapit',  
          data: {$id},
          success: function(data) {
          var result = $.parseJSON(data);
          if(result.success != undefined && result.success == true) {
              Materialize.toast('Deleted', 2000, 'green');
              $this.parents('.allow-gallery').remove();

              justifiedGalleryinitialize();
              lightGalleryinitializeforgallery();
              
              if($('.tab-pane.main-pane.active').find('.allow-gallery').length <=0) {
                $('.tab-pane.main-pane.active').find('.gallery-content').html('<div class="content-box bshadow"> <div class="post-holder bshadow"> <div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>No pinned photos</p> </div> </div> </div>');
              }
            }
          }
      });
    }
  }

	function snapitgallleryRecent()
	{	 
		$.ajax({   
		    url: '?r=snapit/fetchgalleryrecent',  
		    success: function(data) {
		    	$('.lazyloadgallerybox').removeClass('lazyloadgallerybox');
        	$('#pages-suggested').find('.gallery-content').html(data);
			    
  	 			justifiedGalleryinitialize();
  	 			lightGalleryinitializeforgallery();
		    }
		});
	}

	function snapitgallleryPopular()
	{	
		$.ajax({   
		    url: '?r=snapit/fetchgallerypopular',  
		    success: function(data) {
		    	$('.lazyloadgallerybox').removeClass('lazyloadgallerybox');
		    	$('#pages-liked').find('.gallery-content').html(data);
			    
	 			justifiedGalleryinitialize();
	 			lightGalleryinitializeforgallery();
		    }
		});
	}

	function snapitgallleryYour()
	{	
		$.ajax({   
		    url: '?r=snapit/fetchgalleryyour',  
		    success: function(data) {
		    	$('.lazyloadgallerybox').removeClass('lazyloadgallerybox');
		    	$('#pages-yours').find('.gallery-content').html(data);
			    
	 			justifiedGalleryinitialize();
	 			lightGalleryinitializeforgallery();
		    }
		});
	}
</script>
