<div class="cbox-desc md_card_tab">
  <div class="row subheader_i subheader_first">
    <div class="col s8 m6 l5 upload_share_labelarea">
		<h5>Upload and share photo</h5>
	</div>
	<div class="col s7 l4 searcharea">
		<div class="searchbox">
		   <input type="text" id="search" placeholder="Search for photos by title">
		   <i class="mdi mdi-magnify"></i>
		</div>
	</div>
  <div class="col s4 m6 l3 right-align uploadarea">
		<i class="zmdi zmdi-cloud-upload"></i>&nbsp;&nbsp;<a class='dropdown-button btn upload-gallery' href='javascript:void(0)'><span>UPLOAD</span></a>
	</div>
</div>

<div class="row subheader_i subheader_second">
  <div class="col s12 m6 l8 tabarea">
    	<div class="fake-title-area divided-nav mobile-header">
			<ul class="tabs">
				<li class="tab col s3"><a class="active" href="#pages-suggested" data-toggle="tab" aria-expanded="false">Recent</a></li>
				<li class="tab col s3"><a href="#pages-liked" data-toggle="tab" aria-expanded="false">Popular</a></li>
				<li class="tab col s3"><a href="#pages-yours" data-toggle="tab" aria-expanded="false">Your</a></li>
			</ul> 
    	</div>
 	</div>
  <div class="col s12 m6 l4 right-align categoriesarea">
    	<a href="javascript:void(0)" class="dropdown-button more_btn" data-activates='categoriesarea_drp'>All categories <i class="mdi mdi-chevron-down"></i></a>
        <ul id='categoriesarea_drp' class='dropdown-content custom_dropdown'>
          <li><a href="javascript:void(0)">Animals</a></li>
          <li><a href="javascript:void(0)">Celebrities</a></li>
          <li><a href="javascript:void(0)">City & Architecture</a></li>
          <li><a href="javascript:void(0)">Concert</a></li>
          <li><a href="javascript:void(0)">Family</a></li>
          <li><a href="javascript:void(0)">Fashion</a></li>
          <li><a href="javascript:void(0)">Film</a></li>
          <li><a href="javascript:void(0)">Fine Art</a></li>
          <li><a href="javascript:void(0)">Food</a></li>
          <li><a href="javascript:void(0)">Landscapes</a></li>
          <li><a href="javascript:void(0)">Nature</a></li>
          <li><a href="javascript:void(0)">People</a></li>
          <li><a href="javascript:void(0)">Sport</a></li>
          <li><a href="javascript:void(0)">Still Life</a></li>
          <li><a href="javascript:void(0)">Transportation</a></li>
          <li><a href="javascript:void(0)">Travel</a></li>
          <li><a href="javascript:void(0)">Underwater</a></li>
          <li><a href="javascript:void(0)">Urban</a></li>
          <li><a href="javascript:void(0)">Wedding</a></li>
        </ul>
 	</div>
</div>
</div>