<?php
use yii\helpers\Url;
use frontend\assets\AppAsset;

use frontend\models\PinImage;
use frontend\models\Comment; 
use frontend\models\Like;
use frontend\models\PostForm;
use frontend\models\LoginForm;
use frontend\models\Snapit;

$session = Yii::$app->session;
$baseUrl = AppAsset::register($this)->baseUrl;
$user_id = (string)$session->get('user_id');
$fullname = $this->context->getuserdata($user_id,'fullname');
$isEmpty = true;
$allSnapit = Snapit::getallsnapitYour($user_id);
?>
<div id="lgt-gallery-photoGallery" class="lgt-gallery-photoGallery lgt-gallery-justified justified-gallery">
<?php
if(!empty($allSnapit)) {
   foreach ($allSnapit as $Snapit) {
      $image = $Snapit['image']; 
      if(file_exists($image)) {
         $snapit_item_id = (string)$Snapit['_id'];
         $eximg = $image;
         $inameclass = preg_replace('/\\.[^.\\s]{3,4}$/', '', $image);
         $snapit_user_id = $Snapit['user_id']; 
         $title = $Snapit['title']; 
         $description = $Snapit['description']; 
         $location = $Snapit['location']; 
         $tagged_friends = $Snapit['tagged_friends']; 
         $visible_to = $Snapit['visible_to']; 
         $categories = $Snapit['categories']; 
         $store = $Snapit['store']; 
         $place = $Snapit['place']; 
         $placetitle = $Snapit['placetitle']; 
         $created_at = $Snapit['created_at']; 
         $like_count = Like::getLikeCount($snapit_item_id);
         $comments = Comment::getAllPostLikeCount($snapit_item_id);

         $like_active = Like::find()->where(['post_id' => $snapit_item_id,'status' => '1','user_id' => $user_id])->one();
         if(!empty($like_active)) {
            $like_active = 'active';
            $likeIcon = 'mdi-thumb-up';
         } else {
            $like_active = '';
            $likeIcon = 'mdi-thumb-up-outline';
         }

         $time = Yii::$app->EphocTime->comment_time(time(),$created_at);
         $like_buddies = Like::getLikeUser($inameclass .'_'. $snapit_item_id);
         $newlike_buddies = array();
         foreach($like_buddies as $like_buddy) {
            $newlike_buddies[] = ucwords(strtolower($like_buddy['fullname']));
         }
         $newlike_buddies = implode('<br/>', $newlike_buddies);  
         $isEmpty = false;
         ?>
         <div id="photosGallery<?=$snapit_item_id?>" data-src="<?=$eximg?>" class="allow-gallery" data-sizes="<?=$snapit_item_id?>|||Snapit">
            <img class="himg" src="<?=$eximg?>"/>  
            <a href="javascript:void(0)" class="removeicon prevent-gallery" data-id="<?=$snapit_item_id?>" onclick="removesnapit(this)"><i class="mdi mdi-delete"></i></a>
            <div class="caption">
               <div class="pull-left">
                  <span class="title"><?=$title?> ( <?=$time?> )</span> <br>
                  <span class="attribution">By <?=$fullname?></span>
               </div>
               <div class="right icons">
                  <a href="javascript:void(0)" class="prevent-gallery like custom-tooltip pa-like liveliketooltip liketitle_<?=$snapit_item_id?> <?=$like_active?>" onclick="doLikeAlbumbImages('<?=$snapit_item_id?>');" data-title="<?=$newlike_buddies?>">
                     <i class="mdi mdi-15px <?=$likeIcon?>"></i>
                  </a>
                  <?php if($like_count >0 ) { ?>
                     <span class="likecount_<?=$snapit_item_id?> lcount"><?=$like_count?></span>
                  <?php } else { ?>
                     <span class="likecount_<?=$snapit_item_id?> lcount"></span>
                  <?php } ?>

                  <a href="javascript: void(0)" class="prevent-gallery">
                     <i class="mdi mdi-comment-outline mdi-15px cmnt"></i>
                  </a>
                  <?php if($comments > 0){ ?>
                     <span class="lcount commentcountdisplay_<?=$snapit_item_id?>"><?=$comments?></span>
                  <?php } else { ?>
                     <span class="lcount commentcountdisplay_<?=$snapit_item_id?>"></span>
                  <?php } ?>
               </div>
            </div>
         </div>
         <?php
      } 
   }
}

if($isEmpty) { ?>
    <div class="content-box bshadow">            
        <?php $this->context->getnolistfound('nopinnedphotos'); ?>
    </div>
<?php }
?>
</div>
<?php
exit;

?>