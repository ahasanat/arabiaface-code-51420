<?php 
use frontend\assets\AppAsset; 
use yii\helpers\Url;
use frontend\models\Friend;
use frontend\models\SecuritySetting;
use frontend\models\MuteFriend; 
use frontend\models\LoginForm; 
$session = Yii::$app->session;
$uid = (string)$session->get('user_id');
$isEmpty = true;
$baseUrl = AppAsset::register($this)->baseUrl;

if(isset($data) && !empty($data)) { 
	foreach ($data as $key => $sdata) {	
		$sId = (string)$sdata['_id'];
		$uniqKey = rand(9999, 9999999) . $sId;
		if($sId == $uid) {
			continue;
		}
		$thumbnail = $this->context->getimage($sId, 'photo');            
        $name = $sdata['fullname'];
        $city = isset($sdata['city']) ? $sdata['city'] : '';
        $country  = isset($sdata['country']) ? $sdata['country'] : '';
        $address = $city.', '.$country;
        $address = trim($address);
		$address = explode(",", $address);
        $address = array_filter($address);
		if(count($address) >1) {
	        $first = reset($address);
	        $last = end($address);
	        $address = $first.', '.$last;
		} else {
			$address = implode(", ", $address);
		}

        $label = 'Lives in ';
        if($address != '') { 
        	$label  = 'Lives in '. $address;
        }
		
		$friendinfo = LoginForm::find()->select(['_id','fname','lname','country'])->where(['_id' => $sId])->one();
		$friendid = (string)$friendinfo['_id'];
		$isfriend = Friend::find()->select(['_id'])->where(['from_id' => "$friendid",'to_id' => "$uid",'status' => '1'])->one();
		$isfriendrequestsent = Friend::find()->select(['_id'])->where(['from_id' => "$friendid",'to_id' => "$uid",'status' => '0'])->one();

        $isvip = false;
        if(isset($sdata['vip_flag']) && $sdata['vip_flag'] != '0' && $sdata['vip_flag'] != '') {
        	$isvip = true;
        }

        $ctr = Friend::mutualfriendcount($friendid);
        $result_security = SecuritySetting::find()->where(['user_id' => $friendid])->asarray()->one();
        $friend_list = isset($result_security['friend_list']) ? $result_security['friend_list'] : '';
		$mutualLabel =  'No Mutual Friends';
		if($friend_list == 'Public') {
			$totalfriends = Friend::find()->where(['to_id' => (string)$friendid, 'status' => '1'])->count();
			if($totalfriends>1) {
				$mutualLabel = $totalfriends .' Friends';
			} else if($totalfriends == 1) {
				$mutualLabel = '1 Friend';
			} else {
				$mutualLabel = 'No Friend';
			}
		} else if($friend_list == 'Private') {
			if($ctr >0) {
				$mutualLabel =  $ctr.' Mutual Friends';
			} else {
				$mutualLabel =  'No Mutual Friends';
			}
		} else if($friend_list == 'Friends') {
			if(!empty($isfriend)) {
				if($totalfriends>1) {
					$mutualLabel = $totalfriends .' Friends';
				} else if($totalfriends == 1) {
					$mutualLabel = '1 Friend';
				} else {
					$mutualLabel = 'No Friend';
				}	
			} else {
				if($ctr >0) {
					$mutualLabel =  $ctr.' Mutual Friends';
				} else {
					$mutualLabel =  'No Mutual Friends';
				}
			}
		}

		$isEmpty = false;
		?>
		<input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
		<div class="col s12 m3 l3 xl3 <?=$isvip?>">
			<div class="person-box"> 
				<div class="imgholder">
					<img src="<?=$thumbnail?>" class="main-img"/>
					<div class="overlay">
					<?php if($isvip == true) { ?>													
						<div class="vip-span"><img src="<?=$baseUrl?>/images/vip-tag.png"/></div>
					<?php } ?>
					<?php  
					if(!$isfriend) {
						if(!$isfriendrequestsent) { ?>
							<div class="add-span add-icon_<?=$sId?>">
								<a href="javascript:void(0);" title="Add friend" class="add-icon request_btn directcheckuserauthclass gray-text-555" onclick="addFriend('<?=$sId?>')"><i class="mdi mdi-account-plus"></i></a>
								<a href="javascript:void(0)" class="add-btn btn btn-primary directcheckuserauthclass" onclick="addFriend('<?=$sId?>')">Add friend</a>
							</div>
						<?php } else { ?>
							<div class="add-span add-icon_<?=$sId?>">
								<a href="javascript:void(0);" title="Cancle friend request" class="add-icon directcheckuserauthclass request_btn gray-text-555" onclick="removeFriend('<?=$sId?>','<?=$uid?>','<?=$sId?>','cancle_friend_request')"><i class="mdi mdi-account-minus"></i></a>

								<a href="javascript:void(0)" class="add-btn btn btn-primary directcheckuserauthclass" onclick="addFriend('<?=$sId?>')">Add friend</a>
							</div>		
						<?php }
					} else { ?>
						<div class="add-span add-icon_<?=$sId?>"> 
							<div class="dropdown dropdown-custom">
								<a href="javascript:void(0)" class="dropdown-button more_btn <?=$checkuserauthclass?> directcheckuserauthclass gray-text-555" data-activates="<?=$uniqKey?>" onclick="fetchfriendmenu('<?=$sId?>')"><i class="mdi mdi-chevron-down"></i></a>
								<?php 
								if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { ?>
								<ul id="<?=$uniqKey?>" class="dropdown-content custom_dropdown fetchfriendmenu">
								</ul>
								<?php } ?>
							</div>	
						</div>
					<?php } ?>
						<div class="more-span mobile-none-who">
							<div class="dropdown dropdown-custom">
								<a href="javascript:void(0)" class="dropdown-button more_btn <?=$checkuserauthclass?> directcheckuserauthclass gray-text-555" data-activates="<?=$uniqKey?>" onclick="fetchfriendmenu('<?=$sId?>')"><i class="mdi mdi-chevron-down"></i></a>
								<?php 
								if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { ?>
								<ul id="<?=$uniqKey?>" class="dropdown-content custom_dropdown fetchfriendmenu">
								</ul>
								<?php } ?>
							</div>																	
						</div>
					</div>
				</div>
				<div class="descholder">
					<h5><a href="<?php echo Url::to(['userwall/index', 'id' => $sId]); ?>">
						<span class="etext" style="color: #000;"><?=$name?></span>
						<?php 
						if(array_key_exists($sId, $online)) { ?>
							<span class="online-dot"><i class="zmdi zmdi-check"></i></span>
						<?php } ?>
					</a></h5>
					<p><?=$label?></p>
					<p class="info"><?=$mutualLabel?></p>
				</div>
			</div>
		</div>
		<?php }
	} 
	if($isEmpty == true) { 
		$this->context->getnolistfound('norecordfound');
	} 
exit;?>
