<?php
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl;
?>
<div class="text-center fullwidth">
	<div class="groupCard animated fadeInUp">
		<div class="find-friends">
			<h3>Invite your friends</h3>
			<h5>Get started by choosing a service provider</h5>
			<div class="clear"></div>
			<ul class="providers">
				<li>
           <span class="imgholder"><img src="<?=$baseUrl?>/images/provider-fb.png"/></span>
           Facebook
           <a href="javascript:void(0)" class="btn btn-gray popup-modal suggest-friends">Invite</a>
        </li>
        <li>
           <span class="imgholder"><img src="<?=$baseUrl?>/images/provider-gp.png"/></span>
           Google+
           <a href="javascript:void(0)" class="btn btn-gray popup-modal suggest-friends">Invite</a>
        </li>
        <li>
           <span class="imgholder"><img src="<?=$baseUrl?>/images/provider-yahoo.png"/></span>
           Yahoomail
           <a href="javascript:void(0)" class="btn btn-gray popup-modal suggest-friends">Invite</a>
        </li>
			</ul>
		</div>
	</div>
</div>
<?php exit;?>