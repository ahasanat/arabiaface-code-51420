<?php 
use frontend\assets\AppAsset; 
use yii\helpers\Url;
use frontend\models\UnfollowFriend;
use frontend\models\SecuritySetting;
use frontend\models\MuteFriend;
use frontend\models\Friend;
$session = Yii::$app->session;
$uid = (string)$session->get('user_id');            
$isEmpty = true;
$baseUrl = AppAsset::register($this)->baseUrl;

if(isset($data) && !empty($data)) { 
	foreach ($data as $key => $sdata) {		                      
	$idArray = array_values($sdata['_id']);
	$sId = $idArray[0];
	//$sId = (string)$sdata['_id']['$id'];
	$uniqKey = rand(9999, 9999999) . $sId;
	if($sId == $uid) {
		continue;
	} 

	$thumbnail = $this->context->getimage($sId, 'photo');            
    $name = $sdata['fullname'];
    $city = isset($sdata['city']) ? $sdata['city'] : '';
    $country  = isset($sdata['country']) ? $sdata['country'] : '';
    $address = $city.', '.$country;
    $address = trim($address);
	$address = explode(",", $address);
    $address = array_filter($address);
	if(count($address) >1) {
        $first = reset($address);
        $last = end($address);
        $address = $first.', '.$last;
	} else {
		$address = implode(", ", $address);
	}
	
    $label = 'Lives in ';
    if($address != '') {
    	$label  = 'Lives in '. $address;
    }

    $isvip = false;
    if(isset($sdata['vip_flag']) && $sdata['vip_flag'] != '0' && $sdata['vip_flag'] != '') {
    	$isvip = true;
    }

    $isBlocked = SecuritySetting::isBlocked($sId, $uid); 
    $isBlockedLabel = 'Block';
    if(isset($isBlocked['status']) && $isBlocked['status'] == true) {
    	$isBlockedLabel = 'Unblock';
    }
	
	$unfollowuser = UnfollowFriend::find()->where(['user_id' => "$uid"])->andwhere(['like','unfollow_ids',"$sId"])->one();
	if ($unfollowuser) {
		$folstatus = 'Unmute this person';
	} else {
		$folstatus = 'Mute this person';
	}
	
	$userexist = MuteFriend::find()->where(['user_id' => "$uid"])->one();
	if ($userexist) {
		if (strstr($userexist['mute_ids'], "$sId")) {
				$getnot = 'Unmute notifications'; 
		} else {     
				$getnot = 'Mute notifications';
		}
	} else {
		$getnot = 'Mute notifications';
	} 

	$ctr = Friend::mutualfriendcount($sId);
	$result_security = SecuritySetting::find()->where(['user_id' => $sId])->asarray()->one();
    $friend_list = isset($result_security['friend_list']) ? $result_security['friend_list'] : '';
	$mutualLabel =  'No Mutual Friends';
	if($friend_list == 'Public') {
		$totalfriends = Friend::find()->where(['to_id' => (string)$sId, 'status' => '1'])->count();
		if($totalfriends>1) {
			$mutualLabel = $totalfriends .' Friends';
		} else if($totalfriends == 1) {
			$mutualLabel = '1 Friend';
		} else {
			$mutualLabel = 'No Friend';
		}
	} else if($friend_list == 'Private') {
		if($ctr >0) {
			$mutualLabel =  $ctr.' Mutual Friends';
		} else {
			$mutualLabel =  'No Mutual Friends';
		}
	} else if($friend_list == 'Friends') {
		if($totalfriends>1) {
			$mutualLabel = $totalfriends .' Friends';
		} else if($totalfriends == 1) {
			$mutualLabel = '1 Friend';
		} else {
			$mutualLabel = 'No Friend';
		}
	}  
    $isEmpty = false;
?>
<input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
<div class="col s12 m3 l3 xl3 headbox <?=$isvip?>">
	<div class="person-box friend_<?=$sId?>">
		<div class="imgholder">    
			<img src="<?=$thumbnail?>" class="main-img"/>
			<div class="overlay">
				<?php if($isvip == true) { ?>
				<div class="vip-span"><img src="<?=$baseUrl?>/images/vip-tag.png"/></div>
					<?php } ?>
					<div class="add-span add-icon_<?=$sId?> dis-none">
					</div>																
					<div class="more-span">     
					<div class="dropdown dropdown-custom">
						<a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="<?=$uniqKey?>" onclick="fetchfriendmenu('<?=$sId?>')"><i class="mdi mdi-chevron-down"></i>
						</a>
						<ul id="<?=$uniqKey?>" class="dropdown-content custom_dropdown fetchfriendmenu">
						<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
					</div>																	
				</div>
			</div>
		</div>                                                                             
		<div class="descholder">
			<h5>   
				<a href="<?php echo Url::to(['userwall/index', 'id' => $sId]); ?>">
				<span class="etext" style="color: #000;"><?=$name?></span>
				<?php 
				if(array_key_exists($sId, $online)) { ?>
					<span class="online-dot"><i class="zmdi zmdi-check"></i></span>
				<?php } ?>
				</a>
			</h5>
			<p><?=$label?></p>
			<p class="info"><?=$mutualLabel?></p>
		</div>
	</div>
</div>
<?php }                   
} 
if($isEmpty == true) {
	$this->context->getnolistfound('norecordfound');
} 
exit;?>
