<?php   
use frontend\assets\AppAsset;
use backend\models\Googlekey;
 
$baseUrl = AppAsset::register($this)->baseUrl; 
$session = Yii::$app->session;
$user_id = $session->get('user_id'); 
$this->title = 'People';
$GApiKeyL = $GApiKeyP = Googlekey::getkey();
?>
<link href="<?=$baseUrl?>/css/animate.css" rel="stylesheet">
<link href="<?=$baseUrl?>/css/nouislider.css" rel="stylesheet">
<div class="page-wrapper gen-pages">
    <div class="header-section">
        <?php include('../views/layouts/header.php'); ?>
    </div>
    <div class="floating-icon">        
        <div class="scrollup-btnbox anim-side btnbox scrollup-float">
            <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span></div>          
        </div>           
    </div>
    <div class="clear"></div> 
    <div class="container page_container group_container">
		<?php include('../views/layouts/leftmenu.php'); ?>  
		<div class="fixed-layout ipad-mfix">
			<div class="main-content with-lmenu whoisaround-page general-page main-page">
				<div class="combined-column comsepcolumn">
					<div class="content-box nbg">
						<div class="cbox-desc md_card_tab">	
							<div class="fake-title-area divided-nav mobile-header">
								<ul class="tabs" id="maintabs">					
									<li class="tab" onclick="allwhoisaroundusers();"><a href="#whoaround-online">All People</a></li>
									<li onclick="allfriends();" class="tab"><a href="#whoaround-allfriends">Friends</a></li>
									<li class="find_friend tab"><a href="#whoaround-findfriends">Find Friends</a></li>
								</ul>
							</div>
							<div class="filters"> 
								<form id="allwhoisaroundusersfrm" class="howisform">
								<div class="friends-search shifted">
									<div class="fsearch-form closable-search">
										<input type="text" class="aroundsearch" id="whoisaroundsearchname" data-search="search" placeholder="Search for your friends"/>
										<a href="javascript:void(0)" class="gray-text-555"><i class="mdi mdi-magnify mdi-16px"></i></a>
									</div>
								</div>
								<div class="clear"></div>
								<div id="expandable-holderarea" class="filter-area expandable-holder custome-who">
									<a href="javascript:void(0)" class="expand-link gray-text-555" onclick="selectedDefaulttab();mng_expandable(this,'none');">
			                            <i class="mdi mdi-tune mdi-20px"></i>
									</a>
									<div class="main-collape">
										<div class="form-area expandable-area" style="overflow: visible !important;"> 
											<a href="javascript:void(0)" id="closelinkseararea" class="close-link" onclick="mng_expandable(this,'none');">DONE</a>
											<h6>Refine Results</h6>
											<form class="howisform">
												<div class="fitem">
													<div class="entertosend leftbox">
														<input type="checkbox" id="online" onclick="search();">
														<label for="online">Online</label>
													</div>
												</div>
												<div class="fitem">
													<div class="entertosend leftbox">
														<input type="checkbox" id="birthday" onclick="search();">
														<label for="birthday">Birthday</label>
													</div>
												</div>
												<div class="fitem age-filter">
													<label>Age</label>
													<div class="desc">
														<div class="range-slider">
															<div id="test-slider"></div>
															<input type="text" class="amount" readonly>
														</div>
													</div>
												</div>
												<div class="fitem selectholder">
													<label>Gender</label>
													<div class="custom-select">
														<select class="target" onchange="search();" id="gender">
															<option value="All">All</option>
															<option value="Male">Male</option>
															<option value="Female">Female</option>
														</select>
													</div>
												</div>
												<div class="fitem near-who">
													<label>Near by</label>
													<div class="desc">
														<div class="fsearch-form">
															<input type="text" id="searchLocation" data-query="M" onfocus="filderMapLocationModal(this)" autocomplete="off" placeholder="Type your location"/>
															<a href="javascript:void(0)"><i class="zmdi zmdi-search" onclick="search();"></i></a>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								</form>
							</div>
							<div class="tab-content view-holder grid-view custom-tab-who">												
								<div class="tab-pane fade main-pane active in whoaround-online-new" id="whoaround-online">
									<div class="animatedputed">
										<div class="person-list">
											<div class="row">
											</div>
										</div>
									</div> 
								</div>
								<div class="tab-pane fade main-pane" id="whoaround-allfriends">	
									<div class="animated fadeInUp">
										<div class="person-list">
											<div class="row">
											</div>
										</div>
									</div> 		
								</div>
								<div class="tab-pane fade main-pane" id="whoaround-findfriends">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="chatblock">
						<div class="float-chat anim-side">
							<div class="chat-button float-icon directcheckuserauthclass" onclick="getchatcontent();"><span class="icon-holder">icon</span>
							</div>
						</div>
					</div> 
			</div>
		</div>
	</div>
    <?php include('../views/layouts/footer.php'); ?>
</div>  
<div id="suggest-friends" class="modal tbpost_modal custom_modal split-page main_modal"></div>

<div id="giftlist_popup" class="modal  gift_modal  giftlist-popup">
		<div class="custom_message_modal_header">
			<p>Send gift</p>
			<button class="close_modal_icon waves-effect" onclick="gift_modal()">
				<i class="mdi mdi-close mdi-20px	"></i>
			</button>
		</div>
		<div class="gift_content">

			<div class="popup-content">
				<div class="emostickers-holder">
					<div class="emostickers-box">
						<div class="nice-scroll emostickers scroll_div" tabindex="8">
							<ul class="emostickers-list">
								<li>
									<a href="javascript:void(0)" data-class="(wine)" onclick="useGift('(wine)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-wine" title="wine">(wine)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(icecream)" onclick="useGift('(icecream)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-icecream" title="icecream">(icecream)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(coffee)" onclick="useGift('(coffee)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-coffee" title="coffee">(coffee)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(heart)" onclick="useGift('(heart)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-heart" title="heart">(heart)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(flower)" onclick="useGift('(flower)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-flower" title="flower">(flower)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(cake)" onclick="useGift('(cake)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-cake" title="cake">(cake)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(handshake)" onclick="useGift('(handshake)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-handshake" title="handshake">(handshake)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(gift)" onclick="useGift('(gift)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-gift" title="gift">(gift)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(goodmorning)" onclick="useGift('(goodmorning)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-goodmorning" title="goodmorning">(goodmorning)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(goodnight)" onclick="useGift('(goodnight)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-goodnight" title="goodnight">(goodnight)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(backpack)" onclick="useGift('(backpack)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-backpack" title="backpack">(backpack)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(parasailing)" onclick="useGift('(parasailing)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-parasailing" title="parasailing">(parasailing)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(train)" onclick="useGift('(train)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-train" title="train">(train)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(flipflop)" onclick="useGift('(flipflop)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-flipflop" title="flipflop">(flipflop)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(airplane)" onclick="useGift('(airplane)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-airplane" title="airplane">(airplane)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(sunbed)" onclick="useGift('(sunbed)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-sunbed" title="sunbed">(sunbed)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(happyhalloween)" onclick="useGift('(happyhalloween)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-happyhalloween" title="happyhalloween">(happyhalloween)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(merrychristmas)" onclick="useGift('(merrychristmas)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-merrychristmas" title="merrychristmas">(merrychristmas)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(eidmubarak)" onclick="useGift('(eidmubarak)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-eidmubarak" title="eidmubarak">(eidmubarak)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(happyyear)" onclick="useGift('(happyyear)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-happyyear" title="happyyear">(happyyear)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(happymothers)" onclick="useGift('(happymothers)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-happymothers" title="happymothers">(happymothers)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(happyfathers)" onclick="useGift('(happyfathers)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-happyfathers" title="happyfathers">(happyfathers)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(happyaniversary)" onclick="useGift('(happyaniversary)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-happyaniversary" title="happyaniversary">(happyaniversary)</span>
									</a>
								</li>
								<li>
									<a href="javascript:void(0)" data-class="(happybirthday)" onclick="useGift('(happybirthday)','popular','editmode', 'chat');">
										<span class="emosticker emosticker-happybirthday" title="happybirthday">(happybirthday)</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<span class="credits">100 Credits</span>
				</div>
			</div>
		</div>
</div>

<div id="usegift-popup" class="modal usegift_modal popup-area giftlist-popup giftslider-popup">
	<div class="edit-emosticker share-friends">
		<div class="custom_message_modal_header">
			<p style="border: none !important">
				Send a gift to <span class="to_gift">Vipul Patel</span>
			</p>
			<button class="close_modal_icon waves-effect" onclick="usegift_modal()">
			<i class="mdi mdi-close mdi-20px	"></i>
			</button>
		</div>
		<div class="usergift_content">
			<div class="scroll_div">
				<div class="popup-content">
					<div class="more-friends">
						<p class="send_to">Also send to</p>
						<a><i class="compose_addpersonActionShareWith zmdi zmdi-account-add custome-plus">person_add</i></a>
					</div> 

					<div class="carousel_position">
							<div class="carousel carousel-slider center" data-indicators="true">
								<div class="carousel-fixed-item center middle-indicator">
									<div class="left">
										<a href="Previo" class="movePrevCarousel middle-indicator-text waves-effect waves-light content-indicator"><i class="zmdi zmdi-chevron-left middle-indicator-text">chevron_left</i></a>
									</div>

									<div class="right">
										<a href="Siguiente" class=" moveNextCarousel middle-indicator-text waves-effect waves-light content-indicator"><i class="zmdi zmdi mdi-chevron-right middle-indicator-text">chevron_right</i></a>
									</div>
								</div>

								<div class="carousel-item white white-text" href="#one!" data-class='(wine)'>
									<img src="<?=$baseUrl?>/images/es-wine.png"/>
								</div>
								<div class="carousel-item white white-text" href="#two!" data-class='(icecream)'>
									<img src="<?=$baseUrl?>/images/es-icecream.png"/>
								</div>
								<div class="carousel-item white white-text" href="#three!" data-class='(coffee)'>
									<img src="<?=$baseUrl?>/images/es-coffee.png"/>
								</div>
								<div class="carousel-item white white-text" href="#four!" data-class='(heart)'>
									<img src="<?=$baseUrl?>/images/es-heart.png"/>
								</div>
								<div class="carousel-item white white-text" href="#five" data-class='(flower)'>
									<img src="<?=$baseUrl?>/images/es-flower.png"/>
								</div>
								<div class="carousel-item white white-text" href="#six!" data-class='(cake)'>
									<img src="<?=$baseUrl?>/images/es-cake.png"/>
								</div>
								<div class="carousel-item white white-text" href="#seven!" data-class='(handshake)'>
									<img src="<?=$baseUrl?>/images/es-handshake.png"/>
								</div>
								<div class="carousel-item white white-text" href="#eight!" data-class='(gift)'>
									<img src="<?=$baseUrl?>/images/es-gift.png"/>
								</div>
								<div class="carousel-item white white-text" href="#nine!" data-class='(goodmorning)'>
									<img src="<?=$baseUrl?>/images/es-goodmorning.png"/>
								</div>
								<div class="carousel-item white white-text" href="#ten!" data-class='(goodnight)'>
									<img src="<?=$baseUrl?>/images/es-goodnight.png"/>
								</div>
								<div class="carousel-item white white-text" href="#eleone!" data-class='(backpack)'>
									<img src="<?=$baseUrl?>/images/es-backpack.png"/>
								</div>
								<div class="carousel-item white white-text" href="#tewl!" data-class='(parasailing)'>
									<img src="<?=$baseUrl?>/images/es-parasailing.png"/>
								</div>
								<div class="carousel-item white white-text" href="#thirty!" data-class='(train)'>
									<img src="<?=$baseUrl?>/images/es-train.png"/>
								</div>
								<div class="carousel-item white white-text" href="#fourty!" data-class='(flipflop)'>
									<img src="<?=$baseUrl?>/images/es-flipflop.png"/>
								</div>
								<div class="carousel-item white white-text" href="#fifty!" data-class='(airplane)'>
									<img src="<?=$baseUrl?>/images/es-airplane.png"/>
								</div>
								<div class="carousel-item white white-text" href="#sixty!" data-class='(sunbed)'>
									<img src="<?=$baseUrl?>/images/es-sunbed.png"/>
								</div>
								<div class="carousel-item white white-text" href="#seventy!" data-class='(happyhalloween)'>
									<img src="<?=$baseUrl?>/images/es-happyhalloween.png"/>
								</div>
								<div class="carousel-item white white-text" href="#gihty!" data-class='(merrychristmas)'>
									<img src="<?=$baseUrl?>/images/es-merrychristmas.png"/>
								</div>
								<div class="carousel-item white white-text" href="#ninty!" data-class='(eidmubarak)'>
									<img src="<?=$baseUrl?>/images/es-eidmubarak.png"/>
								</div>
								<div class="carousel-item white white-text" href="#twenty!" data-class='(happyyear)'>
									<img src="<?=$baseUrl?>/images/es-happyyear.png"/>
								</div>
								<div class="carousel-item white white-text" href="#twentyon1!" data-class='(happymothers)'>
									<img src="<?=$baseUrl?>/images/es-happymothers.png"/>
								</div>
								<div class="carousel-item white white-text" href="#twentyon2!" data-class='(happyaniversary)'>
									<img src="<?=$baseUrl?>/images/es-happyaniversary.png"/>
								</div>
								<div class="carousel-item white white-text" href="#twentyon3!" data-class='(happybirthday)'>
									<img src="<?=$baseUrl?>/images/es-happybirthday.png"/>
								</div>
							</div>
						
					</div>					
					<div class="custom_textarea usergift_msg">
						<textarea maxlength="40" id="message" class="materialize-textarea" placeholder="Type your message here" data-length="40"></textarea>
					</div>					
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<div class="gift_send">
				<a href="javascript:void(0)" class="btn btn-primary btn-sm waves-effect waves-light" onclick="sendmessagewithgift()">Send</a>
			</div>
		</div>
	</div>
	<div class="preview-emosticker">
	</div>
</div>

<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq">
		<?php include('../views/layouts/mapmodal.php'); ?>
	</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$GApiKeyL?>&libraries=places&callback=initAutocomplete"></script>

<?php include('../views/layouts/commonjs.php'); ?>
<script src="<?=$baseUrl?>/js/nouislider.js" type="text/javascript" charset="utf-8" ></script>
<script src="<?=$baseUrl?>/js/custom-nouislider.js" type="text/javascript" charset="utf-8" ></script>
<script src='<?=$baseUrl?>/js/wNumb.min.js'></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/whoisaround.js"></script>
<script type="text/javascript" src="<?=$baseUrl?>/js/friend.js"></script>
 