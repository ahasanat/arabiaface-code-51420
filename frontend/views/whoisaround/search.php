<?php 
use frontend\assets\AppAsset; 
use yii\helpers\Url;
use frontend\models\Friend;
use frontend\models\SecuritySetting;
use frontend\models\MuteFriend;
use frontend\models\LoginForm; 
$session = Yii::$app->session;
$uid = (string)$session->get('user_id');
$isEmpty = true;
$baseUrl = AppAsset::register($this)->baseUrl;

if(isset($data) && !empty($data)) { 
	$onlineBulkIds = isset($post['onlineIdsList']) ? $post['onlineIdsList'] : array();
	$minAge = $post['age_min'];
	$maxAge = $post['age_max'];
	$whoisaroundsearchname = $post['whoisaroundsearchname'];
	$today = date('d-m-Y');
	$searchLocation = isset($post['searchLocation']) ? $post['searchLocation'] : '';
	$searchLocationArray = array();

	if($searchLocation != '') {
		$searchLocation = strtolower($searchLocation);
        $searchLocation = str_replace(" ", ",", $searchLocation);
        $searchLocation = str_replace(",,", ",", $searchLocation);
        $searchLocation = str_replace(" ", ",", $searchLocation);
        $searchLocationArray = explode(",", $searchLocation);
        $searchLocationArray = array_filter($searchLocationArray);
        $searchLocationArray = array_map('trim', $searchLocationArray);
	}

	$existAtleastOne = false;
	foreach ($data as $key => $sdata) {	
		$idArray = array_values($sdata['_id']);
		$sId = $idArray[0];
		//$sId = (string)$sdata['_id']['$id'];
		$name = $sdata['fullname'];
	
	$uniqKey = rand(9999, 9999999) . $sId;
	// check search by name fire or not..
	if($whoisaroundsearchname != '') {
		$fname = isset($sdata['fname']) ? $sdata['fname'] : '';
		$lname = isset($sdata['lname']) ? $sdata['lname'] : '';
		if (stripos($fname, $whoisaroundsearchname) === 0 || stripos($lname, $whoisaroundsearchname) === 0 || stripos($name, $whoisaroundsearchname) === 0) {
		} else {
			continue;
		}
	}

	// check online is checked or not............. 
	if(!empty($searchLocationArray)) {
		$city = isset($sdata['city']) ? $sdata['city'] : '';
		$country = isset($sdata['country']) ? $sdata['country'] : '';
		
		$mergeExistLocation = $city . ' '. $country;
		$mergeExistLocation = strtolower($mergeExistLocation);
        $mergeExistLocation = str_replace(" ", ",", $mergeExistLocation);
        $mergeExistLocation = str_replace(",,", ",", $mergeExistLocation);
        $mergeExistLocation = str_replace(" ", ",", $mergeExistLocation);

        $mergeExistLocation = explode(",", $mergeExistLocation);
        $mergeExistLocation = array_filter($mergeExistLocation);
        $mergeExistLocation = array_map('trim', $mergeExistLocation);
		foreach ($searchLocationArray as $key => $ssearchLocationArray) {
            if(!in_array($ssearchLocationArray, $mergeExistLocation)) {
                continue 2;
            }
        }
	}

	if($post['online'] == 'true') {
		if(!in_array($sId, $onlineBulkIds)) {
			continue;
		}
	}

	// check birthday is checked or not............
	if($post['birthday'] == 'true') {
		if(isset($sdata['birth_date']) && $sdata['birth_date'] != '') {
			if($today != $sdata['birth_date']) {
				continue;
			}
		} else {
			continue;
		}
	}

	if(isset($sdata['birth_date']) && $sdata['birth_date'] != '') {
		$birthDate = explode("-", $sdata['birth_date']);
		if(count($birthDate) == 3) {
			$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
			? ((date("Y") - $birthDate[2]) - 1)
			: (date("Y") - $birthDate[2]));
			if($age >= $minAge && $age <= $maxAge) {
			} else {
				continue;
			}
		} else {
			continue;
		}
	} else {
		continue;
	}

	if(isset($uid) && $uid != '') {
		if($sId == $uid) {
			continue;
		}
	}
	$thumbnail = $this->context->getimage($sId, 'photo');            
    $city = isset($sdata['city']) ? $sdata['city'] : '';
    $country  = isset($sdata['country']) ? $sdata['country'] : '';
    $address = $city.', '.$country;
    $address = trim($address);
	$address = explode(",", $address);
    $address = array_filter($address);
	if(count($address) >1) {
        $first = reset($address);
        $last = end($address);
        $address = $first.', '.$last;
	} else {
		$address = implode(", ", $address);
	}

    $label = 'Lives in ';
    if($address != '') {
    	$label  = 'Lives in '. $address;
    }
	/*
    $isFriendarray = Friend::isFriend($uid, $sId);
    $isFriend = false;
    $isFriendLabel = 'Add Friend';
    if(!empty($isFriendarray)) {
    	$isFriendLabel = ($isFriendarray['status'] == '' || $isFriendarray['status'] == '0') ? 'Cancel frined request' : 'Unfriend';
    	if($isFriendLabel == 'Unfriend') {
    		$isFriend = true;
    	} else {
    		$isFriend = 'CF';
    	}
    } */
	
	$friendinfo = LoginForm::find()->select(['_id','fname','lname','country'])->where(['_id' => $sId])->one();
	$friendid = (string)$friendinfo['_id'];
	$isfriend = Friend::find()->select(['_id'])->where(['from_id' => "$friendid",'to_id' => "$uid",'status' => '1'])->one();
	$isfriendrequestsent = Friend::find()->select(['_id'])->where(['from_id' => "$friendid",'to_id' => "$uid",'status' => '0'])->one();

    $isvip = false;
    if(isset($sdata['vip_flag']) && $sdata['vip_flag'] != '0' && $sdata['vip_flag'] != '') {
    	$isvip = true;
    }

    $isBlocked = SecuritySetting::isBlocked($sId, $uid);
    $isBlockedLabel = 'Block';
    if(isset($isBlocked['status']) && $isBlocked['status'] == true) {
    	$isBlockedLabel = 'Unblock';
    }

    $userexist = MuteFriend::find()->where(['user_id' => $uid])->one();
	if ($userexist) {
		if (strstr($userexist['mute_ids'], "$sId")) {
				$getnot = 'Unmute notifications';
		} else {
				$getnot = 'Mute notifications';
		}
	} else {
		$getnot = 'Mute notifications';
	}

	$ctr = Friend::mutualfriendcount($friendid);
	$result_security = SecuritySetting::find()->where(['user_id' => $friendid])->asarray()->one();
    $friend_list = isset($result_security['friend_list']) ? $result_security['friend_list'] : '';
	$mutualLabel =  '&nbsp;';
	$totalfriends = Friend::find()->where(['to_id' => (string)$friendid, 'status' => '1'])->count();
    if($friend_list == 'Public') {
        if($totalfriends>1) {
            $mutualLabel = $totalfriends .' Friends';
        } else if($totalfriends == 1) {
            $mutualLabel = '1 Friend';
        }
    } else if($friend_list == 'Private') {
        if($ctr >0) {
            $mutualLabel =  $ctr.' Mutual Friends';
        }
    } else if($friend_list == 'Friends') {
        if(!empty($isfriend)) {
            if($totalfriends>1) {
                $mutualLabel = $totalfriends .' Friends';
            } else if($totalfriends == 1) {
                $mutualLabel = '1 Friend';
            }   
        } else {
            if($ctr >0) {
                $mutualLabel =  $ctr.' Mutual Friends';
            }   
        }
    }

    $isEmpty = false;
?>
<div class="col s12 m3 l3 xl3 user <?=$isvip?>">
	<div class="person-box">
		<div class="imgholder">
			<img src="<?=$thumbnail?>" class="main-img">
			<div class="overlay">
			<?php if($isvip == true) { ?>																
			<div class="vip-span"><img src="<?=$baseUrl?>/images/vip-tag.png"/></div>
			<?php  } ?>
			
				<?php  if(!$isfriend) { ?>
					<?php  if(!$isfriendrequestsent) { ?>
					<div class="add-span add-icon_<?=$sId?>">
						<a href="javascript:void(0);" title="Add friend" class="add-icon request_btn directcheckuserauthclass gray-text-555" onclick="addFriend('<?=$sId?>')"><i class="mdi mdi-account-plus"></i></a>
						<a href="javascript:void(0)" class="add-btn btn btn-primary directcheckuserauthclass" onclick="addFriend('<?=$sId?>')">Add friend</a>
					</div>
					<?php } else { ?>
					<div class="add-span add-icon_<?=$sId?>">
						<a href="javascript:void(0);" title="Cancle friend request" class="add-icon request_btn directcheckuserauthclass gray-text-555" onclick="removeFriend('<?=$sId;?>','<?=$uid?>','<?=$sId?>','cancle_friend_request')"><i class="mdi mdi-account-minus"></i></a>
						<a href="javascript:void(0)" class="add-btn btn btn-primary directcheckuserauthclass" onclick="addFriend('<?=$sId?>')">Add friend</a>
					</div>		
					<?php } ?>
					<?php } else{ ?> 
					<div class="add-span add-icon_<?=$sId?>"> 
						<div class="dropdown dropdown-custom">
							<a href="javascript:void(0)" class="dropdown-button more_btn <?=$checkuserauthclass?> directcheckuserauthclass gray-text-555" data-activates="xsdc<?=$uniqKey?>" onclick="fetchfriendmenu('<?=$sId?>','yes', this)"><i class="mdi mdi-chevron-down"></i></a>
							<?php 
							if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { ?>
							<ul id="xsdc<?=$uniqKey?>" class="dropdown-content custom_dropdown fetchfriendmenu">
							</ul>
							<?php } ?>
						</div>	
					</div>	
					<?php } ?>
					
					<div class="more-span mobile-none-who">
						<div class="dropdown dropdown-custom">
							<a class="dropdown-button more_btn <?=$checkuserauthclass?> directcheckuserauthclass gray-text-555" href="javascript:void(0);" data-activates="<?=$uniqKey?>" onclick="fetchfriendmenu('<?=$sId?>')"><i class="mdi mdi-chevron-down"></i>
							</a>
							<?php 
							if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { ?>
							<ul id="<?=$uniqKey?>" class="dropdown-content custom_dropdown fetchfriendmenu">
								<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
							</ul>
							<?php } ?>
						</div>																		
					</div>
			</div>
		</div>
		<div class="descholder">
			<h5><a href="<?php echo Url::to(['userwall/index', 'id' => $sId]); ?>">
					<span class="etext" style="color: #000;"><?=$name?></span>
					<?php 
					if(in_array($sId, $onlineBulkIds)) { ?>
						<span class="online-dot"><i class="zmdi zmdi-check"></i></span>
					<?php } ?>
				</a></h5>
			<p><?=$label?></p>
			<p class="info"><?=$mutualLabel?></p>
		</div>
	</div>
</div>
<?php }
} 

if($isEmpty == true) {
	$this->context->getnolistfound('nopeoplefound');
}
exit;?>
