<?php 
if(isset($data) && !empty($data)) { 
	$id = (string)$data['_id'];
	$name = $data['fullname'];
	$dp = $this->context->getimage($id,'thumb');
?>
<div class="top-stuff">
	<div class="postuser-info">
		<div class="img-holder">
			<img src="<?=$dp?>">
		</div>
		<div class="desc-holder"><a href="javascript:void(0)"><?=$name?></a></div>
	</div>
</div>
<div class="clear"></div>
<div class="npost-content"> 
	<div class="post-mcontent">                                                     
		<div class="desc paddfix limit-tt">
			<textarea class="message" id="message" placeholder="Write your message."></textarea>
		</div>
	</div>
	<div class="post-bcontent">
		<div class="post-bholder">
			<button class="btn btn-primary" onclick="sendmessage('<?=$id?>');" type="button" name="post">Send Message</button>
		</div>
	</div>
</div>
<?php } exit;?>							