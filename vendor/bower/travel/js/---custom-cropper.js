$(document).ready(function() {
	var cropper = $('.cropper');
	if (cropper.length) {
	  $.each(cropper, function(i, val) {
	    var uploadCrop;

	    var readFile = function(input) {
	      if (input.files && input.files[i]) {
	        var reader = new FileReader();

	        reader.onload = function(e) {
	          uploadCrop.croppie('bind', {
	            url: e.target.result
	          });
	        };

	        reader.readAsDataURL(input.files[i]);
	      } else {
	        alert("Sorry - you're browser doesn't support the FileReader API");
	      }
	    };

	    
		uploadCrop = $('.js-cropping').croppie({ // TODO: fix so its selects right element
	      viewport: {
	        width: 200,
	        height: 200
	      },
	      boundary: {
	        width: 350,
	        height: 355
	      },
	      enableOrientation: true
	    });

	    $('.js-cropper-upload').on('change', function() {
	      $('.crop').show(); // TODO: fix so its selects right element
	      readFile(this);
	    });
	    
	    $('.js-cropper-rotate--btn').on('click', function(ev) {
	      uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
	    });

	    $('.js-cropper-result--btn').on('click', function(ev) {
	      uploadCrop.croppie('result', 'canvas').then(function(resp) {
	        popupResult({
	          src: resp
	        });
	      });
	    });

	    var popupResult = function(result) {
	      var html;

	      if (result.html) {
	        html = result.html;
	      }

	      if (result.src) {
	        html = '<img src="' + result.src + '" />';
	      }
	      $('.js-cropper-result').show();	
	      $('.js-cropper-result').html(html); // TODO: fix so its selects right element
	      $('.crop').hide();
	      $('.image-upload').show();
	    };
	  });
	}
});