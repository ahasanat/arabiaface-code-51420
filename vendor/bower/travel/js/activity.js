$(".di_act_post").click(function() {
	var post_id = $(this).attr('act-id');
	var data_type = $(this).attr('data-type');
	$.ajax({
        url: '?r=activity/postdisplay',  
        type: 'POST',
        data: "post_id="+post_id,
        success: function(data)
        {
			$("#"+data_type+"-activity-post-"+post_id).html(data);
		}
    });
});