/*$(window).resize(function() {		
	if($('.fake-title-area').find('.tabs').find('li').find('a.active').length) {
		$parent = $('.fake-title-area').find('.tabs').find('li').find('a.active');
		$link = $parent.attr('href');
		gridBoxImgUINew($link+' .channelbox');
	}
});	*/

$(document).ready(function(){	
	$(document).on("click",".modal.dis_unsub .modal_discard",function (event) {
		var $channel_id = $(this).data("id");
		var obj = $(".gdetails-summery .action-btns a");
		var htmlContent = $(obj).html();
		var result = htmlContent.match(/Subscribed/i);
		
		if(result) {		 	
			$.ajax({
				type: 'POST',
				url: '?r=channel/subscribe-update',
				data: {$channel_id},
				success: function(data) {
					if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} else {
						if(data == 'S') {
							$(obj).html("Subscribed");
							$(obj).removeClass("disabled");
						} else if(data == 'U') {
							$(obj).html("Subscribe");
							$(obj).removeClass("disabled");
						}
					}
					window.location.href="";
				}
			});
		} else {
			$.ajax({
				type: 'POST',
				url: '?r=channel/subscribe-update',
				data: {$channel_id},
				success: function(data) {
					if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} else {
						if(data == 'S') {
							$(obj).html("Subscribed");
							$(obj).removeClass("disabled");
						} else if(data == 'U') {
							$(obj).html("Subscribe");
							$(obj).removeClass("disabled");
						}
					}
					window.location.href="";
				}
			});	
		}
    });

	suggestedList(0, 'load');
    
});

    /* subscribed list channel */
    function subscribedList($lazyhelpcount=0) {
    	$.ajax({
    		type: 'POST',
			url: '?r=channel/subscribed-list',
			data: {$lazyhelpcount},
			beforeSend: function() {
				$('.loaderblock').remove();
	        	$("#channels-subscribed").find('.row').append($loader); 
	        },
			success: function(data) {
				$('.loaderblock').remove();
				if(data != '') {
		        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
		        		$("#channels-subscribed").find('.row').html(data);
		        	} else {
		            	$("#channels-subscribed").find('.row').append(data);
		        	}
		        	$('#channels-subscribed').find('.channelbox').last().addClass('lazyloadscrollchannel lazycounthelp');
		        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
		        	getnolistfound('nosubscribedchannelfound');
		        	
		        }
			},
	        complete: function(){
	        	//gridBoxImgUINew('#channels-subscribed .channelbox');
	        }
		}); 
    }

    /* suggested list channel */
    function suggestedList($lazyhelpcount=0, $isload='') {
    	$.ajax({
    		type: 'POST',
			url: '?r=channel/suggested-list',
			data: {$lazyhelpcount},
			beforeSend: function() {
				if($lazyhelpcount >0) {
					$('.loaderblock').remove();					
					$("#channels-suggested").find('.row').append($loader); 
				}
	        },
			success: function(data) {
				$('.loaderblock').remove();
				if(data != '') { 
		        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
		        		if($isload == 'load') {
		        			$("#channels-suggested").animateCss('fadeInUp');
		        			$("#channels-suggested").find('.channels-list').addClass('animated fadeInUp');
		        		}
		        		$("#channels-suggested").find('.row').html(data);
		        	} else {
		            	$("#channels-suggested").find('.row').append(data);
		        	}
		        	$('#channels-suggested').find('.channelbox').last().addClass('lazyloadscrollchannel lazycounthelp');
		        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
		        	getnolistfound('nosubsuggestedchannelfound');
		        }
			},
	        complete: function(){
	        	//gridBoxImgUINew('#channels-suggested .channelbox');
	        }
		});
    }

    /* popular list channel */
    function popularList($lazyhelpcount=0) {
    	$.ajax({
    		type: 'POST',
			url: '?r=channel/popular-list',
			data: {$lazyhelpcount},
			beforeSend: function() {
	        	$('.loaderblock').remove();
	        	$("#channels-popular").find('.row').append($loader); 
	        },
			success: function(data) {
				$('.loaderblock').remove();
				if(data != '') {
		        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
		        		$("#channels-popular").find('.row').html(data);
		        	} else {
		            	$("#channels-popular").find('.row').append(data);
		        	}
		        	$('#channels-popular').find('.channelbox').last().addClass('lazyloadscrollchannel lazycounthelp');
		        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
		        	getnolistfound('nopopularchannelfound');
		        }
			},
	        complete: function(){
	        	//gridBoxImgUINew('#channels-popular .channelbox');
	        }
		});
    } 
 
    /* channel subscribe from index page */
    function channelSubscribe(event, $channel_id, obj, ischange, number='', isPermission=false) {
    	event.stopPropagation();
	    if(ischange) {
	    	window.location.href="?r=channel/detail&col_id="+$channel_id+"&rcdno="+number;
		} else {
			$.ajax({ 
				type: 'POST',
				url: '?r=channel/subscribe-update',
				data: {$channel_id},
				success: function(data) {
					if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} else {
						if(data == 'S') {
							$('.channelbox_'+$channel_id).find('.action-btns').find('span').html("Subscribed");
							Materialize.toast('Subscribing to channel.', 2000, 'green'); 
						} else if(data == 'U') {
							$('.channelbox_'+$channel_id).find('.action-btns').find('span').html("Subscribe");
							Materialize.toast('Unsubscribing to channel.', 2000, 'green'); 
						}
					}
					//$(".discard_md_modal").modal("close");
				}
			});
	    }
	}

	/* channel subscribe from detail page */   
	function channelSubscribeDetail($channel_id, obj, isPermission=false) {
		$.ajax({
			type: 'POST',
			url: '?r=channel/subscribe-update',
			data: {$channel_id},
			success: function(data) {
				$(".discard_md_modal").modal("close");
				if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} else {
					if(data == 'S') {
						Materialize.toast('Subscribing to channel.', 2000, 'green'); 
					} else if(data == 'U') {
						Materialize.toast('Unsubscribing to channel.', 2000, 'green'); 
					}
					
					window.location.href="";
				}
			}
		});
	}
 
	/* channel unsubscribe */
	function channelSubscribeDetailMore($channel_id, obj, isPermission=false) {
		if(isPermission) {
			$.ajax({
				type: 'POST',
				url: '?r=channel/subscribe-update',
				data: {$channel_id},
				success: function(data) {
					$(".discard_md_modal").modal("open");
					if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} else {
						Materialize.toast('Unsubscribed.', 2000, 'green'); 
						window.location.href="";
					}
				}
			});
		} else {
			if($(obj).hasClass('checkuserauthclassnv')) {
				checkuserauthclassnv(true);
			} else if($(obj).hasClass('checkuserauthclassg')) {
				checkuserauthclassg(true);
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("You are unsubscribing from this Channel.");
	            btnKeep.html("Keep");
	            btnDiscard.html("Unsubscribe");
	            btnDiscard.attr('onclick', 'channelSubscribeDetailMore(\''+$channel_id+'\', this, true)');
	            $(".discard_md_modal").modal("open");
			}  
		}
	}

	/* Lazy Loading Handler */
	window.onload = function() {
		$.fn.isOnScreen = function(){
		    var win = $(window)
		    var viewport = {
		      top : win.scrollTop(),
		      left : win.scrollLeft()
		    }
		    viewport.right = viewport.left + win.width()
		    viewport.bottom = viewport.top + win.height()
		    var bounds = this.offset()
		    bounds.right = bounds.left + this.outerWidth()
		    bounds.bottom = bounds.top + this.outerHeight()
		    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom))
		}

		function lazyloadChannel() {
			$('.lazyloadscrollchannel').each(function() {
	      		var element = $(this)
				if( element.isOnScreen() ) {
					$activeTabIndex = $('.mobile-header').find('li.active').index();
					if($activeTabIndex == 0 || $activeTabIndex == '') {
						var lazyhelpcount = $('#channels-suggested').find( ".row" ).find('.lazycounthelp').length;
						$('.lazyloadscrollchannel').removeClass("lazyloadscrollchannel");
						suggestedList(lazyhelpcount);
					} else if ($activeTabIndex == 1) {
						var lazyhelpcount = $('#channels-subscribed').find( ".row" ).find('.lazycounthelp').length;
						$('.lazyloadscrollchannel').removeClass("lazyloadscrollchannel");
						subscribedList(lazyhelpcount);
					} else {
						var lazyhelpcount = $('#channels-popular').find( ".row" ).find('.lazycounthelp').length;
						$('.lazyloadscrollchannel').removeClass("lazyloadscrollchannel");
						popularList(lazyhelpcount);
					}
				}
			})
		}

		lazyloadChannel();
		if( $('.lazyloadscrollchannel').length > 0 ) {
			lazyloadChannel();
		}
			
		$('body').bind('touchmove', function(e) { 
			lazyloadChannel();
		});

		$(window).scroll(function() {
			lazyloadChannel();
		});
	}
	/* END Lazy Loading Handler */
