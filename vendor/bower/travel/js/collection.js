var color =''; 
var $thisGlobalStored;

$(window).resize(function() {		
	/*if($('.fake-title-area').find('.tabs').find('li').find('a.active').length) {
		$parent = $('.fake-title-area').find('.tabs').find('li').find('a.active');
		$link = $parent.attr('href'); 
		gridBoxImgUINew($link+' .collectionbox');
	}*/

	if (window.location.href.indexOf("collection/detail") > -1) { 
		var win_w=$(window).width();
		if(win_w>480 && win_w<=568) {
			$('.page-wrapper').removeClass('menutransheader-wrapper menuhideicons-wrapper');		
		} else {
			$('.page-wrapper').addClass('menutransheader-wrapper menuhideicons-wrapper');		
		}
	}
});	
 
$(document).ready(function(){
	$('#image-cropper').cropit({
        height: 188,
        width: 355,
        smallImage: "allow",
        onImageLoaded: function (x) {
            $(".cropit-preview").removeClass("class-hide");
            $(".cropit-preview").addClass("class-show");
            $(".btn-save").removeClass("class-hide");
            $(".btn-save").addClass("class-show");
            $("#removeimg").removeClass("class-hide");
            $("#removeimg").addClass("class-show");
        }
    });
    
	if(window.location.href.indexOf("detail") > -1) {
	} else {
		/* all collection listing */
		allCollection(0, 'load');
	    $('.allcollection').click(function() { 
	        allCollection();
	    }); 
	    /* yours collection listing */
		$(".yours").click(function() { 
	        yoursCollection();
	    });
	    /* all following listing */
		$(".following").click(function() {
	        followingCollection();
	    });
	    /* all popular listing */
		$(".popular").click(function() {
	        popularCollection();
	    });
	}
	
	$(document).on("click",".modal.del_deleteitem .modal_discard",function (event) {
		var col_id = $(this).data("id");		
        $.ajax({
			type: 'POST',
			url: '?r=collection/delete',
			data: 'col_id='+col_id,
			success: function(data){
				window.location.href = "?r=collection/index";
			}
		});
    });	
	$(document).on("click",".modal.dis_unfollow .modal_discard",function (event) {
		var col_id = $(this).data("id");
		var ischange=false;
		
		if($(".main-content").hasClass("generaldetails-page")){
			ischange=true;
		}
		$.ajax({
			type: 'POST',
			url: '?r=collection/follow',
			data: 'col_id='+col_id,
			success: function(data){
				var result = $.parseJSON(data);
				if(result.auth != undefined && result.auth == 'checkuserauthclassnv'){
					checkuserauthclassnv();
				} else if(result.auth != undefined && result.auth == 'checkuserauthclassg') {
					checkuserauthclassg();
				}else {
					var status = result['msg'];
					if(status == '7') {
						return false;
					}

					if(ischange) {
						window.location.href="";
					}
				}
			}
		});
        
    });
});

$(document).on('click', '.composeCreateCollectionBox', function () {
    $.ajax({
        url: '?r=collection/create-collection-prepare',
        success: function(data){
            $('#add_collection_modal').html(data);
            setTimeout(function() { 
                $('#add_collection_modal').modal('open'); 
                initDropdown();
                $('#image-cropper').cropit({
			        height: 188,
			        width: 355,
			        smallImage: "allow",
			        onImageLoaded: function (x) {
			            $(".cropit-preview").removeClass("class-hide");
			            $(".cropit-preview").addClass("class-show");

			            $(".btn-save").removeClass("class-hide");
			            $(".btn-save").addClass("class-show");
			            $("#removeimg").removeClass("class-hide");
			            $("#removeimg").addClass("class-show");
			        }
			    });
            }, 400);
        }
    });
});

function CollectionFollowers() {
    $('.item_members').modal('open');
}

function composeEditCollectionBox($id) {
	if($id != undefined && $id != null && $id != '') {
		$.ajax({
			type: 'POST',
			url: '?r=collection/edit-collection-prepare',
	        data: {$id},
	        success: function(data){
	            $('#add_collection_modal').html(data);
	            setTimeout(function() { 
	                $('#add_collection_modal').modal('open'); 
	            	$('.dropdown-button').dropdown("close");
	                initDropdown();
	                $('#image-cropper').cropit({
				        height: 188,
				        width: 355,
				        smallImage: "allow",
				        onImageLoaded: function (x) {
				            $(".cropit-preview").removeClass("class-hide");
				            $(".cropit-preview").addClass("class-show");

				            $(".btn-save").removeClass("class-hide");
				            $(".btn-save").addClass("class-show");
				            $("#removeimg").removeClass("class-hide");
				            $("#removeimg").addClass("class-show");
				        }
				    });
	            }, 400);
	        }
	    });	
	}
}

$(document).on('click', '.composeEditCollectionBox', function () {
    $.ajax({
        url: '?r=collection/create-collection-prepare',
        success: function(data){
            $('#add_collection_modal').html(data);
            setTimeout(function() { 
                $('#add_collection_modal').modal('open'); 
                initDropdown();
            }, 400);
        }
    });
});

/* key press tagline script */
$(document).on( 'keyup', '#tagline', function(e) {
	var textValue = $('#tagline').val();
	$('.char-limit').html(textValue.length+'/80');
});


$('.collection-colors ul li').click(function(){
	$('.collection-colors ul li').removeClass('active');	
	$(this).addClass('active');
	color = $(this).attr('color'); 
});

/* update collection */
function update_collecction(col_id, obj) {
	var $selectore = $(obj).parents('.modal.open');
	var $isValidate = true;
	if($selectore.length) {
		var $ID = $selectore.attr('id');
		if($ID != undefined && $ID != null && $ID != '') {
			$ID = $('#'+$ID);

			if (color == ''){
				color = "collection-red";
			}

			var visible = $ID.find(".privacy-value").html().trim();
			var title 	= $ID.find("#title").val();
			var tagline = $ID.find("#tagline").val();
			var images = $ID.find('#image-cropper').cropit('export');
			var col_id  = col_id;
	
			if(title == '')
			{
				$ID.find("#title").focus();
				Materialize.toast('Enter collection title.', 2000, 'red');
				return false;
			} else if(tagline == '') {
				$ID.find("#title").focus();
				Materialize.toast('Enter collection tagline.', 2000, 'red');
				return false;
			} else if(visible == undefined || visible == null || visible == '') {
				Materialize.toast('Choose visible to.', 2000, 'red');
				$isValidate = false;
				return false;
			} else if($.inArray(visible, $post_privacy_array) <= -1) {
				Materialize.toast('Choose visible to.', 2000, 'red');
				$isValidate = false;
				return false;
			}

			if($(window).width()>568) {			
				Materialize.toast('Updating...', 2000, 'green');
			}	

			if($isValidate) {
				$.ajax({
					type: 'POST',
					url: '?r=collection/update',
					data: {
						'col_id' : col_id,
						'title' : title,
						'tagline' : tagline,
						'color' : color,
						'visible' : visible,
						'images' : images,
						'custom' : customArray
					},
					success: function(data){
						window.location.href = "?r=collection/detail&col_id="+col_id;
					}
				});
			}
		}
	}
}
 
/* delete collection */
function delete_collecction(isPermission=false) {
	if(isPermission) {
		$.ajax({
			type: 'POST',
			url: '?r=collection/delete',
			data: {col_id},
			success: function(data){
				Materialize.toast('Collection deleted.', 2000, 'green');
				$(".discard_md_modal").modal("close");
				window.location.href = "?r=collection/index";
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
    	disText.html("Delete this collection?");
        btnKeep.html("Keep");
        btnDiscard.html("Delete");
        btnDiscard.attr('onclick', 'delete_collecction(true)');
        $(".discard_md_modal").modal("open");			
	}
}

/* update collection status */
function collectionFollowsnew(col_id, obj) {
	$.ajax({
		type: 'POST',
		url: '?r=collection/follow',
		data: 'col_id='+col_id,
		success: function(data){
			var result = $.parseJSON(data);
			 $(".discard_md_modal").modal("close");
			if(result.auth != undefined && result.auth == 'checkuserauthclassnv'){
				checkuserauthclassnv();
			} else if(result.auth != undefined && result.auth == 'checkuserauthclassg') {
				checkuserauthclassg();
			} else {
				var status = result['msg'];
				if(status == '7') {
					return false;
				}

				if(status=='1') {
					Materialize.toast('Following collection', 2000, 'green');
				} else {
					Materialize.toast('Unfollowing collection', 2000, 'green');
				}
				window.location.href="";
			}
		}
	});
}

/* update collection status */
function collectionFollowsMore(col_id,obj, ischange=false) {
	$.ajax({
		type: 'POST',
		url: '?r=collection/follow',
		data: 'col_id='+col_id,
		success: function(data){
			var result = $.parseJSON(data);
			if(result.auth != undefined && result.auth == 'checkuserauthclassnv'){
				checkuserauthclassnv();
			} else if(result.auth != undefined && result.auth == 'checkuserauthclassg') {
				checkuserauthclassg();
			} else {
				var status = result['msg'];
				if(status == '7') {
					return false;
				}

				if(ischange) {
					window.location.href="";
				} else {
					var count = result['f_count'];
					if(status=='1') {
						$(obj).html("Following");
						$(obj).addClass("disabled");
					} else {
						$(obj).html("Follow");
						$(obj).removeClass("disabled");	
					}
					if(count > 0) {
						count = ''+count+' Followers';
					}
					if(count == 0) {
						count = 'No Followers';
					}
					$('.fcount').html(count);
				}
			}
		}
	});
}

/* report collection */
function reportcollection(pid, isPermission=false) {
	var desc = $("#textInput").val();
	if(desc == '') {	
		Materialize.toast('Enter reporting reason.', 2000, 'red');
		return false;
	}

	if(isPermission) {
		$.ajax({
			type: 'POST',
			url: '?r=collection/reportcollection',
			data: "pid=" + pid + "&desc=" + desc,
			success: function (data) {
				$(".discard_md_modal").modal("close");
				Materialize.toast('Reported.', 2000, 'green');
				location.reload();
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
    	disText.html("Are you sure to Report this ?");
        btnKeep.html("Keep");
        btnDiscard.html("Report");
        btnDiscard.attr('onclick', 'reportcollection(\''+pid+'\', true)');
        $(".discard_md_modal").modal("open");			
	}
}

/* update collections */
function collectionFollows(event, col_id, obj, ischange, isPermission) {
	event.stopPropagation();
    if(ischange) {
		window.location.href = "?r=collection/detail&col_id="+col_id;
	} else {
		$.ajax({ 
			type: 'POST',
			url: '?r=collection/follow',
			data: {col_id},
			success: function(data){
				var result = $.parseJSON(data);
				if(result.auth != undefined && result.auth == 'checkuserauthclassnv'){
					checkuserauthclassnv();
				} else if(result.auth != undefined && result.auth == 'checkuserauthclassg') {
					checkuserauthclassg();
				} else {
					var status = result['msg'];
					if(status == '7')
					{
						return false;
					}

					var count = result['f_count'];
					var names =  result['follower_names'];
					if(status=='1') {
						$('.collectionbox_'+col_id).find('.action-btns').find('span').html("Following");
						Materialize.toast('Following collection', 2000, 'green');
					} else {
						$('.collectionbox_'+col_id).find('.action-btns').find('span').html("Follow");
						Materialize.toast('Unfollowing collection', 2000, 'green');
					}
					if(count > 0) {
						names = ''+names+' followed this';
						count = ''+count+' Followers';
					}
					if(count == 0) {
						names = 'Become a first to follow this';
						count = 'No Followers';
					}
					$('.fcount_'+col_id).html(count);
					$('.foname_'+col_id).html(names);
				}
			}
		});
	}
}

/* all collection listing */
function allCollection($lazyhelpcount=0, $isload='')
{
	$.ajax({
    	type: 'POST',
        url: '?r=collection/all',
		data: {$lazyhelpcount},
		beforeSend: function() {
			if($lazyhelpcount>0) {
				$('.loaderblock').remove();
        		$("#collections-suggested").find('.row').append($loader); 
        	}
        },
		success: function(data) {
			$('.loaderblock').remove();
			if(data != '') {
	        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
	        		if($isload == 'load') {
			        	$('#collections-suggested').animateCss('fadeInUp');
			        	$("#collections-suggested").find('.generalbox-list').addClass('animated fadeInUp');
			    	}
	        		$("#collections-suggested").find('.row').html(data);
	        	} else {
	            	$("#collections-suggested").find('.row').append(data);
	        	}
	        	/*$('#collections-suggested').find('.collectionbox').last().addClass('lazyloadscrollcollection lazycounthelp');*/
	        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
	        	getnolistfound('becomefirsttocreatecollection');
	        }
        },
        complete: function(){
        	//gridBoxImgUINew('#collections-suggested .collectionbox');
        }
    });
}

/* your collection listing */
function yoursCollection($lazyhelpcount=0)
{
    $.ajax({
		data: {$lazyhelpcount},
    	type: 'POST',
        url: '?r=collection/yours',
        beforeSend: function() {
        	$('.loaderblock').remove();
        	$("#collections-yours").find('.row').append($loader); 
        }, 
        success: function(data)
        {	
        	$('.loaderblock').remove();
        	if(data != '') {
        		if($lazyhelpcount == 0 || $lazyhelpcount == '') {
        			$("#collections-yours").find('.row').html(data);
        		} else {
        			$("#collections-yours").find('.row').append(data);
        		}
	            $('#collections-yours').find('.collectionbox').last().addClass('lazyloadscrollcollection lazycounthelp');
	        }
        },
        complete: function(){
        	//gridBoxImgUINew('#collections-yours .collectionbox');
        }
    });
}

/* following collection listing */
function followingCollection($lazyhelpcount=0)
{
    $.ajax({
		data: {$lazyhelpcount},
    	type: 'POST',
        url: '?r=collection/following', 
        beforeSend: function() {
        	$('.loaderblock').remove();
        	$("#collections-following").find('.row').html($loader); 
        },
        success: function(data)
        {
        	$('.loaderblock').remove();
        	if(data != '') {
	        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
	        		$("#collections-following").find('.row').html(data);
	        	} else {
	        		$("#collections-following").find('.row').append(data);	
	        	}
	        	$('#collections-following').find('.collectionbox').last().addClass('lazyloadscrollcollection lazycounthelp');
	        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
	        	getnolistfound('nocollectionfollowedyet');
	        }
        },
        complete: function(){
        	//gridBoxImgUINew('#collections-following .collectionbox');
        }
    });
}

/* popular collection listing */
function popularCollection()
{
	$.ajax({
        url: '?r=collection/popular', 
        success: function(data)
        {
            $("#collections-popular").html(data);
        }
    });
}

/* create collection */
function add_collecction(obj) {
	var $selectore = $(obj).parents('.modal.open');
	var $isValidate = true;
	if($selectore.length) {
		var $ID = $selectore.attr('id');
		if($ID != undefined && $ID != null && $ID != '') {
			$ID = $('#'+$ID);
			var status = $ID.find("#user_status").val();
			if(status == 0) {
				$isValidate = false;
				return false;
			}
			var visible = $ID.find(".privacy-value").html().trim();
			var title 	= $ID.find("#title").val();
			var tagline = $ID.find("#tagline").val();
			var imageData = $ID.find('#image-cropper').cropit('export');
			var selectedUser = [];
			//var images = $ID.find('#images').val();

			if(title == '') {
				$ID.find("#title").focus(); 
				Materialize.toast('Enter collection title.', 2000, 'red');
				$isValidate = false;
				return false;
			} else if (tagline == '') {
				$ID.find("#tagline").focus();
				Materialize.toast('Enter collection tagline.', 2000, 'red');
				$isValidate = false;
				return false;
			} else if(visible == undefined || visible == null || visible == '') {
				Materialize.toast('Choose visible to.', 2000, 'red');
				$isValidate = false;
				return false;
			} else if($.inArray(visible, $post_privacy_array) <= -1) {
				Materialize.toast('Choose visible to.', 2000, 'red');
				$isValidate = false;
				return false;
			}

			if($isValidate) {
				$.ajax({
					type: 'POST',
					url: '?r=collection/add',
					data: {
						'title' : title,
						'tagline' : tagline,
						'color' : color,
						'visible' : visible,
						'images' : imageData,
						'custom' : customArray
					},
					success: function(data){
						var result = $.parseJSON(data);
						if(result.auth != undefined && result.auth == 'checkuserauthclassg') {
							checkuserauthclassg();
						} else if(result.auth != undefined && result.auth == 'checkuserauthclassnv') {
							checkuserauthclassnv();
						} else {
							var id = result['collection_id'];
							Materialize.toast('Collection created.', 2000, 'green');
							window.location.href = "?r=collection/detail&col_id="+id;
						}

					}
				});
			}
		}
	}
}	

/* flag collection */
function flag_collection(c_id,c_uid,uid, isPermission=false){
	var desc = $("#textInput").val();
	if(desc == '') {	
		Materialize.toast('Enter Flaging reason.', 2000, 'red');
		return false;
	}

	if(isPermission) {
		$.ajax({
			type: 'POST',
			url: '?r=collection/flag',
			data: 'c_id='+c_id+'&c_uid='+c_uid+'&uid='+uid+'&desc='+desc,
			success: function(data){
				$(".discard_md_modal").modal("close");
				Materialize.toast('Flag.', 2000, 'green');
				window.location.href = "?r=collection/index";
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
    	disText.html("Are you sure to flag this ?");
        btnKeep.html("Keep");
        btnDiscard.html("Flag");
        btnDiscard.attr('onclick', 'flag_collection(\''+c_id+'\', \''+c_uid+'\', \''+uid+'\', true)');
        $(".discard_md_modal").modal("open");			
	}
}

/* START Lazy Loading */
window.onload = function() {
	$.fn.isOnScreen = function(){
	    var win = $(window)
	    var viewport = {
	      top : win.scrollTop(),
	      left : win.scrollLeft()
	    }
	    viewport.right = viewport.left + win.width()
	    viewport.bottom = viewport.top + win.height()
	    var bounds = this.offset()
	    bounds.right = bounds.left + this.outerWidth()
	    bounds.bottom = bounds.top + this.outerHeight()
	    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom))
	}

	function lazyloadCollection() {
		$('.lazyloadscrollcollection').each(function() {
      		var element = $(this)
			if( element.isOnScreen() ) {
				$activeTabIndex = $('.mobile-header').find('li.active').index();
				if($activeTabIndex == 0 || $activeTabIndex == '') {
					var lazyhelpcount = $('#collections-suggested').find( ".row" ).find('.lazycounthelp').length;
					$('.lazyloadscrollcollection').removeClass("lazyloadscrollcollection");
					allCollection(lazyhelpcount);
				} else if ($activeTabIndex == 1) {
					var lazyhelpcount = $('#collections-following').find( ".row" ).find('.lazycounthelp').length;
					$('.lazyloadscrollcollection').removeClass("lazyloadscrollcollection");
					followingCollection(lazyhelpcount);
				} else {
					var lazyhelpcount = $('#collections-yours').find( ".row" ).find('.lazycounthelp').length;
					$('.lazyloadscrollcollection').removeClass("lazyloadscrollcollection");
					yoursCollection(lazyhelpcount);
				}
			}
		})
	}

	lazyloadCollection();
	if( $('.lazyloadscrollcollection').length > 0 ) {
		lazyloadCollection();
	}
		
	$('body').bind('touchmove', function(e) { 
		lazyloadCollection();
	});

	$(window).scroll(function() {
		lazyloadCollection();
	});
}
/* END Lazy Loading */

