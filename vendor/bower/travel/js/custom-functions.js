var postBtnEle = $(".postbtn");
var postbtn_hide = "HIDE";
var postbtn_show = "SHOW";
var $pageId="";
var $pageOWNER="";
var lastModified = [];
var newct=0;   
var lastSearchSection = "desktop";
var storedFiles = [];
var storedFilesExsting = [];
var autocomplete;
window.URL    = window.URL || window.webkitURL;
var elBrowse  = document.getElementById("browse"),
    elPreview = document.getElementById("preview"),
    useBlob   = false && window.URL; // `true` to use Blob instead of Data-URL
$loader = '<div class="clear loaderblock"></div><center class="loaderblock"><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>';

/*document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.fixed-action-btn');
  var instances = M.FloatingActionButton.init(elems, {
     direction: 'left'
  });
});
*/
function lazyloadvariableisonscreen() {
	/* Lazy Loading Handler */
	/*$.fn.is_on_screen = function() {
		var win = $(window);
		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();
		 
		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();
		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
	};

	if( $('.lazyloadscrolluploadstuff').is_on_screen() ) { // if target element is visible on screen after DOM loaded
		$('.lazyloadscrolluploadstuff').removeClass("lazyloadscrolluploadstuff");
		fetchLoadPost(postNumber);
	}*/
}

function gethtmlcontentforblock($label) {
    if($label != undefined && $label != '') {
    	whoisopeninblock = $label;	
        $.ajax({
            type: 'POST',
            url: "?r=site/get-html-content-for-block", 
            data: {$label},
            success: function (data) {
            	var result = $.parseJSON(data);
                if(result.status != undefined && result.status == true) {
                    var returnlabel = result.returnlabel;
                    var ids = result.ids;
                    $('#'+$label).html(returnlabel);
                    addUserForAccountSettingsArray = ids.slice();
                }
            }
        });
    }
}

function gethtmlcontentforpage($label) {
    if($label != undefined && $label != '') {
    	whoisopeninblock = $label;
        $.ajax({
            type: 'POST',
            url: "?r=page/get-html-content-for-page", 
            data: {$label},
            success: function (data) {
            	var result = $.parseJSON(data);
                if(result.status != undefined && result.status == true) {
                    var returnlabel = result.returnlabel;
                    var ids = result.ids;
                    $('#'+$label).html(returnlabel);
                    addUserForAccountSettingsArray = ids.slice();
                }
            }
        });
    }
}

/* check parameter exist in URL */
function getQueryVariable(variable) {
       var query = decodeURIComponent(window.location.search.substring(1));
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){
               	$pair = pair[1].replace(/\+/g, ' ');
               	return $pair;
               }
       }
       return(false);
}

function Isuncompleteprofile() {
	/*$.ajax({
		url: '?r=site/isuncompleteprofile', 
		success: function (data){
			if(data) {
				window.location.href='?r=site/complete-profile';
			}
		}
	});*/
}
/* FUN hide notification */
	function hideNot(nid){
		if(nid != ''){
			$.ajax({
				url: '?r=notification/hidenotification', 
				type: 'POST',
				data: 'nid=' + nid,
				success: function (data){
					if(data){
						$('#hidenot_'+nid).remove();
						if(!$('#notifications-content').find('.noti-listing').find('.mainli').length) {
							getnolistfound('nonotificationfound');
						}
						Materialize.toast('Hide notification.', 2000, 'green');
					}   
					setTimeout(function(){ initDropdown(); },400);
				}
			});
		}
	}
/* FUN end hide notification */

/* FUN DeleteN Delete Notification */
	function delNot(nid, isPermission=false){
		if(nid != '')
		{
			if(isPermission) {
				$.ajax({
					url: '?r=notification/hidenotification', 
					type: 'POST',
					data: 'nid=' + nid,
					success: function (data)
					{
						$(".discard_md_modal").modal("close");
						if(data)
						{
							$('#hidenot_'+nid).remove();
							Materialize.toast('Deleted', 2000, 'green');
						}   
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
				var btnKeep = $(".discard_md_modal .modal_keep");
				var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Delete notification.");
				btnKeep.html("Keep");
				btnDiscard.html("Delete");
				btnDiscard.attr('onclick', 'delNot(\''+nid+'\', true)');
				$(".discard_md_modal").modal("open");
			}
		}
	}
/* FUN end Delete notification */

/************ chat functions ***********/
function getchatcontent() {
	$.ajax({
		url: "?r=site/getchatblock",
		type: 'GET',
		success: function (data) {
			$('#chatblock').html(data);
			if (!($(this).hasClass('directcheckuserauthclass'))) {
				$(".float-chat").toggleClass("chat-open");
				var win_w=$(window).width();
				if(win_w<=1024){
					removeNiceScroll(".nice-scroll");
				}
				if(win_w<=767){
					if(!$(".float-chat").hasClass("chat-open")){
						//$('body').removeClass("nooverflow");
					}			
				}
				if(!$(".float-chat").hasClass("chat-open")){
					$(".chat-window").removeClass("shown");
				}
			}
		}
	});
}

function setChatHeight(){
	var windowW = $(window).width();
	if (windowW <= 1024){
		//removeNiceScroll(".nice-scroll");
		var chatwindH = $('.chat-window ul.mainul').height();
		var backbtnH = $('.backChatList').height();
		var chattitleH = $('.chatwin-box .topbar').height();
		var chatTextareaH = $('.chat-add').height();
		var totaldiductH = backbtnH + chattitleH + chatTextareaH;

		$('.chat-conversation .chat-area').height(chatwindH - totaldiductH - 170);
	}
	else{			
		$('.chat-conversation .chat-area').height(200);
		$(".nice-scroll").getNiceScroll().resize();
	}
}

function manageChatbox(){
	var winw=$(window).width();				
	if(winw<=1024){			
		if(!$(".float-chat").hasClass("floating")){				
			$(".float-chat").find(".chat-tabs").addClass("shown");
			$(".float-chat").find(".chat-window").addClass("hidden");
		}
	}
	else{
		
		$(".float-chat").find(".chat-tabs").removeClass().addClass("chat-tabs");
		$(".float-chat").find(".chat-window").removeClass().addClass("chat-window");		
	}	
}

function setFloatChat(){		
	var w_width=$(window).width();
	$(".float-chat").addClass("floating");		
}
/************ end chat functions ***********/

/************ ESSENTIALS FUNCTIONS ************/

	/* FUN manage sliding pan - profile tip location */
		 
		/*if(window.location.href.indexOf("detail") > -1) { 
			window.setInterval(function() {
				var $selectorid = '';
				var $selector = $('.post-column').find('.post-list').find('.post-holder:first');
				if($selector.length) {
					$selectorid = $selector.attr('id');
					$selectorid = $selectorid.replace("hide_", "");
				}
			  	    
			  	loadnewpost($selectorid);
			}, 60 * 1000);
		}*/

		function countingCall() {
			$.ajax({ 
				url: '?r=notification/new-notification-count',
				success: function (resp) { 
					var $resp = $.parseJSON(resp);
					if($resp.status == true) {
						$notcount = $resp.notcount;
						$frdcount = $resp.frdcount;

						if($notcount >0) {
							if($("#noti_budge").length) {
								$('#noti_budge').show().html($notcount);
							} else {
								$("#glob_budge").append('<span id="noti_budge" class="new-notification">'+$notcount+' </span>');
							}
						} else {
							if($("#noti_budge").length) {
								$('#noti_budge').html('').hide();
							}
						}

						if($frdcount >0) {
							if($("#request_budge").length) {
								$('#request_budge').show().html($frdcount);
							} else {
								$(".friendcountinner").append('<span id="request_budge" class="new-notification">'+$frdcount+' </span>');
							}
						} else {
							if($("#request_budge").length) {
								$('#request_budge').html('').hide();
							}
						}
					}
				}
			});
		}

		function notificationcall() {
			$.ajax({ 
			    url: '?r=notification/new-notification',
			    success: function (response) { 
			    $('#notifications').html(response);
			   	setTimeout(function() { 
				   	if($('#not_notify_prts_li').find('.seemorenoti').length) {
				   		$('#not_notify_prts_li').find('.noti-list').removeClass('nopad');
				   	} else {
				   		$('#not_notify_prts_li').find('.noti-list').addClass('nopad');
				   	} 

					$("#notifications .not-resultlist").getNiceScroll().remove();
					$("#notifications .not-resultlist").niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"8px"});					
					var budge = $("#new_budge").val();     
					if(budge > 0) {
						if($("#noti_budge").length) {
							$('#noti_budge').show();
							$('#noti_budge').html(budge);
							var not_sound = $("#notification_sound").val();
						} else {
							$(".friendcountinner").append('<span id="noti_budge" class="badge badge-default">'+budge+' </span>');
						}
					}   
				},400);
			   }
		   });   
		}
		
		/* new friend notification get */
		function friendnotificationcall() {         
			$.ajax({
				url: '?r=friend/new-friends', 
				success: function (response) {
					$('#not_frndreq_prts_li').html(response);
					setTimeout(function(){ 
						$(".fr-list .not-resultlist").getNiceScroll().remove();
						$(".fr-list .not-resultlist").niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"8px"});
						var budge = $("#new_budge_friend").val();
						if(budge > 0){
							if($("#request_budge").length) {
								$('#request_budge').show();
								$('#request_budge').html(budge);
								$('#friendcount').val(budge);
								var msg_sound = $("#sound_on_message").val();
							} else {
								$(".friendcountinner").append('<span class="badge" id="request_budge">'+budge+' </span>');
							}
						}
					},400);

				}
			});
		}

		/*function loadnewpost($selectorid='') {
		  	$.ajax({
				type: 'POST',
				data: {$selectorid},
				url: '?r=site/load-new-post',
				success: function(data){
					if(data) {
						$('.post-column').find('.post-list').find('.row').prepend(data);
						Materialize.toast('New posts is added.', 2000, 'green');
						setTimeout(function(){ fixImageUI('newpost'); initDropdown(); },500);
					}
				}
			}); 
		}*/
	/* FUN end manage sliding pan - profile tip location */

	/* FUN set mobile menu */
		function setMobileMenu(state){
			var isMobileMenu=$(".sidemenu-holder").hasClass("m-hide");
			if(isMobileMenu){	
				if(state == 'open') {
					if(!$(".sidemenu-holder").hasClass("m-open")) {					
						$(".sidemenu-holder").addClass("m-hide m-open");
					}
				} else if( state == 'alt') {
					if($(".sidemenu-holder").hasClass("m-open")) {
						$(".sidemenu-holder").removeClass("m-open");					
					} else {					
						$(".sidemenu-holder").addClass("m-hide m-open");
					}
				} else {
					$(".sidemenu-holder").removeClass("m-open");				
				}
			}
			
			var isTabMenu=$(".sidemenu-holder").hasClass("tab-hide");
			if(isTabMenu){	
				if(state == 'open') {
					if(!$(".sidemenu-holder").hasClass("tab-open")) {					
						$(".sidemenu-holder").addClass("tab-hide tab-open");
					}
				} else if( state == 'alt') {
					if($(".sidemenu-holder").hasClass("tab-open")) {
						$(".sidemenu-holder").removeClass("tab-open");					
					} else {					
						$(".sidemenu-holder").addClass("tab-hide tab-open");
					}
				} else {
					$(".sidemenu-holder").removeClass("tab-open");				
				}
			}
			
			setTimeout(function(){ initNiceScroll(".nice-scroll");},400);
		}
	/* FUN end set mobile menu */

	/* FUN set left menu */
		function setLeftMenu(){
			
			var w_width=$(window).width();
			if(!$(".page-wrapper").hasClass("hidemenu-wrapper")){			
				if(w_width>1024){
					$(".sidemenu-holder").removeClass("m-hide");
					$(".sidemenu-holder").removeClass("m-open");
					$(".with-lmenu").removeClass("m-hide");
					$(".sidemenu-holder").addClass("open");
					$(".with-lmenu").addClass("open");
					
					$(".sidemenu-holder").removeClass("tab-hide");
					$(".sidemenu-holder").removeClass("tab-open");
				}
				else if(w_width>=768 && w_width<1024){	
					$(".sidemenu-holder").removeClass("m-hide");
					$(".sidemenu-holder").removeClass("m-open");
					$(".with-lmenu").removeClass("m-hide");
					$(".sidemenu-holder").removeClass("open");
					$(".with-lmenu").removeClass("open");
					
					$(".sidemenu-holder").addClass("tab-hide");
				}	
				else{		
					$(".sidemenu-holder").removeClass("open");
					$(".with-lmenu").removeClass("open");		
					$(".sidemenu-holder").addClass("m-hide");
					$(".with-lmenu").addClass("m-hide");
					
					$(".sidemenu-holder").removeClass("tab-hide");
					$(".sidemenu-holder").removeClass("tab-open");
				}
			}
		}
	/* FUN end set left menu */

	/* FUN close all floating drawer */
		function closeAllSideDrawer(which){
			
			$(".btnbox").each(function(){			
				var cls=$(this).attr("class");
				cls=cls.replace("box-open","").trim();		
				
				if(cls!=which){
					$(this).removeClass("box-open");
				}
			});	
		}
	/* FUN end close all floating drawer */

	/* FUN manage search section */		
		function closeSearchSection(e) {
			var trgt = $('.search-holder');
			if (!trgt.is(e.target) & trgt.has(e.target).length === 0) {			
				closeSearch();
			}
		}
		function openSearch(){	

			var main_sparent=$(".main-sholder");		
			
			if(!main_sparent.hasClass("keepopen")){
			
				main_sparent.addClass("opened");
				main_sparent.find(".search-input").css("display", "block");
				main_sparent.find(".search-input").css("width", "100%");
				main_sparent.find(".search-input").css("background-color", "#fff");
				main_sparent.find(".search-section").css("width", "100%");
				main_sparent.find(".search-section").addClass("opened");
				
				$(".search-section .search-input").focus();
			}
					
			setTimeout(function(){
				setSearchResult();
			},400);
			$('.search-result').getNiceScroll().resize();
		}
		function closeSearch(){
			
			var main_sparent=$(".main-sholder");		
			
			main_sparent.find(".search-result").slideUp();
			
			if(!main_sparent.hasClass("keepopen")){
				setTimeout(function(){
					
					main_sparent.removeClass("opened");
					main_sparent.find(".search-input").css("width", "0%");
					main_sparent.find(".search-section").css("width", "0%");
					main_sparent.find(".search-section").removeClass("opened");
					
					setTimeout(function () {
						main_sparent.find(".search-input").css("display", "none");
						main_sparent.find(".search-input").css("background-color", "rgba(0,0,0,0)");					
					}, 200);					
					
				},300);
			}
		}
		function manageSearch(obj){
			
			var super_parent=$(obj).parents(".search-holder");
		  
			var sh_hasClass = super_parent.hasClass("opened");

			$(obj).parents('.search-holder').find('input#search').val('');
			
			if(!sh_hasClass){						
				openSearch();
			}
			else{			
				closeSearch();
			}		
		}
		function setSearchResult(){
			var main_sparent=$(".main-sholder");		
			var isDeskSearchText=main_sparent.find(".search-input").val().trim();
			
			if(isDeskSearchText!="" && isDeskSearchText!= null){			
			
				if(isDeskSearchText != '') {
					if(lastSearchSection=="desktop"){
						main_sparent.find(".search-input").val(isDeskSearchText);					
					}
				}
				else{
					if(isDeskSearchText != ''){				
						main_sparent.find(".search-input").val(isDeskSearchText);					
						lastSearchSection=="desktop";
					}
				}
							
				setTimeout(function(){				
					main_sparent.find(".search-result").slideDown();				
				},400);
			}
			else{			
				main_sparent.find(".search-result").hide();			
			}
		}
	/* FUN end manage search section */

	/* FUN reset add photo popup */	
		function resetAddPhotoPopup(){		
			$(".popup-area#add-photo-popup .post-photos .img-box").each(function(){$(this).remove();});
		}
	/* FUN end reset add photo popup */

	/* FUN loader hide */
		function hideLoader(){
			clearInterval(interval);
			
			setTimeout(function(){$(".loader-holder .wrapper").fadeOut(400);},1000);
			
			setTimeout(function(){
				$('.loader-logo').hide();
				$('.loader-text').hide();
				$('body').addClass("loaded");
			},500);
		}	
	/* FUN end loader hide */

	/* FUN init nice scroll */
		function initNiceScroll(secName){
			winw=$(window).width();
			if(winw>1024) {
				if(secName=="homebody"){
					$("body").niceScroll({horizrailenabled:false,cursorcolor:"#666",cursorwidth:"10px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.8)"});
					$("body").getNiceScroll(0).rail.addClass('bodyScroll');
				}
				else{	
					if($(secName).hasClass("chatlist-scroll") || $(secName).hasClass("recentchat-scroll"))
						$(secName).niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
					
					else if($(secName).parents(".invite-holder").length > 0 || $(secName).hasClass(".dest-list"))
						$(secName).niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});				
					
					else
						$(secName).niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
					
					// set opacity of sidemenu
					if($(secName).hasClass("sidemenu")){
						setTimeout(function(){
							$(".sidemenu").css("opacity",1);
						},400);
					}				
				}
			} else {
				$(".sidemenu").css("opacity",1);
			}
		}

		function removeNiceScroll(secName){
			if(secName=="homebody"){			
				$("body").getNiceScroll().remove();
			}
			else{
				$(secName).getNiceScroll().remove();			
			}
		}
	/* FUN end init nice scroll */

/************ END ESSENTIALS FUNCTIONS ************/

/************ COMMON FUNCTIONS ************/

	/* Google Place API */
		$("body").on("click keypress", ".getplacelocation", function(e){
			  id = $(this).attr('id');
			  initAutocomplete(id);
		});
	/*END  Google Place API */

	/* FUN manage expandable -photo slider : places : more info */	
		function initSubtabSlider(obj){
			
			 if($(obj).find("a").attr("data-tab")=="subtab-photos"/* && count==0*/){
				setTimeout(function() {
					
					var sparent = $(obj).parents(".explandable-tabs").find(".tab-pane.subtab-photos");
					// new slider 
					if(sparent.find(".photo-gallery").length>0){					
						fixImageUI("photo-gallery");
					}
					
				}, 400);
			}
		}
	/* FUN end manage expandable -photo slider : places : more info */	

	/* open like popup */
		/*function openLikePopup(thisId){
		}*/
	/* end open like popup */

	/* FUN search string entered */
		function searchStringEntered(e){
		  var thisValue=$(this).val();
		  
		  var super_parent=$(this).parents(".search-holder");
		  
		  if(super_parent.hasClass("main-sholder")){   
		   lastSearchSection = "desktop";
		  }
		  else{lastSearchSection = "mobile";}
			 
		  $(".main-sholder").find(".search-input").val(thisValue);
		  
		  /* manage search code */
		  
		  var textValue = $(this).val();
		  var id = $(this).attr('data-id');
		  var textValue = $(this).val();
			if (textValue != '') {
				data = 'key=' + textValue;
				$.ajax({
					url: "?r=site/search", 
					type: 'GET',
					data: data,
					success: function (data) {
						$("."+id).html(data).show();					
					}
				});
			} 
			
		  /* end manage search code */
		  
		 }
	/* FUN end search string entered */

	/* FUN fix image centering */
		function resizeToHeight(image_width,image_height, container_width) {
			
			var newWidth = container_width;
			var width = image_width;
			var proportion = newWidth/width;
			var newHeight =image_height  * proportion;

			return newHeight;
		}
		function resizeToWidth(image_width,image_height, container_height) {
			
			var newHeight = container_height;
			var height = image_height;
			var proportion = newHeight/height;
			var newWidth =image_width  * proportion;

			return newWidth;
		}
		function resetPopupImageFixClass(super_parent){
			
			if($(super_parent+" .grid-list").hasClass("photo-list")){
				$(super_parent+" .photo-list .grid-box").each(function(){
						
					if($(this).find(".photo-box").hasClass("imgfix"))
						$(this).find(".photo-box").removeClass("imgfix");
						
					$(this).find(".photo-box").find("img").css("margin-left","0");
					$(this).find(".photo-box").find("img").css("margin-top","0");				
				});
			}
			
		}
		function resetImageFixClass(super_parent){
			
			if(super_parent==".saved-content" || super_parent==".reviews-list"){
				
				$(super_parent+" .post-holder").each(function(){
						
					if($(this).find(".pimg-holder").hasClass("imgfix"))
						$(this).find(".pimg-holder").removeClass("imgfix");
						
					$(this).find(".pimg-holder").find("img").css("margin-left","0");
					$(this).find(".pimg-holder").find("img").css("margin-top","0");				
					$(this).find(".pimg-holder").find("img").css("opacity",0);
				});
			}
			if(super_parent==".activity-content"){
				
				$(super_parent+" .activity-post-detail").each(function(){
					if($(this).find(".post-holder").find(".pimg-holder").hasClass("imgfix"))
						$(this).find(".pimg-holder").removeClass("imgfix");
					
					$(this).find(".post-holder").find(".pimg-holder").find("img").css("margin-left","0");
					$(this).find(".post-holder").find(".pimg-holder").find("img").css("margin-top","0");				
					$(this).find(".post-holder").find(".pimg-holder").find("img").css("opacity",0);
				});
			}
			
		}
		function resetReviewPostImageFixClass(){		
			$(".reviews-column .post-holder").each(function(){
					
				if($(this).find(".pimg-holder").hasClass("imgfix"))
					$(this).find(".pimg-holder").removeClass("imgfix");
					
				$(this).find(".pimg-holder").find("img").css("margin-left","0");
				$(this).find(".pimg-holder").find("img").css("margin-top","0");				
				$(this).find(".pimg-holder").find("img").css("opacity",0);
			});		
		}
		function fixImageUIPopup(){
			resetPopupImageFixClass(".pic-change-popup");
			
			setTimeout(function(){
				var gb_count=0;
					
				if($(".pic-change-popup .choosephoto .tab-pane .grid-list").hasClass("photo-list")){
					$(".pic-change-popup .choosephoto .tab-pane").each(function(){
						
						if(!$(this).hasClass("visited")){
							$(this).each(".grid-box",function(){
								gb_count++;
								var cont_width=$(this).width();
								var cont_height=$(this).height();
								var img_width=$(this).find("img").width();
								var img_height=$(this).find("img").height();
								if($(this).find(".photo-box").hasClass("himg-box")){
									if(img_width < cont_width){
										$(this).find(".photo-box").addClass("imgfix");
									}
								} 
								if(img_width > cont_width){
									
									var iwidth = resizeToWidth(img_width,img_height,cont_height);
									var lfix= ( iwidth - cont_width ) / 2;
									$(this).find(".photo-box").find("img").css("margin-left","-"+lfix+"px");							
								}
								if(img_height > cont_height || $(this).hasClass("imgfix")){
									var iheight = resizeToHeight(img_width,img_height,cont_width);
									var tfix= ( iheight - cont_height ) / 2;
									$(this).find(".photo-box").find("img").css("margin-top","-"+tfix+"px");							
								}
								
							});
						}
					});
				}
			},300); 
		}
		function resetMessageImages(super_parent){
			$(super_parent+" .albums-grid .grid-box").each(function(){
					
				if($(this).find(".photo-box").hasClass("imgfix"))
					$(this).find(".photo-box").removeClass("imgfix");
					
				$(this).find(".photo-box").find("img").css("margin-left","0");
				$(this).find(".photo-box").find("img").css("margin-top","0");
			});
		}
		function fixMessageImagesPopup(){
			resetMessageImages(".photos-thread");
			
			setTimeout(function(){
			
				$(".photos-thread .albums-grid .grid-box").each(function(i, v){
					var cont_width=$(this).width();
					var cont_height=$(this).height();
					var img_width=$(this).find("img").width();
					var img_height=$(this).find("img").height();
					
					if($(this).find(".photo-box").hasClass("himg-box")){
						if(img_width < cont_width){
							$(this).find(".photo-box").addClass("imgfix");
						}
					} 
					
					if(img_width > cont_width){
						
						var iwidth = resizeToWidth(img_width,img_height,cont_height);
						var lfix= ( iwidth - cont_width ) / 2;
						$(this).find(".photo-box").find("img").css("margin-left","-"+lfix+"px");							
					}
					
					if(img_height > cont_height || $(this).hasClass("imgfix")){
						var iheight = resizeToHeight(img_width,img_height,cont_width);
						var tfix= ( iheight - cont_height ) / 2;
						$(this).find(".photo-box").find("img").css("margin-top","-"+tfix+"px");							
					}
				});		
			
			},300);
		}
		
		function fixPostImages() {
			var elemCount = $('.restingimagefixes').length;
			var $obj =1;
			$('.restingimagefixes').each(function() {
				if($(this).find(".post-img").length > 0 ) {
					$(this).find(".pimg-holder").each(function(e, v) {
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img = new Image();
					    img.onload = function($counting){
					        img_width = this.width;
					        img_height = this.height; 

							if(img_width > cont_width) {
								$(v).find("img").css('width', 'auto');
							} else {
								$(v).find("img").css('width', '100%');
							}

							if(img_height > cont_height) {
								$(v).find("img").css('height', 'auto');
							} else {
								$(v).find("img").css('height', '100%');
							}
							$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);

						}($obj);

					    img.src = $(v).find("img").attr('src');
					});
				}
				
				if(elemCount == $obj) {
					setTimeout(function(){
						$('.restingimagefixes').removeClass('restingimagefixes');
					}, 500);
				}
				$obj++;
			});
		}

		function resizefixPostImages() {
			if($(".post-img").length > 0 ) {
				$(".pimg-holder").each(function(e, v) {
					var cont_width=$(v).width();
					var cont_height=$(v).height();
					var img = new Image();
				    img.onload = function(){
				        img_width = this.width;
				        img_height = this.height;

						if(img_width > cont_width) {
							$(v).find("img").css('width', 'auto');
						} else {
							$(v).find("img").css('width', '100%');
						}

						if(img_height > cont_height) {
							$(v).find("img").css('height', 'auto');
						} else {
							$(v).find("img").css('height', '100%');
						}
						$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
				    };
				    img.src = $(v).find("img").attr('src');

				});
			}
		}

		function resetActivityImages(){
			$(".activity-content .activity-box").each(function(){
					
				if($(this).find(".pimg-holder").hasClass("imgfix"))
					$(this).find(".pimg-holder").removeClass("imgfix");
					
				$(this).find(".pimg-holder").find("img").css("margin-left","0");
				$(this).find(".pimg-holder").find("img").css("margin-top","0");
			});		
		}
		function resetPhotoGalleryImageFixClass(){
			$(".subtab-photos.active .photo-gallery .thumbs-img ul > li").each(function(){
					
				if($(this).find("a").hasClass("imgfix"))
					$(this).find("a").removeClass("imgfix");
					
				$(this).find("a").find("img").css("margin-left","0");
				$(this).find("a").find("img").css("margin-top","0");
			});		
		}

		function wallPhotoAlbumsSliderFixClass(){	
			if($(".albums-area").css("display")=="block" && $(".albums-area").length>0){
				$(".albums-area .album-box").each(function(){
						
					if($(this).find("a").hasClass("imgfix"))
						$(this).find("a").removeClass("imgfix");
						
					$(this).find("a").find("img").css("margin-left","0");
					$(this).find("a").find("img").css("margin-top","0");
				});		
				
			}

			if($(".photos-area").css("display")=="block" && $(".photos-area").length>0){
				$(".photos-area .photo-box").each(function(){
					if($(this).find("a").hasClass("imgfix"))
						$(this).find("a").removeClass("imgfix");
						
					$(this).find("a").find("img").css("margin-left","0");
					$(this).find("a").find("img").css("margin-top","0");
				});		
				
			}
					
		}

		/*function gridBoxImgUINew($parent) {
			setTimeout(function(){
				$($parent).each(function() {
					$(this).find(".photo-holder").each(function(e, v) {
						var cont_width=$(v).width();
						var cont_height=$(v).height();

						$(v).find("img").css('width', cont_width+'px');
						$(v).find("img").css('height', cont_height+'px');
						
					});
				});
			},400);
		}*/

		function gridBoxImgUI($parent) {
			/*$($parent).each(function(){
				if(!$(this).find(".photo-holder").find('img').hasClass('defaultprofile')) {
					if($(this).find('.general-box').hasClass("imgfix")) {
						$(this).find('.general-box').removeClass("imgfix");
					}
						
					$(this).find(".photo-holder").find('img').css("margin-left","0");
					$(this).find(".photo-holder").find("img").css("margin-top","0");
				}
			});	*/

			$($parent).each(function() {
				$(this).find(".photo-holder").each(function(e, v) {
					//if($(v).find('img').hasClass('defaultprofile')) {
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						$(v).find("img").css('width', cont_width+'px');
						$(v).find("img").css('height', cont_height+'px');
					/*} else {
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")) {										
							if(img_width < cont_width) {							
								$(v).addClass("imgfix");
							}
						} 
						
						if(img_width > cont_width) {						
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
					}*/
				});
			});
		}

		function wallImageSliderFixClass(){
			$(".user-photos ul.lightSlider > li").each(function(){
					
				if($(this).find("a").hasClass("imgfix"))
					$(this).find("a").removeClass("imgfix");
					
				$(this).find("a").find("img").css("margin-left","0");
				$(this).find("a").find("img").css("margin-top","0");
			});		
		}
		
		function fixImageUI(which){
		
			var pimgholder_count=0;	
			var pcount=0;
		
			/* post image fixes */		
			var postlistSel = ".post-list .post-holder";
			
			if($(".main-content").hasClass("moments-page")){
				postlistSel = ".tab-pane.main-pane.active .post-list .post-holder";			
			}
			
			$(postlistSel).each(function(){
				pcount++;
				
				//if((which=="newpost" && pcount==1) || (!which)){
				if((which=="newpost") || (!which)){
					
					if($(this).find(".post-img").length > 0 ){				
						pimgholder_count++;
						
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
												
							if($(v).hasClass("himg-box")){
								if(img_width < cont_width){
									$(v).addClass("imgfix");
								}
							} 
							
							if(img_width > cont_width){
								
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
							$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
						});
					
					}
				}
				//if(which=="newpost" && pcount==1){return false;}
				
			});
			
			/* end post image fixes */
			
			/* fix images inside popup - share */
			if(which=="popup-images"){
				var pcount=0;
				$(".modal .post-list .post-holder").each(function(){
					pcount++;
					
					if(pcount==1){
						
						if($(this).find(".post-img").length > 0 ){				
							pimgholder_count++;
							
							$(this).find(".pimg-holder").each(function(e, v){
								
								var cont_width=$(v).width();
								var cont_height=$(v).height();
								var img_width=$(v).find("img").width();
								var img_height=$(v).find("img").height();
								
								if($(v).hasClass("himg-box")) {										
									if(img_width < cont_width) {							
										$(v).addClass("imgfix");
									}						
								} 
								
								if(img_width > cont_width) {						
									var iwidth = resizeToWidth(img_width,img_height,cont_height);
									var lfix= ( iwidth - cont_width ) / 2;
									$(v).find("img").css("margin-left","-"+lfix+"px");							
								}
								
								if(img_height > cont_height || $(v).hasClass("imgfix")){
									var iheight = resizeToHeight(img_width,img_height,cont_width);
									var tfix= ( iheight - cont_height ) / 2;
									$(v).find("img").css("margin-top","-"+tfix+"px");							
								}
								$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
							});
						}
					}
					if(which=="newpost" && pcount==1){return false;}
					
				});
				
			}
			/* end fix images inside popup - share */
			
			/* reivew post image fixes */
					
			if($("body").find(".reviewpost-tab").css("display")=="block" || $("body").find(".reviewphoto-tab").css("display")=="block"){
				
				resetImageFixClass(".reviews-list");
				/* post image fixes */		
				$(".reviews-list .post-holder").each(function(){
									
					if($(this).find(".post-img").length > 0 ){				
						pimgholder_count++;
						
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
												
							if($(v).hasClass("himg-box")){
								if(img_width < cont_width){
									$(v).addClass("imgfix");
								}
							} 
							
							if(img_width > cont_width){
								
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
							$(v).find("img").animate({opacity: 1},2000);
						});
					
					}			

				});
			}
			
			/* end reivew post image fixes */
			
			/* saved post image fixes */
					
			pimgholder_count=0;	
			pcount=0;
			if($("body").find(".saved-content").css("display")=="block"){
				
				resetImageFixClass(".saved-content");
				/* post image fixes */		
				$(".saved-content .post-holder").each(function(){
									
					if($(this).find(".post-img").length > 0 ){				
						pimgholder_count++;
						
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
												
							if($(v).hasClass("himg-box")){
								if(img_width < cont_width){
									$(v).addClass("imgfix");
								}
							} 
							
							if(img_width > cont_width){
								
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
							
						});
					
					}			

				});
			}
			
			pimgholder_count=0;	
			pcount=0;
			if(which=="openSavedPost"){
				$(".saved-post-detail .post-holder").each(function(){				
					pcount++;
					if($(this).parents(".saved-post-detail").css("display")=="block"){
						
						if($(this).find(".post-img").length > 0 ){				
							pimgholder_count++;
							
							$(this).find(".pimg-holder").each(function(e, v){
								
								var cont_width=$(v).width();
								var cont_height=$(v).height();
								var img_width=$(v).find("img").width();
								var img_height=$(v).find("img").height();
													
								if($(v).hasClass("himg-box")){
									if(img_width < cont_width){
										$(v).addClass("imgfix");
									}
								} 
								
								if(img_width > cont_width){
									
									var iwidth = resizeToWidth(img_width,img_height,cont_height);
									var lfix= ( iwidth - cont_width ) / 2;
									$(v).find("img").css("margin-left","-"+lfix+"px");							
								}
								
								
								if(img_height > cont_height || $(v).hasClass("imgfix")){
									var iheight = resizeToHeight(img_width,img_height,cont_width);
									var tfix= ( iheight - cont_height ) / 2;
									$(v).find("img").css("margin-top","-"+tfix+"px");							
								}
								$(v).find("img").animate({opacity: 1},2000);						
							});
						
						}
						
					}
				});
			}	
			
			/* end saved post image fixes */
			
			/* popup post image fixes */		
			$(".mfp-content .popup-area .post-holder").each(function() {
				
				if($(this).find(".post-img").length > 0 ) {				
					pimgholder_count++;
					
					$(this).find(".pimg-holder").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){
							if(img_width < cont_width){
								$(v).addClass("imgfix");
							}
						} 
						
						if(img_width > cont_width){
							
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
						
					});
				
				}			

			});
			
			/* end popup post image fixes */		
			
			/* wall page stuff - image fixes */
			var gb_count=0;
			if($(".page-wrapper").hasClass("wallpage")){
				
				var allSections=["photo","destination","like"];
				$.each(allSections,function(i,v){	
					if($(".wallcontent-column .grid-list").hasClass("photo-list")){
						$(".wallcontent-column ."+v+"-list .grid-box").each(function(){
							gb_count++;
							
							var cont_width=$(this).width();
							if(v=="like")
								cont_width=$(this).find(".imgholder").width();
							
							var cont_height=$(this).height();
							if(v=="like")
								cont_height=$(this).find(".imgholder").height();
							
							var img_width=$(this).find("img").width();
							var img_height=$(this).find("img").height();
							
							if($(this).find("."+v+"-box").hasClass("himg-box")){
								if(img_width < cont_width){
									$(this).find("."+v+"-box").addClass("imgfix");
								}
							} 
						
							if(img_width > cont_width){							
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(this).find("."+v+"-box").find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(this).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(this).find("."+v+"-box").find("img").css("margin-top","-"+tfix+"px");							
							}
							
						});
					}
				});			
			}

			/* share popup - image fixes */
			pcount=0;
			if(which=="share-popup"){
				$(".sharepost-popup .org-post .post-holder").each(function(){
					pcount++;	
					if($(this).find(".post-img").length > 0 ){				
						pimgholder_count++;
						
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
												
							if($(v).hasClass("himg-box")){
								if(img_width < cont_width){
									$(v).addClass("imgfix");
								}
							} 
							
							if(img_width > cont_width){
								
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
							$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
						});
					
					}			
					
				});			
			}
			
			/* not-messages - image fixes */
			pcount=0;
			if(which=="not-messages"){
				
				$(".msg-listing li").each(function(){
						
					if($(this).find(".chat-img-wrapper").length > 0 ){				
							var img_width=$(this).find(".chat-img-wrapper").find("img").width();
							var img_height=$(this).find(".chat-img-wrapper").find("img").height();
							if(img_width > img_height){
								$(this).find(".chat-img-wrapper").find("img").addClass('himg');	
								$(this).find(".chat-img-wrapper").find("img").removeClass('vimg');								
							}
							else{
								$(this).find(".chat-img-wrapper").find("img").removeClass('himg');	
								$(this).find(".chat-img-wrapper").find("img").addClass('vimg');	
							}
						
					
					}			
					
				});			
			}
			
			/* not-messages - image fixes */
			pcount=0;
			if(which=="main-msgwindow"){
				
				$(".albums-grid .grid-box").each(function(){
					var img_width=$(this).find(".imgholder").find("img").width();
					var img_height=$(this).find(".imgholder").find("img").height();
					if(img_width > img_height){
					
						$(this).find(".imgholder").find("img").addClass('himg');	
						$(this).find(".imgholder").find("img").removeClass('vimg');								
					}
					else{
					
						$(this).find(".imgholder").find("img").removeClass('himg');	
						$(this).find(".imgholder").find("img").addClass('vimg');	
					}
				});			
			}
			
			/* activity post image fixes */
					
			pimgholder_count=0;	
			pcount=0;
			if($("body").find(".activity-content").css("display")=="block"){
				
				resetImageFixClass(".activity-content");
				/* post image fixes */		
				$(".activity-content .post-holder").each(function(){
									
					if($(this).find(".post-img").length > 0 ){				
						pimgholder_count++;
						
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
												
							if($(v).hasClass("himg-box")){
								if(img_width < cont_width){
									$(v).addClass("imgfix");
								}
							} 
							
							if(img_width > cont_width){
								
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
							
						});
					
					}			

				});
			}
			
			pimgholder_count=0;	
			pcount=0;
			if(which=="openActivityPost"){
				$(".activity-post-detail .post-holder").each(function(){				
					pcount++;
					if($(this).parent(".activity-post-detail").css("display")=="block"){
						
						if($(this).find(".post-img").length > 0 ){				
							pimgholder_count++;
							
							$(this).find(".pimg-holder").each(function(e, v){
								
								var cont_width=$(v).width();
								var cont_height=$(v).height();
								var img_width=$(v).find("img").width();
								var img_height=$(v).find("img").height();
													
								if($(v).hasClass("himg-box")){
									if(img_width < cont_width){
										$(v).addClass("imgfix");
									}
								} 
								
								if(img_width > cont_width){
									
									var iwidth = resizeToWidth(img_width,img_height,cont_height);
									var lfix= ( iwidth - cont_width ) / 2;
									$(v).find("img").css("margin-left","-"+lfix+"px");							
								}
								
								
								if(img_height > cont_height || $(v).hasClass("imgfix")){
									var iheight = resizeToHeight(img_width,img_height,cont_width);
									var tfix= ( iheight - cont_height ) / 2;
									$(v).find("img").css("margin-top","-"+tfix+"px");							
								}
								$(v).find("img").animate({opacity: 1},2000);						
							});
						
						}
						
					}
				});
			}	
			
			/* end activity post image fixes */
			
			/* activitiy post - image fixes */
			
			if(which=="activity"){
				
				resetActivityImages();
				
				$(".activity-content .activity-box").each(function(){
					if($(this).find(".imgpreview").length > 0 ){				
											
						$(this).find(".pimg-holder").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
							
							if($(v).hasClass("himg-box")){										
								
								if(img_width < cont_width){							
									$(v).addClass("imgfix");
								}
							
							} 
							
							if(img_width > cont_width){
																				
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
							$(v).find("img").css({visibility:"visible", opacity: 0.0}).animate({opacity: 1.0},200);
						});
					
					}
					
				});
				
				/* activity - photos image fix */
				$(".activity-content .activity-post-detail").each(function(){
					$(this).find(".albums-grid .grid-box").each(function(){
							
						var img_width=$(this).find(".imgholder").find("img").width();
						var img_height=$(this).find(".imgholder").find("img").height();
						
						if(img_width > img_height){
						
							$(this).find(".imgholder").find("img").addClass('himg');	
							$(this).find(".imgholder").find("img").removeClass('vimg');								
						}
						else{
						
							$(this).find(".imgholder").find("img").removeClass('himg');	
							$(this).find(".imgholder").find("img").addClass('vimg');	
						}
											
					});
				});
				
			}
		
			if(which=="photo-gallery"){
				
				resetPhotoGalleryImageFixClass();
			
				$(".subtab-photos.active .photo-gallery .thumbs-img ul > li").each(function(){
															
					$(this).find("a").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}
						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
					});
				
				});
			
			}
		
			/* wall page : photos - image slider fixing */
			if(which=="wall-photoslider"){
				
				wallImageSliderFixClass();
			
				$(".user-photos ul.lightSlider > li").each(function(){
															
					$(this).find("a").each(function(e, v){
						
						var cont_width=$(v).width();
						var cont_height=$(v).height();
						var img_width=$(v).find("img").width();
						var img_height=$(v).find("img").height();
						
						if($(v).hasClass("himg-box")){										
							
							if(img_width < cont_width){							
								$(v).addClass("imgfix");
							}
						
						} 
						
						if(img_width > cont_width){
																			
							var iwidth = resizeToWidth(img_width,img_height,cont_height);
							var lfix= ( iwidth - cont_width ) / 2;
							$(v).find("img").css("margin-left","-"+lfix+"px");							
						}
						
						if(img_height > cont_height || $(v).hasClass("imgfix")){
							var iheight = resizeToHeight(img_width,img_height,cont_width);
							var tfix= ( iheight - cont_height ) / 2;
							$(v).find("img").css("margin-top","-"+tfix+"px");							
						}
					});
				
				});
			
			}

			if(which=="wall-photoalbums") {
				wallPhotoAlbumsSliderFixClass();
				if($(".albums-area").css("display")=="block" && $(".albums-area").length>0){
					$(".albums-area .album-box").each(function(){

						$(this).find("a").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
							
							if($(v).hasClass("himg-box")){										
								if(img_width < cont_width){							
									$(v).addClass("imgfix");
								}
							
							} 
							
							if(img_width > cont_width){
																				
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
						});
					
					});
				}
				
				if($(".photos-area").css("display")=="block" && $(".photos-area").length>0){
					//alert('ALERT III');
					$(".photos-area .photo-box").each(function(){
																
						$(this).find("a").each(function(e, v){
							
							var cont_width=$(v).width();
							var cont_height=$(v).height();
							var img_width=$(v).find("img").width();
							var img_height=$(v).find("img").height();
							
							if($(v).hasClass("himg-box")){										
								
								if(img_width < cont_width){							
									$(v).addClass("imgfix");
								}
							
							} 
							
							if(img_width > cont_width){
																				
								var iwidth = resizeToWidth(img_width,img_height,cont_height);
								var lfix= ( iwidth - cont_width ) / 2;
								$(v).find("img").css("margin-left","-"+lfix+"px");							
							}
							
							if(img_height > cont_height || $(v).hasClass("imgfix")){
								var iheight = resizeToHeight(img_width,img_height,cont_width);
								var tfix= ( iheight - cont_height ) / 2;
								$(v).find("img").css("margin-top","-"+tfix+"px");							
							}
						});
					});
				}
			}
		
		}
	/* FUN end fix image centering */
			
	/* FUN after resize */
		function doneResizing(){
			/* manage left menu */
			setLeftMenu();		
			/* end manage left menu */
			
			var isWall=$(".page-wrapper").hasClass("wallpage");
			if(isWall){			
				initPhotoCarousel("wallpage");
			}
			
			$(".nice-scroll").getNiceScroll().resize();
			
			var win_w=$(window).width();
			if(win_w>1024){		
				initNiceScroll(".nice-scroll");
			}
			else{
				$(".sidemenu").css("opacity",1);			
				removeNiceScroll(".nice-scroll");			
			}
			/* FUN set single page height */
			setSinglePageHeight(); 
			/* FUN end set single page height */
			
			/* show travelbuddy advanced search */
			reset_drop_searcharea();
			/* end show travelbuddy advanced search */
					
			/* show general-details page summery expandable */
			reset_gdetails_expandable();
			/* end general-details page summery expandable */
			
			/* manage places tabs selection */

			/* manage message list for mobile */
				manageMessagePage();
			/* end manage message list for mobile */
			
			if($('.place-wrapper').length) {
				if($(window).width()<=767){
					if($('.home').find('a.active').length) {
						$('.header-section').show();
					} else {
						$('.header-section').hide();
					}
				} else {
					$('.header-section').show();
				}
			}

			/* end manage places tabs selection */
			
			/* manage header hide */
			if($('body').hasClass("hideHeader")){
				$('body').removeClass("hideHeader");
			}
			/* manage end header hide */
			
			
			/* manage keepopen */
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("full-wrapper")){
				
				if( win_w<=450 ){
					if($(".search-holder").hasClass("keepopen")){
						$(".search-holder").removeClass("keepopen");
					}
				}else{
					
					if(!$(".page-wrapper").hasClass("noopened-search")){
						if(!$(".search-holder").hasClass("keepopen")){
							$(".search-holder").addClass("keepopen");
						}
					}
				}
				
			}
			/* end manage keepopen */
			
			/* trip page */
			
				/* reset left section */
				if(win_w>568){
					$(".trip-page .trip-mapview .desc-box").attr("style","");
				}
				/* end reset left section */
				
				/* reset mobile fullview */		
				if(win_w>568){
					if($(".trip-wrapper").hasClass("mobile-fullview")){
						$(".trip-wrapper").removeClass("mobile-fullview");
					}
				}else{
					$(".page-wrapper .trip-mapview .side-section .section-layer").each(function(){
						
						if($(this).hasClass("front") && $(this).attr("id")!="trip-list"){
							$(".trip-wrapper").addClass("mobile-fullview");
						}
						
					});
				}
				/* end reset mobile fullview */
			
			/* end trip page */
			
			var fromTop=$(window).scrollTop();
			if($('.page-wrapper').length > 0){
				
				var top = $('.page-wrapper').offset().top;
				setNewPostBubble(win_w,top,'page-wrapper');
				
			}
			
			// reset notification bar
			if(win_w > 767)
				$("body").find(".mobile-whitebar").slideUp();
			
			// reset split page : mbl-fiter-icon
			resetMblFilterIcon();
			
			// reset settings menu for mobile
			if($(".page-wrapper").hasClass(".settings-wrapper")){
				if(win_w < 768)
					resetInnerPage("settings","hide");
			}
			
			// reset wall menu for mobile		
			if(win_w < 768){
				if($(".page-wrapper").hasClass("wallpage") || $(".page-wrapper").hasClass("fixed-wrapper")){
					$(".page-wrapper").removeClass("fixed-wrapper");
				}
				var selTab = $(".main-content .main-tabpane.active").attr("tabname");
				if(selTab != "Wall"){			
					resetInnerPage("wall","hide");
				}
			}
			
			// reset fixed header
			var fromTop=$('body').scrollTop();
			var fromTop1=$(window).scrollTop();
			
			/* fixed header */				
			if(fromTop > 0 || fromTop1 > 0){
				if(win_w>1024){
					$(".header-section").addClass("fixed-header");
					$(".page-wrapper").addClass('fixed-wrapper');			
				}else{
					$(".header-section").removeClass("fixed-header");
					$(".page-wrapper").removeClass('fixed-wrapper');
				}

				if($(".page-wrapper").hasClass("transheadereffectall")) {
					$(".page-wrapper").addClass('page-scrolled');
					$(".page-wrapper").removeClass('JIS3829');
				} else if(win_w<=568) {
					if($(".page-wrapper").hasClass("transheadereffect")){
						$(".page-wrapper").addClass('page-scrolled')
					}
				}
			} else {
				$(".header-section").removeClass("fixed-header");
				$(".page-wrapper").removeClass('fixed-wrapper');
				$(".page-wrapper").removeClass('page-scrolled');
				
				if($(".page-wrapper").hasClass("transheadereffectall")) {
					$(".page-wrapper").removeClass('page-scrolled');
					$(".page-wrapper").addClass('JIS3829');
				} else if(win_w<=568) {
					if($(".page-wrapper").hasClass("transheadereffect")){
						$(".page-wrapper").removeClass('page-scrolled')
					}
				}
			}
			/* end fixed header */
			
			/* groups/events detail page : reset menu scrollbar */
			if($(".main-page").hasClass("generaldetails-page") && win_w<768){
				if($(".main-page").hasClass("groups-page") || $(".main-page").hasClass("commevents-page")){
					$(".hori-menus").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
				}
			}	
		}
	/* FUN end after resize */

	/* FUN unreg user login popup */
		function openLoginPopup(){ 
			$.ajax({
				type: 'POST', 
				url: '?r=site/general-login-popup', 
				success: function(data) {
					$("#login_modal").html(data);	
					setTimeout(function(){
						$('#login_modal').modal('open');			
						flipSectionTo("login");
						resetRegisterSteps();
						$(".signup-part #create-account").show();
						initNiceScroll("#login_modal");
						$('select').material_select();
						
						var cropper = $('.cropper');
						if (cropper.length) {
						  $.each(cropper, function(i, val) {
						    var uploadCrop;

						    var readFile = function(input) {
						      if (input.files && input.files[i]) {
						        var reader = new FileReader();

						        reader.onload = function(e) {
						          uploadCrop.croppie('bind', {
						            url: e.target.result
						          });
						        };

						        reader.readAsDataURL(input.files[i]);
						      } else {
						        alert("Sorry - you're browser doesn't support the FileReader API");
						      }
						    };

							uploadCrop = $('.js-cropping').croppie({ // TODO: fix so its selects right element
						      viewport: {
						        width: 200,
						        height: 200
						      },
						      boundary: {
						        width: 375,
						        height: 360
						      },
						      enableOrientation: true
						    });
						    
						    $('.js-cropper-upload').on('change', function() {
						      $('.crop').show(); // TODO: fix so its selects right element
						      readFile(this);
						    });
						    
						    $('.js-cropper-rotate--btn').on('click', function(ev) {
						      uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
						    });

						    $('.js-cropper-result--btn').on('click', function(ev) {
						      uploadCrop.croppie('result', 'canvas').then(function(resp) {
						        popupResult({
						          src: resp
						        });
						      });
						    });

						    var popupResult = function(result) {
						    	if(result.src != undefined && result.src != '') {
									$.ajax({
									    type: 'POST',
									    url: "?r=site/profile-image-crop",
									    data: {
									    	file: result.src
									   	},
									    success: function (data) {
									    	var $data = $.parseJSON(data);
									    	if($data.status != undefined && $data.status == 'success') {
												if($data.url != undefined && $data.url != '') {
													Materialize.toast('Profile picture changed.', 2000, 'green'); 
													$('.setting-pic').find('img').attr('src', $data.url);
													$('.profile-top').find('img').attr('src', $data.url);
													var $html = '<img src="' + $data.url + '" />';
													$('.js-cropper-result').show();	
													$('.js-cropper-result').html($html);
													$('.crop').hide();
													$('.image-upload').show();
												}
											}
									    }
									});
								}
						    };
						  });
						}

						grecaptcha.render(document.getElementById('g-recaptcha'), {
						  'sitekey' : '6LdZs1sUAAAAAKtNHR72Wb__55sQXghN-AKs_Qct'
						});
					},500);		
				},
			});
		}
	/* FUN end unreg user login popup */

	/* FUN home page layout */
		function flipSectionTo(which){
		
			$('html, body').animate({ scrollTop: 0 }, 'slow');
					
			if(which=="login"){
				$(".signup-part").css("display","none");
				$(".forgot-part").css("display","none");								
				$(".login-part").css("display","inline-block");								
				$(".search-part").css("display","none");				
			}
			else if(which=="signup"){
				resetRegisterSteps();
				$(".signup-part #create-account").show();
			
				$(".signup-part").css("display","inline-block");
				$(".forgot-part").css("display","none");								
				$(".login-part").css("display","none");				
				$(".search-part").css("display","none");				
				
			}else if(which=="search"){
				$(".search-part").css("display","inline-block");
				$(".forgot-part").css("display","none");								
				$(".login-part").css("display","none");				
				$(".signup-part").css("display","none");
				
			}
			else{				
				$(".forgot-part").css("display","inline-block");
				$(".signup-part").css("display","none");
				$(".login-part").css("display","none");				
				$(".search-part").css("display","none");				
				
			}	
		}
	/* FUN end home page layout */

	/* FUN set dropdown value on change */
		function setDropVal(obj){
			
			$(obj).parents(".dropdown").find('.dropdown-toggle').html('<span class="getvalue">'+$(obj).html()+ "</span> <span class='caret'></span>");
			$(obj).parents(".dropdown").find('.dropdown-toggle').val($(obj).html());
		}
	/* FUN end set dropdown value on change */

	/* swap normal/edit mode */
		function swapMode(obj){
			
			var nearestMainParent=$(obj).parents(".swapping-parent");
			
			var whichMode;
			if(nearestMainParent.find(".normal-mode").css("display")=="none")
				whichMode="edit"
			else
				whichMode="normal";
			
			if(whichMode=="normal"){
				nearestMainParent.find(".normal-mode").slideUp();
				nearestMainParent.find(".edit-mode").slideDown();
				setWallEditTextarea(obj);
			}
			else{
				nearestMainParent.find(".normal-mode").slideDown();
				nearestMainParent.find(".edit-mode").slideUp();		
			}
		}
	/* end swap normal/edit mode */

	/* show all content - wall info */
		function showAllContent(obj){
			var sparent=$(obj).parents(".para-section");
			sparent.find(".para").addClass("opened");
			$(obj).hide();
			sparent.find(".read_Less").show();
		}
	/* end show all content - wall info */

	/* show all content - wall info */
	/* end show all content - wall info */

	/* FUN set single page height */
		function setSinglePageHeight(){
			var win_w=$(window).width();
			var win_h=$(window).height();
			 
			//$(".page-wrapper").css("min-height",win_h);
			var header_h=$(".header-section").height();
			var mcontent_h=$(".main-content").height();
			var footer_h=$(".footer-section").height();
			if( (header_h + mcontent_h + footer_h ) < win_h){
			$(".footer-section").addClass("abs-footer");
			}
			else{
			$(".footer-section").removeClass("abs-footer");
			}
		}
	/* FUN end set single page height */
	 
	/* FUN pin image */ 
		function pinImage(iname,pid,type) {
			if(iname != '')
			{
				$.ajax({
					type: 'POST',
					url: '?r=site/pinimage',
					data: {
						iname: iname,
						pid: pid,
						type: type,
					},
					success: function (data)
					{
						var result = $.parseJSON(data);
						if(result.status != undefined && result.status == true) {
							$lbl = result.label;
							$inameclass = result.inameclass;

							var curPin=$(".pinlink.pin_"+$inameclass);
							if(curPin.length) {
								if($lbl == 'pin') {
									curPin.addClass('active');
									Materialize.toast('Pinned', 2000, 'green');
								} else {
									curPin.removeClass('active');
									Materialize.toast('Unpinned', 2000, 'green');
								}
							}
						}
					}
				});				
			}
		}
	/* FUN end pin image */ 

	/* FUN general initializations */ 
		function setGeneralThings(){
		}
	/* FUN end general initializations */ 

	/* set new post bubble */
		function setNewPostBubble(winw,top,fromWhere){
				
			if (winw > 767) {
				if(fromWhere == "window"){
					if (top > 250) {
						$('.new-post-mobile').find('a').show();
					}
					else{
						$('.new-post-mobile').find('a').hide();
					}
				}else if(fromWhere == "fixed-layout"){
					if (top < 250) {
						
						$('.new-post-mobile').find('a').show();
					}
					else{
						$('.new-post-mobile').find('a').hide();
					}
				}else if(fromWhere == "page-wrapper"){
					
					var fromTop=$(window).scrollTop();
					if (fromTop > 250 || top < -250) {
						var win_w = $(window).width();
						$('.new-post-mobile').find('a').show();
					}
					else{
						$('.new-post-mobile').find('a').hide();
					}
				}else if(fromWhere == "body"){
					if (top < -250) {
						$('.new-post-mobile').find('a').show();
					}
					else{
						$('.new-post-mobile').find('a').hide();
					}
				}else{ // fromWhere =  'docready'
					if (top > 250) {
						$('.new-post-mobile').find('a').show();
					}
					else{
						$('.new-post-mobile').find('a').hide();
					}
				}
			}else{
				$('.new-post-mobile').find('a').show();			
			}		
		}
	/* end set new post bubble */

	/* manage new post bubble */
		function manageNewPostBubble(doWhat){
			if(doWhat == "hideit"){
				$(".fixed-layout").addClass("hide-addflow");
			}else{ // doWhat == "showit"
				if($(".fixed-layout").hasClass("hide-addflow"))
					$(".fixed-layout").removeClass("hide-addflow");
			}
		}
	/* end manage new post bubble */

	/* open this popup */
		function openThisPopup(popupid){ 
			setTimeout(function(){
				setGeneralThings();
			},400);
		}
	/* end open this popup */

	/* FUN normal/detail mode */
		function close_all_detail(){
		   
		   $(".mode-holder").each(function(){
				
			   var detailmode=$(this).find(".detail-mode").css("display");		
			
			   if(detailmode!="none"){
					
					$(this).find(".normal-mode").slideDown(300);
					$(this).find(".detail-mode").slideUp(300);
					$(this).removeClass("opened");
				}
		   });
	    }
		
		function open_detail(obj) {
			close_all_detail();
			var sparent=$(obj).parents(".mode-holder");
			var detailmode=sparent.find(".detail-mode").css("display");
			if(detailmode=="none"){
				sparent.find(".normal-mode").slideUp(300);
				setTimeout(function(){
					sparent.find(".detail-mode").slideDown(300);
				},600);
				sparent.addClass("opened");
			}
			else{
				sparent.find(".normal-mode").slideDown(300);
				setTimeout(function(){
					sparent.find(".normal-mode").slideDown(300);
				},600);
				sparent.removeClass("opened");
			}
			initNiceScroll(".select-dropdown");

			setTimeout(function(){
				setGeneralThings();

				if($('.editable-summery').length) {
					$('.editable-summery').find('input.select-dropdown').css('overflow-y', 'unset');
				}
			},400);
	    }

		function close_detail(obj){
			
			var objParent=$(obj).parents(".mode-holder");
			var dmode=objParent.find(".detail-mode");
			var nmode=objParent.find(".normal-mode");
					
			var detailmode=dmode.css("display");

			if(detailmode=="none"){
				nmode.slideUp(300);
				dmode.slideDown(300);
				objParent.addClass("opened");
			}
			else{
				nmode.slideDown(300);
				dmode.slideUp(300);
				objParent.removeClass("opened");
			}
	   }
	/* FUN end normal/detail mode */
 
	/* FUN set rating stars */
		function setRating(obj, curRate){
			var startext="";
			if(curRate==1)
				startext="Poor";
			else if(curRate==2)
				startext="Good";
			else if(curRate==3)
				startext="Better";
			else if(curRate==4)
				startext="Superb";
			else if(curRate==5)
				startext="Excellent";
			else
				startext="Roll over stars, then click to rate";
		}

		function setRating(obj,rate){
			var win_w = $(window).width()
			var mparent = $(obj).parents(".setRating");
			var curRate=0;
			
			if(win_w < 100){
				if($(obj).parents(".popup-area").length > 0){
					mparent.find(".fa").each(function(){						
						$(this).removeClass("active");
						var dvalue=$(this).attr("data-value");
						if(dvalue<=rate){
							$(this).addClass("active");
							if(curRate < dvalue)
								curRate = dvalue;
						}
						
					});
					$(obj).parents(".new-post").addClass("expandReview");
					expandNewpost($(obj).parents(".new-post"));
				}
			}else{
				
				mparent.find(".fa").each(function(){						
					$(this).removeClass("active");
					var dvalue=$(this).attr("data-value");
					if(dvalue<=rate){
						$(this).addClass("active");
						if(curRate < dvalue)
							curRate = dvalue;
					}
					
				});
				$(obj).parents(".new-post").addClass("expandReview");
				expandNewpost($(obj).parents(".new-post"));
			}
			
			if(mparent.find(".star-text").length>0){
							
				var startext="";
				if(curRate==1)
					startext="Poor";
				else if(curRate==2)
					startext="Good";
				else if(curRate==3)
					startext="Better";
				else if(curRate==4)
					startext="Superb";
				else if(curRate==5)
					startext="Excellent";
				else
					startext="Roll over stars, then click to rate";
				mparent.find(".star-text").html(startext);
			}

			if($(obj).parents(".editRating").find(".star-text").length>0)
			{
				var id = $(obj).parents(".editRating").attr('data-prid');
				$("#placereviewrate"+id).val(rate);
			}
			else
			{
				$("#pagereviewrate").val(rate);
			}
		}
		function setStarText(obj,rate){
			
			var startext="Roll over stars, then click to rate";
			
			var mparent = $(obj).parent();
			var isActive=false;
			var curRate=0;
			
			mparent.find(".fa").each(function(){
				if($(this).hasClass("active")){
					isActive=true;
					var dvalue=$(this).attr("data-value");
					if(curRate < dvalue)
						curRate = dvalue;
				}
			});
			if(!isActive){
					
				if(rate==1)
					startext="Poor";
				else if(rate==2)
					startext="Good";
				else if(rate==3)
					startext="Better";
				else if(rate==4)
					startext="Superb";
				else if(rate==5)
					startext="Excellent";
				else
					startext="Roll over stars, then click to rate";
				
			} else {
				var deftext="";
				if(curRate==1)
					deftext="Poor";
				else if(curRate==2)
					deftext="Good";
				else if(curRate==3)
					deftext="Better";
				else if(curRate==4)
					deftext="Superb";
				else
					deftext="Excellent";

				if(rate > curRate) {
					if(rate==1)
						startext="Poor";
					else if(rate==2)
						startext="Good";
					else if(rate==3)
						startext="Better";
					else if(rate==4)
						startext="Superb";
					else if(rate==5)
						startext="Excellent";				
					else
						startext=deftext;
				}else{
					startext=deftext;
				}
			}
			mparent.find(".star-text").html(startext);
			
		}
	/* FUN end set rating stars */

	/* FUN edit rating stars */
		function editRating(obj,rate,id){
			
			var mparent = $(obj).parents(".setRating");
			mparent.find(".fa").each(function(){						
				$(this).removeClass("active");
				var dvalue=$(this).attr("data-value");
				if(dvalue<=rate){
					$(this).addClass("active");
				}
			});
			$(obj).parents(".new-post").addClass("expandReview");
			$("#pagereviewrate"+id).val(rate);
			expandNewpost();
		}
	/* FUN edit set rating stars */

	/* FUN manage search area */
		function mng_drop_searcharea(obj){	
			
			var sparent=$(obj).parents(".search-area");
			if(!sparent.hasClass("expanded")){
				sparent.addClass("expanded");
			}
			else{
				sparent.removeClass("expanded");
				
				if($(".gdetails-summery .search_string").length > -1){				
					$(".gdetails-summery .search_string").val(""); // clear search field			
				}
			}
			
		}
		function mbl_mng_drop_searcharea(obj,searcharea_id){
			$(".search-area#"+searcharea_id).find(".expand-link").trigger("click");
		}
		function reset_drop_searcharea(){
			
			var pageClass="";
			if($(".general-page").hasClass("refers-page"))
				pageClass=".refers-page";
			if($(".general-page").hasClass("moments-page"))
				pageClass=".moments-page";
			
			if(pageClass!=""){
				var sparent=$(pageClass).find(".search-area");
				
				var win_w=$(window).width();
				if(win_w>800){
					if(sparent.hasClass("expanded")){
						sparent.removeClass("expanded");
					}
				}
			}
				
		}
	/* FUN end manage search area */

	/* FUN manage expandable area */
		function invertExpandLink(obj, direction,maindir){
			// needs a fix for replacing tags
			var currentText=$(obj).html()+"";
			var actionText="";
			if(direction=="up"){						
				var replacing = '<i class="mdi mdi-menu-up"></i>';
				
				if(currentText.indexOf('<i class="mdi mdi-menu-down"></i>')>-1 && maindir=="maindown"){
					actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-menu-down"></i>'));				
					replacing = '<i class="mdi mdi-menu-up"></i>';
				}
				if(currentText.indexOf('<i class="mdi mdi-chevron')>-1 && maindir=="maindown"){
					actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-chevron-down"></i>'));
					replacing = '<i class="mdi mdi-chevron-up"></i>';
				}
				if(currentText.indexOf('<i class="mdi mdi-menu-right"></i>')>-1 && maindir=="mainright"){
					actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-menu-right"></i>'));				
					replacing = '<i class="mdi mdi-menu-down"></i>';
				}
				if(actionText.length>0){
					$(obj).html(actionText + replacing);
				}
				else{
					$(obj).html(replacing);
				}
			}
			else{
				var replacing = '<i class="mdi mdi-menu-down"></i>';			
							
				if(currentText.indexOf('<i class="mdi mdi-menu-up"></i>')>-1 && maindir=="maindown"){
					actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-menu-up"></i>'));				
					replacing = '<i class="mdi mdi-menu-down"></i>';
				}
				if(currentText.indexOf('<i class="mdi mdi-chevron')>-1 && maindir=="maindown"){
					actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-chevron-up"></i>'));				
					replacing = '<i class="mdi mdi-chevron-down"></i>';
				}
				if(currentText.indexOf('<i class="mdi mdi-menu-down"></i>')>-1 && maindir=="mainright"){
					actionText = currentText.substring(0,currentText.indexOf('<i class="mdi mdi-menu-down"></i>'));				
					replacing = '<i class="mdi mdi-menu-right"></i>';
				}
				if(actionText.length>0){
					$(obj).html(actionText + replacing);
				}
				else{
					$(obj).html(replacing);				
				}				
			}
			setTimeout(function(){
				$('.payment-info').animate({
			         scrollTop: 500
			    }, 1500);
			}, 400);
		}	
		function mng_expandable(obj,maindir){	
			var dir="";
			if(maindir=="hasClose" || maindir=="closeIt"){
				dir="maindown";			
			}else{
				if(maindir==undefined)
					dir="maindown";
				else
					dir=maindir;
			}
			var win_w=$(window).width();
			
			var sparent=$(obj).parents(".expandable-holder");
			if(maindir=="hasClose"){
				if(win_w<768){
					if(sparent.hasClass("mobilelist")){
						openPlacesMoreInfo(obj);
					}
				}else{
					
					if(!sparent.hasClass("expanded")){			
						sparent.addClass("expanded");
						sparent.find(".expandable-area").slideDown();
						$(obj).addClass("active");
						if($(obj).hasClass("invertsign")){
							invertExpandLink(obj, "up",dir);
						}
					}
				}
			}else if(maindir=="closeIt"){
				if(win_w<768){
					if(sparent.hasClass("mobilelist")){
						closePlacesMoreInfo(obj);
					}
				}else{
					
					if(sparent.hasClass("expanded")){			
						sparent.removeClass("expanded");
						sparent.find(".expandable-area").slideUp();		
						$(obj).removeClass("active");
						sparent.find(".expand-link").each(function(){
							if($(this).hasClass("active"))
								$(this).removeClass("active");	
						});	
					}
				}	
			}else if(maindir=="none"){
					
				if(sparent.hasClass("expanded")){			
					sparent.removeClass("expanded");
					sparent.find(".expandable-area").slideUp();		
					$(obj).removeClass("active");
					sparent.find(".expand-link").each(function(){
						if($(this).hasClass("active"))
							$(this).removeClass("active");	
					});	
				}else{
					sparent.find(".expandable-area").css('overflow', 'visible');
					sparent.find(".expandable-area").slideDown();
					$(obj).addClass("active");				
					sparent.addClass("expanded");
				}
			
			}else{
				
				if(!sparent.hasClass("expanded")){			
					sparent.addClass("expanded");
					sparent.find(".expandable-area").slideDown();
					$(obj).addClass("active");
					if($(obj).hasClass("invertsign")){
						invertExpandLink(obj, "up",dir);
					}
				}
				else{
					
					if($(obj).hasClass("active")) {				
						sparent.removeClass("expanded");
						sparent.find(".expandable-area").slideUp();		
						$(obj).removeClass("active");
						sparent.find(".expand-link").each(function(){
							if($(this).hasClass("active"))
								$(this).removeClass("active");	
						});
						if($(obj).hasClass("invertsign")){
							invertExpandLink(obj, "down",dir);
						}
					} else{
						$(obj).addClass("active");
						if($(obj).hasClass("invertsign")){
							invertExpandLink(obj, "up",dir);
						}
					}
					
				}
				if($(".main-page").hasClass("generaldetails-page") && (!$(".main-page").hasClass("groups-page") && !$(".main-page").hasClass("commevents-page"))){
					var win_w = $(window).width();
					if($(obj).parents(".gdetails-summery").length > -1 && win_w < 768){
						
						var initScroll = 180;				
						
						$('html, body').animate({ scrollTop: initScroll }, 'slow');
					}
				}
			}
		
			setTimeout(function(){
				$(".hori-menus").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
				
				setGeneralThings();
			},400);
		}		
	/* FUN end manage expandable area */

	/* FUN preview gallery */
		function previewImage(obj){
			
			var sparent=$(obj).parents(".photo-gallery");
			var imgsrc=$(obj).find("img").attr("src");
			sparent.find(".img-preview").find("img").attr("src",imgsrc);
		}
	/* FUN end preview gallery */

	/* Expand text for longer comment text */
		function explandReadMore(obj){
			
			var sparent = $(obj).parents(".shorten");
			sparent.removeClass("shorten");
			sparent.find("a.overlay").remove();
			
		}
	/* End Expand text for longer comment text */

	/* open view map inside about section */
		function showEventMap(obj){
			var sparent=$(obj).parents(".eventinfo-row");
			var mapholder=sparent.find(".mapholder");
			
			if(mapholder.css("display")=="block"){
				mapholder.slideUp();
				$(obj).html("Show map");
			}else{
				mapholder.slideDown();
				$(obj).html("Close map");
			}
		}
	/* end open view map inside about section */

	/* unverified user block */
		function toggleLoadingBtn(action){
			if(action == "SHOW"){
				$(postBtnEle).attr("disabled",false);
				$(".loading").css("display","none");
			}else{
				$(postBtnEle).attr("disabled",true);
				$(".loading").css("display","inline-block");
			}
		}
		function account_verify(t){
			if(t == 2) {
				$(".notice1 .post_loadin_img").css("display","inline-block");			
			}
			else if(t == 1){ 
				$(".unreg-modal .loading").css("display","inline-block");
			}     
	    }	
	/* end unverified user block */

	function uploadstuff(obj) {
		$cls = $(obj).attr('data-class');
		$('.turantupload').remove();
		storedFilesExsting = [];
		
		$totalImages = $('.getuploadstuffbox').length;
		$('.getuploadstuffbox').each(function(i, v) {
			if($(v).find('.check-image').hasClass('active-class')) {
				var imgCls = ''; 
				$imgSrc = $(v).find('.check-image').find('img').attr('src');
				var extn = $imgSrc.substr( ($imgSrc.lastIndexOf('.') +1) );
				if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "tif") {
					$current = [];
					var tmpImg = new Image();
					tmpImg.src = $imgSrc;
					$(tmpImg).one('load',function(){
						orgWidth = tmpImg.width;
						orgHeight = tmpImg.height;

						if(orgWidth > orgHeight){
							imgCls ="himg";
						} else if(orgWidth < orgHeight) {
							imgCls ="vimg";  
						} else {
							imgCls ="himg";
						}
					});

					$uniqId = Math.floor(Math.random() * 26) + Date.now();

					$current['lastModified'] = $uniqId;
					$current['name'] = $imgSrc;

					$htmlBlock = '<div class="img-box turantupload"> <img src="'+$imgSrc+'" class="thumb-image '+imgCls+'"> <div class="loader"></div> <a href="javascript:void(0)" class="removePhotoFile" data-code="'+$uniqId+'"> <i class="mdi mdi-close"></i> </a> </div>';

					$($cls).prepend($htmlBlock);

					storedFilesExsting.push($current);
					
					setTimeout(function(){
						HideImage();
					},400);
					
					setPostBtnStatus();
				}
			}
		});

		$('.customuploadbox').remove();
		$($cls).append( "<div class='img-box customuploadbox'><div class='custom-file addimg-box'><div class='addimg-icon'><i class='zmdi zmdi-plus zmdi-hc-lg'></i></div><input type='file'  class='upload custom-upload remove-custom-upload' title='Choose a file to upload' required='' data-class='"+$cls+"' multiple='true'/></div></div>" ); 
		$('#compose_uploadphotomodal').modal('close');
	}

	/* general function to upload image */
		function changePhotoFileInput(cls, obj, t) {
			if($(obj).hasClass('custom-upload-new')) {
				/*storedFiles = [];
				storedFilesExsting = [];*/
			}

			//Get count of selected files
			var countFiles = $(obj)[0].files.length;
			var image_holder = $(cls);
			if ( window.FileReader && window.File && window.FileList && window.Blob ) {
				for (var i = 0; i < countFiles; i++) {
					var imgCls='';
					file = obj.files[i];
					filename = file.name;
					extn = filename.substr( (filename.lastIndexOf('.') +1) );
					if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "tif") {
						lastModified.push(file.lastModified);
						storedFiles.push(file);
						readImage( file,  image_holder ); 					
						$(cls.replace(".img-row","").trim()).show();                              
						image_holder.show(); 
						$(".nice-scroll").getNiceScroll().resize();
						$(".post-photos").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"8px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)",autohidemode:"scroll"});
					}  else {
						Materialize.toast('Your photo has to be in file format of GIF, JPG, PNG or TIFF and less than 500k in size.', 2000, 'red');
						return false;
					}
				}

				if($(obj).hasClass('custom-upload-new')) {
					uploadstuff(obj);
				}

			} else {
				Materialize.toast('Your browser does not support file reader, please upgrade your browser.', 2000, 'red');
				return false;
			}

			initDropdown();
			mainFeedPostBtn();	
		}

		function readImage (file, prepandto) {
			ShowImage();
		    var reader = new FileReader();
		    reader.addEventListener("load", function (e) {
			var image  = new Image();
			image.addEventListener("load", function () {
				var imgCls = ''; 
				if(image.width>image.height){
					imgCls ="himg";
				} else if(image.width<image.height) {
					imgCls ="vimg";
				} else {newct
					imgCls ="himg";
				}

				$("<img />", { 
				"src": e.target.result,
				"class": "thumb-image "+imgCls,
				})
				.add("<div class='loader'></div><a href='javascript:void(0)' class='removePhotoFile' data-code='"+lastModified[newct]+"'><i class='mdi mdi-close' ></i></a>") 
				.wrapAll("<div class='img-box'></div>")
				.parents()
				.prependTo(prepandto);	

				if($('#upload-gallery-popup').length) {
					if($('#upload-gallery-popup').hasClass('open')) {
						if($('.photos.tab').length) {
							if($('.photos.tab').find('a').hasClass('active')) {
								photoUpload.unshift({});
							}
						}
					}
				}

				$('#compose_uploadphotomodal').modal('close');
				newct++; 
				setTimeout(function(){
					HideImage();
				},400);
				
				setPostBtnStatus();
			});
			image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;
			if (useBlob) {
			  window.URL.revokeObjectURL(file);
			}
		  });
		  reader.readAsDataURL(file);  
		}
	/* end general function to upload image */

	/* FUN clear input text animation line */
		function titleUnderline(e){			
			clearUnderline();
			$(e).toggleClass("focused");
			$(e).children("input[type='text']").focus();
		}
		function clearUnderline(){
			$(".sliding-middle-out").each(function(){
				if($(this).hasClass("focused")) $(this).removeClass("focused");
			});
		}
	/* FUN end clear input text animation line */

	/* FUN share post image fix */
		function fixImageSharePopup(){	
			setTimeout(function(){fixImageUI("popup-images");},400);
		}
	/* FUN end share post image fix */

	/* set first letter to uppercase */
		function firstToUpperCase(string) {
			return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
		}      
	/* end set first letter to uppercase */

	/* set dropdown for icondrop */
		function setIcondropValue(){
			
			var thisVal=$(this).html().trim();
			  var newVal="<span class='mdi mdi-lock'></span>";
			  if($(this).parents("li").attr("class").indexOf("private")>=0)
				newVal="<span class='mdi mdi-lock'></span>";
			  else if($(this).parents("li").attr("class").indexOf("friends")>=0)
				newVal="<span class='mdi mdi-account'></span>";
			  else
				newVal="<span class='mdi mdi-earth'></span>";
				
			  $(this).parents(".dropdown").find('.dropdown-toggle').html(newVal);
			  $(this).parents(".dropdown").find('.dropdown-toggle').val($(this).html());
		}
	/* end set dropdown for icondrop */

	/* add/remove friend */	
		function acceptFriendRequest(from_id,to_id){	
			$.ajax({
				url: '?r=friend/accept-friend', 
				type: 'POST',
				data: 'to_id=' + to_id + '&from_id=' + from_id,
				success: function (data) {
					if(data){
						$('.accept_'+from_id).hide();
						$('.showlabel_'+from_id).show();
						$('#acceptmsg_'+from_id).html(data);
						$('#acceptmsg_'+from_id).show();
						$('.1travfriends_'+from_id).hide();
						$('.2travfriends_'+from_id).hide();
						$('.3travfriends_'+from_id).show();
						var reqbudge = $('#friendcount').val();
						reqbudge = reqbudge - 1;
						if(reqbudge == 0){$('#request_budge').hide();}
						else{$('#request_budge').html(reqbudge);}
					}
				}
			});
		}
	/* end add/remove friend */
	
	/* Start remove friend / unfriend / cancle_friend_request / delete friend */
		function removeFriend(rid, from_id, to_id, type='')
		{
			$.ajax({
				url: '?r=friend/delete-request', //Server script to process data
				type: 'POST',
				data: 'from_id=' + from_id + '&to_id=' + to_id,
				success: function (data)
				{
					if(type == 'unfriend' || type == 'cancle_friend_request') {
						$('.friend_'+rid).remove();								
						var frnd_count = $('.friends-grid .friend_search .grid-box').length;
						$(".frnd_count").html(frnd_count);
						
						if(type == 'cancle_friend_request') {
							Materialize.toast('Cancelling friend request.', 2000, 'green');
							$('.acceptmsg_'+rid).html('');
						} else {
							Materialize.toast('Unfriend user.', 2000, 'green'); u
						}
						
						$(".addfriend").html('<a href="javascript:void(0);" onclick="addFriend(\''+rid+'\')">Add Friend</a>');
						$(".unfriend").html('<a href="javascript:void(0);" onclick="addFriend(\''+rid+'\')">Add Friend</a>');
						$('.add-icon_'+rid).css('display','block');
						$('.add-icon_'+rid).html('<a href="javascript:void(0);" class="gray-text-555" onclick="addFriend(\''+rid+'\')"><i class="mdi mdi-account-plus"></i></a>');
						$('.sendmsgdisplay_'+rid).html('');
						$('.sendmsg_'+rid).hide();
						$('.people_'+rid).show();
						$('.travfriends_'+rid).html('<a href="javascript:void(0)" title="Add friend" class="gray-text-555"><i class="mdi mdi-account-plus" onclick="addFriend(\''+rid+'\')"></i><i class="mdi mdi-account-minus dis-none" onclick="removeFriend(\''+rid+'\',\''+from_id+'\',\''+to_id+'\')"></i></a><a href="javascript:void(0)" class="tb-pyk-remove"></a>');

						$('.travfriends_'+rid).find('.people_').addClass('people_'+rid);
						$('.travfriends_'+rid).find('.sendmsg_').addClass('sendmsg_'+rid);

						if($('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-times').length) {
							$('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-times').removeClass('fa-user-times').addClass('fa-user-plus');

							$('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-plus').attr("onclick","addFriend('"+rid+"')");
						}
					
					} else if(type == 'deleteRequest'){
						$('#request_'+rid).hide();
						var reqbudge = $('#friendcount').val();
						reqbudge = reqbudge - 1;
						if(reqbudge == 0){$('#request_budge').hide();}
						else{$('#request_budge').html(reqbudge);}
					} 
					
				}
			});
		}

		function genFrdAction($id, obj)
		{
			if($id) {
				$.ajax({
					url: '?r=friend/gen-frd-action', //Server script to process data
					type: 'POST',
					data: {$id, wall_user_id},
					success: function (data) {
						var result = $.parseJSON(data);
						if(result.status != undefined && result.status == true) {
							var icon = result.icon;
							var code = result.code;
							$(obj).removeClass();
							$(obj).addClass(icon);
							Materialize.toast(code, 2000, 'green');
						}
					}
				});
			}
		}
	/* End remove friend / unfriend / cancle_friend_request / delete friend */

	/* view notification */
		function view_notification(){
			$('#noti_budge').hide();
				$.ajax({
			type: 'POST',
			url: '?r=notification/view-notification',
			success: function (data) {
			}		
		});
		}
	/* end view notification */

	/* google map initialization */   

		function initAutocomplete() {
			
			if($isMapLocationId != undefined && $isMapLocationId != null && $isMapLocationId != '') {
				var id = $isMapLocationId;	
				var input = document.getElementById($isMapLocationId);
	        } else {
				var id = 'autocomplete1';
				var input = document.getElementById('autocomplete1');
	        }

	        if($('#'+id).length) {
				if($('#'+id).hasClass('placerestriction')) {
					var options = {
						types: ['(cities)'],
					};
				} else {
					var options = {
						types: ['geocode'],
					};
				}	

				autocomplete = new google.maps.places.Autocomplete(input, options);
				autocomplete.addListener('place_changed', fillInAddress);
				
				if(document.getElementById('placeSearch') != null){
					placeSearch = new google.maps.places.Autocomplete((document.getElementById('#'+id)),
					{types: ['geocode']});
					placeSearch.addListener('place_changed', fillInAddress);			
				}
			}
		}
		function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();
			// Get each component of the address from the place details
			// and fill the corresponding field on the form.
			if(place){
				for (var i = 0; i < place.address_components.length; i++) {
					var addressType = place.address_components[i].types[0];
					if (addressType == "country") {

						var val = place.address_components[i]["long_name"];
						var country_code = place.address_components[i]["short_name"].toLowerCase();
				
						if(document.getElementById("country") != null )
							document.getElementById("country").value = val;
						if(document.getElementById("au_country") != null )
							document.getElementById("au_country").value = val;

						$.ajax({
							url: "?r=site/isd-code",
							data:'country='+$("#country").val(),
							type: "POST",
							success:function(data) {
								if($('#isd_code').length) {
									document.getElementById("isd_code").value = data;
								}
							},
							error:function (){}
						});
						if($('#country_code').length) {
							document.getElementById("country_code").value = country_code;
						}
					}
				}
			
			}
		}
		// Bias the autocomplete object to the user's geographical location,
		// as supplied by the browser's 'navigator.geolocation' object.
		function geolocate() {
			//$.scrollTo($('#success'), 1000);
			initAutocomplete();
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var geolocation = {
					  lat: position.coords.latitude,
					  lng: position.coords.longitude
					};
					var circle = new google.maps.Circle({
					  center: geolocation,
					  radius: position.coords.accuracy
					});
					autocomplete.setBounds(circle.getBounds());
				});
			}
		}
	/* end google map initialization */   

	/* get notification for mute friend */
		function getNotification(obj, fid, isPermission=false) {
			if(fid) {
				if(isPermission) {	
					$.ajax({
						type: 'POST',
						url: '?r=site/mutefriend',
						data: "fid=" + fid,
						success: function (data) {
							if (data){
								if(data === '1'){
									$(".mute_friend_" + fid).html('Mute notifications');
									Materialize.toast('Unmute notifications.', 2000, 'green');
								}
								else if(data === '2'){
									$(".mute_friend_" + fid).html('Unmute notifications');
									Materialize.toast('Mute notifications.', 2000, 'green');
								}
							}
						}
					});
				} else {
					var disText = $(".discard_md_modal .discard_modal_msg");
				    var btnKeep = $(".discard_md_modal .modal_keep");
				    var btnDiscard = $(".discard_md_modal .modal_discard");
				    var htmlContent = $(obj).html();
			    	disText.html(htmlContent+"?");
		            btnKeep.html("Keep");
		            if(htmlContent.indexOf("Mute") > -1) {
		            	btnDiscard.html("Mute");
		            } else {
		            	btnDiscard.html("Unmute");
		            }

		            btnDiscard.attr('onclick', 'getNotification(this, \''+fid+'\', true)');
		            $(".discard_md_modal").modal("open");
				}
			}
		}
	/* end get notification for mute friend */

	/* block friend */ 
		function blockFriend(obj, fid, isPermission=false){
			if(fid) {
				$('.dropdown-button').dropdown("close");
				var htmlContent = $(obj).html();
		    	if(htmlContent.indexOf("Unblock") > -1) {
		    		isPermission = true;
		    	}
				if(isPermission) {	

					$.ajax({
						type: 'POST',
						url: '?r=site/blockfriend', 
						data: "fid=" + fid,
						success: function (data) {
							var result = $.parseJSON(data);
							if(result.status != undefined && result.status == true) {
								$label = result.label;
								if($label == 'Unblock') {
									$label = 'Unblocking';
								} else {
									$label = 'Blocking';
								}
								Materialize.toast($label, 2000, 'green');
							}
						}
					});
				} else {
					var disText = $(".discard_md_modal .discard_modal_msg");
				    var btnKeep = $(".discard_md_modal .modal_keep");
				    var btnDiscard = $(".discard_md_modal .modal_discard");
				    var htmlContent = $(obj).html();
			    	disText.html(htmlContent+" user?");
		            btnKeep.html("Keep");
		            if(htmlContent.indexOf("Block") > -1) {
		            	btnDiscard.html("Block");
		            } else {
		            	btnDiscard.html("Unblock");
		            }

		            btnDiscard.attr('onclick', 'blockFriend(this, \''+fid+'\', true)');
		            $(".discard_md_modal").modal("open");
				}
			}
		}
	/* end block friend */

	/* Reset New Post Input And HTML Dom */
	  function closeAllDrawers(e) {
	    resetNewPost();
	    $('.tags-added').html('');
	    
	        var trgt = $('.tarrow .drawer');

	        if (!trgt.is(e.target) & trgt.has(e.target).length === 0) {
	            $(".tarrow .drawer").fadeOut(500);
	        }

	        var trgt1 = $('.droparea .drawer');

	        if (!trgt1.is(e.target) & trgt1.has(e.target).length === 0) {

	            clearActive();
	            $(".droparea .drawer").fadeOut(500);
	        }

	        var name = $(e).data("name");
	        if(name != undefined || name != null || name != '') {
	          if(name == 'cancel_post') {
	            resetAllNewPostInputsAndOther();
	          }
	        }
	    }
	/* end Reset New Post Input And HTML Dom */

	/* Profile Image Validator for cropping */
		function cropTriggerClk() {
			var file = $('#file-input').prop("files");
			var msg='';
			if (file.length >0) {
				var nm = file[0].name;
				var ext = nm.substr(nm.lastIndexOf('.') +1).toLowerCase();
				var _URL = window.URL || window.webkitURL;

				if($.inArray(ext, ['gif','png','jpg','jpeg','tif']) == -1) {
					Materialize.toast('Upload photo allow only (jpg / gif / jpeg / png / tif) image extension.', 2000, 'red');
				} else {
					img = new Image();
					img.onload = function () {
						if(img.width >= 150 && img.height >= 150) {
							//$( ".cropControlCrop" ).trigger( "click" );
							tbSignupNavigation('', 'security-check');
							return false;
						} else {
							Materialize.toast('Your photo must be at least 200x200 pixels.', 2000, 'red');
						}
					};
					img.src = _URL.createObjectURL(file[0]);
				}
			} else {
				Materialize.toast('Please upload profile picture.', 2000, 'red');
			}
			return false;				
		}
	/* END Profile Image Validator for cropping */

	/* FUN Feedback Mail*/
		function feedbackMail(){
			$.ajax({
			   type: 'POST',
			   url: '?r=site/feedbackmail',
			   success: function (data){
			   }
			});
		}
	/* FUN End Feedback Mail*/

	/* tranform to capital */
		function capitalize(str){
			if(str){			
				var strcap = str[0].toUpperCase() + str.slice(1);
				return strcap;
			}
		}
	/* end tranform to capital */

	/* check user status */	
		function CheckUserStatus(){
			var ans = '';
			$.ajax({
			  url: '?r=site/check-user-status', 
			  type: 'POST',
			  async: false,
			  success: function (data){
				  return data;
			  }
			});
		}
	/* end check user status */

	/* FUN manage offers */
		function backToOffer(obj){
			
			var fparent=$(obj).parents(".offer-details");
			var sparent=$(obj).parents(".offer-tab");
			
			fparent.hide();
			sparent.find(".offer-ul").show();
			
			$(obj).parents(".main-page").find(".side-area.main-search").show();
	  		$(obj).parents(".main-page").find(".side-area.offer-profile").hide();
			
			var win_w=$(window).width();
			
			if(win_w<=800){
				$(".mbl-filter-icon.main-icon").show();
				$(".mbl-filter-icon.offer-profile-icon").hide();
			}
		}
		function resetMblFilterIcon(){
			if($(".main-page").hasClass("split-page")){
				
				var offerTabFound = $(".tab-pane.offer-tab").length;
				var isOfferDetailTab = $(".tab-pane.offer-tab .offer-details").css("display");
				var win_w=$(window).width();
				
				if(offerTabFound){
					if(isOfferDetailTab=="none"){
						if(win_w>800){
							$(".mbl-filter-icon.main-icon").attr("style","");
							$(".mbl-filter-icon.offer-profile-icon").attr("style","");
						}
					}else{				
						if(win_w>800){
							$(".mbl-filter-icon.main-icon").attr("style","");
							$(".mbl-filter-icon.offer-profile-icon").attr("style","");
						}else{
							$(".mbl-filter-icon.main-icon").show();
							$(".mbl-filter-icon.offer-profile-icon").hide();
							
							$(".main-page").find(".side-area.main-search").show();
							$(".main-page").find(".side-area.offer-profile").hide();
						}
					}
				}else{
					if(win_w>800){
						$(".mbl-filter-icon.main-icon").attr("style","");
						$(".mbl-filter-icon.offer-profile-icon").attr("style","");
					}
				}
			}
		}
		function resetOfferSidebar(obj){
			
			var fparent=$(obj).parents(".main-page").find(".offer-details");
			var sparent=$(obj).parents(".main-page").find(".offer-tab");
			
			$(obj).parents(".main-page").find(".side-area.main-search").show();
	  		$(obj).parents(".main-page").find(".side-area.offer-profile").hide();
			
			$(".mbl-filter-icon.main-icon").attr("style","");
			$(".mbl-filter-icon.offer-profile-icon").attr("style","");
			
			setTimeout(function(){
				fparent.hide();
				sparent.find(".offer-ul").show();
			},400);
			
		}
		function resetGeneralPageTabs(obj){
			
			var whichTab = $(obj).html().trim();
			if(whichTab == "Yours"){
				manageNewPostBubble("showit");
			}else{
				manageNewPostBubble("hideit");			
			}
		}
	/* FUN end manage offers */
 
	/* Report Abuse popup Functions */ 
		function reportabuseopenpopup(id, label){
			$.ajax({ 
				url: '?r=groups/report-abuse',
				type: 'POST', 
				data: {id, label},
				success: function (data) {
					$("#reportpost-popup").html(data);
					$("#reportpost-popup").modal("open");
					$("#reportpost-popup").css("z-index", "1100");
				}
			});
		}
	/* end Report Abuse popup Functions */

	/* preferences popup */
		function preferencesopenpopup(id, label){
			if(id != undefined && id != '') {
				if(label != undefined && label != '') {
					$.ajax({
						url: '?r=groups/preferences',
						type: 'POST',
						data: {id, label},
						success: function (data) {
							$('#preference_modal').html(data);
							$('#preference_modal').modal('open');
							initDropdown();
						}
					});
				}
			}
		}
	/* end preferences popup */

	/* FUN start share post on fb */
		window.fbAsyncInit = function() {
	      // init the FB JS SDK
	      FB.init({
	        appId      : '1565956400095697',                        // App ID from the app dashboard for live
	        //appId      : '454389514767288',                        // App ID from the app dashboard for local
	        channelUrl : 'https://www.arabiaface.com/', // Channel file for x-domain comms
	        status     : true,                                 // Check Facebook Login status
	        xfbml      : true                                  // Look for social plugins on the page
	      });
	  
	      // Additional initialization code such as adding Event Listeners goes here
	    };
	  
	    // Load the SDK asynchronously
	    (function(d, s, id){
	       var js, fjs = d.getElementsByTagName(s)[0];
	       if (d.getElementById(id)) {return;}
	       js = d.createElement(s); js.id = id;
	       js.src = "//connect.facebook.net/en_US/all.js";
	       fjs.parentNode.insertBefore(js, fjs);
	    }(document, 'script', 'facebook-jssdk'));
	  
		function tbpostonfb(pid,ptitle,pt){
			// facebook share dialog
			FB.ui( {
				method: 'feed',
				name: ptitle,
				link: "https://www.arabiaface.com/frontend/web/index.php?r=site/travpost&postid="+pid,
				//picture: pimage,
				caption: pt
			}, function( response ) {
			} );
		}
	/* FUN end share post on fb */

	/* START Added by BH */
		function removeinvitesearchinput(obj) {
			$(obj).parent('.underlined').find('input').val('').trigger('click').focus();
		}
	/* END Added by BH */

	// Fn to allow an event to fire after all images are loaded
		$.fn.imagesLoaded = function () {

		    // get all the images (excluding those with no src attribute)
		    var $imgs = this.find('img[src!=""]');
		    // if there's no images, just return an already resolved promise
		    if (!$imgs.length) {return $.Deferred().resolve().promise();}

		    // for each image, add a deferred object to the array which resolves when the image is loaded (or if loading fails)
		    var dfds = [];  
		    $imgs.each(function(){

		        var dfd = $.Deferred();
		        dfds.push(dfd);
		        var img = new Image();
		        img.onload = function(){dfd.resolve();}
		        img.onerror = function(){dfd.resolve();}
		        img.src = this.src;

		    });

		    // return a master promise object which will resolve when all the deferred objects have resolved
		    // IE - when all the images are loaded
		    return $.when.apply($,dfds);
	 
		}

	function checkuserauthclassnv($isServer=false) {
		Materialize.toast('Veryfied your account.', 2000, 'red');

		/*if($isServer == true) {
			$isServer = '(Client side)';
		} else {
			$isServer = '(Server side)';
		}
		openLoginPopup();	*/
	}
	function checkuserauthclassg($isServer=false) { 
		if($isServer == true) {
			$isServer = '(Client side)';
		} else {
			$isServer = '(Server side)';
		}	
		openLoginPopup();	
	}
	/* reset tabs */
		function resetTabs(sparent){
			$(sparent+" li").each(function(){
				$(this).removeClass("active");
			});
		}
	/* end reset tabs */

	/* manage inner pages in mobile view */	
		function resetInnerPage(fromWhere,state){
			console.log('First');
			if(fromWhere == "settings"){			
				// settings page
				var sparent = $(".settings-menuholder");
				if(state == "hide"){			
					sparent.addClass("gone");
					
					var subtitle = $(".settings-content.active .formtitle h4").html();
					$(".page-wrapper .header-themebar .innerpage-name").html(subtitle);
					$(".page-wrapper .header-themebar").addClass("innerpage");
				} else {			
					if(sparent.hasClass("gone"))
						sparent.removeClass("gone");
					$(".page-wrapper .header-themebar .innerpage-name").html("");
					$(".page-wrapper .header-themebar").removeClass("innerpage");
				}			
			}
			if(fromWhere == "wall") {
				console.log('Two');
				// wall page
				var sparent = $(".main-content");
				
				if(state == "hide"){
					console.log('Three');
					// opening a new tab except wall
					if($(".wallpage .wall-header .tabs li a.active").length > 0){
						console.log('Four');	
						var tabname = $(".wallpage .wall-header .tabs li a.active").attr("tabname");				
						if(tabname!="Wall"){
							console.log('Five');	
							$(".page-wrapper .header-themebar .innerpage-name").html(tabname);
							$(".page-wrapper .header-themebar").addClass("innerpage");
							if(sparent.hasClass("gone")){
								console.log('Six');	
								sparent.removeClass("gone");
							}
						}
					}
				}else{
					console.log('Seven');	
					// closing a new tab except wall
					$(".page-wrapper .header-themebar .innerpage-name").html("");
					$(".page-wrapper .header-themebar").removeClass("innerpage");				
					$("ul.tabs li a[href='#wall-content']").trigger("click");	
					//$(".wallpage .wall-header ul.tabs li a[href='#wall-content']").trigger("click");
					sparent.addClass("gone");
				}			
			}
		
		}
	/* end manage inner pages in mobile view */	
/************ END COMMON FUNCTIONS ************/

/************ MESSAGE FUNCTIONS ************/
	/* FUN mark message as read */
		function markRead(obj){
			var sparent=$(obj).parents("li");
			if(sparent.hasClass("read")){
				sparent.removeClass("read");
				$(obj).attr("title","Mark as read");
			}
			else{
				sparent.addClass("read");
				$(obj).attr("title","Mark as unread");
			}
		}

		function showAllMsgRead(){
			$(".msg-listing li").each(function(){
				$(this).addClass("read");
				$(this).find("a.readicon").attr("title","Mark as unread");
			});
		}
	/* FUN end mark message as read */
	
/************ END MESSAGE FUNCTIONS ************/

/************ GENERAL PAGE FUNCTIONS ************/

	/* FUN general details manage expandable - summery */
		function reset_gdetails_expandable() {
			if($(".main-content").hasClass("generaldetails-page")){			
				var sparent=$(".generaldetails-page").find(".gdetails-summery").find(".gdetails-moreinfo");
				
				var win_w=$(window).width();			
				sparent.find(".expandable-area").slideDown();
				sparent.addClass("expanded");
				var obj=sparent.find(".invertsign");
				invertExpandLink(obj, "up","maindown");
			}			
		}	
	/* FUN end general details manage expandable - summery */

	/* FUN general-details page : reset detail tabs */
		$.urlParam = function(name){
			var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
			if(results != null && results[1] != undefined) {
				return results[1] ;
			} else {
				return 0;
			}
		}
	/* FUN end general-details page : reset detail tabs */

/************ END GENERAL PAGE FUNCTIONS ************/

/* Start Reset All Fields of Post And Calls Where We Click on Cross Icon Of Popup*/
	function resetNewPost(parent) {  

		if(!parent) {
			parent = '';
		}
		$(".post-photos").html('');
		$(".location_parent").html('');
		$("#tag_person").html('');
		storedFiles = [];
		$("#frmnewpost").trigger("reset");
		$("#frmnewpost .post-info-added").html('').hide();
		$(".select2-container .select2-selection__rendered .select2-selection__choice").remove();
		$("#frmnewpost .post-tag").hide();
		$("#frmnewpost .post-location").hide();
		if($(".post-price").length>0) $(".post-price").hide();
		if($(".post-category").length>0) $(".post-category").hide();
		$("#frmnewpost .post-photos .img-row").html('');
		$("#customin1").val('');
		$("#customin3").val('');
		$('#customchk').attr('checked', false).hide();
		$('#frmnewpost .pvideo-holder').html('');
		$('#frmnewpost .pvideo-holder').remove();
		$("#link_title").val('');
		$("#link_url").val('');
		$("#link_description").val('');
		$("#link_image").val('');
		$("#share_setting").val('');
		$("#comment_setting").val('');
		$("#imgfilecount").val('');
		$("#imgfile-count").val('0');
		$("#counter").val('');
		$("#hiddenCount").val('1');
		$("#textInput").css("height", "31px");
		$(parent + ' ' + '.npost-title').slideUp();
		$(parent + ' ' + '.echeck-list').find('li').removeClass('selected');
		$("#ptextInput").val('');
		$("#pagereviewrate").val('0');
		$("#ptextInput").css("height", "31px");	
		if($(parent).find(".rating-stars").length > 0){
		  $(parent).find(".rating-stars").find(".fa-star.active").each(function(){
		   $(".rating-stars").children().removeClass("active");
		  });  
		 }
		 if($(".refers-newpost").hasClass("active")) {
			 $(".refers-newpost").removeClass("active");
		 }
		 $(".refers-newpost").find('.post-bcontent').hide();
		 $(".refers-newpost #textInputs").val('');
		 $(".new-post #textInput").val('');
		 
		 setTimeout(function(){$(".mobile-menu.topicon .gotohome a").focus();},1000); 
		 
		 $(".popup-postbtn").addClass("disabled");
		 $(".popup-postreview").removeClass("disabled");
	}
	
	/* End Reset All Fields of Post */

	/* group admin delete */
	function groupadmindelete($id, isPermission=false) {
		if($id != undefined && $id != null && $id != '') {
			if(isPermission) {
				$.ajax({
				    url: '?r=notification/groupadminrequestdelete',
				    type: 'POST',
				    data: {$id},
				    success: function (response) { 
						var result = $.parseJSON(response);
						if(result.status != undefined && result.status != null && result.status == true) {
							$('.noti-listing').find('li#noti_'+$id).find('.btn-holder').html('<button class="btn btn-primary btn-sm btn-gray">Rejected</button>');
							Materialize.toast('Request deleted.', 2000, 'green');
				  		}
				  		$(".discard_md_modal").modal("close");
				    } 
			   });
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("Request delete.");
	            btnKeep.html("Keep");
	            btnDiscard.html("Delete");
	            btnDiscard.attr('onclick', 'groupadmindelete(\''+$id+'\', true)');
	            $(".discard_md_modal").modal("open");
			}	
		}
	}
 	
 	/* group admin accept */
	function groupadminaccept($id, isPermission=false) {
		if($id != undefined && $id != null && $id != '') {
			if(isPermission) {
				$.ajax({
				    url: '?r=notification/groupadminrequestaccept',
				    type: 'POST',
				    data: {$id},
				    success: function (response) { 
				  		var result = $.parseJSON(response);
						if(result.status != undefined && result.status != null && result.status == true) {
							$('.noti-listing').find('li#noti_'+$id).find('.btn-holder').html('<button class="btn btn-primary btn-sm btn-gray">Accepted</button>');
							Materialize.toast('Request accepted.', 2000, 'green');
				  		}
				  		$(".discard_md_modal").modal("close");
				    }
			   });
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("Request accept.");
	            btnKeep.html("Keep");
	            btnDiscard.html("Accept");
	            btnDiscard.attr('onclick', 'groupadminaccept(\''+$id+'\', true)');
	            $(".discard_md_modal").modal("open");
			}
		}
	}

	/* event organizer request delete */
	function eventorganizerrequestdelete($id, isPermission=false) {
		if($id != undefined && $id != null && $id != '') {
			if(isPermission) {
				$.ajax({
				    url: '?r=notification/eventorganizerrequestdelete',
				    type: 'POST',
				    data: {$id},
				    success: function (response) { 
						var result = $.parseJSON(response);
						if(result.status != undefined && result.status != null && result.status == true) {
							$('.noti-listing').find('li#noti_'+$id).find('.btn-holder').html('<button class="btn btn-primary btn-sm btn-gray">Rejected</button>');
							Materialize.toast('Request deleted.', 2000, 'green');
				  		}
				    } 
			   });
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("Request delete.");
	            btnKeep.html("Keep");
	            btnDiscard.html("Delete");
	            btnDiscard.attr('onclick', 'eventorganizerrequestdelete(\''+$id+'\', true)');
	            $(".discard_md_modal").modal("open");
			}
		}
	}
 	
 	/* event organizer reqest accept */
	function eventorganizerrequestaccept($id, isPermission=false) {
		if($id != undefined && $id != null && $id != '') {
			if(isPermission) {
				$.ajax({
				    url: '?r=notification/eventorganizerrequestaccept',
				    type: 'POST',
				    data: {$id},
				    success: function (response) { 
				  		var result = $.parseJSON(response);
						if(result.status != undefined && result.status != null && result.status == true) {
							$('.noti-listing').find('li#noti_'+$id).find('.btn-holder').html('<button class="btn btn-primary btn-sm btn-gray">Accepted</button>');
							Materialize.toast('Request accepted.', 2000, 'green');
				  		}
				  		$(".discard_md_modal").modal("close");
				    }
			   });
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("Request accept.");
	            btnKeep.html("Keep");
	            btnDiscard.html("Accept");
	            btnDiscard.attr('onclick', 'eventorganizerrequestaccept(\''+$id+'\', true)');
	            $(".discard_md_modal").modal("open");
			}
		}
	}

	/* Start Link Preview Code */
	function updatevip(){
		$.ajax({
		   url: '?r=site/updatevipflag',
		   type: 'POST',
		   data: {},
		   success: function (response) {			   }
	   });
	}
	
function ShowImage() {
	$(".loader").show();
}
function HideImage() {
	$(".loader").hide();
}

/* publish post */
	function publishpost(pid, isPermission=false){
		if(isPermission) {
			$.ajax({
				url: '?r=site/publishpost', 
				type: 'POST',
				data: 'pid=' + pid,
				success: function (data){
					$(".discard_md_modal").modal("close");
					$('#publishposticon').hide();
					Materialize.toast('Published', 2000, 'green');
				}
			});
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Publish Post.");
			btnKeep.html("Keep");                                                   
			btnDiscard.html("Publish");
			btnDiscard.attr('onclick', 'publishpost(\''+pid+'\', true)');
			$(".discard_md_modal").modal("open");
		}
	}
/* end publish post */

/* publish collection */
	function publish_collection(c_id, isPermission=false){
		if(isPermission) {
			$.ajax({
				url: '?r=collection/publish-collection', 
				type: 'POST',
				data: 'c_id=' + c_id,
				success: function (data){
					$(".discard_md_modal").modal("close");
					$('#publish_collection').hide();
					Materialize.toast('Published', 2000, 'green');
					window.location.href = "?r=collection/index";
				}
			});
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Publish collection.");
			btnKeep.html("Keep");                                                   
			btnDiscard.html("Publish");
			btnDiscard.attr('onclick', 'publish_collection(\''+c_id+'\', true)');
			$(".discard_md_modal").modal("open");
		}
	}
/* end publish collection */

/* confirm posting to wall */
	function add_to_wall(pid,ntype, isPermission=false){
		if(isPermission) {
			$.ajax({
				url: '?r=site/approve',  
				type: 'POST',
				data: 'post_id=' + pid + '&ntype=' + ntype,
				success: function(data) {
					$(".discard_md_modal").modal("close");
					if(data == '1')
					{
						$("#add_to_wall_"+pid).hide();
						Materialize.toast('Added', 2000, 'green');
					}    
				},
			});
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Add this post to your wall.");
			btnKeep.html("Keep");                                                   
			btnDiscard.html("Add");
			btnDiscard.attr('onclick', 'add_to_wall(\''+pid+'\', \''+ntype+'\', true)');
			$(".discard_md_modal").modal("open");
		}
	}
/* end confirm posting to wall */

/* approve tag */	
	function approve_tag(pid, isPermission=false){
		if(isPermission) {
			$.ajax({
				url: '?r=site/approvetags',  
				type: 'POST',
				data: 'post_id=' + pid,
				success: function(data) {
					$(".discard_md_modal").modal("close");
					if(data == '1')
					{
						$("#approve_tag_"+pid).hide();
						Materialize.toast('Approved', 2000, 'green');
					}    
				},
			});
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Approve post.");
			btnKeep.html("Keep");                                                   
			btnDiscard.html("Approve");
			btnDiscard.attr('onclick', 'approve_tag(\''+pid+'\', true)');
			$(".discard_md_modal").modal("open");
		}
	}
/* end approve tag */

/* Start Generalize Function for No Record Found */ 
	function getnolistfound($key)
	{	
		$.ajax({ 
			url: '?r=site/fetchlabelmessage',  
			type: 'POST',
			data: {$key},
			success: function(data) { 
				var result = $.parseJSON(data);
				if(result.status != undefined && result.status == true) {
					$selectore = result.selectore;
					$message = result.html;
					$($selectore).html('<div class="post-holder bshadow"><div class="joined-tb"><i class="mdi mdi-file-outline"></i><p>'+$message+'</p></div></div>');	
				}
			}
		});
	}

	/* End Generalize Function for No Record Found */

	function GetMap($location)
	{
		var Content = '<iframe src="https://maps.google.it/maps?q='+$location+'&output=embed" width="100%" height="260" frameborder="0" style="border:0" allowfullscreen></iframe>';
		return Content;
	}

	/* block user request */
	function blockofferrequest($id, isPermission=false) {
		if($id) {
			if(isPermission) {
	            $.ajax({ 
	                url: '?r=site/blockofferrequest',
	                type: 'POST',
	                data: {$id},
	                success: function(data) {
	                	Materialize.toast('Blocking', 2000, 'green');
	                	$(".discard_md_modal").modal("close");
	                	requestslist();
	                }
	            });
	        } else {
	        	$('.dropdown-button').dropdown("close");
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("Block this person requests?");
	            btnKeep.html("Keep");
	            btnDiscard.html("Block");
	            btnDiscard.attr('onclick', 'blockofferrequest(\''+$id+'\', true)');
	            $(".discard_md_modal").modal("open");
			}
		}
	}

	/* open view map inside post */
	function openViewMap(obj){
		var sparent=$(obj).parents(".post-content");
		var mapholder=sparent.find(".map-holder");
		
		if(mapholder.css("display")=="block"){
			mapholder.slideUp();
			$(obj).html("View on map");
		}else{
			mapholder.slideDown();
			$(obj).html("Close map");
		}
	}
	/* end open view map inside post */

function photoscontentsplit()                                                           
{
    if(wall_user_id != '')
    { 
        $.ajax({
            url: '?r=userwall/photos-content-split',  
            type: 'POST', 
            data: "id="+wall_user_id,
            success: function(data)
            {
            	if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else {
					$("#photos-content").find('#pc-photos').find('.photos-area').html(data);
					setTimeout(function() { 
						var boxes = $('#pc-photos').find('.grid-box.countgrid-box').length;
                    	$('.getAgainPhotoscount').html('('+boxes+')'); 
					
						/* album slider */
						initPhotoCarousel("wallpage");

						/* end album slider */
						initGalleryImageSlider();
						initDropdown(); 
 
						$('.carousel_master .carousel').carousel();

						var owl = $('.owl-carousel');
							owl.owlCarousel({
								margin: 10,
								loop: false,		
								dots: true,			
								nav: true,
								responsive: {
								  0: {
									items: 1
								  },
								  600: {
									items: 2
								  },
								  1000: {
									items: 3
								  }
							}
						});                                     
						
						if($("#photos-content").find(".user-photos").length > 0) {
							var win_w = $(window).width();
							var settime = 400;
							if(win_w < 768)
								settime = 700;
								
								setTimeout(function(){					
									$('.user-photos').addClass('photoshow');
								},settime);
							
								fixImageUI("wall-photoslider");
						}                   

						fixImageUI("wall-photoalbums");	
					},700);

					justifiedGalleryinitialize();
					lightGalleryinitialize();
				}
			}
        }); 
    }
}

function albumscontentsplit()
{ 

	if(typeof page_owner == undefined || typeof page_owner == 'undefined') {
		page_owner = '';
	}

    if(wall_user_id != '')
    {
        $.ajax({
            url: '?r=userwall/albums-photos-content',  
            type: 'POST',
            data: "id="+wall_user_id+"&page_owner="+page_owner,
            success: function(data)
            {
            	$('.tbpagelikes').show();
                $("#photos-content").find('#pc-albums').html(data);

                setTimeout(function() { 
					initDropdown(); 
					$('.tabs').tabs(); 
                	var boxes = $('#pc-albums').find('.grid-box.countgrid-box').length;
                	$('.getAgainAlbumscount').html('('+boxes+')');

                	var boxes = $('#pc-photos').find('.grid-box.countgrid-box').length;
                	$('.getAgainPhotoscount').html('('+boxes+')'); 

	                initPhotoCarousel("wallpage");			          
	                /* end album slider */
					initGalleryImageSlider();

					$('.carousel_master .carousel').carousel();

					var owl = $('.owl-carousel');
						owl.owlCarousel({
							margin: 10,
							loop: false,		
							dots: true,			
							nav: true,
							responsive: {
							  0: {
								items: 1
							  },
							  600: {
								items: 2
							  },
							  1000: {
								items: 3
							  }
						}
					});
					
					if($("#photos-content").find(".user-photos").length > 0){
						
						var win_w = $(window).width();
						var settime = 400;
						if(win_w < 768) {
							settime = 700;
							
							setTimeout(function(){					
								$('.user-photos').addClass('photoshow');
							},settime);
						
							fixImageUI("wall-photoslider");
						}
					}  
					fixImageUI("wall-photoalbums");	
                },700);
            }
        });
    }
}

function callPaymentPop(e) {
	$code = $(e).attr('data-callpayment');
	if($code) {
		$.ajax({
            type: 'POST',
            url: "?r=site/payment", 
            data: {'code': $code},
            success: function (data) {
    			$('#payment-popup').html(data);

            	setTimeout(function(){ 
            		$('#payment-popup').modal('open');
            		initDropdown();
					$('.tabs').tabs(); 
            		setModelMaxHeight();
            	},400);
            }
        });
	}
}

function invitefriend() {
	var id = $(this).attr('data-id');
	var textValue = $.trim($(this).val());
	var baseurl = $("#baseurl").val();
	var data = 'key='+textValue+'&baseurl='+baseurl;
	$.ajax({
		url: "?r=event/invite-friends-event", 
		type: 'GET',
		data: data,
		success: function (data) {	 
			$(".block"+id).html(data).show();
		}
	});
}

function mainFeedPostBtn() {
	if($('.modal.open').length) {
		$id = $('.modal.open').attr('id');
		var $textarea = $('#'+$id).find('textarea');
	    var $submitBtn = $('#'+$id).find('.submit');
	    var $textareaValue = $textarea.val().trim();
	    if(storedFiles.length <=0 && storedFilesExsting.length <=0) {
	        if($textareaValue != '') {
	            $submitBtn.removeClass('disabled');
	        } else {
	            $submitBtn.addClass('disabled');
	        }
	    } else {
	        $submitBtn.removeClass('disabled');
	    }
	}
}

/* FUN start page invite for like functions */
function cancelinviteevent(fid){
	$('.invite_'+fid).hide();
}
function sendinviteevent(fid,pid){
	if (fid != '' && pid != '')
	{
		$.ajax({
			type: 'POST',
			url: '?r=event/sendinvite',
			data: "fid="+fid+"&pid="+pid,
			success: function (data) {
				if (data)
				{
					if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} 
					else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} 
					else {
						$('.events_'+fid).hide();
						$('.sendinvitation_'+fid).show();
					}
				}
			}
		});
	}                                            
}
/* FUN end page invite for like functions */

/* Report Abuse stored */
function reportabusestored(id, label){  
	if(id != undefined && id != '') {	
		var desc = $.trim($('#report_abuse_form').find('#desc').val());
		if(desc.length <= 0) {
			Materialize.toast('Write your message.', 2000, 'red');
		} else {
			if(desc != undefined && desc != '') {
				$('#reportpost-popup').find('.post-loader').show();	
				$.ajax({
					url: '?r=groups/report-abuse-stored',
					type: 'POST',
					data: {
						'$id' : id, 
						'$reason' : desc, 
						'$type' : label
					},
					success: function (data) {
						$('#reportpost-popup').modal('close');
						if(data == true) {
							Materialize.toast('Reported successfully.', 2000, 'green');
						} else {
							Materialize.toast('Oops, something is wrong please try after sometimes.', 2000, 'red');
						}
					}
				});
			}
		} 
	}
}
/* end Report Abuse stored */

function manageMessagePage(){
  var winw=$(window).width();   
  var isMessagePage=$(".main-content").hasClass("messages-page");
  
  $('.messages-page').find('.messages-list').removeClass("newmsg-mode");
  //$('.messages-page').removeClass("innerpage");
  
  if(isMessagePage){
    if(winw<800){
      if(!$(".main-content").hasClass("mblMessagePage")){         
        $(".main-content").addClass("mblMessagePage");                
        $(".messages-list").find(".left-section").addClass("shown");
        $(".messages-list").find(".right-section").addClass("hidden");
      }
      removeNiceScroll(".nice-scroll");
    }
    else{
      $('.messages-page').removeClass("innerpage");
      
      if($(".main-content").hasClass("mblMessagePage")){
        $(".main-content").removeClass("mblMessagePage");
        $(".messages-list").find(".left-section").removeClass().addClass("left-section");
        $(".messages-list").find(".right-section").removeClass().addClass("right-section");
      }
      initNiceScroll(".nice-scroll");
    }
  }
  
  var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
  if(isBusinessPage){
    if(winw<800){
      if(!$(".messages-content").hasClass("mblMessagePage")){         
        $(".messages-content").addClass("mblMessagePage");                
        $(".messages-list").find(".left-section").addClass("shown");
        $(".messages-list").find(".right-section").addClass("hidden");
      }
      removeNiceScroll(".nice-scroll");
    }
    else{
      if($(".messages-content").hasClass("mblMessagePage")){
        $(".messages-content").removeClass("mblMessagePage");
        $(".messages-list").find(".left-section").removeClass().addClass("left-section");
        $(".messages-list").find(".right-section").removeClass().addClass("right-section");
      }
      initNiceScroll(".nice-scroll");
    }
  }
  fixMessageImagesPopup();
  
}


function closeAddNewMsg(){
  window.location.href="";
  /*if ($(window).width() <= 799) {
    $('.messagesearch-xs-btn').hide();
    $('.search-xs-btn').show();
    $('.globel_setting').show();
    $('.person_dropdown').hide();
  }
  
  $(".allmsgs-holder .newmessage").hide();
  $('.messages-page').find('.messages-list').removeClass("newmsg-mode");
  
  var isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
  if(isMblMessagePage){
    
    $(".messages-list").find(".right-section").addClass("hidden");
    
    if($(".messages-list").find(".left-section").hasClass("hidden")){
      $(".messages-list").find(".left-section").removeClass("hidden");
      $(".messages-list").find(".left-section").addClass("shown");
      
      $(".header-themebar .mbl-innerhead").hide();
      $(".header-nav").find(".mobile-menu").show();
      
      $('.mobile-footer').show();       
      $(".mblMessagePage").removeClass("innerpage");
    }     
  }
  
  var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
  if(isBusinessPage){
    var isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
    if(isMblMessageTab){
      $(".messages-list").find(".right-section").addClass("hidden");
    
      if($(".messages-list").find(".left-section").hasClass("hidden")){
        $(".messages-list").find(".left-section").removeClass("hidden");
        $(".messages-list").find(".left-section").addClass("shown");
        
        $(".header-themebar .mbl-innerhead").hide();
        
        $('.mobile-footer').show();       
        $(".mblMessagePage").removeClass("innerpage");
      }     
    }
  }*/
}

function openalert() {
	if($('.master_alert').length) {
		if($('.master_alert').hasClass('active')) {
			$('.master_alert').removeClass('active');
		} else {
			$('.master_alert').addClass('active');
		}
	}
}

/* FUN post button manage */
function managePostButton(action){
	if(action == "SHOW"){
		$(postBtnEle).attr("disabled",false);
	}else{
		$(postBtnEle).attr("disabled",true);
	}
}
/* FUN end post button manage */

function adjustHeight(textareaElement, minHeight) {
	var outerHeight = parseInt(window.getComputedStyle(textareaElement).height, 10);
    var diff = outerHeight - textareaElement.clientHeight;
    textareaElement.style.height = 0;
    var hgt = Math.max(minHeight, textareaElement.scrollHeight + diff);

    if(hgt > 127) {
        textareaElement.style.overflowY = 'scroll';    
        textareaElement.style.height = '127px';
    } else {
        textareaElement.style.overflowY = 'hidden';
        textareaElement.style.height = hgt + 'px';
    }
} 

function HasArabicCharacters(text){
    var arregex = /[\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufbc1]|[\ufbd3-\ufd3f]|[\ufd50-\ufd8f]|[\ufd92-\ufdc7]|[\ufe70-\ufefc]|[\uFDF0-\uFDFD]/;
    return arregex.test(text);
}

function fullScreenOpen() {
  var doc = window.document;
  var docEl = doc.documentElement;

  var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
  if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
    requestFullScreen.call(docEl);
  }
}

function fullScreenClose() {
  var doc = window.document;
  var docEl = doc.documentElement;
  var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
  cancelFullScreen.call(doc);
}