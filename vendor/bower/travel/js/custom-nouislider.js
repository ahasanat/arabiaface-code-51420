/* FUN range slider */
	function initSlidernew(sliderParentId,min,max,curval,symbol) {
		if($('#'+sliderParentId).length) {
			var keypressSlider = document.getElementById(sliderParentId);
			
			noUiSlider.create(keypressSlider, {
				connect: true,
				start: [min, max],
				step: curval,
				range: {
					'min': min,
					'max': max
				},
				format: wNumb({
					decimals: 0
				})
			});

			var nodes = [
				document.getElementById(sliderParentId+'-min'), // 0
				document.getElementById(sliderParentId+'-max')  // 1
			];

			if(symbol == '$') {
				keypressSlider.noUiSlider.on('update', function ( values, handle, unencoded, isTap, positions ) {
					nodes[handle].innerHTML = symbol+values[handle];
				});
			} else if(symbol == 'km') {
				keypressSlider.noUiSlider.on('update', function ( values, handle, unencoded, isTap, positions ) {
					nodes[handle].innerHTML = values[handle]+symbol;
				});
			}
		}	
	}

/* FUN end range slider */
