/************ START Design Code ************/

/* FUN join / unjoin community events */	
	function commeventsGoing(event,ce_id,obj){		
		var isNoClick=$(event.target).hasClass("noClick");		
		event.stopPropagation();
		if(isNoClick){
		   // Follow clicked
		   var stat=$(obj).html();
			if(stat=="Join"){
				// start following
				$(obj).html("Going...");
				$(obj).addClass("disabled");
			}else{
				// do unfollow
				$(obj).html("Join");
				$(obj).removeClass("disabled");
			}
		} 
	}	
/* FUN end join / unjoin community events */
/************ END Design Code ************/

/*$(window).resize(function() {		
	if($('.fake-title-area').find('.tabs').find('li').find('a.active').length) {
		$parent = $('.fake-title-area').find('.tabs').find('li').find('a.active');
		$link = $parent.attr('href');
		gridBoxImgUINew($link+' .uniqidentitybox');
	}
});	*/

/* listing event handler */
$(document).ready(function()
{
	$('#image-cropper').cropit({
        height: 188,
        width: 355,
        smallImage: "allow",
        onImageLoaded: function (x) {
            
            $(".cropit-preview").removeClass("class-hide");
            $(".cropit-preview").addClass("class-show");

            $(".btn-save").removeClass("class-hide");
            $(".btn-save").addClass("class-show");
            $("#removeimg").removeClass("class-hide");
            $("#removeimg").addClass("class-show");
        }
    });

	$('#commevents-suggested').find('.uniqidentitybox').last().addClass('lazyloadscrollevent lazycounthelp');
	if(window.location.href.indexOf("detail") > -1) {
        map();
	    $('.map').click(function(){
	        map();
	    });
	    $(".invite").click(function() {
	        invites();
	    });
	    $(".attending").click(function() {
	        attending();
	    });

	    $(".phototab").click(function() {
		    photo();
		});
    } else {
		var location = getQueryVariable('location');
		if(location != undefined && location != null && location != '') {
			groupssearchwithlocation('evall', location);
		} else {
			allEvents('evall',0,'load');
		}

	    $('.allevents').click(function(){
	        $('.eventsearch').val('');
	        allEvents('evall');
	    });
	    $(".going").click(function() {
	        goingEvents();
	    });
	    $(".yours").click(function() {
	        yoursEvents();
	    });
	}
});

/* listing event handler */
$(document).on('keyup','.eventsearch',function(e){
    $('#eventdrop').val('evall');
    var search_id = $(this).attr('data-search');
    var text = $(this).val();
    if(search_id =='all-search')
    {
        allEvents(text);
    }
    else if(search_id =='going-search')
    {
        goingEvents(text);
    }
    else if(search_id =='yours-search')
    {
        yoursEvents(text);
    }
});

$(document).on('click', '.myCheckbox1', function(){
	var checkbox = $(".myCheckbox1").attr('data-value');
	if(checkbox=='1'){
		$(this).attr('data-value','0');
	}else{
		$(this).attr('data-value','1');
	}
});

function promotetoorganizerbtnclk() {
    setTimeout(function(){ $('#directusepromote').trigger('click'); }, 600);
}

function promotetoorganizerbtnclkwarning(isPermission=false) {
	if(isPermission) {
		$(".discard_md_modal").modal("close");
		setTimeout(function(){ $('#directusepromote').trigger('click'); }, 600);
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Before you leave the event, please promte another member to become organizer.");
        btnKeep.html("Keep");
        btnDiscard.html("Promote to Organizer");
        btnDiscard.attr('onclick', 'promotetoorganizerbtnclkwarning(true)');
        $(".discard_md_modal").modal("open");
	}
}

function resetDetailTabs(obj, pageName, tabContent) { 
		
	// check allow 
	if(tabContent == "commevents-discussion" || tabContent == "groups-discussion"){
		$(".discuss-search").show();
		manageNewPostBubble("showit");
	}else{
		$(".discuss-search").hide();
		manageNewPostBubble("hideit");
	}
	
	if(tabContent=="groups-discussion") {
		what="discussion";$(".details.searcharea").show();}
	else if(tabContent=="groups-events"){what="event";$(".details.searcharea").show();}
	else if(tabContent=="groups-photos"){what="photos";$(".details.searcharea").show();
		initGalleryImageSlider(".images-container");
	}
	else if(tabContent=="groups-members"){what="member";$(".details.searcharea").show();}
	else{what="";$(".details.searcharea").hide();}
	
	$(".groupsearch-input").attr("placeholder","Search "+what);
	
	// reset the active tabs menu
	if( $(this).parents("people").length > -1 ){
		$(".gdetails-summery .tabs").each(function(){				
			if($(this).parents("people").length <= 0 ){
				$(this).find("li.active").removeClass("active");
			}
		});
	}else{
		$(".gdetails-summery .people .tabs li.active").removeClass("active");
	}
	
	var win_w = $(window).width();
	if(win_w < 1024){
		var invertLink = $(obj).parents(".gdetails-moreinfo").find(".expandable-link");
		invertLink.trigger("click");			
	}
	
	// scroll upto the section
	if(win_w < 768){
		$('html,body').animate({
			scrollTop: $(".general-details .post-column").offset().top - 110},
		'slow');
	}else{
		$('html,body').animate({
		scrollTop: 0},
		'slow');
	}		
}

/* fetch parameter exist in url or not */
function getQueryVariable(variable)
{
       var query = decodeURIComponent(window.location.search.substring(1));
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){
               	$pair = pair[1].replace(/\+/g, ' ');
               	return $pair;
               }
       }
       return(false);
}

/* request responce */
function request_responce(status,id){
	$.ajax({
        url: '?r=groups/requestresponcce',  
        type: 'POST',
        data: "status="+status+"&id="+id,
        success: function(data)
        {
			var result = $.parseJSON(data);
			var status = result['msg'];
			if(status=='1')
			{
				var button = '<button class="btn btn-primary btn-sm" disabled>Member added</button>';
				$('.accept_'+id).html(button);
			}	
		}
    });
}

/* remove member */
function remove_member(member_id, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=event/removemember',  
			type: 'POST',
			data: "member_id="+member_id,
			success: function(data)
			{
				$(".discard_md_modal").modal("close");
				var result = $.parseJSON(data);
				var status = result['msg'];
				if(status=='1')
				{					  
					Materialize.toast('Removed', 2000, 'green');
					$('.member_'+member_id).remove();
				}	
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("This attendee will be removed.");
        btnKeep.html("Keep");
        btnDiscard.html("Remove");
        btnDiscard.attr('onclick', 'remove_member(\''+member_id+'\', true)');
        $(".discard_md_modal").modal("open");
	}
}

/* set organizer */
function setorganizer(id, event_id, obj, remove=false){
	if(id != undefined && event_id != undefined) {
		$.ajax({
	        url: '?r=event/setorganizer',  
	        type: 'POST',
	        data: {id, event_id},
	        success: function(data)
	        {	
	        	if(data == 'P') {
	        		Materialize.toast('Cancel previous admin request.', 2000, 'red');
	        		return false;
	        	} 

	        	if(remove == true) {
	        		$(obj).parent('.member_'+event_id).remove();
	        		getnolistfound('noeventrequestfound');
	        	} else if(data == 'A') {
	        		$(obj).text('Set as Admin');
	        	} else if (data == 'C') {
	        		$(obj).text('Cancel Admin Request');
	        	}
			}
	    });
	}
}

/* set organizer */
function admin_detail(event_id){
	$.ajax({
		url: '?r=groups/admindetail',  
		type: 'POST',
		data: {event_id},
		success: function(data)
		{
			var result = $.parseJSON(data);
			var status = result['msg'];
			if(status=='1')
			{					  
				$('.member_'+member_id).remove();
			}	
		}
	});
}
 
/* all member for organizer */
function allmemberfororganizer(){
	$.ajax({
        url: '?r=event/allmemberfororganizer',  
        type: 'POST',
        data: {event_id},
        beforeSend: function() {
        	$('#manageorganizer-popup').modal('open');
	    	$("#manageorganizer-popup").find("#member-all").find('ul').html('<center><div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue-only"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div></center>');	
	    },
        success: function(data)
        {
        	$('#manageorganizer-popup').modal('open');
            $("#manageorganizer-popup").find("#member-all").find('ul').html(data);
            $('.tabs').tabs();
        }
    });	
}

/* member request to organizer */
function memberrequestorganizer() { 
	$.ajax({
        url: '?r=event/memberrequestorganizer',  
        type: 'POST',
        data: {event_id},
        beforeSend: function() {
	    	$("#manageorganizer-popup").find("#member-request").find('ul').html('<center><div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue-only"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div></center>');	
	    },
        success: function(data)
        {	
        	$("#manageorganizer-popup").find("#member-request").find('ul').html(data);
        }
    });	
}

/* map calling */
function map()
{
    $.ajax({
        url: '?r=event/map',  
        type: 'POST', 
        data: "e="+event_id,
        success: function(data)
        {
            $("#commevents-maps").html(data);
        }
    });
}

/* invites */
function invites()
{
    $.ajax({
        url: '?r=event/invites',  
        type: 'POST',
        data: "e="+event_id,
        success: function(data)
        {
            $("#commevents-invite").html(data);
        }
    });
}

/* photo calling */
function photo()
{
	$.ajax({
	    url: '?r=event/photo',  
	    type: 'POST',
	    data: "event_id="+event_id,
	    success: function(data)
	    {
	        $("#commevents-photos").html(data);
			initGalleryImageSlider();
	    }
	});
}

/* add event photo */
function addEventPhoto()
{
	var filelength = $('.hidden_uploader').get(0).files.length;
	if(filelength <= 0)
	{
		Materialize.toast('Add photos to the event.', 2000, 'red');
	    return false;
	}
	else
	{
		$('input[name="imageFile1"]').val('');
		var formdata;
		formdata = new FormData($('form')[1]);
		for(var i=0, len=storedFiles.length; i<len; i++) {
			formdata.append('imageFile1[]', storedFiles[i]); 
		}
		var event_id = $(".event_id").val();
		formdata.append('event_id', event_id);
		if(event_id != '') 
		{
			$(".post-loader").css("display","inline-block");
			$.ajax({ 
				url: '?r=event/addphoto',  
				type: 'POST',
				data:formdata,
				processData: false,
				contentType: false,
				success: function(data) {
					$('#add-photo-popup').modal('close');
					photo();
				}
			});
		}
	}
}

/* delete photo */
function delete_photo(photo_id, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=event/deletephoto',  
			type: 'POST',
			data: 'photo_id='+photo_id,
			success: function(data){
				$(".discard_md_modal").modal("close");
				Materialize.toast('Deleted', 2000, 'green');
				photo();
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Photo will be deleted permanently.");
		btnKeep.html("Keep");
		btnDiscard.html("Delete");
		btnDiscard.attr('onclick', 'delete_photo(\''+photo_id+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}

/* attending calling */
function attending()
{
    $.ajax({
        url: '?r=event/attending',  
        type: 'POST',
        data: "e="+event_id,
        success: function(data)
        {
            $("#commevents-attending").html(data);
        }
    });
}

/* delete event */
function deleteEvent(isPermission=false)
{	
	if(isPermission) {
		$.ajax({
            type: 'POST',
            url: '?r=event/delete',
            data: 'e='+event_id,
            success: function(data){
            	$(".discard_md_modal").modal("close");
            	Materialize.toast('Event deleted.', 2000, 'green');
                window.location.href = "?r=event";
            }
        });
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Delete event.");
		btnKeep.html("Keep");
		btnDiscard.html("Delete");
		btnDiscard.attr('onclick', 'deleteEvent(true)');
		$(".discard_md_modal").modal("open");
	}	 
}

/* left group */
/* left group */
function left_event(isPermission=false) {
	if(event_id) {
		$.ajax({
			type: 'POST',
			url: '?r=event/leftevent',
			data: {event_id},
			success: function(data){
				$(".discard_md_modal").modal("close");
				if(data == 'S') {
					setTimeout(function() {
						promotetoorganizerbtnclkwarning();
					},500);
				} else {
					Materialize.toast('Unattending event', 2000, 'green'); 

					window.location.href = "";
				}
			}
		});
	}
}

/* manage members of event */
function manage_members() { 
	$.ajax({ 
        url: '?r=event/managemembers',  
        type: 'POST',
        data: "event_id="+event_id,
        success: function(data)
        {	
        	$("#managemembers-popup").html(data);
        	setTimeout(function() {
				$('#managemembers-popup').modal('open');
        		$('.tabs').tabs();
			},500);
        }
    });	
}

function allrequests($isNew=false) { 
	$.ajax({ 
        url: '?r=event/allrequests',  
        type: 'POST',
        data: {event_id},
        success: function(data) {
        	$("#managemembers-popup").find('#member-request-new').html(data);
        	if($isNew) {
	        	setTimeout(function() {
					$('#managemembers-popup').modal('open');
	        		$('.tabs').tabs();
				},500);
        	} 
        	setTimeout(function(){
                initDropdown();
            },400);
        }
    });	
}

function allmembers() { 
	$.ajax({ 
        url: '?r=event/allmembers',  
        type: 'POST',
        data: {event_id},
        success: function(data)
        {	
        	$("#managemembers-popup").find('#member-all-new').html(data);
        	setTimeout(function(){
                initDropdown();
            },400);
        }
    });	
}

/* set organizer */
function groupssearchwithlocation(text, location) {
	if(location != undefined && location != null && location != '') { 
		 $.ajax({
	        url: '?r=event/all',  
	        type: 'POST',
	        data: 'search='+text+'&location='+location,
	        success: function(data)
	        {
	            var eventdrop = $('#eventdrop').val();
				$('#commevents-suggested').animateCss('fadeInUp');
				$("#commevents-suggested").find('.generalbox-list').addClass('animated fadeInUp');
	            $("#commevents-suggested").find('.row').html(data);
	            if(text === 'evall'){eventdrop = 'evall';}
	            $('#eventdrop').val(eventdrop);
	        }
	    });
    }
}

/* drop event */
function eventdrop()
{
    $('.eventsearch').val('');
    var eventdrop = $('#eventdrop').val();
    allEvents(eventdrop);
}

/* all events */
function allEvents(text, $lazyhelpcount=0, $isload='') 
{
	var location = getQueryVariable('location');
	if(location != undefined && location != null && location != '') {
		window.location.href='?r=event/index';
	}

    $('.event-search').show();
    $('.eventsearch').attr('data-search','all-search');
    $.ajax({
        url: '?r=event/all',
        type: 'POST',
        data: {
        	search : text,
        	$lazyhelpcount : $lazyhelpcount
        },
        success: function(data)
        {
        	var eventdrop = $('#eventdrop').val();
        	if(data != '') {
	        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
	        		if($isload == 'load') {
			        	$('#commevents-suggested').animateCss('fadeInUp');
			        	$("#commevents-suggested").find('.generalbox-list').addClass('animated fadeInUp');
			    	}
	        		$("#commevents-suggested").find('.row').html(data);
	        	} else {
	            	$("#commevents-suggested").find('.row').append(data);
	        	}
	        	$('#commevents-suggested').find('.uniqidentitybox').last().addClass('lazyloadscrollevent lazycounthelp');
	        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
	        	getnolistfound('beecomefirsttocreateevent');
	        }
            if(text === 'evall'){eventdrop = 'evall';}
            $('#eventdrop').val(eventdrop);
        },
        complete: function(){
        	//gridBoxImgUINew('#commevents-suggested .uniqidentitybox');
        }
    });
}

/* events on going */
function goingEvents($lazyhelpcount=0)
{
    $('.event-search').hide();
    $('.eventsearch').attr('data-search','going-search');
    $.ajax({
        url: '?r=event/going',
        type: 'POST',
        data: {$lazyhelpcount},
        beforeSend: function() {
        	$('.loaderblock').remove();
        	$("#commevents-attending").find('.row').append($loader); 
        },  
        success: function(data)
        {
        	$('.loaderblock').remove();
        	if(data != '') {
	        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
	        		$("#commevents-attending").find('.row').html(data);
	        	} else {
	            	$("#commevents-attending").find('.row').append(data);
	        	}
	        	$('#commevents-attending').find('.uniqidentitybox').last().addClass('lazyloadscrollevent lazycounthelp');
	        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
	        	getnolistfound('noeventjoinedyet');
	        }
        },
        complete: function(){
        	//gridBoxImgUINew('#commevents-attending .uniqidentitybox');
        }
    });
}

/* yours events */
function yoursEvents($lazyhelpcount=0)
{
    $('.event-search').hide();
    $('.eventsearch').attr('data-search','yours-search');
    $.ajax({
        url: '?r=event/yours',
        type: 'POST',
        data: {$lazyhelpcount},
        beforeSend: function() {
        	$('.loaderblock').remove();
        	$("#commevents-yours").find('.row').append($loader); 
        },
        success: function(data)
        {
        	$('.loaderblock').remove();
        	if(data != '') {
        		if($lazyhelpcount == 0 || $lazyhelpcount == '') {
        			$("#commevents-yours").find('.row').find('.uniqidentitybox').remove();
	            	$("#commevents-yours").find('.row').html(data);
        		} else {
        			$("#commevents-yours").find('.row').append(data);
        		}
	            $('#commevents-yours').find('.uniqidentitybox').last().addClass('lazyloadscrollevent lazycounthelp');
	        }
        },
        complete: function(){
        	//gridBoxImgUINew('#commevents-yours .uniqidentitybox');
        }
    });
}

/* new events */
function newevents()
{
    $.ajax({
        url: '?r=event/add-new-event',  
        type: 'POST',
        success: function(data)
        {
            var eventdrop = $('#eventdrop').val();
            $("#commevents-suggested").html(data);
            $('#eventdrop').val(eventdrop);
        }
    });
}

function getprivacydata() {
	$.ajax({ 
        url: '?r=event/getprivacydata',
        success: function(data){
        	var result = $.parseJSON(data);
        	if(result.ids != undefined) {
        		var ids = result.ids;
        		customArray = ids;
        		customArrayTemp = ids
        	}
        }
    });	
}


/* add event */
function addEvent(obj) {
	var status = $("#user_status").val();
	if(status == 0)
	{
		return false;
	}

	var $selectore = $(obj).parents('.modal.open');
	var $isValidate = true;
	if($selectore.length) {
		var $ID = $selectore.attr('id');
		$ID = $('#'+$ID);
		var eventcover = $ID.find("#eventcover").val();
		var title = $ID.find("#title").val();
		var tagline = $ID.find("#tagline").val();
		var address = $ID.find("#placelocationsearch").val();
		var eventdate = $ID.find("#eventdatepicker").val();
		var eventtime = $ID.find("#eventtimepicker").val();
		var aboutevent = $ID.find("#aboutevent").val();
		var eventprivacy = $ID.find(".privacyarea").text().trim();
		var event_images = $ID.find('#image-cropper').cropit('export');
		var isasktojoin = $ID.find("#ask_to_join_switch").attr('data-value');
		var wu = $ID.find("#wu").val();
		var tu = $ID.find("#tu").val();
		var yu = $ID.find("#yu").val();
		var tpiu = $ID.find("#tpiu").val();
		var urlvalidate = /^(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		if(title == '')
		{
			$ID.find("#title").focus(); 
			Materialize.toast('Enter event name.', 2000, 'red');
			return false;
		}
		if(tagline == '')
		{
			$ID.find("#tagline").focus(); 
			Materialize.toast('Enter event tagline.', 2000, 'red');
			return false;
		}	
		if(address == '')
		{
			$ID.find("#placelocationsearch").focus(); 
			Materialize.toast('Enter event location.', 2000, 'red');
			return false;
		}
		if(eventdate == '')
		{
			$ID.find("#eventdatepicker").focus(); 
			Materialize.toast('Select event date.', 2000, 'red');
			return false;
		}
		if(eventtime == '')
		{
			$ID.find("#eventtimepicker").focus(); 
			Materialize.toast('Select event time.', 2000, 'red');
			return false;
		}
		if(aboutevent == '')
		{
			$ID.find("#aboutevent").focus(); 
			Materialize.toast('Enter some event details.', 2000, 'red');
			return false;
		}
		if(wu != '' && !urlvalidate.test(wu))
		{
			$ID.find("#wu").focus(); 
			Materialize.toast('Enter valid website url.', 2000, 'red');
			return false;
		}
		if(tu != '' && !urlvalidate.test(tu))
		{
			$ID.find("#tu").focus(); 
			Materialize.toast('Enter valid ticket seller url.', 2000, 'red');
			return false;
		}
		if(yu != '' && !urlvalidate.test(yu))
		{
			$ID.find("#yu").focus(); 
			Materialize.toast('Enter valid youtube url.', 2000, 'red');
			return false;
		}
		if(tpiu != '' && !urlvalidate.test(tpiu))
		{
			$ID.find("#tpiu").focus(); 
			Materialize.toast('Enter transit info.', 2000, 'red');
			return false;
		}
		var formdata;
		formdata = new FormData($('form')[1]);
		formdata.append('ec', eventcover);
		formdata.append('en', title);
		formdata.append('tl', tagline);
		formdata.append('ed', eventdate);
		formdata.append('et', eventtime);
		formdata.append('sd', aboutevent);
		formdata.append('ep', eventprivacy);
		formdata.append('ad', address);
		formdata.append('wu', wu);
		formdata.append('tu', tu);
		formdata.append('yu', yu);
		formdata.append('tpiu', tpiu);
		formdata.append('images', event_images);
		formdata.append('isasktojoin', isasktojoin);
		formdata.append('custom', customArray);
		
		$.ajax({
			type: 'POST',
			url: '?r=event/createevent',
			data: formdata,
			processData: false,
			contentType: false,
			success: function(data){
				var result = $.parseJSON(data);
				var id = result['event_id'];
				window.location.href = "?r=event/detail&e="+id;
				if(window.location.href.indexOf("detail") > -1) {
					Materialize.toast('Event updated.', 2000, 'green'); 
				} else {
					Materialize.toast('Event created.', 2000, 'green'); 
				}
			}
		});
	}
}
  
/* join event */
function joinEvent(event,e,obj,isChange,isPermission){
	event.stopPropagation();
	if(isChange) {
		window.location.href = "?r=event/detail&e="+e;
	} else {
		$.ajax({
			type: 'POST',
			url: '?r=event/join',
			data: 'e='+e,
			success: function(data) {
				var result = $.parseJSON(data);
				if(result.auth != undefined && result.auth == 'checkuserauthclassnv'){
					checkuserauthclassnv();
				} else if(result.auth != undefined && result.auth == 'checkuserauthclassg') {
					checkuserauthclassg();
				} else {
					var realcount = result['goingcount'];
					var label = result['label'];
					if(label == '7') {
						return false;
					}
				   	
				   	if(window.location.href.indexOf("event") > -1) {
					   	if(label.toLowerCase() == 'attending') {
					   		Materialize.toast('Attending event', 2000, 'green'); 
					   	} else if(label.toLowerCase() == 'join') {
					   		Materialize.toast('Unattending event', 2000, 'green'); 
					   	} else if(label.toLowerCase() == 'ask to join') {
					   		Materialize.toast('Canceling request', 2000, 'green'); 
					   	} else if(label.toLowerCase() == 'request sent') {
					   		Materialize.toast('Request sent', 2000, 'green'); 
					   	}
					}

					if(window.location.href.indexOf("detail") > -1) {
						window.location.href = "";
					} else {
						$('.eventbox_'+e).find('.action-btns').find('span').html(label)
						$(obj).html(label);
						$('.fcount_'+e).html(realcount+' Attending');
					}
				}
			}
		});
	}
}

/* publish event */
function publish_event(e_id, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=event/publish-event',
			type: 'POST',
			data: 'e_id=' + e_id,
			success: function (data){
				$(".discard_md_modal").modal("close");
				$('#publish_event').hide();
				Materialize.toast('Published', 2000, 'green');
				window.location.href = "?r=event";
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Publish this event.");
		btnKeep.html("Keep");
		btnDiscard.html("Publish");
		btnDiscard.attr('onclick', 'publish_event(\''+e_id+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}

/* flag event */
function flag_event(event_id,e_uid,uid,isPermission=false){	
	var desc = $("#textInput").val();
	if(desc == '')
	{	
		Materialize.toast('Enter Flaging reason.', 2000, 'red');
		return false;			
	}

	if(isPermission) {
		$.ajax({
		  url: '?r=event/flag-event', 
		  type: 'POST',
		  data: 'event_id=' + event_id+'&e_uid='+e_uid+'&uid='+uid+'&desc='+desc,
		  success: function (data) 
		  {
		  	$(".discard_md_modal").modal("close");
			if(data)
			{
				Materialize.toast('Flag', 2000, 'green');
				window.location.href='?r=event';
			}
		  }
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Flag this.");
		btnKeep.html("Keep");
		btnDiscard.html("Flag");
		btnDiscard.attr('onclick', 'flag_event(\''+event_id+'\', \''+e_uid+'\', \''+uid+'\', \''+isPermission+'\',true)');
		$(".discard_md_modal").modal("open");
	}
}

/* START Lazy Loading */
window.onload = function() {
	$.fn.isOnScreen = function(){
	    var win = $(window)
	    var viewport = {
	      top : win.scrollTop(),
	      left : win.scrollLeft()
	    }
	    viewport.right = viewport.left + win.width()
	    viewport.bottom = viewport.top + win.height()
	    var bounds = this.offset()
	    bounds.right = bounds.left + this.outerWidth()
	    bounds.bottom = bounds.top + this.outerHeight()
	    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom))
	}

	function lazyloadEvent() {
		$('.lazyloadscrollevent').each(function() {
      		var element = $(this)
			if( element.isOnScreen() ) {
				$activeTabIndex = $('.mobile-header').find('li.active').index();
				if($activeTabIndex == 0 || $activeTabIndex == '') {
					var lazyhelpcount = $('#commevents-suggested').find( ".row" ).find('.lazycounthelp').length;
					$('.lazyloadscrollevent').removeClass("lazyloadscrollevent");
					allEvents('evall', lazyhelpcount);
				} else if ($activeTabIndex == 1) {
					var lazyhelpcount = $('#commevents-attending').find( ".row" ).find('.lazycounthelp').length;
					$('.lazyloadscrollevent').removeClass("lazyloadscrollevent");
					goingEvents(lazyhelpcount);
				} else {
					var lazyhelpcount = $('#commevents-yours').find( ".row" ).find('.lazycounthelp').length;
					$('.lazyloadscrollevent').removeClass("lazyloadscrollevent");
					yoursEvents(lazyhelpcount);
				}
			}
		})
	}

	lazyloadEvent();
	if( $('.lazyloadscrollevent').length > 0 ) {
		lazyloadEvent();
	}
		
	$('body').bind('touchmove', function(e) { 
		lazyloadEvent();
	});

	$(window).scroll(function() {
		lazyloadEvent();
	});
}
/* END Lazy Loading */
