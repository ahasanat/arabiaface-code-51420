/* suggest friend */
	function suggestFriend(friendid, suggestto){
		if (friendid != '' && suggestto != '' )
		{
			$.ajax({ 
				type: 'POST',
				url: '?r=site/suggestfriend',
				data: "friendid=" + friendid + "&suggestto=" + suggestto,
				success: function (data)
				{
					var result = $.parseJSON(data);
					if (result['status'] != '0')
					{
						$("#send_"+friendid).hide();
						$("#done_"+friendid).html(result['msg']);
						$("#done_"+friendid).show();
					}
				}
			});
		}
	}
/* end suggest friend */
	
/* friend listing */
	function friendList(fid){
		if (fid != ''){ 
			$.ajax({
				type: 'POST',
				url: '?r=site/suggestfriendlist',
				data: "fid="+fid,
				success: function (data)
				{
					$("#suggest-friends").html(data);
					$("#suggest-friends").css('width', '900px');
					$('#suggest-friends').modal('open');
				}
			});
		}
	}
/* end friend listing */

/* Start Function Friend menu Generalize */
	function fetchfriendmenu(guser_id, isnew='', obj='') {
		$.ajax({
			url: '?r=friend/fetchfriendmenu',  
			type: 'POST',
			data: {guser_id},
			success: function(data) {
				if(isnew == 'yes') {
					$(obj).parents('.dropdown.dropdown-custom').find('ul').html(data);
				} else {
					$('.add-icon_'+guser_id).parent('.overlay').find('.more-span').find('ul').html(data);
				}
			}
		});
	}
/* End  Function Friend menu Generalize */

function removeFriendFromListing(fid){
	$('#remove_' + fid).hide();
}

/* alert for not accepting friend request */ 
	function privateMessage(){
		Materialize.toast('This member is not accepting friend request.', 2000, 'red');
		return false;
	}
/* end alert for not accepting friend request */ 

/* Start Add/Remove Friend Function */
function addFriend(id){
	var status = $("#user_status").val();
	if(status == 0){
		return false;
	}	
	
	var login_user_id = $('#login_id').val();
	$.ajax({
		url: '?r=friend/add-friend', 
		type: 'POST',
		data: 'to_id=' + id + '&from_id=' + login_user_id,
		success: function (data) {
			if(data)
			{	
				var result = $.parseJSON(data);

				if(result.auth == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(result.auth == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else { 
					$('.people_'+id).hide();
					$('.sendmsgdisplay_'+id).html(result.msg);
					$('.sendmsgdisplay_'+id).show();
					$('.sendmsg_'+id).show();
					$('.acceptmsg_'+id).html(result.msg);
					$('.acceptmsg_'+id).show();
					$('.wallfriendaction').attr('title',result.msg);
					$('.title_'+id).attr('title',result.msg);
					$('#frnd_rmv_img').remove();
					$('#frnd_img').html('<a class="gray-text-555"><i class="mdi mdi-account-minus"></i></a>');
					$('.add-icon_'+id).css('display','block');
					$('.add-icon_'+id).html('<a href="javascript:void(0);" class="gray-text-555" onclick="removeFriend(\''+id+'\',\''+login_user_id+'\',\''+id+'\', \'cancle_friend_request\')"><i class="mdi mdi-account-minus"></i></a>');

					if($('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-plus').length) {
						$('.people_'+id).show();
						$('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-plus').removeClass('fa-user-plus').addClass('fa-user-times');

						$('.wallpage-content').find('.wall-header').find('.header-strip').find('.fa-user-times').attr("onclick","removeFriend('"+id+"','"+login_user_id+"','"+id+"', 'cancle_friend_request')");
					}


					$(".addfriend").html('<a href="javascript:void(0);" onclick="removeFriend(\''+id+'\',\''+login_user_id+'\',\''+id+'\', \'cancle_friend_request\')">Cancle friend request</a>');
					$(".canclefriendrequest").html('<a href="javascript:void(0);" onclick="addFriend(\''+id+'\')">Add friend</a>');

					$('.travfriends_'+id).html('<a href="javascript:void(0)" title="Add friend" class="gray-text-555"><i class="mdi mdi-account-plus  dis-none" onclick="addFriend(\''+id+'\')"></i><i class="mdi mdi-account-minus" onclick="removeFriend(\''+id+'\',\''+login_user_id+'\',\''+id+'\',\'cancle_friend_request\')"></i></a><a href="javascript:void(0)" class="tb-pyk-remove"></a>');

					$('.travfriends_'+id).find('.people_').addClass('people_'+id);
					$('.travfriends_'+id).find('.sendmsg_').addClass('sendmsg_'+id);
					Materialize.toast('Friend request sent.', 2000, 'green');
				}
			}   
		}
	});
}
/* End Add/Remove Friend Function */	

/* follow friend */
function followFriend(fid){
	if (fid != ''){
		$.ajax({
			type: 'POST',
			url: '?r=site/unfollowfriend',
			data: "fid=" + fid,
			success: function (data) {
				if (data){
					if(data === '1'){
						$(".follow_" + fid).html('Mute friend posts');
					}
					else if(data === '2'){
						$(".follow_" + fid).html('Unmute friend posts');
					}else{}
					$.ajax({
						type: 'POST',
						url: '?r=site/mutefriend',
						data: "fid=" + fid,
						success: function (data) {
							if (data){
								if(data === '1'){
									$(".mute_friend_" + fid).html('Mute notifications');
									Materialize.toast('Mute notifications', 2000, 'green');
								}
								else if(data === '2'){
									$(".mute_friend_" + fid).html('Unmute notifications');
									Materialize.toast('Unmute notifications', 2000, 'green');
								}
							}
						}
					});
				}
			}
		});
	}
}
/* end follow friend */

/* mute friend */
	function muteFriend(fid,  isPermission=false) {
		if($id) {
			if(isPermission) {	
				$.ajax({
					type: 'POST',
					url: '?r=site/mutefriend',
					data: "fid=" + fid,
					success: function (data) { 
						if (data)
						{
							if(data === '1'){
								$(".mute_friend_" + fid).html('Mute notifications');
								Materialize.toast('Mute notifications', 2000, 'green');
							}
							else if(data === '2'){
								$(".mute_friend_" + fid).html('Unmute notifications');
								Materialize.toast('Unmute notifications', 2000, 'green');
							}

							if(data === '1')
							{
								$(".mute_friend_" + fid).html('Mute notifications');
							}
							else if(data === '2')
							{
								$(".mute_friend_" + fid).html('Unmute notifications');
							}
							$(".discard_md_modal").modal("close");
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("Mute friend?");
	            btnKeep.html("Keep");
	            btnDiscard.html("Mute");
	            btnDiscard.attr('onclick', 'muteFriend(\''+fid+'\', true)');
	            $(".discard_md_modal").modal("open");
			}
		}
	}
/* end mute friend */