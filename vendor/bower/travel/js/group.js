/*$(window).resize(function() {		
	if($('.fake-title-area').find('.tabs').find('li').find('a.active').length) {
		$parent = $('.fake-title-area').find('.tabs').find('li').find('a.active');
		$link = $parent.attr('href');
		gridBoxImgUINew($link+' .uniqidentitybox');
	}
});*/

/* checkbox */
$(document).on('click', '.myCheckbox1', function(){
	var checkbox = $(".myCheckbox1").attr('data-value');
	if(checkbox=='1'){
		$(this).attr('data-value','0'); 
	} else {
		$(this).attr('data-value','1');
	}
});

/* onready event call */
$(document).ready(function() {
	$('#image-cropper').cropit({
        height: 188,
        width: 355,
        smallImage: "allow",
        onImageLoaded: function (x) {
            $(".cropit-preview").removeClass("class-hide");
            $(".cropit-preview").addClass("class-show");
            $(".btn-save").removeClass("class-hide");
            $(".btn-save").addClass("class-show");
            $("#removeimg").removeClass("class-hide");
            $("#removeimg").addClass("class-show");
        }
    });
    
	if(window.location.href.indexOf("detail") > -1) {
		if("<?= $par;?>" == "event") {
			event();
		} else {
			about();
		}
		$('.abouttab').click(function() {
			about();
		});
		$(".eventtab").click(function() {
		    event();
		});
		$(".phototab").click(function() {
		    photo();
		});
		$(".invite").click(function() {
		    invites();
		});
		 $(".membertab").click(function() {
		    membertab();
		});
	} else {
		var location = getQueryVariable('location');
		if(location != undefined && location != null && location != '') {
			groupssearchwithlocation(location);
		} else {
			allGroup(0, 'load');
		}

	    $('.allgroup').click(function() {
			allGroup();
	    });
		
		$(".yoursgroup").click(function() {
		    yoursGroup();
	    });
		
		$(".joingroup").click(function() {
		    joinGroup();
	    });
	}
});

/* Start Invite Friend Search From Group*/
$( document ).on( 'keyup keypress paste click', '.invite_friend_group', function(e) {
	var id = $(this).attr('data-id');
	var textValue = $(this).val();
	var baseurl = $("#baseurl").val();
	var data = 'key='+textValue+'&baseurl='+baseurl;
	$.ajax({
		url: "?r=groups/invite-friends-group",
		type: 'GET',
		data: data,
		success: function (data) {				
			$(".block"+id).html(data).show();
		}
	});
});


function getprivacydata() {
	$.ajax({
        url: '?r=groups/getprivacydata',
        success: function(data){
        	var result = $.parseJSON(data);
        	if(result.ids != undefined) {
        		var ids = result.ids;
        		customArray = ids;
        		customArrayTemp = ids
        	}
        }
    });	
}

function promoteAdminModalOpen() {
    $('#manageadmin-popup').modal('open');
    setTimeout(function() { 
        allmemberforadmin();
    },1500);
}

function openCreateGroupModal() {
    $.ajax({
        url: '?r=groups/new-create-group',
        success: function(data){
            $('#add_event_modal').html(data);
            setTimeout(function() { 
                $('.add-item-popup').modal('open'); 
                initDropdown();
                $('#image-cropper').cropit({
                    height: 188,
                    width: 355,
                    smallImage: "allow",
                    onImageLoaded: function (x) {
                        $(".cropit-preview").removeClass("class-hide");
                        $(".cropit-preview").addClass("class-show");

                        $(".btn-save").removeClass("class-hide");
                        $(".btn-save").addClass("class-show");
                        $("#removeimg").removeClass("class-hide");
                        $("#removeimg").addClass("class-show");

                    }
                }); 
            }, 400);
            
        }
    });
}

function openEditGroupModal($id) {
    $.ajax({
        type: 'POST',
        data: {$id},
        url: '?r=groups/edit-group',
        success: function(data){
            $('#add_gruop_modal').html(data);
            setTimeout(function(){ 
                $('#add_gruop_modal').modal('open'); 
                getprivacydata();
                $('.dropdown-button').dropdown('close');
                initDropdown();
                $('#image-cropper').cropit({
                    height: 188,
                    width: 355,
                    smallImage: "allow",
                    onImageLoaded: function (x) {
                        $(".cropit-preview").removeClass("class-hide");
                        $(".cropit-preview").addClass("class-show");

                        $(".btn-save").removeClass("class-hide");
                        $(".btn-save").addClass("class-show");
                        $("#removeimg").removeClass("class-hide");
                        $("#removeimg").addClass("class-show");
                    }
                }); 

                setTimeout(function() {
                	getprivacydata();
                }, 1000);

            }, 400);
        }
    });
}

function openAddItemModalevent() { 
    $.ajax({
        url: '?r=groups/new-create-event',
        success: function(data){
            $('#add_event_modal').html(data);
            setTimeout(function() { 
                $('.add-item-popup-event').modal('open'); 
                initDropdown();
                $('#image-cropper').cropit({
                    height: 188,
                    width: 355,
                    smallImage: "allow",
                    onImageLoaded: function (x) {
                        $(".cropit-preview").removeClass("class-hide");
                        $(".cropit-preview").addClass("class-show");

                        $(".btn-save").removeClass("class-hide");
                        $(".btn-save").addClass("class-show");
                        $("#removeimg").removeClass("class-hide");
                        $("#removeimg").addClass("class-show");
                    }
                }); 
            }, 400);
            
        }
    });
}

function promotetoadminbtnclk() {
    setTimeout(function(){ $('#directusepromote').trigger('click'); },600);
}

function promotetoadminbtnclkwarning(isPermission=false) {
	if(isPermission) {
		$(".discard_md_modal").modal("close");
		setTimeout(function(){ $('#directusepromote').trigger('click'); }, 600);
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
	    var btnKeep = $(".discard_md_modal .modal_keep");
	    var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Before you leave the group, please promte another member to become admin.");
        btnKeep.html("Keep");
        btnDiscard.html("Promote to Admin");
        btnDiscard.attr('onclick', 'promotetoadminbtnclkwarning(true)');
        $(".discard_md_modal").modal("open");
	}
}

function resetDetailTabs(obj, pageName, tabContent) { 		
	// check allow 
	if(tabContent == "commevents-discussion" || tabContent == "groups-discussion"){
		$(".discuss-search").show();
		manageNewPostBubble("showit");
	}else{
		$(".discuss-search").hide();
		manageNewPostBubble("hideit");
	}
	
	if(tabContent=="groups-discussion") {
		what="discussion";$(".details.searcharea").show();}
	else if(tabContent=="groups-events"){what="event";$(".details.searcharea").show();}
	else if(tabContent=="groups-photos"){what="photos";$(".details.searcharea").show();
		initGalleryImageSlider(".images-container");
	}
	else if(tabContent=="groups-members"){what="member";$(".details.searcharea").show();}
	else{what="";$(".details.searcharea").hide();}
	
	$(".groupsearch-input").attr("placeholder","Search "+what);
	
	// reset the active tabs menu
	if( $(this).parents("people").length > -1 ){
		$(".gdetails-summery .nav-tabs").each(function(){				
			if($(this).parents("people").length <= 0 ){
				$(this).find("li.active").removeClass("active");
			}
		});
	}else{
		$(".gdetails-summery .people .nav-tabs li.active").removeClass("active");
	}
	
	var win_w = $(window).width();
	if(win_w < 1024){
		var invertLink = $(obj).parents(".gdetails-moreinfo").find(".expandable-link");
		invertLink.trigger("click");			
	}
	
	// scroll upto the section
	if(win_w < 768){
		$('html,body').animate({
			scrollTop: $(".general-details .post-column").offset().top - 110},
		'slow');
	}else{
		$('html,body').animate({
		scrollTop: 0},
		'slow');
	}		
}

/* check parameter exist in url */
function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
       var pair = vars[i].split("=");
       if(pair[0] == variable){return pair[1];}
   }
   return(false);
}

/* listing group with particular location */
function groupssearchwithlocation(location) { 
	if(location != undefined && location != null && location != '') {
		$.ajax({
	        url: '?r=groups/search-with-location',
	        type: 'POST',
	        data: {location},
	        success: function(data)
	        {
            	$("#groups-suggested").find('.row').html(data);
            	$('#groups-suggested').animateCss('fadeInUp');
			    $("#groups-suggested").find('.generalbox-list').addClass('animated fadeInUp');
	        }
	    });
    } 
}

/* listing all groups */
function allGroup($lazyhelpcount=0, $isload='') {
	var location = getQueryVariable('location');
	if(location != undefined && location != null && location != '') {
		window.location.href='?r=groups/index';
	}

    $.ajax({
        url: '?r=groups/all',
        type: 'POST',
        data: {$lazyhelpcount},
        beforeSend: function() {
        	if($lazyhelpcount >0) {
        		$('.loaderblock').remove();
        		$("#groups-suggested").find('.row').append($loader); 
        	}
        },
        success: function(data)
        {
        	$('.loaderblock').remove();
        	if(data != '') {
	        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
	        		if($isload == 'load') {
	        			$('#groups-suggested').animateCss('fadeInUp');
			        	$("#groups-suggested").find('.generalbox-list').addClass('animated fadeInUp');
	        		}
	        		$("#groups-suggested").find('.row').html(data);
	        	} else {
	            	$("#groups-suggested").find('.row').append(data);
	        	}
	        	$('#groups-suggested').find('.uniqidentitybox').last().addClass('lazyloadscrollgroup lazycounthelp');
	        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
	        	getnolistfound('becomefirsttocreategroup');
	        }
        },
        complete: function(){
        	//gridBoxImgUINew('#groups-suggested .uniqidentitybox');
        }
    });
}

/* listing yours groups */
function yoursGroup($lazyhelpcount=0) {
    $.ajax({
        url: '?r=groups/yours',
        type: 'POST',
        data: {
        	baseUrl: baseUrl,
        	$lazyhelpcount: $lazyhelpcount
        },
        beforeSend: function() {
        	$('.loaderblock').remove();
        	$("#groups-yours").find('.row').append($loader); 
        },
        success: function(data)
        {
        	$('.loaderblock').remove();
        	if(data != '') { 
        		if($lazyhelpcount == 0 || $lazyhelpcount == '') {
        			$("#groups-yours").find('.row').find('.uniqidentitybox').remove();
	            	$("#groups-yours").find('.row').html(data);
        		} else {
        			$("#groups-yours").find('.row').append(data);
        		}
	            $('#groups-yours').find('.uniqidentitybox').last().addClass('lazyloadscrollgroup lazycounthelp');
	        }
        },
        complete: function(){
        	//gridBoxImgUINew('#groups-yours .uniqidentitybox');
        }
    });
}

/* listing all joined groups */
function joinGroup($lazyhelpcount=0) {
	$.ajax({
        url: '?r=groups/join', 
        type: 'POST',
        data: {
        	baseUrl: baseUrl,
        	$lazyhelpcount: $lazyhelpcount
        },
        beforeSend: function() {
        	$('.loaderblock').remove();
        	$("#groups-member").find('.row').append($loader); 
        },
        success: function(data)
        {
        	$('.loaderblock').remove();
        	if(data != '') {
	        	if($lazyhelpcount == '' || $lazyhelpcount == 0) {
	        		$("#groups-member").find('.row').html(data);
	        	} else {
	        		$("#groups-member").find('.row').append(data);	
	        	}
	        	$('#groups-member').find('.uniqidentitybox').last().addClass('lazyloadscrollgroup lazycounthelp');
	        } else if(data == '' && ($lazyhelpcount == 0 || $lazyhelpcount == '')) {
	        	getnolistfound('nogroupjoinedyet');
	        }
        },
        complete: function(){
        	//gridBoxImgUINew('#groups-member .uniqidentitybox');
        }
    });
} 

/* join event */
function joinEvent(event,e,obj,isChange,isPermission){
	event.stopPropagation();
	if(isChange) {
		window.location.href = "?r=event/detail&e="+e;
	} else {
		if($(obj).hasClass('checkuserauthclassnv')) {
			checkuserauthclassnv(true);
		} else if($(obj).hasClass('checkuserauthclassg')) {
			checkuserauthclassg(true);
		} else {
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=event/join',
					data: 'e='+e,
					success: function(data) {
						var result = $.parseJSON(data);
						if(result.auth != undefined && result.auth == 'checkuserauthclassnv'){
							checkuserauthclassnv();
						} else if(result.auth != undefined && result.auth == 'checkuserauthclassg') {
							checkuserauthclassg();
						} else {
							var realcount = result['goingcount'];
							var label = result['label'];
							if(label == '7') {
								return false;
							}
						   
							if(isChange) {
								window.location.href = "";
							} else {
								$('.eventbox_'+e).find('.action-btns').find('span').html(label)
								$(obj).html(label);
								$('.fcount_'+e).html(realcount+' Attending');
							}
						}
						$(".discard_md_modal").modal("close");
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	var htmlContent = $(obj).html();
		    	if(htmlContent.match(/request/i)) {
					disText.html("Canceling request?");
		            btnKeep.html("Keep");
		            btnDiscard.html("Cancel");
		            btnDiscard.attr('onclick', 'joinEvent(event, \''+e+'\', this, false, true)');
		            $(".discard_md_modal").modal("open");
				} else if(htmlContent.match(/attending/i)) {
					disText.html("Unattending event?");
		            btnKeep.html("Stay");
		            btnDiscard.html("Unattend");
		            btnDiscard.attr('onclick', 'joinEvent(event, \''+e+'\', this, false, true)');
		            $(".discard_md_modal").modal("open");
				} else {
					joinEvent(event, e, 'this', false, true);
				}
			}
		}  
	}
}

/* group status update */
function groupsJoins(event,group_id,obj, isChange, isPermission) {
	event.stopPropagation();
	if(isChange) {
		window.location.href = "?r=groups/detail&group_id="+group_id;
	} else {
		$.ajax({
			type: 'POST', 
			url: '?r=groups/member',
			data: 'group_id='+group_id,
			success: function(data){
				var result = $.parseJSON(data);
				$(".discard_md_modal").modal("close");
				
				if(result.auth != undefined && result.auth == 'checkuserauthclassnv'){
					checkuserauthclassnv();
				} else if(result.auth != undefined && result.auth == 'checkuserauthclassg') {
					checkuserauthclassg();
				} else {
					var count = result['member_count'];
					var label = result['label'];
					if(label == '7') {
			        	return false;
			        }

					if(label.toLowerCase() == 'member') {
				   		Materialize.toast('Joining group.', 2000, 'green'); 
				   	} else if(label.toLowerCase() == 'join') {
				   		Materialize.toast('Leaving group', 2000, 'green'); 
				   	} else if(label.toLowerCase() == 'request sent') {
				   		Materialize.toast('Request sent', 2000, 'green'); 
				   	}else if(label.toLowerCase() == 'ask to join') {
				   		Materialize.toast('Canceling request', 2000, 'green'); 
				   	}

					if(window.location.href.indexOf("detail") > -1) {
						window.location.href = "";
					} else {
						$('.groupbox_'+group_id).find('.action-btns').find('span').html(label)
						$('.member_count_'+group_id).html(count+' Members');
					}
				}
			}
		});
	}
}

/* group detail about */
function about() {
	$.ajax({
	    url: '?r=groups/about',
	    type: 'POST',
	    data: "group_id="+group_id+"&baseUrl="+baseUrl,
	    success: function(data)
	    {
	        $("#groups-about").html(data);
	    }
	});
}

/* add event */
function addEvent(obj) {
	var status = $("#user_status").val();
	if(status == 0)
	{
		return false;
	}

	var $selectore = $(obj).parents('.modal.open');
	var $isValidate = true;
	if($selectore.length) {
		var $ID = $selectore.attr('id');
		$ID = $('#'+$ID);
		var eventcover = $ID.find("#eventcover").val();
		var title = $ID.find("#title").val();
		var tagline = $ID.find("#tagline").val();
		var address = $ID.find("#address").val();
		var eventdate = $ID.find("#eventdatepicker").val();
		var eventtime = $ID.find("#eventtimepicker").val();
		var aboutevent = $ID.find("#aboutevent").val();
		var eventprivacy = $ID.find("#post_privacy").val();
		var event_images = $ID.find('#image-cropper').cropit('export');
		var isasktojoin = $ID.find("#ask_to_join_switch").attr('data-value');
		var wu = $ID.find("#wu").val();
		var tu = $ID.find("#tu").val();
		var yu = $ID.find("#yu").val();
		var tpiu = $ID.find("#tpiu").val();
		var urlvalidate = /^(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		if(title == '')
		{
			$ID.find("#title").focus(); 
			Materialize.toast('Enter event name.', 2000, 'red');
			return false;
		}
		if(tagline == '')
		{
			$ID.find("#tagline").focus(); 
			Materialize.toast('Enter event tagline.', 2000, 'red');
			return false;
		}
		if(address == '')
		{
			$ID.find("#address").focus(); 
			Materialize.toast('Enter event location.', 2000, 'red');
			return false;
		}
		if(eventdate == '')
		{
			$ID.find("#eventdatepicker").focus(); 
			Materialize.toast('Select event date.', 2000, 'red');
			return false;
		}
		if(eventtime == '')
		{
			$ID.find("#eventtimepicker").focus(); 
			Materialize.toast('Select event time.', 2000, 'red');
			return false;
		}
		if(aboutevent == '')
		{
			$ID.find("#aboutevent").focus(); 
			Materialize.toast('Enter some event details.', 2000, 'red');
			return false;
		}
		if(wu != '' && !urlvalidate.test(wu))
		{
			$ID.find("#wu").focus(); 
			Materialize.toast('Enter valid website url.', 2000, 'red');
			return false;
		}
		if(tu != '' && !urlvalidate.test(tu))
		{
			$ID.find("#tu").focus(); 
			Materialize.toast('Enter valid ticket seller url.', 2000, 'red');
			return false;
		}
		if(yu != '' && !urlvalidate.test(yu))
		{
			$ID.find("#yu").focus(); 
			Materialize.toast('Enter valid youtube url.', 2000, 'red');
			return false;
		}
		if(tpiu != '' && !urlvalidate.test(tpiu))
		{
			$ID.find("#tpiu").focus(); 
			Materialize.toast('Enter transit info.', 2000, 'red');
			return false;
		}
		var formdata;
		formdata = new FormData($('form')[1]);
		formdata.append('ec', eventcover);
		formdata.append('en', title);
		formdata.append('tl', tagline);
		formdata.append('ed', eventdate);
		formdata.append('et', eventtime);
		formdata.append('sd', aboutevent);
		formdata.append('ep', eventprivacy);
		formdata.append('ad', address);
		formdata.append('wu', wu);
		formdata.append('tu', tu);
		formdata.append('yu', yu);
		formdata.append('tpiu', tpiu);
		formdata.append('images', event_images);
		formdata.append('isasktojoin', isasktojoin);
		
		$.ajax({
			type: 'POST',
			url: '?r=event/createevent',
			data: formdata,
			processData: false,
			contentType: false,
			success: function(data){
				Materialize.toast('Event created...', 2000, 'green'); 
				$('.add-item-popup-event').modal('close');
				event();
			}
		});
	}
}
/* group detail event */
function event() {
	$.ajax({
	    url: '?r=groups/event',
	    type: 'POST',
	    data: "group_id="+group_id+"&baseUrl="+baseUrl,
	    success: function(data)
	    {
	        $("#groups-events").html(data);
	    }
	});
}

/* group detail photo */
function photo() {
	$.ajax({
	    url: '?r=groups/photo',
	    type: 'POST',
	    data: "group_id="+group_id+"&baseUrl="+baseUrl,
	    success: function(data)
	    {
	        $("#groups-photos").html(data);
			initGalleryImageSlider();
	    }
	});
}

/* group detail invites */
function invites() {
	$.ajax({
	    url: '?r=groups/invites',
	    type: 'POST',
	    data: "group_id="+group_id+"&baseUrl="+baseUrl,
	    success: function(data)
	    {
	        $("#groups-invite").html(data);
	    }
	});
}

/* group detail member tab */
function membertab() {
	$.ajax({
	    url: '?r=groups/membertab',
	    type: 'POST',
	    data: "group_id="+group_id+"&baseUrl="+baseUrl,
	    success: function(data)
	    {
	        $("#groups-members").html(data);
	    }
	});
}

/* group detail manage member */
function manage_members() {
	$.ajax({
	    url: '?r=groups/managemembers',
	    type: 'POST',
	    data: "group_id="+group_id+"&baseUrl="+baseUrl,
	    success: function(data)
	    {
	        $("#managemembers-popup").html(data);
	        $('#managemembers-popup').modal('open');
	        $('.tabs').tabs();
	        initDropdown();
	    }
	});
}

/* group all member for admin */
function allmemberforadmin() {
	$.ajax({
	    url: '?r=groups/allmemberforadmin',
	    type: 'POST',
	    data: {group_id},
	    beforeSend: function() {
	    	$("#manageadmin-popup").find("#member-all").find('ul').html('<center><div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue-only"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div></center>');	
	    },
	    success: function(data)
	    {
	        $("#manageadmin-popup").find("#member-all").find('ul').html(data);
	    }
	});
}

/* group member request */
function memberrequest($group_id) { 
	$.ajax({
	    url: '?r=groups/memberrequest',
	    type: 'POST',
	    data: {group_id},
	    beforeSend: function() {
	    	$("#manageadmin-popup").find("#member-request").find('ul').html('<center><div class="preloader-wrapper small active"> <div class="spinner-layer spinner-blue-only"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div></center>');	
	    },
	    success: function(data)
	    {	
	    	$("#manageadmin-popup").find("#member-request").find('ul').html(data);
	    }
	});
}

/* delete group */
function delete_group(group_id, isPermission=false) {
	if(group_id) {
		if(isPermission) {	
			$.ajax({
				type: 'POST',
				url: '?r=groups/delete',
				data: 'group_id='+group_id,
				success: function(data) {
					$(".discard_md_modal").modal("close");
					Materialize.toast('Group deleted.', 2000, 'green');
					window.location.href = "?r=groups/index";
				}
			});
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
	    	disText.html("Delete group?");
            btnKeep.html("Keep");
            btnDiscard.html("Delete");
            btnDiscard.attr('onclick', 'delete_group(\''+$id+'\', true)');
            $(".discard_md_modal").modal("open");
		}
	}
}

/* left group */
function left_group(group_id, isPermission=false) {
	if(group_id) {
		$.ajax({
			type: 'POST',
			url: '?r=groups/leftgroup',
			data: {group_id},
			success: function(data){
				$(".discard_md_modal").modal("close");
				if(data == 'S') {
					setTimeout(function(){
						promotetoadminbtnclkwarning();
					},500);
				} else {
					Materialize.toast('Leaving group', 2000, 'green'); 
					window.location.href = "";
				}
			}
		});
	}
}

/* update group */
function update_group(group_id, obj) {
	var $selectore = $(obj).parents('.modal.open');
	var data = {}; 
	var $isValidate = true;
	var validate = true; 
	if($selectore.length) {
		var $ID = $selectore.attr('id');
		$ID = $('#'+$ID);

		var visible = $ID.find(".privacyLabel").html().trim();
		var title 	= $ID.find("input.title").val();
		var tagline = $ID.find("input.tagline").val();
		var location = $ID.find("input.placelocationsearch").val();
		var description = $ID.find(".description").val();
		//var images = $('#images').val();
		var images = $ID.find('#image-cropper').cropit('export');
		var review = false;
		var isasktojoin = false;
		if($ID.find("#friend_activity_switch").prop('checked') == true) {
			review = true;
		}
		if($ID.find("#ask_to_join_switch").prop('checked') == true) {
			isasktojoin = true;
		}

		if(title == '') {
			validate = false;
			$ID.find("input.title").focus();
			Materialize.toast('Enter group name.', 2000, 'red');
			return false;
		}
		if(tagline == '') {
			validate = false;
			$ID.find("input.tagline").focus();
			Materialize.toast('Enter group tagline.', 2000, 'red');
			return false;
		}	
		if(location == '') {
			validate = false;
			$ID.find("input.placelocationsearch").focus();
			Materialize.toast('Enter group location.', 2000, 'red');
			return false;
		}
		if(validate) {
			$.ajax({
				type: 'POST',
				url: '?r=groups/update',
				data: 'title='+title+'&tagline='+tagline+'&location='+location+'&visible='+visible+'&group_id='+group_id+'&description='+description+'&review='+review+'&images='+images+'&isasktojoin='+isasktojoin+'&custom='+customArrayTemp,
				success: function(data){
					Materialize.toast('Group updated.', 2000, 'green');
					window.location.href = "?r=groups/detail&group_id="+group_id;
				}
			});
		}
	}
}

/* add group photos */
function addGroupPhoto() {
	var filelength = $('.hidden_uploader').get(0).files.length;
	if(filelength <= 0) {
		Materialize.toast('Add photos to the group.', 2000, 'red');
	    return false;
	} else {
		$('input[name="imageFile1"]').val('');
		var formdata;
		formdata = new FormData($('form')[1]);
		for(var i=0, len=storedFiles.length; i<len; i++) {
			formdata.append('imageFile1[]', storedFiles[i]); 
		}
		var group_id = $(".group_id").val();
		formdata.append('group_id', group_id);
		if(group_id != '')
		{
			$(".post-loader").css("display","inline-block");
			$.ajax({ 
				url: '?r=groups/addphoto',
				type: 'POST',
				data:formdata,
				processData: false,
				contentType: false,
				success: function(data) {
					$('#add-photo-popup').modal('close');
					photo();
				}
			});
		}
	}
}

/* group detail event */
function delete_photo(photo_id, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=groups/deletephoto',
			type: 'POST',
			data: 'photo_id='+photo_id,
			success: function(data){
				$(".discard_md_modal").modal("close");
				Materialize.toast('Deleted', 2000, 'green');
				photo();
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Photo will be deleted permanently.");
		btnKeep.html("Keep");
		btnDiscard.html("Delete");
		btnDiscard.attr('onclick', 'delete_photo(\''+photo_id+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}

/* group set admin */
function setadmin(id, group_id, obj, remove=false) {
	if(id != undefined && group_id != undefined) {
		$.ajax({
	        url: '?r=groups/setadmin',
	        type: 'POST',
	        data: {id, group_id},
	        success: function(data)
	        {
	        	if(data == 'P') {
	        		Materialize.toast('Cancel previous admin request.', 2000, 'red');
	        		return false;
	        	} 
	        	if(remove == true) {
	        		$(obj).parent('.member_'+group_id).remove();
	        		getnolistfound('nogrouprequestfound');
	        	} 
	        	if(data == 'A') {
	        		$(obj).text('Set as Admin');
	        	} else if (data == 'C') {
	        		$(obj).text('Cancel Admin Request');
	        	}
			}
	    });
	}
}

/* group admin detail */
function admin_detail(group_id) {
	$.ajax({
		url: '?r=groups/admindetail',
		type: 'POST',
		data: {group_id},
		success: function(data)
		{
			var result = $.parseJSON(data);
			var status = result['msg'];
			if(status=='1')
			{					  
				//dialogItself.close();
				$('.member_'+member_id).remove();
			}	
		}
	});
}

/* group cancel invite group */
function cancelinvitegroup(fid) {
	$('.invite_'+fid).hide();
}

/* group send invite group */
function sendinvitegroup(fid,pid) {
	if (fid != '' && pid != '') {
		$.ajax({
			type: 'POST',
			url: '?r=groups/sendinvite',
			data: "fid="+fid+"&pid="+pid,
			success: function (data) {
				if (data)
				{
					$('.events_'+fid).hide();
					$('.sendinvitation_'+fid).show();
				}
			}
		});
	}
}

/* flag group */
function flag_group(group_id,g_uid,uid,isPermission=false) {	
	var desc = $("#textInput").val();
	if(desc == '') {
		Materialize.toast('Enter Flaging reason.', 2000, 'red');
		return false;			
	}
	
	if(isPermission) {
		$.ajax({
		  url: '?r=groups/flag-group',
		  type: 'POST',
		  data: 'group_id=' + group_id+'&g_uid='+g_uid+'&uid='+uid+'&desc='+desc,
		  success: function (data) 
		  {
		  	$(".discard_md_modal").modal("close");
			if(data) {
				Materialize.toast('Flag', 2000, 'green');
				window.location.href='?r=groups/index';
			}
		  }
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Flag this.");
		btnKeep.html("Keep");
		btnDiscard.html("Flag");
		btnDiscard.attr('onclick', 'flag_group(\''+group_id+'\', \''+g_uid+'\', \''+uid+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}

/* publish group */
function publish_group(g_id, isPermission=false) {
	if(isPermission) {
		$.ajax({
			url: '?r=groups/publish-group',
			type: 'POST',
			data: 'g_id=' + g_id,
			success: function (data){
				$(".discard_md_modal").modal("close");
				$('#publish_group').hide();
				Materialize.toast('Flag', 2000, 'Published');
				window.location.href = "?r=groups/index";
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Publish this group.");
		btnKeep.html("Keep");
		btnDiscard.html("Publish");
		btnDiscard.attr('onclick', 'publish_group(\''+g_id+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}

/* add group */
function add_group(obj) {
	var $selectore = $(obj).parents('.modal.open');
	var data = {}; 
	var $isValidate = true;
	var validate = true; 
	if($selectore.length) {
		var $ID = $selectore.attr('id');
		$ID = $('#'+$ID);

		var visible = $ID.find(".privacyLabel").html().trim();
		var title 	= $ID.find("input.title").val();
		var tagline = $ID.find("input.tagline").val();
		var location = $ID.find("input.placelocationsearch").val();
		var description = $ID.find(".description").val();
		//var images = $('#images').val();
		var images = $ID.find('#image-cropper').cropit('export');
		var review = false;
		var isasktojoin = false;
		if($ID.find("#friend_activity_switch").prop('checked') == true) {
			review = true;
		}
		if($ID.find("#ask_to_join_switch").prop('checked') == true) {
			isasktojoin = true;
		}

		if(title == '') { 
			validate = false;
			$ID.find("input.title").focus();
			Materialize.toast('Enter group name.', 2000, 'red');
			return false;
		}
		if(tagline == '') {
			validate = false;
			$ID.find("input.tagline").focus();
			Materialize.toast('Enter group tagline.', 2000, 'red');
			return false;
		}	
		if(location == '') {
			validate = false;
			$ID.find("input.placelocationsearch").focus();
			Materialize.toast('Enter group location.', 2000, 'red');
			return false;
		}
		if(validate) {
			$.ajax({
				type: 'POST',
				url: '?r=groups/add',
				data: 'title='+title+'&tagline='+tagline+'&location='+location+'&visible='+visible+'&description='+description+'&review='+review+'&images='+images+'&isasktojoin='+isasktojoin+'&custom='+customArrayTemp,
				success: function(data){
					Materialize.toast('Group created.', 2000, 'green');
					var result = $.parseJSON(data);
					var id = result['group_id'];
					if(window.location.href.indexOf("detail") > -1) {
						Materialize.toast('Group updated.', 2000, 'green'); 
					} else {
						Materialize.toast('Group created.', 2000, 'green'); 
					}
					window.location.href = "?r=groups/detail&group_id="+id; 
				}
			});
		}
	}
}

/* request group responce */
function request_responce(status,id){
	$.ajax({
        url: '?r=groups/requestresponcce',
        type: 'POST',
        data: "status="+status+"&id="+id,
        success: function(data)
        {
			var result = $.parseJSON(data);
			var status = result['msg'];
			if(status=='1')
			{
				var button = '<button class="btn btn-primary btn-sm" disabled>Member added</button>';
				$('.accept_'+id).html(button);
			}	
		}
    });
}

/* group member remove */
function remove_member(member_id, isPermission=false){
	if(isPermission) {
		$.ajax({
			url: '?r=groups/removemember',
			type: 'POST',
			data: "member_id="+member_id,
			success: function(data)
			{
				$(".discard_md_modal").modal("close");
				var result = $.parseJSON(data);
				var status = result['msg'];
				if(status=='1')
				{					  
					$('.member_'+member_id).remove();
					Materialize.toast('Removed', 2000, 'green');
				}	
			}
		});
	} else {
		var disText = $(".discard_md_modal .discard_modal_msg");
		var btnKeep = $(".discard_md_modal .modal_keep");
		var btnDiscard = $(".discard_md_modal .modal_discard");
		disText.html("Member will be removed.");
		btnKeep.html("Keep");
		btnDiscard.html("Remove");
		btnDiscard.attr('onclick', 'remove_member(\''+member_id+'\', true)');
		$(".discard_md_modal").modal("open");
	}
}

/* Lazy Loading Handler */
window.onload = function() {
	$.fn.isOnScreen = function(){
	    var win = $(window)
	    var viewport = {
	      top : win.scrollTop(),
	      left : win.scrollLeft()
	    }
	    viewport.right = viewport.left + win.width()
	    viewport.bottom = viewport.top + win.height()
	    var bounds = this.offset()
	    bounds.right = bounds.left + this.outerWidth()
	    bounds.bottom = bounds.top + this.outerHeight()
	    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom))
	}

	function lazyloadGroup() {
		$('.lazyloadscrollgroup').each(function() {
      		var element = $(this)
			if( element.isOnScreen() ) {
				$activeTabIndex = $('.mobile-header').find('li').find('a.active').index();
				if($activeTabIndex == 0 || $activeTabIndex == '') {
					var lazyhelpcount = $('#groups-suggested').find('.row').find('.lazycounthelp').length;
					$('.lazyloadscrollgroup').removeClass("lazyloadscrollgroup");
					allGroup(lazyhelpcount);
				} else if ($activeTabIndex == 1) {
					var lazyhelpcount = $('#groups-attending').find('.row').find('.lazycounthelp').length;
					$('.lazyloadscrollgroup').removeClass("lazyloadscrollgroup");
					joinGroup(lazyhelpcount);
				} else {
					var lazyhelpcount = $('#groups-yours').find('.row').find('.lazycounthelp').length;
					$('.lazyloadscrollgroup').removeClass("lazyloadscrollgroup");
					yoursGroup(lazyhelpcount);
				}
			}
		})
	}

	lazyloadGroup();
	if( $('.lazyloadscrollgroup').length > 0 ) {
		lazyloadGroup();
	}
		
	$('body').bind('touchmove', function(e) { 
		lazyloadGroup();
	});

	$(window).scroll(function() {
		lazyloadGroup();
	});
}
/* END Lazy Loading Handler */
