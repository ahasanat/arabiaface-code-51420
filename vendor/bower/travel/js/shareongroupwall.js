$('#shareongroupwall').modal({
    dismissible: false,
    opacity: .5,
    inDuration: 500,
    outDuration: 700,
    startingTop: '50%',
    endingTop: '50%',    
    ready: function (modal, trigger) {
        shareongroupwall();
        $('#shareongroupwall').find('.search_box').val('');
        shareongroupwallArrayTemp = shareongroupwallArray.slice();     
        $('#shareongroupwall').css('z-index', '1100');       
    },
    complete: function () { 
        shareongroupwalllabel();   
    }
});

$(document).on("keyup", "#shareongroupwall .search_box", function () {
    var $key = $(this).val().trim();
    $.ajax({
        url: "?r=site/shareongroupwall", 
        type: 'POST',
        data: {$key, shareongroupwallArrayTemp},
        success: function (data) {
            var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var content = result.html;
                var userCount = shareongroupwallArrayTemp.length;
                if(userCount>1) {
                    var label = userCount + ' groups selected';
                } else {
                    var label = userCount + ' group selected';
                }

                $('#shareongroupwall').find('.selected_photo_text').html(label);
                if(content != '') {
                    $('#shareongroupwall').find('.person_box').html(content);
                } else {
                    getnolistfound('norecordfoundaddperson');
                }
            } else {
                getnolistfound('norecordfoundaddperson');
            }
        }
    });
}); 

$(document).on('click', '#shareongroupwall .person_detail_container', function () {
    $(this).find(".chk_person").click();
    var $thisvalue = $(this).find(".chk_person").val();    

    if($(this).find(".chk_person").prop('checked') == true){
        if($.inArray($thisvalue, shareongroupwallArrayTemp) !== -1) {
        } else {
            shareongroupwallArrayTemp.push($thisvalue);
        }
    } else {
        shareongroupwallArrayTemp = $.grep(shareongroupwallArrayTemp, function(value) {
          return value != $thisvalue; 
        });
    }

    if(shareongroupwallArrayTemp.length >0) {
        $label = shareongroupwallArrayTemp.length + ' groups selected.';
        $('#shareongroupwall').find('.shareongroupwalldone').removeClass('disabled');
    } else {
        $label = '0 group selected.';            
        $('#shareongroupwall').find('.shareongroupwalldone').addClass('disabled');
    }
    $('#shareongroupwall').find('.selected_photo_text').html($label);
});  

$(document).on('click', '.shareongroupwalldone', function () {
    shareongroupwallArray = shareongroupwallArrayTemp.slice();
    $('#shareongroupwall').modal('close');
});

$(document).on('click', '.shareongroupwall', function () {
    shareongroupwall();
});

function shareongroupwalllabel() {
    $.ajax({ 
        url: "?r=site/shareongroupwalllabel", 
        type: 'POST', 
        data: {ids: shareongroupwallArrayTemp},
        success: function (data) {
            $('.shareongroupwallarea').html(data);
        }
    });
}

function shareongroupwall() {
    //$('#shareongroupwall').find('.person_box').html('');
    $.ajax({  
        url: "?r=site/shareongroupwall", 
        type: 'POST', 
        data: {ids: shareongroupwallArrayTemp},
        success: function (data) {
            var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var content = result.html;
                var userCount = shareongroupwallArray.length;
                if(userCount>1) {
                    var label = userCount + ' groups selected';
                    $('#shareongroupwall').find('.shareongroupwalldone').removeClass('disabled');
                } else {
                    var label = userCount + ' group selected';
                    $('#shareongroupwall').find('.shareongroupwalldone').addClass('disabled');
                }
                $('#shareongroupwall').find('.selected_photo_text').html(label);
                $('#shareongroupwall').find('.person_box').html(content);
                
                setTimeout(function(){ 
                    $('#shareongroupwall').modal('open');
                },400);
            } else {
                $('#shareongroupwall').find('.selected_photo_text').html('0 group selected.');
                $('#shareongroupwall').find('.shareongroupwalldone').addClass('disabled');
                getnolistfound('norecordfoundtag');
            }
        }
    });
}