/* Start Transfer Credits To Your Friend Account Function */
function transfer(){
	var friend_name = $('#friend_name').val();
	var amount = $('#amount').val();
	
	if(friend_name == ''){
		Materialize.toast('Enter friend name.', 2000, 'red');		
		$('#friend_name').focus();
		return false;
	}
	else if(amount == ''){
		Materialize.toast('Enter credits amount to transfer.', 2000, 'red');	
		$('#amount').focus();
		return false;
	}
	else
	{
		$('#payment').attr("disabled",true);
		
		$.ajax({ 
			url: '?r=site/transfercredits', 
			type: 'POST',
			data: {
				friend_name: friend_name,
				amount: amount
			},
			success: function (data) {
				if(data != 'error')
				{
					$(".credit_total").html(data);
					Materialize.toast('Transfer credit successfully.', 2000, 'green');		
				}
				
				$('#amount').val('');
				$('#amount').val('');
				$('#payment').attr("disabled",false);
			}
		});
	}
}
/* End Transfer Credits To Your Friend Account Function */