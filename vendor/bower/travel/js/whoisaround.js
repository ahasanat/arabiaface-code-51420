/* search name */
$(document).on("keyup", "#whoisaroundsearchname", function() {
	search();
}); 

/* search location */
$(document).on("keyup", "#searchLocation", function(e) {
	if ( e.keyCode === 13 ) {
 		e.stopPropagation();
		search();
	} else if ($.trim($(this).val()) == '') {
		search();
	}
}); 
 
$(document).on('click', '#closelinkseararea', function() {
	search();
});

$(document).on('click', '#maintabs', function() {
	$('#allwhoisaroundusersfrm')[0].reset();
	var slider = document.getElementById('test-slider');
	slider.noUiSlider.set([0, 100]);
	var sparent=$("#expandable-holderarea");
	sparent.removeClass("expanded");
	sparent.find(".expandable-area").slideUp();		
	$('.expand-link').removeClass("active");
	sparent.find(".expand-link").each(function(){
		if($(this).hasClass("active"))
			$(this).removeClass("active");	
	});	
});

/* on page refresh event handling */
$(document).ready( function() {
	$('body').addClass('whoisaroundpagebody');
	$('.tabs').tabs();
	allwhoisaroundusers('load');
    $(".find_friend").click(function() {
    	/* find friends */
        findfriends();
    });

	var slider = document.getElementById('test-slider');
	noUiSlider.create(slider, {
	   start: [0, 100],
	   connect: true,
	   step: 1,
	   orientation: 'horizontal', // 'horizontal' or 'vertical'
	   range: {
	     'min': 0,
	     'max': 100
	   },
	   format: wNumb({
	     decimals: 0
	   })
	});
	
	initDropdown();

	slider.noUiSlider.on('change', function(){
		search();
	});
}); 

function selectedDefaulttab() {
	//$('#maintabs').tabs('select_tab', 'whoaround-online');
}

/* search user */
function search() {
	$online = false;
	$birthday = false;
	$age_min = $('.noUi-handle-lower').find('.noUi-tooltip').find('span').text().trim();
	$age_max = $('.noUi-handle-upper').find('.noUi-tooltip').find('span').text().trim();
	if(!($age_min >=0)) {
		$age_min = 0;
	}

	if(!($age_max <=100)) {
		$age_max = 100;
	}
	
	$searchLocation = $("#searchLocation").val();
	$whoisaroundsearchname = $.trim($("#whoisaroundsearchname").val());
	$gender = $.trim(document.getElementById("gender").value);
	if($gender != '' && $gender != 'Male' && $gender != 'Female' && $gender != 'All') {
		return false;
	}
	if($("#online").prop('checked') == true) {
		$online = true;
	}
	if($("#birthday").prop('checked') == true) { 
		$birthday = true;
	} 
	$result = { 
		online: $online,
		birthday: $birthday,
		age_min: $age_min,
		age_max: $age_max,
		gender: $gender,
		searchLocation: $searchLocation,
		whoisaroundsearchname: $whoisaroundsearchname
	};

 	$('#whoaround-online').find('.person-list').find('.row').html('<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>');
	//whoisaroundsearch($data);
	$.ajax({
	    url: '?r=whoisaround/search',
	    type: 'POST',
	    async: false,
	    data : {$result},   
	    success: function(data) {
	    	$('.fake-title-area').find('li').find('a.active').removeClass('active');
	        $('.fake-title-area').find('li:first').find('a').addClass('active');
	        $('.main-pane.active').hide();
	        $('#whoaround-online').show();
	        $('#whoaround-online').find('.row').html(data);
	        setTimeout(function(){ 
	        	initDropdown(); $('.tabs').tabs(); 
	        },500);
	    }, complete: function(data) {
	    }
	});       
}

/* find friends */
function findfriends() {
	$('#whoaround-findfriends').html('<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>');
	$('.friends-search').hide();
	$.ajax({
        url: '?r=whoisaround/find-friends',  
        success: function(data)
        {
            $("#whoaround-findfriends").html(data);
            setTimeout(function(){ initDropdown(); },500);
        }
    });
}

/* fetch message content */
function fetchsendmessagecontent($id) {
	if($id != undefined && $id != '') {
		$.ajax({
	        url: '?r=whoisaround/send-message-content',  
	        type: 'POST',
	        data: {$id},
	        success: function(data)
	        {
	            $("#send-message-popup").find('form').html(data);
				$("#send-message-popup").find('#message').trigger('click');
	        }
	    });
	}
}

/* send message */
function sendmessage($id) {
	if($id != undefined && $id != '') {
		$message = $.trim($('#send-message-popup').find('form').find('#message').val());
 		if($message.length == 0){
 			Materialize.toast('Write a message.', 2000, 'red');
		}
		if($id != undefined && $id != '') {
			if($message != '') {
				$.ajax({
			        url: '?r=whoisaround/send-message',
			        type: 'POST',
			        data: {$id, $message},
			        success: function(data) {
			        	$('#send-message-popup').find('.popup-modal-dismiss').trigger('click');
			        	if(data == true) {
			        		Materialize.toast('Message sent.', 2000, 'red');
			            } else {
			            	Materialize.toast('Oops ! something is wrong please contact your administrator.', 2000, 'red');
			            }
			        }
			    });
			}
		}
	}
}

/* search bar */
function searchexbar() {
	$online = false;
	$birthday = false;
	if($("#online").prop('checked') == true){
		$online = true;
	}
	if($("#birthday").prop('checked') == true){
		$birthday = true;
	}
}

/* chat box open from who is around */
function openchatboxfromwhoisaround($id) {
	if($id != undefined && $id != '') {
		$.ajax({
	        url: '?r=whoisaround/is-block',  
	        type: 'POST',
	        data: {$id},
	        success: function(data) {
	        	var result = $.parseJSON(data);
	        	if(result.status == true && result.by == 'self') {
	        		Materialize.toast('You are not able to send message becuase you have blocked this person.', 2000, 'red');
	        	} else if(result.status == true && result.by == 'other') {
	        		Materialize.toast('This person isn\'t receiving message from you at the moment.', 2000, 'red');
	        	} else {
	  	        	if($(".chat-tabs").find('#chat-friends').find('ul.chatuserid_'+$id).length) {
						if(!($(".chat-open").length))  {
							$(".chat-button").trigger('click');
						}
						$(".chat-tabs").find('.tab-pane').find('active').removeClass('active in');
						$(".chat-tabs").find('#chat-friends').addClass('active in');
						$(".chat-tabs").find('#chat-friends').find('ul.chatuserid_'+$id).find('a').trigger('click');
					}
				}
	        }
	    });
	}
}

/* open gift box from who is around */
function opengiftboxfromwhoisaround($id) {
	if($id != undefined && $id != '') {
		$.ajax({
	        url: '?r=whoisaround/is-block', 
	        type: 'POST',
	        data: {$id},
	        success: function(data) {
	        	var result = $.parseJSON(data);

	        	if(result.status == true && result.by == 'self') {
	        		Materialize.toast('You are not able to send gift becuase you have blocked this person.', 2000, 'red');
	        	} else if(result.status == true && result.by == 'other') {
	        		Materialize.toast('This person isn\'t receiving gift from you at the moment.', 2000, 'red');
	        	} else {
	  	        	if($(".chat-tabs").find('#chat-friends').find('ul.chatuserid_'+$id).length) {
						if(!($(".chat-open").length)) {
							$(".chat-button").trigger('click');
						}
						$(".chat-tabs").find('.tab-pane').find('active').removeClass('active in');
						$(".chat-tabs").find('#chat-friends').addClass('active in');
						$(".chat-tabs").find('#chat-friends').find('ul.chatuserid_'+$id).find('a').trigger('click');
						$('.gift_modal').modal('open');
					}
				}
	        }
	    });
	}
}


//  who is around module funtinos...........
function allwhoisaroundusers($isload='') { 
	// Reset Search Filter 
	$('#whoaround-online').find('.row').html('<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>');

	$('.friends-search').show();
	
	//socket.emit('allwhoisaroundusers', data2.id); 
	$result = [];
	$.ajax({
	    url: '?r=whoisaround/all',
	    type: 'POST', 
	    data : {$result},   
	    success: function(data) {
	    	if($isload == 'load') {
	        	$('#whoaround-online').animateCss('fadeInUp');
	        	$("#whoaround-online").find('.animatedputed').addClass('animated fadeInUp');
	    	}
	        $('#whoaround-online').find('.row').html(data);
	        setTimeout(function() {
	        	initDropdown(); 
	        },500);
	    }, complete: function(data) {
	    }
	});
}

function allfriends() { 
	// Reset Search Filter 
    $('#whoaround-allfriends').find('.row').html('<center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>');

	$('.friends-search').show();
	$('#allwhoisaroundusersfrm')[0].reset();
	
	if($('#allwhoisaroundusersfrm').parents('.expanded')) {
	  $('#allwhoisaroundusersfrm').parents('.expanded').removeClass('expanded');
	}

	var slider = document.getElementById('test-slider');
	if(slider){
	  slider.noUiSlider.set([0, 100]);
	}
	 
	var $result = []; 
    $.ajax({
        url: '?r=whoisaround/allfriends',
        type: 'POST',
        data : {$result},   
        success: function(data) {
        	$('#whoaround-allfriends').find('.row').html(data);
        	setTimeout(function(){ 
            	initDropdown(); 
            },500);
        }
    });

	/*if(data2.id != undefined && $.trim(data2.id) != '') {
	  socket.emit('friendswhoisaround', data2.id);
	} else {
	   $('#whoaround-allfriends').html('<div class="no-listcontent">No record found.</div>');
	}*/
}

function whoisaroundsearch($data) {  
	$('.friends-search').show();
}